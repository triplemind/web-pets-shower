<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;

class ForgotPasswordController extends Controller
{
    public function getForgotPassword(){

        // เรียกหน้า view register
        return view('auth.forgotpassword');
    }


    // function สำหรับ save ข้อมูลลง database 
    public function postForgotPassword(){
        $new_pass           = \Input::has('new_pass') ? \Input::get('new_pass') : '';
        $email              = \Input::has('email') ? \Input::get('email') : '';

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_email  = User::where('email', $email)->first();
        // ถ้ามีก็ return error กลับไป
        if(empty($chk_email)) return ['status' => 'error', 'msg' => 'อีเมลนี้ไม่มีอยู่ในระบบ กรุณาตรวจสอบความถูกต้องของอีเมลอีกครั้ง'];

        // save ข้อมูลลง database
        $chk_email->password     = md5($new_pass);
        $chk_email->save();

        return ['status' => 'success'];
    }
}
