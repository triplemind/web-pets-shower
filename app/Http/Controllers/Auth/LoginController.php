<?php

namespace App\Http\Controllers\Auth;

use App;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;
     use AuthenticatesUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        // if (UserAuth::check()) {
        //     return \Redirect::route('dashboard.index.get');
        // }

        if (UserAuth::check()) {
            // ถ้ามีการ login อยู่ก็ check ต่อว่า userที่ login อยู่เป็นประเภทใด
            if(UserAuth::check_user_type()){
                // user == admin
                return \Redirect::route('dashboard.index.get');
            }else{
                // user == user
                return \Redirect::route('shoponline.index.get');

            }
        }

        return view('auth.login');
    }

     public function postLogin()
    {
        $username = \Input::has('username') ? \Input::get('username') : '';
        $password = \Input::has('password') ? \Input::get('password') : '';

        // d($username);
        // sd($password);

        //  if ($user = UserAuth::attempt($username, $password)) {
        //     return \Redirect::route('dashboard.index.get');
        // }

        if ($user = UserAuth::attempt($username, md5($password))) {
            // return \Redirect::route('dashboard.index.get');
            // ตรวจสอบประเภท user
            if($user->user_type == 'ผู้ดูแลระบบ' || $user->user_type == 'Root' || $user->user_type == 'เจ้าของกิจการ'){
                return \Redirect::route('dashboard.index.get');
            }else{
                return \Redirect::route('shoponline.index.get');
            }
        }

        // Redirect to Login Page.
        return redirect("/auth/login")->withInput(\Input::except('password'))
                ->withErrors(['username' => 'รหัสผ่านหรือชื่อผู้ใช้งานไม่ถูกต้อง กรุณาตรวจสอบความถูกต้องอีกครั้ง']);
        $user   = User::where('username', $username)->where('password', $password)->first();

        // $login_page = 'auth.login';
        // return view($login_page);
    }

        public function logout()
    {
        // UserAuth::clear();

        // return \Redirect::route('auth.login.get');

        if(UserAuth::check_user_type()){

            UserAuth::clear();

            return \Redirect::route('auth.login.get');

        }else{

            UserAuth::clear();

            return \Redirect::route('shoponline.index.get');
        }
    }

}
