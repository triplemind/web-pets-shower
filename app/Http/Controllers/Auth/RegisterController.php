<?php

namespace App\Http\Controllers\Auth;

// use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;
// use App\Services\Amphur\Amphur;
// use App\Services\District\District;
// use App\Services\Province\Province;
// use App\Services\Zipcode\Zipcode;


class RegisterController extends Controller
{
    public function getRegister(){

        // $provinces = Province::get();

        // $amphurs = Amphur::get();

        // $districts = District::get();

        // $zipcodes = Zipcode::get();, compact('provinces', 'amphurs', 'districts', 'zipcodes')

        // เรียกหน้า view register
        return view('auth.register');
    }

    public function getRegisterOwner(){

        // $provinces = Province::get();

        // $amphurs = Amphur::get();

        // $districts = District::get();

        // $zipcodes = Zipcode::get();, compact('provinces', 'amphurs', 'districts', 'zipcodes')

        // เรียกหน้า view register
        return view('auth.registerowner');
    }


    // function สำหรับ save ข้อมูลลง database 
    public function postRegister(){
        $first_name     = \Input::has('first_name') ? \Input::get('first_name') : '';
        $last_name      = \Input::has('last_name') ? \Input::get('last_name') : '';
        $username       = \Input::has('username') ? \Input::get('username') : '';
        // $user_type      = \Input::has('user_type') ? \Input::get('user_type') : '';
        $email          = \Input::has('email') ? \Input::get('email') : '';
        $password       = \Input::has('password') ? \Input::get('password') : '';
        $user_img       = \Input::hasFile('user_img') ? \Input::file('user_img') : '';
        $address        = \Input::has('address') ? \Input::get('address') : '';
        $tel            = \Input::has('tel') ? \Input::get('tel') : '';
        $user_type      = \Input::has('user_type') ? \Input::get('user_type') : '';
        // $province       = \Input::has('province') ? \Input::get('province') : '';
        // $amphur         = \Input::has('amphur') ? \Input::get('amphur') : '';
        // $district       = \Input::has('district') ? \Input::get('district') : '';
        // $zip            = \Input::has('zip') ? \Input::get('zip') : '';

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_username   = User::where('username', $username)->count();
        // ถ้ามีก็ return error กลับไป
        if($chk_username >= 1) return ['status' => 'error', 'msg' => 'Username is ready exits.'];

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_email  = User::where('email', $email)->count();
        // ถ้ามีก็ return error กลับไป
        if($chk_email >= 1) return ['status' => 'error', 'msg' => 'Email is ready exits.'];

       //  d($username);
       //  d($password);
       //  d($email);
       //  d($firstname);
       //  d($lastname);
       //  d($sex);
       //  d($birthday);
       //  d($address);
       // sd($tel);

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/userImg")){
         $oldmask = umask(0);
         mkdir($path."/public/userImg", 0777);
         umask($oldmask);
        }

        if(!empty($user_img))
        {
         $image_name = "/public/userImg/".$date."-".$user_img->getClientOriginalName();
         copy($user_img, $path.$image_name);
        }
        
        // save ข้อมูลลง database
        $new_user               = new User;
        $new_user->name_owner   = $first_name.' '.$last_name;
        $new_user->user_img     = (empty($image_name)) ? "" : $image_name;
        $new_user->email        = $email;
        $new_user->user_type    = $user_type;
        $new_user->username     = $username;
        $new_user->password     = md5($password);
        $new_user->address      = $address;
        $new_user->telephone    = $tel;
        // $new_user->province     = $province;
        // $new_user->amphur       = $amphur;
        // $new_user->district     = $district;
        // $new_user->zip          = $zip;
        $new_user->save();

        return ['status' => 'success'];
    }
}
