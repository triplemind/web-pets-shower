<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Services\Users\UserObject;
use App\Services\Users\User;
use App\Services\Amphur\Amphur;
use App\Services\District\District;
use App\Services\Province\Province;
use App\Services\Zipcode\Zipcode;
use App\Services\Shop\Shop;
use App\Services\PetService\PetService;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $userObject;


    public function __construct()
    {
        $this->userObject   = new UserObject;
    }
     public function view($page, $data = [])
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $check_premium = Shop::where('owner_id', $user_id);
        $check_premium = $check_premium->first();

        $layout     = 'layouts/main';

        return view($layout, compact('page', 'data', 'name_owner', 'username', 'user_type', 'user_img', 'user_id', 'check_premium'));
    }

    public function view_online($page, $data = [])
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        
        $getServices = PetService::with(['user','shop','pet']);
        $getServices = $getServices->where('id_login', $user_id);
        // $getServices = $getServices->where('booking_date','>', $date_now);
        // $getServices = $getServices->where('status', 'ชำระเงินแล้ว')->get();
        $getServices = $getServices->where('status', 'รอชำระเงิน')->get();

        // sd($getServices->toArray());

        $count_serv = $getServices->count() != 0 ? $getServices->count() : 0;

        \Session::put('count_all', $count_serv);

        $count_all = \Session::has('count_all') ? \Session::get('count_all') : 0;

        $filters_noti = [];
        if($getServices->count() != 0){
            foreach ($getServices as $key => $getService) {
                $service_txt = "";
                if($getService->id_service == "PG0001"){
                    $service_txt = "อาบน้ำ-ตัดขน";
                }else if($getService->id_service == "PG0002"){
                    $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
                }else if($getService->id_service == "PV0001"){
                    $service_txt = "วัคซีน";
                }else if($getService->id_service == "PV0002"){
                    $service_txt = "โปรโมชันวัคซีน";
                }

                $booking_time_txt = "";
                if($getService->booking_time == "บ่าย"){
                    $booking_time_txt = "บ่าย 09.00น.-12.30น.";
                }else if($getService->booking_time == "เช้า"){
                    $booking_time_txt = "เช้า 09.00น.-12.30น.";
                }

                $filters                    = [];
                $filters['id']              = $getService->id_petservice;
                $filters['name']            = empty($getService->pet) ? '' : $getService->pet->name;
                $filters['img']             = empty($getService->pet) ? '' : $getService->pet->pic;
                $filters['service']         = $service_txt;
                $filters['booking_date']    = $getService->booking_date;
                $filters['booking_time']    = $booking_time_txt;
                $filters['que']             = $getService->service_order;
                $filters['time_serv']       = $getService->service_time;

                array_push($filters_noti, $filters);
            }
        }

        \Session::put('noti', $filters_noti);

        $notiServices = \Session::has('noti') ? \Session::get('noti') : [];


        // $userObject = $this->userObject;
        $layout     = 'shoponline/main';

        return view($layout, compact('page', 'data', 'count_all', 'name_owner', 'username', 'user_type', 'user_img', 'user_id','notiServices'));
    }
}
