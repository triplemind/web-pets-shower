<?php

namespace App\Http\Controllers\Course;

use DB;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Stock\Stock;
use App\Services\Partner\Partner;
use App\Services\Bill\Bill;
use App\Services\Pos\Pos;
use App\Services\Orderpos\Orderpos;
use App\Services\Vaccine\Vaccine;
use App\Services\Promotion\Promotion;
use App\Services\Shop\Shop;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;


class CourseController extends Controller
{
    public function getIndex()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;
        
        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();

        if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'){
            $courses = Course::with(['shop','user']);
            $courses = $courses->where('id', '!=', 0);
            $courses = $courses->paginate(20);
        }elseif($user_type == 'เจ้าของกิจการ'){
            $courses = Course::with(['shop','user']);
            $courses = $courses->where('id', '!=', 0);
            $courses = $courses->where('shop_id', $shop->id_shop);
            $courses = $courses->paginate(20);
        }

        foreach ($courses as $key => $course) {
            if($course->status == 'Active'){
                $diff = date_diff(date_create($course->enddate),date_create(date('Y-m-d')));
                // sd($diff);
                if($diff->days <= 0){
                    $course->status = 'Inactive';
                    $course->save();
                }
            }
        }


    	return $this->view('course.index',compact('courses'));
    }


    public function getAdd()
    {

        return $this->view('course.add');
    }

    public function getView(Request $request, $id)
    {
        $courses = Course::with(['shop','user'])->where('id', $id)->first();

        return $this->view('course.view',compact('courses'));
    }

    public function getEdit(Request $request, $id)
    {
        $courses = Course::where('id', $id)->first();

        return $this->view('course.edit',compact('courses'));
    }

    public function postAdd()
    {
        $name           = \Input::has('name') ? \Input::get('name') : '';
        $price          = \Input::has('price') ? \Input::get('price') : '';
        $status         = \Input::has('status') ? \Input::get('status') : '';
        $times          = \Input::has('times') ? \Input::get('times') : '';
        $description    = \Input::has('description') ? \Input::get('description') : '';
        $startdate      = \Input::has('startdate') ? \Input::get('startdate') : '';
        $enddate        = \Input::has('enddate') ? \Input::get('enddate') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;
        
        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();
        

        $new_courses                = new Course;
        $new_courses->name          = $name;
        $new_courses->price         = $price;
        $new_courses->status        = $status;
        $new_courses->numoftimes    = $times;
        $new_courses->shop_id       = $shop->id_shop;
        $new_courses->user_id       = $user_id;
        $new_courses->description   = $description;
        $new_courses->startdate     = $startdate;
        $new_courses->enddate       = $enddate;
        $new_courses->save();

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $name           = \Input::has('name') ? \Input::get('name') : '';
        $price          = \Input::has('price') ? \Input::get('price') : '';
        $status         = \Input::has('status') ? \Input::get('status') : '';
        $times          = \Input::has('times') ? \Input::get('times') : '';
        $description    = \Input::has('description') ? \Input::get('description') : '';
        $startdate      = \Input::has('startdate') ? \Input::get('startdate') : '';
        $enddate        = \Input::has('enddate') ? \Input::get('enddate') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;
        
        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();

        $course = Course::where('id', $id)->first();
        if(empty($course)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        

        $course->name          = $name;
        $course->price         = $price;
        $course->status        = $status;
        $course->numoftimes    = $times;
        // $course->shop_id       = empty($shop) ? null : $shop->id_shop;
        $course->user_id       = $user_id;
        $course->description   = $description;
        $course->startdate     = $startdate;
        $course->enddate       = $enddate;
        $course->save();


        return ['status' => 'success'];
    }

    public function postRemove()
    {
        $id    = \Input::has('id') ? \Input::get('id') : '';

        $course = Course::where('id', $id)->first();
        if(empty($course)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $course->delete();

        return ['status' => 'success'];
    }


    public function ajaxCenter()
    {
        $method  = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
             // case 'addFilterToSession':

             //    $filters                    = [];
             //    $filters['search_txt']      = \Input::get('search_txt');
             //    $filters['vaccine_type']    = \Input::get('vaccine_type');
             //    $filters['vaccine_status']  = \Input::get('vaccine_status');

             //    \Session::put('report_filters', $filters);

             //    return ['status' => 'success'];
             //    break;

             // case 'clearFilterToSession':

             //    \Session::forget('report_filters');
                
             //    return ['status' => 'success'];
             //    break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
