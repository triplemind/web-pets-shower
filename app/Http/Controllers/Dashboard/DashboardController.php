<?php

namespace App\Http\Controllers\Dashboard;

use DB;
use DateTime;
use DateInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Shop\Shop;
use App\Services\Partner\Partner;
use App\Services\Amphur\Amphur;
use App\Services\District\District;
use App\Services\Province\Province;
use App\Services\Zipcode\Zipcode;
use App\Services\Description\Description;
use App\Services\PetService\PetService;
use App\Services\ShopService\ShopService;
use App\Services\Veterinary\Veterinary;
use App\Services\Vaccinrecord\Vaccinrecord;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Promotion\Promotion;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;
use App\Services\PetsShop\PetsShop;


class DashboardController extends Controller
{
    public function getIndex()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $filters = \Session::has('chart_range') ? \Session::get('chart_range') : [];

        // sd($filters);

        $getshop = Shop::where('owner_id', $user_id)->first();

        $datetoday = date('Y-m-d H:i:s');
        $datetoday = DateThai($datetoday, true, true);

        $numofUsernormal = 0;
        $numofShop = 0;
        $numofPremuim = 0;
        $numofVaccineBook = 0;
        $numofShowerBook = 0;
        $numofPetUse = 0;
        $numofServiceMor = 0;
        $numofServiceNoon = 0;

        // การนัดหมายวัคซีน
        $numofVaccineBook = PetService::with(['user','shop','pet']);


        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofVaccineBook = $numofVaccineBook->whereDate('booking_date' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofVaccineBook = $numofVaccineBook->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofVaccineBook = $numofVaccineBook->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }

            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                // sd(date('Y-m-d',strtotime('-1 month')));
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofVaccineBook = $numofVaccineBook->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofVaccineBook = $numofVaccineBook->whereDate('booking_date' , Carbon::today());
        }

        if($user_type != 'ผู้ดูแลระบบ' || $user_type != 'Root'){
            if(!empty($getshop)){
                $numofVaccineBook = $numofVaccineBook->where('id_shop', $getshop->id_shop);
            }
        }
        $numofVaccineBook = $numofVaccineBook->where('id_service' , 'PV0001');
        $numofVaccineBook = $numofVaccineBook->count();

        // การนัดหมายอาบน้ำ
        $numofShowerBook = PetService::with(['user','shop','pet']);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofShowerBook = $numofShowerBook->whereDate('booking_date' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofShowerBook = $numofShowerBook->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofShowerBook = $numofShowerBook->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }

            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofShowerBook = $numofShowerBook->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofShowerBook = $numofShowerBook->whereDate('booking_date' , Carbon::today());
        }

        if($user_type != 'ผู้ดูแลระบบ' || $user_type != 'Root'){
            if(!empty($getshop)){
                $numofShowerBook = $numofShowerBook->where('id_shop', $getshop->id_shop);
            }
        }
        $numofShowerBook = $numofShowerBook->where('id_service' , 'PG0001');
        $numofShowerBook = $numofShowerBook->count();


        // สัตว์เลี้ยงที่เข้ามาใช้บริการ

        $numofPetUse = PetsShop::where('id', '!=', 0);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofPetUse = $numofPetUse->whereDate('created_at' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofPetUse = $numofPetUse->whereBetween('created_at', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofPetUse = $numofPetUse->whereBetween('created_at', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofPetUse = $numofPetUse->whereBetween('created_at', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofPetUse = $numofPetUse->whereDate('created_at' , Carbon::today());
        }

        if($user_type != 'ผู้ดูแลระบบ' || $user_type != 'Root'){
            if(!empty($getshop)){
                $numofPetUse = $numofPetUse->where('shop_id', $getshop->id_shop);
            }
        }
        $numofPetUse = $numofPetUse->count();


        // การนัดหมายช่วงเวลาเช้า
        $numofServiceMor = PetService::where('id_petservice', '!=', 0);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofServiceMor = $numofServiceMor->whereDate('booking_date' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofServiceMor = $numofServiceMor->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofServiceMor = $numofServiceMor->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofServiceMor = $numofServiceMor->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofServiceMor = $numofServiceMor->whereDate('booking_date' , Carbon::today());
        }

        if($user_type != 'ผู้ดูแลระบบ' || $user_type != 'Root'){
            if(!empty($getshop)){
                $numofServiceMor = $numofServiceMor->where('id_shop', $getshop->id_shop);
            }
        }
        $numofServiceMor = $numofServiceMor->where('booking_time', 'เช้า');
        $numofServiceMor = $numofServiceMor->count();


        // การนัดหมายช่วงเวลาบ่าย
        $numofServiceNoon = PetService::where('id_petservice', '!=', 0);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofServiceNoon = $numofServiceNoon->whereDate('booking_date' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofServiceNoon = $numofServiceNoon->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofServiceNoon = $numofServiceNoon->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofServiceNoon = $numofServiceNoon->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofServiceNoon = $numofServiceNoon->whereDate('booking_date' , Carbon::today());
        }

        if($user_type != 'ผู้ดูแลระบบ' || $user_type != 'Root'){
            if(!empty($getshop)){
                $numofServiceNoon = $numofServiceNoon->where('id_shop', $getshop->id_shop);
            }
        }
        $numofServiceNoon = $numofServiceNoon->where('booking_time', 'บ่าย');
        $numofServiceNoon = $numofServiceNoon->count();


        // สมาชิกร้านค้า
        $numofShop = Shop::where('id_shop', '!=', 0);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofShop = $numofShop->whereDate('created_at' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofShop = $numofShop->whereBetween('created_at', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofShop = $numofShop->whereBetween('created_at', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofShop = $numofShop->whereBetween('created_at', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofShop = $numofShop->whereDate('created_at' , Carbon::today());
        }
        $numofShop = $numofShop->count();


        // สมาชิกพรีเมี่ยม
        $numofPremuim = Shop::where('id_shop', '!=', 0);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofPremuim = $numofPremuim->whereDate('pre_startdate' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofPremuim = $numofPremuim->whereBetween('pre_startdate', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofPremuim = $numofPremuim->whereBetween('pre_startdate', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofPremuim = $numofPremuim->whereBetween('pre_startdate', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofPremuim = $numofPremuim->whereDate('pre_startdate' , Carbon::today());
        }
        $numofPremuim = $numofPremuim->where('status_premium', true);
        $numofPremuim = $numofPremuim->count();


        // สมาชิกลูกค้า
        $numofUsernormal = User::where('id_owner', '!=', 0);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $numofUsernormal = $numofUsernormal->whereDate('created_at' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $numofUsernormal = $numofUsernormal->whereBetween('created_at', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $numofUsernormal = $numofUsernormal->whereBetween('created_at', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }

            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $numofUsernormal = $numofUsernormal->whereBetween('created_at', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $numofUsernormal = $numofUsernormal->whereDate('created_at' , Carbon::today());
        }
        $numofUsernormal = $numofUsernormal->count();

        // sd($numofVaccineBook);

        $getvaccine_stat = '';
        $getstat_ser_vac = '';
        $getstat_ser_bath = '';

        // FOR CHART
        $getvaccine_stat = Vaccinrecord::with(['vaccine','veterinary','shop','pet']);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $getvaccine_stat = $getvaccine_stat->whereDate('created_at' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $getvaccine_stat = $getvaccine_stat->whereBetween('created_at', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $getvaccine_stat = $getvaccine_stat->whereBetween('created_at', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $getvaccine_stat = $getvaccine_stat->whereBetween('created_at', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $getvaccine_stat = $getvaccine_stat->whereDate('created_at' , Carbon::today());
        }
        if($user_type != 'ผู้ดูแลระบบ' && $user_type != 'Root'){
            if(!empty($getshop)){
                $getvaccine_stat = $getvaccine_stat->where('shop_id', $getshop->id_shop);
            }
        }
        $getvaccine_stat = $getvaccine_stat->groupBy(['vaccine_ID']);
        $getvaccine_stat = $getvaccine_stat->select('vaccine_ID', DB::raw('count(vaccine_ID) as count_vac'));
        $getvaccine_stat = $getvaccine_stat->get();


        // ช่วงเวลาใช้บริการวัคซีน
        $getstat_ser_vac = PetService::with(['user','shop','pet']);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $getstat_ser_vac = $getstat_ser_vac->whereDate('booking_date' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $getstat_ser_vac = $getstat_ser_vac->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $getstat_ser_vac = $getstat_ser_vac->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $getstat_ser_vac = $getstat_ser_vac->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $getstat_ser_vac = $getstat_ser_vac->whereDate('booking_date' , Carbon::today());
        }
        if($user_type != 'ผู้ดูแลระบบ' && $user_type != 'Root'){
            if(!empty($getshop)){
                $getstat_ser_vac = $getstat_ser_vac->where('id_shop', $getshop->id_shop);
            }
        }
        $getstat_ser_vac = $getstat_ser_vac->where('status' , '!=', 'รอชำระเงิน');
        $getstat_ser_vac = $getstat_ser_vac->where(function($getstat_ser_vac){
                $getstat_ser_vac = $getstat_ser_vac->orWhere('id_service' , 'PV0001');
                $getstat_ser_vac = $getstat_ser_vac->orWhere('id_service' , 'PV0002');
        });                        
        $getstat_ser_vac = $getstat_ser_vac->groupBy(['booking_time']);
        $getstat_ser_vac = $getstat_ser_vac->select('booking_time', DB::raw('count(booking_time) as count_booking_time'));
        $getstat_ser_vac = $getstat_ser_vac->get();


        // ช่วงเวลาใช้บริการอาบน้ำ-ตัดขน
        // $getstat_ser_bath
        $getstat_ser_bath = PetService::with(['user','shop','pet']);
        if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
            if(!empty($filters['stat_range'])){
                if($filters['stat_range'] == 'day'){
                    $getstat_ser_bath = $getstat_ser_bath->whereDate('booking_date' , Carbon::today());
                }else if($filters['stat_range'] == 'month'){
                    $getstat_ser_bath = $getstat_ser_bath->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                }else if($filters['stat_range'] == 'year'){
                    $getstat_ser_bath = $getstat_ser_bath->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                }
            }
            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $getstat_ser_bath = $getstat_ser_bath->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
            }
        }else{
            $getstat_ser_bath = $getstat_ser_bath->whereDate('booking_date' , Carbon::today());
        }

        if($user_type != 'ผู้ดูแลระบบ' && $user_type != 'Root'){
            if(!empty($getshop)){
                $getstat_ser_bath = $getstat_ser_bath->where('id_shop', $getshop->id_shop);
            }
        }
        $getstat_ser_bath = $getstat_ser_bath->where('status' , '!=', 'รอชำระเงิน');
        $getstat_ser_bath = $getstat_ser_bath->where(function($getstat_ser_bath){
                $getstat_ser_bath = $getstat_ser_bath->orWhere('id_service' , 'PG0001');
                $getstat_ser_bath = $getstat_ser_bath->orWhere('id_service' , 'PG0002');
        });                        
        $getstat_ser_bath = $getstat_ser_bath->groupBy(['booking_time']);
        $getstat_ser_bath = $getstat_ser_bath->select('booking_time', DB::raw('count(booking_time) as count_booking_time'));
        $getstat_ser_bath = $getstat_ser_bath->get();


    	return $this->view('dashboard.index',compact('datetoday', 'getstat_ser_bath', 'getstat_ser_vac', 'getvaccine_stat', 'getshop', 'user_type', 'numofUsernormal', 'numofShop', 'numofPremuim', 'numofVaccineBook', 'numofShowerBook', 'numofPetUse', 'numofServiceMor', 'numofServiceNoon', 'filters'));
    }

    //รายละเอียดการจองวัคซีน
    public function getOrderDetail(Request $request, $id)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id) ? "" : $user_obj->id;


        $orders = '';

        if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'){
            $getdetail = PetService::with(['user','shop','pet','petsShop','vaccineRC','vaccineRC']);
            $getdetail = $getdetail->where('id_petservice', $id);
            $getdetail = $getdetail->first();

            // $veterinarys = DB::table('veterinary')->with(['shop'])->where('id', '!=', 0)->orderBy('Fname', 'asc')->get();
            $veterinarys = Veterinary::with(['shop'])->where('id', '!=', 0)->orderBy('Fname', 'asc')->get();

            $vaccines = Shop::where('id_shop', $getdetail->id_shop)->first();
        }else{
            //เห็นเฉพาะร้านค้า
            $getdetail = PetService::with(['user','shop','pet','petsShop','vaccineRC']);
            $getdetail = $getdetail->where('id_petservice', $id)->first();

            // $veterinarys = DB::table('veterinary')->with(['shop']);
            $veterinarys = Veterinary::with(['shop']);
            $veterinarys = $veterinarys->where('id', '!=', 0);
            // $veterinarys = $veterinarys->where('user_id', $user_id);
            $veterinarys = $veterinarys->where('shop_id', $getdetail->id_shop);
            $veterinarys = $veterinarys->orderBy('Fname', 'asc');
            $veterinarys = $veterinarys->get();

            $vaccines = Shop::where('id_shop', $getdetail->id_shop)->first();
        }

        // sd(count($cc));
        $getcourse = '';
        $getcoursedata = '';
        
        $price = '-';
        if($getdetail->iscourse == true){

            $getcourse = CourseRecord::where('course_id', $getdetail->courseid);
            $getcourse = $getcourse->where('user_id', $getdetail->id_login);
            $getcourse = $getcourse->where('shop_id', $getdetail->id_shop);
            $getcourse = $getcourse->where('pet_id', $getdetail->pet_id);
            $getcourse = $getcourse->count();


            $getcoursedata = CourseRecord::with(['user', 'shop', 'pet', 'course']);
            $getcoursedata = $getcoursedata->where('course_id', $getdetail->courseid);
            $getcoursedata = $getcoursedata->where('user_id', $getdetail->id_login);
            $getcoursedata = $getcoursedata->where('shop_id', $getdetail->id_shop);
            $getcoursedata = $getcoursedata->where('pet_id', $getdetail->pet_id);
            $getcoursedata = $getcoursedata->get();

            // sd($getcoursedata->toArray());
            if(!empty($getcoursedata)){
                foreach ($getcoursedata as $key => $getcourse) {
                    if(!empty($getcourse->startdate) && $getcourse->isuse == false){
                        // $txt_startdate = empty($getcourse->startdate) ? '' : $getcourse->startdate;

                        $expire = strtotime($getcourse->startdate);
                        $today  = strtotime("today midnight");
                        // $diffexpired_date = date_diff(date_create($txt_enddate),date_create(date('Y-m-d')));
                        if($today >= $expire){
                            $getcourse->isuse = true;
                            $getcourse->save();
                        }
                    }
                }
            }



            $courses = Course::with(['shop','user'])->where('id', $getdetail->courseid)->first();

            if(!empty($getdetail->promo_code) && $getdetail->ispromotion == true){
                $getpromotion = Promotion::where('shop_id', $getdetail->id_shop)->where('code', 'like', $getdetail->promo_code)->first();
                
                if(!empty($getpromotion)){

                    $percent = str_replace("%","",$getpromotion->discount);

                    $discount_value = ($courses->price / 100) * $percent;

                    $realprice = $courses->price-$discount_value;

                    $price = $realprice.' บาท (ท่านได้รับรับส่วนลด '.$getpromotion->discount.' จากราคาคอร์ส)';
                }else{
                    $price = $courses->price.' บาท';
                }
            }else{
                $price = $courses->price.' บาท';
            }
        }else{
            if(!empty($getdetail->promo_code) && $getdetail->ispromotion == true){
                $getpromotion = Promotion::where('shop_id', $getdetail->id_shop)->where('code', 'like', $getdetail->promo_code)->first();
                if(!empty($getpromotion)){

                    $percent = str_replace("%","",$getpromotion->discount);

                    $discount_value = ($getdetail->shop->deposit / 100) * $percent;

                    $realprice = $getdetail->shop->deposit-$discount_value;

                    $price = $realprice.' บาท (ท่านได้รับรับส่วนลด '.$getpromotion->discount.' จากราคาคอร์ส)';
                }else{
                    $price = $getdetail->shop->deposit.' บาท';
                }
            }else{
                $price = $getdetail->shop->deposit.' บาท';
            }
        }

        // sd($getcoursedata->toArray());

        return $this->view('dashboard.orderdetail', compact('getdetail', 'orders', 'user_type', 'price', 'veterinarys','vaccines', 'getcourse', 'getcoursedata'));
    }


    public function getHitoriesOrder()
    {
        // ORDER STATUS 
        // เสร็จสิ้น
        // รอดำเนินการ
        // ยกเลิกรายการ

        $filters = \Session::has('histories_filters') ? \Session::get('histories_filters') : [];

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getshop = Shop::where('owner_id', $user_id)->first();

        $getServices = '';

        if(!empty($getshop)){
            if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'){
                $getServices = PetService::with(['user','shop','pet']);
            }else{
                $getshop = Shop::where('owner_id', $user_id)->first();

                $getServices = PetService::with(['user','shop','pet']);
                $getServices = $getServices->where('id_shop', $getshop->id_shop);
            }

            if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                $getServices = $getServices->whereBetween('created_at', [$filters['rangestart'], $rangeend_add]);
            }

            if(!empty($filters['order_status'])){
                $getServices = $getServices->where('status', $filters['order_status']);
            }

            $getServices = $getServices->orderBy('created_at','desc');
            $getServices = $getServices->get();

        }

        // d(date('d-m-Y',strtotime($filters['rangeend'])));
        // sd($filters['rangestart']);

        return $this->view('dashboard.histories', compact('getServices', 'filters'));
    }


    public function postSavePaySlip(){
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $date_payment   = \Input::has('date_payment') ? \Input::get('date_payment') : '';
        $time_payment   = \Input::has('time_payment') ? \Input::get('time_payment') : '';
        $bank_payment   = \Input::has('bank_payment') ? \Input::get('bank_payment') : '';
        $pic_payment    = \Input::hasFile('pic_payment') ? \Input::file('pic_payment') : '';

        $service    = PetService::where('id_petservice', $id)->first();
        if(empty($service)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        // d($date_payment);
        // sd($time_payment);

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/paySlipImg")){
            $oldmask = umask(0);
            mkdir($path."/public/paySlipImg", 0777);
            umask($oldmask);
        }

        if(!empty($pic_payment))
        {
            $image_name = "/public/paySlipImg/".$date."-".$pic_payment->getClientOriginalName();
            copy($pic_payment, $path.$image_name);
        }


        $service->status        = 'รอการตรวจสอบยอดเงิน';
        if(!empty($date_payment) && $date_payment != null){
            $service->date_payment  = $date_payment;
        }
        if(!empty($time_payment) && $time_payment != null){
            $service->time_payment  = $time_payment;
        }
        if(!empty($bank_payment) && $bank_payment != null){
            $service->bank_payment  = $bank_payment;
        }
        if(!empty($image_name)){
            $service->pic_payment = $image_name;
        }
        $service->save();

        
        return ['status' => 'success'];
    }


    public function postSaveVaccineRC(){
        $vaccineRC_id   = \Input::has('vaccineRC_id') ? \Input::get('vaccineRC_id') : '';
        $vaccin         = \Input::has('vaccin') ? \Input::get('vaccin') : '';
        $namevaccin     = \Input::has('namevaccin') ? \Input::get('namevaccin') : '';
        $veterinarian   = \Input::has('veterinarian') ? \Input::get('veterinarian') : '';
        $notevaccin     = \Input::has('notevaccin') ? \Input::get('notevaccin') : '';
        $veterinary_ID  = \Input::has('veterinary_ID') ? \Input::get('veterinary_ID') : '';
        $vaccine_ID     = \Input::has('vaccine_ID') ? \Input::get('vaccine_ID') : '';
        $img            = \Input::hasFile('img') ? \Input::file('img') : '';

        $vaccineRC    = Vaccinrecord::where('id_vaccin', $vaccineRC_id)->first();
        if(empty($vaccineRC)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $vaccine = Vaccine::where('id', $vaccine_ID)->first();
        if(empty($vaccine)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        // d($vaccine_ID);
        // d($vaccine->timeperiod);
        // d(' +'.$vaccine->timeperiod.' years');
        // d(date('Y-m-d', strtotime(date('Y-m-d').' +'.$vaccine->timeperiod.' years')));

        if($vaccine->timeperiod_unit == 'วัน'){
            $anewdatetxt = ' +'.$vaccine->timeperiod.' days';
        }else if($vaccine->timeperiod_unit == 'เดือน'){
            $anewdatetxt = ' +'.$vaccine->timeperiod.' month';
        }else if($vaccine->timeperiod_unit == 'ปี'){
            $anewdatetxt = ' +'.$vaccine->timeperiod.' years';
        }else{
            $anewdatetxt = null;
        }
        // sd(date('Y-m-d', strtotime(date('Y-m-d').$anewdatetxt)));

        $vaccineRC->veterinary_ID   = $veterinary_ID;
        $vaccineRC->vaccin          = $vaccin;
        $vaccineRC->vaccine_ID      = $vaccine_ID;
        $vaccineRC->namevaccin      = $namevaccin;
        $vaccineRC->veterinarian    = $veterinarian;
        $vaccineRC->notevaccin      = $notevaccin;
        $vaccineRC->img             = $vaccine->img_vaccine;
        $vaccineRC->numoftimes      = $vaccine->numoftimes;
        $vaccineRC->timeperiod      = $vaccine->timeperiod;
        $vaccineRC->timeperiod_unit = $vaccine->timeperiod_unit;
        $vaccineRC->startdate       = date('Y-m-d');
        $vaccineRC->anewdate        = date('Y-m-d', strtotime(date('Y-m-d').$anewdatetxt));
        $vaccineRC->save();

        
        return ['status' => 'success'];
    }


    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {

            case 'CompletedOrder':

                $pos_id = \Input::has('pos_id') ? \Input::get('pos_id') : '';
                $status = \Input::has('status') ? \Input::get('status') : '';

                $time_val = "";
                $que_val = "";

                $add_order = PetService::where('id_petservice', $pos_id)->first();

                if(empty($add_order)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $getAll_orders = PetService::where('id_shop', $add_order->id_shop)
                                    ->where('id_service', $add_order->id_service)
                                    ->where('booking_time', $add_order->booking_time)
                                    ->whereDate('booking_date', $add_order->booking_date)
                                    ->where('status', 'ชำระเงินแล้ว')
                                    ->get();

                $getservices = ShopService::where('shop_id', $add_order->id_shop)
                                    ->where('service', $add_order->id_service)
                                    ->where('time_period', $add_order->booking_time)
                                    ->first();



                // shop_detail, all_que
                // d(date("13:30"));
                // d(date("H:i",strtotime('+1 hour',strtotime(date("13:30")))));
                // d(date("H:i:s"));
                // sd($getAll_orders->toArray());

                if($getAll_orders->count() != 0){//กรณีมีคิวแล้ว


                    $getlatest_Que = PetService::where('id_shop', $add_order->id_shop)
                                    ->where('id_service', $add_order->id_service)
                                    ->where('booking_time', $add_order->booking_time)
                                    ->whereDate('booking_date', $add_order->booking_date)
                                    ->where('status', 'ชำระเงินแล้ว')
                                    ->latest('updated_at')->first();
                    // sd($getlatest_Que);

                    if($getservices->que > $getAll_orders->count()){// กรณีที่คิวที่ตั้งค่าไว้ ไม่มากกว่า คิวที่มีทั้งหมด
                        if($add_order->booking_time == "บ่าย"){// ช่วงเวลาบ่าย
                            $que_val    = $getAll_orders->count() + 1;//บวกคิว

                            $date = date($getlatest_Que->service_time); //เวลาเริ่มต้น ที่จะใช้บวก
                            $time_val = date("H:i",strtotime('+1 hour +30 minutes',strtotime($date)));
                            // บวก +1 hour +30 minutes สามารถแก้ที่ตัวเลขได้เลย

                        }else if($add_order->booking_time == "เช้า"){ // ช่วงเวลาเช้า
                            $que_val    = $getAll_orders->count() + 1;

                            $date = date($getlatest_Que->service_time);//เวลาเริ่มต้น ที่จะใช้บวก
                            $time_val = date("H:i",strtotime('+1 hour +30 minutes',strtotime($date)));
                            // บวก +1 hour +30 minutes สามารถแก้ที่ตัวเลขได้เลย
                        }   
                    }else{
                        // กรณีที่คิวที่ตั้งค่าไว้ มากกว่า คิวที่มีทั้งหมด
                        return ['status' => 'error', 'msg' => 'คิวเต็มกรุณาแจ้งผู้จอง'];
                    }
                }else{//กรณียังไม่มีคิว
                    if($add_order->booking_time == "บ่าย"){// ช่วงเวลาบ่าย
                        $que_val    = 1; //คิวแรก
                        $time_val   = date("13:30"); //เวลาเริ่มต้น
                    }else if($add_order->booking_time == "เช้า"){ // ช่วงเวลาเช้า
                        $que_val    = 1; //คิวแรก
                        $time_val   = date("09:00"); //เวลาเริ่มต้น
                    }   
                }

                // sd($time_val);

                // บันทึกข้อมูลลงดาต้าเบส               
                $add_order->status        = $status;
                $add_order->service_order = empty($que_val) ? null : $que_val;
                $add_order->service_time  = empty($time_val) ? null : $time_val;
                $add_order->save();

                return ['status' => 'success'];
                break;

            case 'FinishOrder':

                $pos_id = \Input::has('pos_id') ? \Input::get('pos_id') : '';
                $status = \Input::has('status') ? \Input::get('status') : '';

                $add_order = PetService::where('id_petservice', $pos_id)->first();

                if(empty($add_order)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $add_order->status  = $status;
                $add_order->appoint = false;
                $add_order->save();

                return ['status' => 'success'];
                break;

            case 'CancelOrder':

                $pos_id = \Input::has('pos_id') ? \Input::get('pos_id') : '';

                $cancel_order = PetService::where('id_petservice', $pos_id)->first();

                if(empty($cancel_order)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $cancel_order->status = "ยกเลิกรายการ";
                $cancel_order->appoint = false;
                $cancel_order->save();

                return ['status' => 'success'];
                break;


            case 'addFilterToSessionChart':

                $filters                    = [];
                $filters['stat_range']      = \Input::get('stat_range');
                $filters['rangestart']      = \Input::get('rangestart');
                $filters['rangeend']        = \Input::get('rangeend');

                \Session::put('chart_range', $filters);

                return ['status' => 'success'];
                break;


            case 'addFilterToSession':

                $filters                    = [];
                $filters['rangestart']      = \Input::get('rangestart');
                $filters['rangeend']        = \Input::get('rangeend');
                $filters['order_status']    = \Input::get('order_status');

                \Session::put('histories_filters', $filters);

                return ['status' => 'success'];
                break;

             case 'clearFilterToSession':

                \Session::forget('histories_filters');
                \Session::forget('chart_range');
                
                return ['status' => 'success'];
                break;

            case 'getVaccine':

                $id_shop = \Input::has('id_shop') ? \Input::get('id_shop') : '';

                $shop = Shop::where('id_shop', $id_shop)->where('status_open', '!=', 0)->first();
                // sd($District->toArray());

                if(!empty($shop)){
                    return ['status' => 'success', 'data' => json_decode($shop->vaccine), 'datareal' => json_decode($shop->vaccineArr)];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'ConfirmPostponeADate':

                $pos_id = \Input::has('pos_id') ? \Input::get('pos_id') : '';

                $time_val = "";
                $que_val = "";
                $postponeAdate_status = "";

                $add_order = PetService::where('id_petservice', $pos_id)->first();

                if(empty($add_order)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $getAll_orders = PetService::where('id_shop', $add_order->id_shop)
                                    ->where('id_service', $add_order->id_service)
                                    ->where('booking_time', $add_order->booking_time)
                                    ->whereDate('booking_date', $add_order->booking_date)
                                    ->where('status', 'ชำระเงินแล้ว')
                                    ->get();

                $getservices = ShopService::where('shop_id', $add_order->id_shop)
                                    ->where('service', $add_order->id_service)
                                    ->where('time_period', $add_order->booking_time)
                                    ->first();


                if($getAll_orders->count() != 0){//กรณีมีคิวแล้ว
                    $getlatest_Que = PetService::where('id_shop', $add_order->id_shop)
                                    ->where('id_service', $add_order->id_service)
                                    ->where('booking_time', $add_order->booking_time)
                                    ->whereDate('booking_date', $add_order->booking_date)
                                    ->where('status', 'ชำระเงินแล้ว')
                                    ->latest('updated_at')->first();
                    // sd($getlatest_Que);

                    if($getservices->que > $getAll_orders->count()){// กรณีที่คิวที่ตั้งค่าไว้ ไม่มากกว่า คิวที่มีทั้งหมด
                        if($add_order->booking_time == "บ่าย"){// ช่วงเวลาบ่าย
                            $que_val    = $getAll_orders->count() + 1;//บวกคิว

                            $date = date($getlatest_Que->service_time); //เวลาเริ่มต้น ที่จะใช้บวก
                            $time_val = date("H:i",strtotime('+1 hour +30 minutes',strtotime($date)));
                            // บวก +1 hour +30 minutes สามารถแก้ที่ตัวเลขได้เลย

                            $postponeAdate_status = "เลื่อนนัดสำเร็จ";

                        }else if($add_order->booking_time == "เช้า"){ // ช่วงเวลาเช้า
                            $que_val    = $getAll_orders->count() + 1;

                            $date = date($getlatest_Que->service_time);//เวลาเริ่มต้น ที่จะใช้บวก
                            $time_val = date("H:i",strtotime('+1 hour +30 minutes',strtotime($date)));
                            // บวก +1 hour +30 minutes สามารถแก้ที่ตัวเลขได้เลย

                            $postponeAdate_status = "เลื่อนนัดสำเร็จ";
                        }   
                    }else{
                        $postponeAdate_status = "ไม่สามารถเลื่อนนัดได้";
                        // กรณีที่คิวที่ตั้งค่าไว้ มากกว่า คิวที่มีทั้งหมด
                        return ['status' => 'error', 'msg' => 'ไม่สามารถเลื่อนนัดได้ เนื่องจากคิวเต็ม ระบบได้ทำการแก้ไขสถานะการขอเลื่อนนัดให้ทางผู้จองได้ทราบแล้ว'];
                    }
                }else{//กรณียังไม่มีคิว
                    if($add_order->booking_time == "บ่าย"){// ช่วงเวลาบ่าย
                        $que_val    = 1; //คิวแรก
                        $time_val   = date("13:30"); //เวลาเริ่มต้น

                        $postponeAdate_status = "เลื่อนนัดสำเร็จ";
                    }else if($add_order->booking_time == "เช้า"){ // ช่วงเวลาเช้า
                        $que_val    = 1; //คิวแรก
                        $time_val   = date("09:00"); //เวลาเริ่มต้น

                        $postponeAdate_status = "เลื่อนนัดสำเร็จ";
                    }   
                }

                // sd($time_val);

                // บันทึกข้อมูลลงดาต้าเบส               
                $add_order->postponeAdate_status        = $postponeAdate_status;
                $add_order->service_order               = $que_val;
                $add_order->service_time                = $time_val;
                $add_order->save();

                return ['status' => 'success', 'msg' => 'สามารถเลื่อนนัดได้ ระบบได้ทำการแก้ไขสถานะการขอเลื่อนนัดให้ทางผู้จองได้ทราบแล้ว'];
                break;


            case 'getDataTochart':

                $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
                $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
                $username       = empty($user_obj->username) ? "" : $user_obj->username;
                $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
                $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
                $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

                $filters = \Session::has('chart_range') ? \Session::get('chart_range') : [];

                $getshop = Shop::where('owner_id', $user_id)->first();

                $getServices = '';
                $datetoday = date('Y-m-d H:i:s');
                $datetoday = DateThai($datetoday, true, true);

                $getvaccine_stat = '';
                $getstat_ser_vac = '';
                $getstat_ser_bath = '';



                // FOR CHART
                $getvaccine_stat = Vaccinrecord::with(['vaccine','veterinary','shop','pet']);
                if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
                    if(!empty($filters['stat_range'])){
                        if($filters['stat_range'] == 'day'){
                            $getvaccine_stat = $getvaccine_stat->whereDate('created_at' , Carbon::today());
                        }else if($filters['stat_range'] == 'month'){
                            $getvaccine_stat = $getvaccine_stat->whereBetween('created_at', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                        }else if($filters['stat_range'] == 'year'){
                            $getvaccine_stat = $getvaccine_stat->whereBetween('created_at', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                        }
                    }
                    if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                        $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                        $getvaccine_stat = $getvaccine_stat->whereBetween('created_at', [$filters['rangestart'], $rangeend_add]);
                    }
                }else{
                    $getvaccine_stat = $getvaccine_stat->whereDate('created_at' , Carbon::today());
                }
                if($user_type != 'ผู้ดูแลระบบ' && $user_type != 'Root'){
                    if(!empty($getshop)){
                        $getvaccine_stat = $getvaccine_stat->where('shop_id', $getshop->id_shop);
                    }
                }
                $getvaccine_stat = $getvaccine_stat->groupBy(['vaccine_ID']);
                $getvaccine_stat = $getvaccine_stat->select('vaccine_ID', DB::raw('count(vaccine_ID) as count_vac'));
                $getvaccine_stat = $getvaccine_stat->get();


                // ช่วงเวลาใช้บริการวัคซีน
                $getstat_ser_vac = PetService::with(['user','shop','pet']);
                if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
                    if(!empty($filters['stat_range'])){
                        if($filters['stat_range'] == 'day'){
                            $getstat_ser_vac = $getstat_ser_vac->whereDate('booking_date' , Carbon::today());
                        }else if($filters['stat_range'] == 'month'){
                            $getstat_ser_vac = $getstat_ser_vac->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                        }else if($filters['stat_range'] == 'year'){
                            $getstat_ser_vac = $getstat_ser_vac->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                        }
                    }
                    if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                        $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                        $getstat_ser_vac = $getstat_ser_vac->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
                    }
                }else{
                    $getstat_ser_vac = $getstat_ser_vac->whereDate('booking_date' , Carbon::today());
                }
                if($user_type != 'ผู้ดูแลระบบ' && $user_type != 'Root'){
                    if(!empty($getshop)){
                        $getstat_ser_vac = $getstat_ser_vac->where('id_shop', $getshop->id_shop);
                    }
                }
                $getstat_ser_vac = $getstat_ser_vac->where('status' , '!=', 'รอชำระเงิน');
                $getstat_ser_vac = $getstat_ser_vac->where(function($getstat_ser_vac){
                        $getstat_ser_vac = $getstat_ser_vac->orWhere('id_service' , 'PV0001');
                        $getstat_ser_vac = $getstat_ser_vac->orWhere('id_service' , 'PV0002');
                });                        
                $getstat_ser_vac = $getstat_ser_vac->groupBy(['booking_time']);
                $getstat_ser_vac = $getstat_ser_vac->select('booking_time', DB::raw('count(booking_time) as count_booking_time'));
                $getstat_ser_vac = $getstat_ser_vac->get();


                // ช่วงเวลาใช้บริการอาบน้ำ-ตัดขน
                // $getstat_ser_bath
                $getstat_ser_bath = PetService::with(['user','shop','pet']);
                if(!empty($filters['stat_range']) || (!empty($filters['rangestart']) && !empty($filters['rangeend']))){
                    if(!empty($filters['stat_range'])){
                        if($filters['stat_range'] == 'day'){
                            $getstat_ser_bath = $getstat_ser_bath->whereDate('booking_date' , Carbon::today());
                        }else if($filters['stat_range'] == 'month'){
                            $getstat_ser_bath = $getstat_ser_bath->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 month')), date('Y-m-d')]);
                        }else if($filters['stat_range'] == 'year'){
                            $getstat_ser_bath = $getstat_ser_bath->whereBetween('booking_date', [date('Y-m-d',strtotime('-1 year')), date('Y-m-d')]);
                        }
                    }
                    if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
                        $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
                        $getstat_ser_bath = $getstat_ser_bath->whereBetween('booking_date', [$filters['rangestart'], $rangeend_add]);
                    }
                }else{
                    $getstat_ser_bath = $getstat_ser_bath->whereDate('booking_date' , Carbon::today());
                }
                if($user_type != 'ผู้ดูแลระบบ' && $user_type != 'Root'){
                    if(!empty($getshop)){
                        $getstat_ser_bath = $getstat_ser_bath->where('id_shop', $getshop->id_shop);
                    }
                }
                $getstat_ser_bath = $getstat_ser_bath->where('status' , '!=', 'รอชำระเงิน');
                $getstat_ser_bath = $getstat_ser_bath->where(function($getstat_ser_bath){
                        $getstat_ser_bath = $getstat_ser_bath->orWhere('id_service' , 'PG0001');
                        $getstat_ser_bath = $getstat_ser_bath->orWhere('id_service' , 'PG0002');
                });                        
                $getstat_ser_bath = $getstat_ser_bath->groupBy(['booking_time']);
                $getstat_ser_bath = $getstat_ser_bath->select('booking_time', DB::raw('count(booking_time) as count_booking_time'));
                $getstat_ser_bath = $getstat_ser_bath->get();

                
                if(!empty($getstat_ser_vac)){
                    if($getstat_ser_vac->count() != 0){
                        if($getstat_ser_vac->count() <= 1){
                            $valtstat_ser_vac = [$getstat_ser_vac[0]['count_booking_time']];
                            $txttstat_ser_vac = [$getstat_ser_vac[0]['booking_time']];
                        }else if($getstat_ser_vac->count() > 1) {
                            $valtstat_ser_vac = [$getstat_ser_vac[0]['count_booking_time'],$getstat_ser_vac[1]['count_booking_time']];
                            $txttstat_ser_vac = [$getstat_ser_vac[0]['booking_time'],$getstat_ser_vac[1]['booking_time']];
                        }
                    }else{
                        $valtstat_ser_vac = [];
                        $txttstat_ser_vac = [];
                    }
                }else{
                    $valtstat_ser_vac = [];
                    $txttstat_ser_vac = [];
                }

                if(!empty($getstat_ser_bath)){
                    if($getstat_ser_bath->count() != 0){
                        if($getstat_ser_bath->count() == 1){
                            $valtstat_ser_bath = [$getstat_ser_bath[0]['count_booking_time']];
                            $txttstat_ser_bath = [$getstat_ser_bath[0]['booking_time']];
                        }else if($getstat_ser_bath->count() > 1) {
                            $valtstat_ser_bath = [$getstat_ser_bath[0]['count_booking_time'],$getstat_ser_bath[1]['count_booking_time']];
                            $txttstat_ser_bath = [$getstat_ser_bath[0]['booking_time'],$getstat_ser_bath[1]['booking_time']];
                        }
                    }else{
                        $valtstat_ser_bath = [];
                        $txttstat_ser_bath = [];
                    }
                }else{
                    $valtstat_ser_bath = [];
                    $txttstat_ser_bath = [];
                }

                $valstat_vac = [];
                $txtstat_vac = [];

                if(!empty($getvaccine_stat)){
                    foreach ($getvaccine_stat as $key => $getvaccinestat) {
                        array_push($valstat_vac, $getvaccinestat->count_vac);
                        array_push($txtstat_vac, $getvaccinestat->vaccine->vaccine_name);
                    }
                }else{
                    $valstat_vac = [];
                    $txtstat_vac = [];
                }

                // d($valstat_vac);
                // d($txtstat_vac);
                // sd($getvaccine_stat->toArray());

                return ['status' => 'success', 'valstat_vac' => $valstat_vac, 'txtstat_vac' => $txtstat_vac, 'getstat_ser_vac' => $valtstat_ser_vac, 'getstat_ser_bath' => $valtstat_ser_bath, 'txttstat_ser_bath' => $txttstat_ser_bath, 'txttstat_ser_vac' => $txttstat_ser_vac];
                   

                    // return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                
                break;

            
            case 'postAddCourseRecord':

                $course_id      = \Input::get('course_id');
                // $thetime = \Input::has('thetime') ? \Input::get('thetime') : '';
                $id_petservice  = \Input::get('id_petservice');
                $booking_time   = \Input::get('booking_time');
                $startdate      = \Input::get('startdate');

                   // d($courses);
                // d($course_id);
                // d($booking_time);
                // sd($startdate);

                $time_val = "";
                $que_val = "";
                $postponeAdate_status = "";

                $add_order = PetService::where('id_petservice', $id_petservice)->first();

                if(empty($add_order)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $getAll_orders = PetService::where('id_shop', $add_order->id_shop)
                                    ->where('id_service', 'PG0001')
                                    ->where('booking_time', $booking_time)
                                    ->whereDate('booking_date', $startdate)
                                    ->where('status', 'ชำระเงินแล้ว')
                                    ->get();

                $getservices = ShopService::where('shop_id', $add_order->id_shop)
                                    ->where('service', 'PG0001')
                                    ->where('time_period', $booking_time)
                                    ->first();


                if($getAll_orders->count() != 0){//กรณีมีคิวแล้ว
                    $getlatest_Que = PetService::where('id_shop', $add_order->id_shop)
                                    ->where('id_service', 'PG0001')
                                    ->where('booking_time', $booking_time)
                                    ->whereDate('booking_date', $startdate)
                                    ->where('status', 'ชำระเงินแล้ว')
                                    ->latest('updated_at')->first();
                    // sd($getlatest_Que);

                    if($getservices->que > $getAll_orders->count()){// กรณีที่คิวที่ตั้งค่าไว้ ไม่มากกว่า คิวที่มีทั้งหมด
                        if($booking_time == "บ่าย"){// ช่วงเวลาบ่าย
                            $que_val    = $getAll_orders->count() + 1;//บวกคิว

                            $date = date($getlatest_Que->service_time); //เวลาเริ่มต้น ที่จะใช้บวก
                            $time_val = date("H:i",strtotime('+1 hour +30 minutes',strtotime($date)));
                            // บวก +1 hour +30 minutes สามารถแก้ที่ตัวเลขได้เลย

                            // $postponeAdate_status = "เลื่อนนัดสำเร็จ";

                        }else if($booking_time == "เช้า"){ // ช่วงเวลาเช้า
                            $que_val    = $getAll_orders->count() + 1;

                            $date = date($getlatest_Que->service_time);//เวลาเริ่มต้น ที่จะใช้บวก
                            $time_val = date("H:i",strtotime('+1 hour +30 minutes',strtotime($date)));
                            // บวก +1 hour +30 minutes สามารถแก้ที่ตัวเลขได้เลย

                            // $postponeAdate_status = "เลื่อนนัดสำเร็จ";
                        }   
                    }else{
                        // $postponeAdate_status = "ไม่สามารถเลื่อนนัดได้";
                        // กรณีที่คิวที่ตั้งค่าไว้ มากกว่า คิวที่มีทั้งหมด
                        return ['status' => 'error', 'msg' => 'ไม่สามารถเลื่อนนัดได้ เนื่องจากคิวเต็ม ระบบได้ทำการแก้ไขสถานะการขอเลื่อนนัดให้ทางผู้จองได้ทราบแล้ว'];
                    }
                }else{//กรณียังไม่มีคิว
                    if($booking_time == "บ่าย"){// ช่วงเวลาบ่าย
                        $que_val    = 1; //คิวแรก
                        $time_val   = date("13:30"); //เวลาเริ่มต้น

                        // $postponeAdate_status = "เลื่อนนัดสำเร็จ";
                    }else if($booking_time == "เช้า"){ // ช่วงเวลาเช้า
                        $que_val    = 1; //คิวแรก
                        $time_val   = date("09:00"); //เวลาเริ่มต้น

                        // $postponeAdate_status = "เลื่อนนัดสำเร็จ";
                    }   
                }

                // บันทึกข้อมูลลงดาต้าเบส               
                // $add_order->postponeAdate_status        = $postponeAdate_status;
                $add_order->service_order               = $que_val;
                $add_order->service_time                = $time_val;
                $add_order->save();

                $courses = CourseRecord::where('id', $course_id)->first();
                if(empty($courses)) return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];

                // d($courses);
                // d($course_id);
                // d($booking_time);
                // sd($startdate);

                if(!empty($courses)){
                    $courses->startdate     = $startdate;
                    $courses->usetime       = $time_val;
                    $courses->courseque     = $que_val;
                    $courses->save();
                }

                return ['status' => 'success'];
                
                break;

            default:
                return ['status' => 'error'];
                break;
        }
    }
}
