<?php

namespace App\Http\Controllers\PetsShop;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Veterinary\Veterinary;
use App\Services\Description\Description;
use App\Services\Shop\Shop;
use App\Services\PetsShop\PetsShop;
use App\Services\Vaccinrecord\Vaccinrecord;
use App\Services\PetService\PetService;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;

class PetsShopController extends Controller
{
    public function getIndex()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'){
            $petsShops = PetsShop::with(['shop', 'pet', 'user'])->where('id', '!=', 0)->get();

            $shops = Shop::where('owner_id', '!=', 0);
            $shops = $shops->get(); 
        }else{
            $petsShops = PetsShop::with(['shop', 'pet', 'user']);
            $petsShops = $petsShops->where('id', '!=', 0);
            $petsShops = $petsShops->where('user_id', $user_id);
            $petsShops = $petsShops->get();
            
            $shops = Shop::where('owner_id', $user_id);
            $shops = $shops->get(); 
        }

        $species   = Species::where('status', 'Active')->orderBy('name', 'asc')->get();

        $pettypes  = PetType::where('status', 'Active')->orderBy('name', 'asc')->get();

        // sd($users->toArray());

    	return $this->view('petsShop.index',compact('petsShops', 'shops', 'species', 'user_type', 'pettypes'));
    }


    public function getManageSpecies()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $species   = Species::get();

        $pettypes  = PetType::where('status', 'Active')->orderBy('name', 'asc')->get();


        return $this->view('petsShop.managespecies',compact('species', 'user_type', 'pettypes'));
    }


    public function getManagePetType()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $pettypes  = PetType::get();


        return $this->view('petsShop.managepettype',compact('pettypes', 'user_type'));
    }


    public function getView(Request $request, $id)
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $veterinarys = Veterinary::with(['shop']);
        $veterinarys = $veterinarys->where('id', '!=', 0);
        $veterinarys = $veterinarys->where('user_id', $user_id);
        $veterinarys = $veterinarys->get();
        
        $petsShop = PetsShop::with(['shop', 'pet', 'user'])->where('id', $id)->first();

        $getbookings = PetService::with(['user','shop','pet']);
        $getbookings = $getbookings->where('pet_id', $petsShop->pet_id);
        $getbookings = $getbookings->where('id_shop', $petsShop->shop_id)->get();

        $getvaccineRCs = Vaccinrecord::with(['vaccine','veterinary','shop','pet']);
        $getvaccineRCs = $getvaccineRCs->where('pet_id', $petsShop->pet_id);
        $getvaccineRCs = $getvaccineRCs->where('shop_id', $petsShop->shop_id);
        $getvaccineRCs = $getvaccineRCs->get();

        // sd($getbookings->toArray());
        // sd($getvaccineRCs->toArray());

        return $this->view('petsShop.view',compact('petsShop', 'veterinarys','getvaccineRCs'));
    }

    public function getViewImg(Request $request, $id)
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getvaccineRC = Vaccinrecord::with(['vaccine','veterinary','shop','pet']);
        $getvaccineRC = $getvaccineRC->where('id_vaccin', $id);
        $getvaccineRC = $getvaccineRC->first();

        // sd($getbookings->toArray());
        // sd($getvaccineRC->toArray());

        return $this->view('petsShop.viewImg',compact('getvaccineRC'));
    }

    public function postAdd()
    {
        $shop_id            = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $pet_name           = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $pet_type           = \Input::has('pet_type') ? \Input::get('pet_type') : '';
        $breed              = \Input::has('breed') ? \Input::get('breed') : '';
        $color              = \Input::has('color') ? \Input::get('color') : '';
        $weight             = \Input::has('weight') ? \Input::get('weight') : '';
        $notedescription    = \Input::has('notedescription') ? \Input::get('notedescription') : '';
        $birthday           = \Input::has('birthday') ? \Input::get('birthday') : '';
        $gender             = \Input::has('gender') ? \Input::get('gender') : '';
        $pets_img           = \Input::hasFile('pic') ? \Input::file('pic') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/petImg")){
         $oldmask = umask(0);
         mkdir($path."/public/petImg", 0777);
         umask($oldmask);
        }

        if(!empty($pets_img))
        {
         $image_name = "/public/petImg/".$date."-".$pets_img->getClientOriginalName();
         copy($pets_img, $path.$image_name);
        }


        $new_pet                   = new Description;
        $new_pet->name             = $pet_name;
        $new_pet->id_owner         = $user_id;
        $new_pet->pic              = (empty($image_name)) ? "" : $image_name;
        $new_pet->type             = $pet_type;
        $new_pet->breed            = $breed;
        $new_pet->color            = $color;
        $new_pet->weight           = $weight;
        $new_pet->notedescription  = $notedescription;
        $new_pet->birthday         = $birthday;
        $new_pet->gender           = $gender;
        $new_pet->save();


        $new_pets_shop              = new PetsShop;
        $new_pets_shop->shop_id     = $shop_id;
        $new_pets_shop->pet_id      = $new_pet->pet_id;
        $new_pets_shop->user_id     = $user_id;
        $new_pets_shop->save();

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $shop_id            = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $petshop_id            = \Input::has('petshop_id') ? \Input::get('petshop_id') : '';
        $pet_name           = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $pet_type           = \Input::has('pet_type') ? \Input::get('pet_type') : '';
        $breed              = \Input::has('breed') ? \Input::get('breed') : '';
        $color              = \Input::has('color') ? \Input::get('color') : '';
        $weight             = \Input::has('weight') ? \Input::get('weight') : '';
        $notedescription    = \Input::has('notedescription') ? \Input::get('notedescription') : '';
        $birthday           = \Input::has('birthday') ? \Input::get('birthday') : '';
        $gender             = \Input::has('gender') ? \Input::get('gender') : '';
        $pets_img           = \Input::hasFile('pic') ? \Input::file('pic') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        // d(date($birthday, 'Y-m-d'));
        // d(date('Y-m-d',strtotime($birthday)));
        // sd($birthday);
        $petsShop  = PetsShop::where('id', $petshop_id)->first();
        if(empty($petsShop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        if(!empty($petsShop)){
            $pet = Description::where('pet_id', $petsShop->pet_id)->first();
            if(empty($pet)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        }

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/petImg")){
         $oldmask = umask(0);
         mkdir($path."/public/petImg", 0777);
         umask($oldmask);
        }

        if(!empty($pets_img))
        {
         $image_name = "/public/petImg/".$date."-".$pets_img->getClientOriginalName();
         copy($pets_img, $path.$image_name);
        }
        // d($user_img);
        // d($menu_accress);
        // sd($image_name);

        $pet->name             = $pet_name;
        $pet->id_owner         = $user_id;
        if(!empty($image_name)){
            $pet->pic           = $image_name;
        }
        $pet->type             = $pet_type;
        $pet->breed            = $breed;
        $pet->color            = $color;
        $pet->weight           = $weight;
        $pet->notedescription  = $notedescription;
        $pet->birthday         = $birthday;
        $pet->gender           = $gender;
        $pet->save();

        return ['status' => 'success'];
    }



    public function postRemove()
    {
        $id    = \Input::has('id') ? \Input::get('id') : '';

        $petsShop  = PetsShop::where('id', $petshop_id)->first();
        if(empty($petsShop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        if(!empty($petsShop)){
            $pet = Description::where('pet_id', $petsShop->pet_id)->first();
            if(empty($pet)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        }

        $path = base_path();
        if(!empty($pet->pic)){
            if(file_exists($path.$pet->pic)){
                unlink($path.$pet->pic);
            }
        }

        $pet->delete();
        $petsShop->delete();

        return ['status' => 'success'];
    }



    public function postManageSpeciesAdd()
    {
        $name       = \Input::has('name') ? \Input::get('name') : '';
        $shortname  = \Input::has('shortname') ? \Input::get('shortname') : '';
        $status     = \Input::has('status') ? \Input::get('status') : '';
        $pet_type   = \Input::has('pet_type') ? \Input::get('pet_type') : '';

        $new_species                = new Species;
        $new_species->name          = $name;
        $new_species->shortname     = $shortname;
        $new_species->status        = $status;
        $new_species->pet_type      = $pet_type;
        $new_species->save();

        return ['status' => 'success'];
    }


    public function postManageSpeciesEdit()
    {
        $species_id = \Input::has('id') ? \Input::get('id') : '';
        $name       = \Input::has('name') ? \Input::get('name') : '';
        $shortname  = \Input::has('shortname') ? \Input::get('shortname') : '';
        $status     = \Input::has('status') ? \Input::get('status') : '';
        $pet_type   = \Input::has('pet_type') ? \Input::get('pet_type') : '';

        $species  = Species::where('id', $species_id)->first();
        if(empty($species)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        
        $species->name          = $name;
        $species->shortname     = $shortname;
        $species->status        = $status;
        $species->pet_type      = $pet_type;
        $species->save();

        return ['status' => 'success'];
    }


    public function postManageSpeciesRemove()
    {
        $species_id    = \Input::has('id') ? \Input::get('id') : '';

        $species  = Species::where('id', $species_id)->first();
        if(empty($species)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $species->delete();

        return ['status' => 'success'];
    }


    public function postManagePetTypeAdd()
    {
        $name       = \Input::has('name') ? \Input::get('name') : '';
        $status     = \Input::has('status') ? \Input::get('status') : '';

        $new_pettype                = new PetType;
        $new_pettype->name          = $name;
        $new_pettype->status        = $status;
        $new_pettype->save();

        return ['status' => 'success'];
    }


    public function postManagePetTypeEdit()
    {
        $id         = \Input::has('id') ? \Input::get('id') : '';
        $name       = \Input::has('name') ? \Input::get('name') : '';
        $status     = \Input::has('status') ? \Input::get('status') : '';

        $pettype  = PetType::where('id', $id)->first();
        if(empty($pettype)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        
        $pettype->name          = $name;
        $pettype->status        = $status;
        $pettype->save();

        return ['status' => 'success'];
    }


    public function postManagePetTypeRemove()
    {
        $id    = \Input::has('id') ? \Input::get('id') : '';

        $pettype  = PetType::where('id', $id)->first();
        if(empty($pettype)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $pettype->delete();

        return ['status' => 'success'];
    }


    public function postAddBooking()
    {
        $id               = \Input::has('id') ? \Input::get('id') : '';
        $id_shop          = \Input::has('id_shop') ? \Input::get('id_shop') : '';
        $pet_id           = \Input::has('pet_id') ? \Input::get('pet_id') : '';
        $booking_date     = \Input::has('booking_date') ? \Input::get('booking_date') : '';
        // $promo_code       = \Input::has('promo_code') ? \Input::get('promo_code') : '';
        $id_service       = \Input::has('id_service') ? \Input::get('id_service') : '';
        // $booking_time     = \Input::has('booking_time') ? \Input::get('booking_time') : '';
        $note             = \Input::has('note') ? \Input::get('note') : '';
        $vaccine_val      = \Input::has('vaccine_val') ? \Input::get('vaccine_val') : '';
        $vaccine_id       = \Input::has('vaccine_id') ? \Input::get('vaccine_id') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $pet = Description::where('pet_id', $pet_id)->first();
        if(empty($pet)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        
        // $getshop_ownerID = Shop::where('id_shop', $id_shop)->first();

        // $get_pets_shop = PetsShop::where('pet_id', $pet_id);
        // $get_pets_shop = $get_pets_shop->where('shop_id', $id_shop);
        // $get_pets_shop = $get_pets_shop->where('user_id', $getshop_ownerID->owner_id);
        // $get_pets_shop = $get_pets_shop->first();

        $getShopUsed = ShopUsed::where('shop_id', $id_shop)->where('user_id', $user_id)->first();

        if(empty($getShopUsed)){
            $newShopUsed            = new ShopUsed;
            $newShopUsed->shop_id   = $id_shop;
            $newShopUsed->user_id   = $user_id;
            $newShopUsed->save();
        }
        

        if($vaccine_val != ''){
            $new_vaccinrecord                   = new Vaccinrecord;
            $new_vaccinrecord->vaccin           = $vaccine_val;
            $new_vaccinrecord->vaccine_ID       = $vaccine_id;
            $new_vaccinrecord->veterinary_ID    = null;
            $new_vaccinrecord->pet_id           = $pet_id;
            $new_vaccinrecord->shop_id          = $id_shop;
            $new_vaccinrecord->namevaccin       = null;
            $new_vaccinrecord->veterinarian     = null;
            $new_vaccinrecord->notevaccin       = null;
            $new_vaccinrecord->img              = null;
            $new_vaccinrecord->save();
        }

        $new_service                 = new PetService;
        $new_service->id_shop        = $id_shop;
        $new_service->vaccine_ID     = $vaccine_id;
        $new_service->note           = $note;
        $new_service->id_login       = $pet->id_owner;
        $new_service->pets_shopID    = $id;
        $new_service->booking_date   = $booking_date;
        $new_service->promo_code     = null;
        $new_service->id_service     = $id_service;
        $new_service->booking_time   = '';
        $new_service->pet_id         = $pet_id;
        if($vaccine_val != ''){
            $new_service->vaccine    = $vaccine_val;
            $new_service->vaccineRC_id    = $new_vaccinrecord->id_vaccin;
        }
        $new_service->status         = 'รอชำระเงิน';
        $new_service->appoint        = true;
        $new_service->postponeAdate  = false;
        $new_service->postponeAdate_status  = null;
        $new_service->save();

        return ['status' => 'success', 'serviceid' => $new_service->id_petservice];
    }



    public function ajaxCenter()
    {
        $method         = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'getPetData':

                $id = \Input::has('id') ? \Input::get('id') : '';

                $petsShop = PetsShop::with(['shop', 'pet', 'user'])->where('id', $id)->first();

                $species  = Species::where('status', 'Active')->orderBy('name', 'asc')->get();

                if(!empty($petsShop)){
                    return ['status' => 'success', 'data' => $petsShop, 'species' => $species];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'getSpeciesData':

                $species_id = \Input::has('id') ? \Input::get('id') : '';

                $species  = Species::where('id', $species_id)->first();
                // if(empty($species)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                if(!empty($species)){
                    return ['status' => 'success', 'data' => $species];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'getPetTypeData':

                $id = \Input::has('id') ? \Input::get('id') : '';

                $pettype  = PetType::where('id', $id)->first();

                if(!empty($pettype)){
                    return ['status' => 'success', 'data' => $pettype];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;


            case 'getSpeciesByPetType':

                $pettype = \Input::has('pettype') ? \Input::get('pettype') : '';

                $species  = Species::where('pet_type', $pettype)->orderBy('name', 'asc')->get();

                if(!empty($species)){
                    return ['status' => 'success', 'species' => $species];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;


            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
