<?php

namespace App\Http\Controllers\Premium;

use DB;
use DateTime;
use DateInterval;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Shop\Shop;
use App\Services\Partner\Partner;
use App\Services\Amphur\Amphur;
use App\Services\District\District;
use App\Services\Province\Province;
use App\Services\Zipcode\Zipcode;
use App\Services\Description\Description;
use App\Services\PetService\PetService;
use App\Services\ShopService\ShopService;
use App\Services\Veterinary\Veterinary;
use App\Services\Vaccinrecord\Vaccinrecord;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;


class PremiumController extends Controller
{
    public function getIndex()
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $shops_admin = Shop::where('id_shop', '!=', 0)->get();
        // $shops_admin = Shop::where('id_shop', '!=', 0)->where('status_premium', 1)->get();
        
        $shops = Shop::where('id_shop', '!=', 0);
        $shops = $shops->where('owner_id', $user_id);
        $shops = $shops->first();


        $txt_enddate = empty($shops) ? '' : $shops->pre_enddate;
        $expire = strtotime($txt_enddate);
        $today  = strtotime("today midnight");
        // $diffexpired_date = date_diff(date_create($txt_enddate),date_create(date('Y-m-d')));
        if($today >= $expire){
            if(!empty($shops)){
                $shops->status_premium = false;
                $shops->save();
            }
            $diffexpired_date = 'expire';
        }else{
            $diffexpired_date = 'active';
        }



        if(!empty($shops_admin)){
            foreach ($shops_admin as $key => $shopsadmin) {
                $txt_enddateadmin = $shopsadmin->pre_enddate;
                $expireadmin = strtotime($txt_enddateadmin);
                $todayadmin  = strtotime("today midnight");
                    // d($shopsadmin->pre_enddate);
                    // d($today);
                    // sd($expire);
                    // d($diffexpired_dateadmin);
                if($todayadmin >= $expireadmin){
                    $shopsadmin->status_premium = false;
                    $shopsadmin->save();
                }

            }

        }

       
    	return $this->view('premium.index',compact('shops','user_type', 'shops_admin', 'diffexpired_date'));
    }


    public function postAdd()
    {
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $price          = \Input::has('price') ? \Input::get('price') : '';
        $pre_startdate  = \Input::has('pre_startdate') ? \Input::get('pre_startdate') : '';
        $pre_enddate    = \Input::has('pre_enddate') ? \Input::get('pre_enddate') : '';
        $date_payment   = \Input::has('date_payment') ? \Input::get('date_payment') : '';
        $time_payment   = \Input::has('time_payment') ? \Input::get('time_payment') : '';
        $bank_payment   = \Input::has('bank_payment') ? \Input::get('bank_payment') : '';
        $pic_payment    = \Input::hasFile('pic_payment') ? \Input::file('pic_payment') : '';

        $shop    = Shop::where('id_shop', $id)->first();
        if(empty($shop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/paySlipImg")){
            $oldmask = umask(0);
            mkdir($path."/public/paySlipImg", 0777);
            umask($oldmask);
        }

        if(!empty($pic_payment))
        {
            $image_name = "/public/paySlipImg/".$date."-".$pic_payment->getClientOriginalName();
            copy($pic_payment, $path.$image_name);
        }
        

        $shop->status_premium   = true;
        $shop->price            = $price;
        $shop->pre_startdate    = $pre_startdate;
        $shop->pre_enddate      = $pre_enddate;
        $shop->date_payment     = $date_payment;
        $shop->time_payment     = $time_payment;
        $shop->bank_payment     = $bank_payment;
        $shop->pic_payment      = (empty($image_name)) ? "" : $image_name;
        $shop->save();


       
       return ['status' => 'success'];
    }


    public function postEdit()
    {
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $date_payment   = \Input::has('date_payment') ? \Input::get('date_payment') : '';
        $time_payment   = \Input::has('time_payment') ? \Input::get('time_payment') : '';
        $bank_payment   = \Input::has('bank_payment') ? \Input::get('bank_payment') : '';
        $pic_payment    = \Input::hasFile('pic_payment') ? \Input::file('pic_payment') : '';

        $shop    = Shop::where('id_shop', $id)->first();
        if(empty($shop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/paySlipImg")){
            $oldmask = umask(0);
            mkdir($path."/public/paySlipImg", 0777);
            umask($oldmask);
        }

        if(!empty($pic_payment))
        {
            $image_name = "/public/paySlipImg/".$date."-".$pic_payment->getClientOriginalName();
            copy($pic_payment, $path.$image_name);
        }
        

        $shop->date_payment     = $date_payment;
        $shop->time_payment     = $time_payment;
        $shop->bank_payment     = $bank_payment;
        if(!empty($image_name)){
            $shop->pic_payment      = $image_name;
        }
        $shop->save();


       return ['status' => 'success'];
    }

    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {

            case 'activeOrInactivePremium':

                if (!\Input::has('shop_id')) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $shop_id    = \Input::get('shop_id');
                $new_staus  = \Input::get('status_change');

                $shop = Shop::where('id_shop', $shop_id)->first();
                if(empty($shop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                if (!empty($shop)){
                    $shop->status_premium = ($new_staus != 'false') ? true : false;
                    $shop->save();
                    
                    return ['status' => 'success'];
                }
                    return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
            break;
            default:
                return ['status' => 'error'];
                break;
        }
    }
}
