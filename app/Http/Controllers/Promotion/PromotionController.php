<?php

namespace App\Http\Controllers\Promotion;

use DB;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Stock\Stock;
use App\Services\Partner\Partner;
use App\Services\Bill\Bill;
use App\Services\Pos\Pos;
use App\Services\Orderpos\Orderpos;
use App\Services\Vaccine\Vaccine;
use App\Services\Promotion\Promotion;
use App\Services\Shop\Shop;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;


class PromotionController extends Controller
{
    public function getIndex()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;
        
        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();

        if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'){
            $getPromotions = Promotion::where('id', '!=', 0);
            $getPromotions = $getPromotions->paginate(20);
        }elseif($user_type == 'เจ้าของกิจการ'){
            $getPromotions = Promotion::where('id', '!=', 0);
            $getPromotions = $getPromotions->where('shop_id', $shop->id_shop);
            $getPromotions = $getPromotions->paginate(20);
        }

        foreach ($getPromotions as $key => $getPromotion) {
            $diff = date_diff(date_create($getPromotion->enddate),date_create(date('Y-m-d')));
            // sd($diff);
            // if($getPromotion->enddate == date('Y-m-d')){
            if($diff->days <= 0){
                $getPromotion->status = 'Inactive';
                $getPromotion->save();
            }
        }


    	return $this->view('promotion.index',compact('getPromotions'));
    }


    public function getAdd()
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getShops = Shop::where('status_open', 1)->get();

        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();

        return $this->view('promotion.add',compact('getShops', 'shop'));
    }

    public function getView(Request $request, $id)
    {
        $promotion = Promotion::with(['shop'])->where('id', $id)->first();

        return $this->view('promotion.view',compact('promotion'));
    }

    public function getEdit(Request $request, $id)
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $promotion = Promotion::where('id', $id)->first();
        $getShops = Shop::where('status_open', 1)->get();

        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();

        return $this->view('promotion.edit',compact('promotion','getShops', 'shop'));
    }

    public function postAdd()
    {
        $title          = \Input::has('title') ? \Input::get('title') : '';
        $startdate      = \Input::has('startdate') ? \Input::get('startdate') : '';
        $enddate        = \Input::has('enddate') ? \Input::get('enddate') : '';
        $code           = \Input::has('code') ? \Input::get('code') : '';
        $shop_id        = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $discount       = \Input::has('discount') ? \Input::get('discount') : '';
        $status         = \Input::has('status') ? \Input::get('status') : '';
        $link           = \Input::has('link') ? \Input::get('link') : '';
        $service_type   = \Input::has('service_type') ? \Input::get('service_type') : '';
        $description    = \Input::has('description') ? \Input::get('description') : '';
        $img            = \Input::hasFile('img') ? \Input::file('img') : '';


        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;


        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();


        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/promotionImg")){
         $oldmask = umask(0);
         mkdir($path."/public/promotionImg", 0777);
         umask($oldmask);
        }

        if(!empty($img))
        {
         $image_name = "/public/promotionImg/".$date."-".$img->getClientOriginalName();
         copy($img, $path.$image_name);
        }

        $new_promotion                = new Promotion;
        $new_promotion->title         = $title;
        $new_promotion->startdate     = $startdate;
        $new_promotion->enddate       = $enddate;
        $new_promotion->code          = $code;
        $new_promotion->shop_id       = $shop->id_shop;
        $new_promotion->discount      = $discount;
        $new_promotion->status        = $status;
        $new_promotion->link          = $link;
        $new_promotion->service_type  = $service_type;
        $new_promotion->description   = $description;
        $new_promotion->img           = (empty($image_name)) ? "" : $image_name;
        $new_promotion->save();

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $title          = \Input::has('title') ? \Input::get('title') : '';
        $startdate      = \Input::has('startdate') ? \Input::get('startdate') : '';
        $enddate        = \Input::has('enddate') ? \Input::get('enddate') : '';
        $code           = \Input::has('code') ? \Input::get('code') : '';
        $shop_id        = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $discount       = \Input::has('discount') ? \Input::get('discount') : '';
        $status         = \Input::has('status') ? \Input::get('status') : '';
        $link           = \Input::has('link') ? \Input::get('link') : '';
        $service_type   = \Input::has('service_type') ? \Input::get('service_type') : '';
        $description    = \Input::has('description') ? \Input::get('description') : '';
        $img            = \Input::hasFile('img') ? \Input::file('img') : '';

        $promotion = Promotion::where('id', $id)->first();
        if(empty($promotion)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;


        $shop = Shop::where('owner_id', $user_id);
        $shop = $shop->first();
        

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/promotionImg")){
         $oldmask = umask(0);
         mkdir($path."/public/promotionImg", 0777);
         umask($oldmask);
        }

        if(!empty($img))
        {
         $image_name = "/public/promotionImg/".$date."-".$img->getClientOriginalName();
         copy($img, $path.$image_name);
        }

        $promotion->title         = $title;
        $promotion->startdate     = $startdate;
        $promotion->enddate       = $enddate;
        $promotion->code          = $code;
        $promotion->shop_id       = $shop->id_shop;
        $promotion->discount      = $discount;
        $promotion->status        = $status;
        $promotion->link          = $link;
        $promotion->service_type  = $service_type;
        $promotion->description   = $description;
        if(!empty($image_name)){
            $promotion->img       = $image_name;
        }
        $promotion->save();

        return ['status' => 'success'];
    }

    public function postRemove()
    {
        $id    = \Input::has('id') ? \Input::get('id') : '';

        $promotion = Promotion::where('id', $id)->first();
        if(empty($promotion)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $path = base_path();
        if(!empty($promotion->img)){
            if(file_exists($path.$promotion->img)){
                unlink($path.$promotion->img);
            }
        }

        $promotion->delete();

        return ['status' => 'success'];
    }


    public function ajaxCenter()
    {
        $method  = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
             // case 'addFilterToSession':

             //    $filters                    = [];
             //    $filters['search_txt']      = \Input::get('search_txt');
             //    $filters['vaccine_type']    = \Input::get('vaccine_type');
             //    $filters['vaccine_status']  = \Input::get('vaccine_status');

             //    \Session::put('report_filters', $filters);

             //    return ['status' => 'success'];
             //    break;

             // case 'clearFilterToSession':

             //    \Session::forget('report_filters');
                
             //    return ['status' => 'success'];
             //    break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
