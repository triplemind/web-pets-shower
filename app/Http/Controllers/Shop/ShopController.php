<?php

namespace App\Http\Controllers\Shop;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Shop\Shop;
use App\Services\Partner\Partner;
use App\Services\Amphur\Amphur;
use App\Services\District\District;
use App\Services\Province\Province;
use App\Services\Zipcode\Zipcode;
use App\Services\ShopService\ShopService;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;


class ShopController extends Controller
{
    public function getIndex()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $filters = \Session::has('shop_filters') ? \Session::get('shop_filters') : [];
            
        $shops = Shop::where('id_shop', '!=', 0);
        if($user_type == 'เจ้าของกิจการ'){
            $shops = $shops->where('owner_id', $user_id);
        }

        if(!empty($filters['type_shopfil'])){
            $shops = $shops->where('type_shop', $filters['type_shopfil']);
        }

        if(!empty($filters['status_openfill'])){
            if($filters['status_openfill'] == 'ปิด'){
                $shops = $shops->where('status_open', false);
            }else{
                $shops = $shops->where('status_open', true);
            }
        }

        if(!empty($filters['search_txt'])){
            $keyword = $filters['search_txt'];
            $shops = $shops->where(function($shops) use ($keyword){
                            $shops->where('name_shop', 'like', '%'.$keyword.'%');
                            $shops->orWhere('type_shop', 'like', '%'.$keyword.'%');
                            $shops->orWhere('status_open', 'like', '%'.$keyword.'%');
                            $shops->orWhere('address_shop', 'like', '%'.$keyword.'%');
                            $shops->orWhere('licenesNo', 'like', '%'.$keyword.'%');
                            $shops->orWhere('shop_description', 'like', '%'.$keyword.'%');
                            $shops->orWhere('shop_detail', 'like', '%'.$keyword.'%');
            });
        }
        $shops = $shops->get();

        // sd($filters);

    	return $this->view('shop.index',compact('shops','filters'));
    }

    public function getAdd()
    {

        $provinces = Province::get();

        $amphurs = Amphur::get();

        $districts = District::get();

        $zipcodes = Zipcode::get();

        return $this->view('shop.add', compact('provinces', 'amphurs', 'districts', 'zipcodes'));
    }

    public function getView(Request $request, $id)
    {
        $shop = Shop::where('id_shop', $id)->first();

        $province = Province::where('PROVINCE_ID', $shop->province)->first();

        $amphur = Amphur::where('AMPHUR_ID', $shop->amphur)->first();

        $district = District::where('DISTRICT_ID', $shop->district)->first();


        return $this->view('shop.view',compact('shop', 'province', 'amphur', 'district'));
    }

    public function getManageService(Request $request, $id)//query ข้อมูลร้านค้า
    {
        $shop = Shop::where('id_shop', $id)->first(); //ข้อมูลร้านค้า

        $shop_services = ShopService::where('shop_id', $id)->get(); //บริการของร้านค้า

        $vaccines = DB::table('vaccine')->where('vaccine_status', 'Active')->get(); //วัคซีน

        return $this->view('shop.manageservice',compact('shop', 'shop_services', 'vaccines'));
    }

    public function getEdit(Request $request, $id)
    {
        $shop = Shop::where('id_shop', $id)->first();

        $provinces = Province::get();

        $amphurs = Amphur::get();

        $districts = District::get();

        $zipcodes = Zipcode::get();

        return $this->view('shop.edit',compact('shop','provinces', 'amphurs', 'districts', 'zipcodes'));
    }

    public function postAdd()
    {
        $name_shop          = \Input::has('name_shop') ? \Input::get('name_shop') : '';
        $shop_lat           = \Input::has('shop_lat') ? \Input::get('shop_lat') : '';
        $shop_long          = \Input::has('shop_long') ? \Input::get('shop_long') : '';
        $deposit            = \Input::has('deposit') ? \Input::get('deposit') : '';
        $type_shop          = \Input::has('type_shop') ? \Input::get('type_shop') : '';
        $licenesNo          = \Input::has('licenesNo') ? \Input::get('licenesNo') : '';
        $tel_shop           = \Input::has('tel_shop') ? \Input::get('tel_shop') : '';
        $address_shop       = \Input::has('address_shop') ? \Input::get('address_shop') : '';
        $zip                = \Input::has('zip') ? \Input::get('zip') : '';
        $shop_description   = \Input::has('shop_description') ? \Input::get('shop_description') : '';
        $province           = \Input::has('province') ? \Input::get('province') : '';
        $amphur             = \Input::has('amphur') ? \Input::get('amphur') : '';
        $district           = \Input::has('district') ? \Input::get('district') : '';
        $status_open        = \Input::has('status_open') ? \Input::get('status_open') : '';
        $shop_img           = \Input::hasFile('shop_img') ? \Input::file('shop_img') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/shopImg")){
         $oldmask = umask(0);
         mkdir($path."/public/shopImg", 0777);
         umask($oldmask);
        }

        if(!empty($shop_img))
        {
         $image_name = "/public/shopImg/".$date."-".$shop_img->getClientOriginalName();
         copy($shop_img, $path.$image_name);
        }

        $new_shop                   = new Shop;
        $new_shop->owner_id         = $user_id;
        $new_shop->name_shop        = $name_shop;
        $new_shop->deposit          = $deposit;
        $new_shop->status_premium   = false;
        $new_shop->shop_img         = (empty($image_name)) ? "" : $image_name;
        $new_shop->type_shop        = $type_shop;
        $new_shop->licenesNo        = $licenesNo;
        $new_shop->tel_shop         = $tel_shop;
        $new_shop->address_shop     = $address_shop;
        $new_shop->zip              = $zip;
        $new_shop->shop_description = $shop_description;
        $new_shop->province         = $province;
        $new_shop->amphur           = $amphur;
        $new_shop->district         = $district;
        $new_shop->status_open      = $status_open;
        $new_shop->shop_lat         = $shop_lat;
        $new_shop->shop_long        = $shop_long;
        $new_shop->save();

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $id_shop            = \Input::has('id_shop') ? \Input::get('id_shop') : '';
        $deposit            = \Input::has('deposit') ? \Input::get('deposit') : '';
        $shop_lat           = \Input::has('shop_lat') ? \Input::get('shop_lat') : '';
        $shop_long          = \Input::has('shop_long') ? \Input::get('shop_long') : '';
        $name_shop          = \Input::has('name_shop') ? \Input::get('name_shop') : '';
        $type_shop          = \Input::has('type_shop') ? \Input::get('type_shop') : '';
        $licenesNo          = \Input::has('licenesNo') ? \Input::get('licenesNo') : '';
        $tel_shop           = \Input::has('tel_shop') ? \Input::get('tel_shop') : '';
        $address_shop       = \Input::has('address_shop') ? \Input::get('address_shop') : '';
        $zip                = \Input::has('zip') ? \Input::get('zip') : '';
        $shop_description   = \Input::has('shop_description') ? \Input::get('shop_description') : '';
        $province           = \Input::has('province') ? \Input::get('province') : '';
        $amphur             = \Input::has('amphur') ? \Input::get('amphur') : '';
        $district           = \Input::has('district') ? \Input::get('district') : '';
        $status_open        = \Input::has('status_open') ? \Input::get('status_open') : '';
        $shop_img           = \Input::hasFile('shop_img') ? \Input::file('shop_img') : '';

        $shop    = Shop::where('id_shop', $id_shop)->first();
        if(empty($shop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/shopImg")){
         $oldmask = umask(0);
         mkdir($path."/public/shopImg", 0777);
         umask($oldmask);
        }

        if(!empty($shop_img))
        {
         $image_name = "/public/shopImg/".$date."-".$shop_img->getClientOriginalName();
         copy($shop_img, $path.$image_name);
        }

        $shop->name_shop        = $name_shop;
        $shop->deposit          = $deposit;
        $shop->status_premium   = false;
        if(!empty($image_name)){
            $shop->shop_img     = $image_name;
        }
        $shop->type_shop        = $type_shop;
        $shop->licenesNo        = $licenesNo;
        $shop->tel_shop         = $tel_shop;
        $shop->address_shop     = $address_shop;
        $shop->zip              = $zip;
        $shop->shop_description = $shop_description;
        $shop->province         = $province;
        $shop->amphur           = $amphur;
        $shop->district         = $district;
        $shop->status_open      = $status_open;
        $shop->shop_lat         = $shop_lat;
        $shop->shop_long        = $shop_long;
        $shop->save();

        return ['status' => 'success'];
    }


    public function postAddSevice(){

        $Que           = \Input::has('Que') ? \Input::get('Que') : '';
        $id_service    = \Input::has('id_service') ? \Input::get('id_service') : '';
        $booking_time  = \Input::has('booking_time') ? \Input::get('booking_time') : '';
        $shop_id       = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $startTime     = \Input::has('startTime') ? \Input::get('startTime') : '';
        $endTime       = \Input::has('endTime') ? \Input::get('endTime') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $new_shopservice                    = new ShopService;
        $new_shopservice->shop_id           = $shop_id;
        $new_shopservice->que               = $Que;
        $new_shopservice->service           = $id_service;
        $new_shopservice->time_period       = $booking_time;
        $new_shopservice->startTime         = $startTime;
        $new_shopservice->endTime           = $endTime;
        $new_shopservice->save();

        return ['status' => 'success'];
    }

    public function postAddVaccine(){

        $vaccine        = \Input::has('id_vaccine') ? \Input::get('id_vaccine') : '';
        $shop_id        = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $vacArr_length  = \Input::has('vacArr_length') ? \Input::get('vacArr_length') : '';
        
        $shop       = Shop::where('id_shop', $shop_id)->first();
        // d(json_encode($vaccine));
        // sd($vaccine);
        if(empty($shop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        // d($vacArr_length);

        $vacArr = array();

        if(!empty($vaccine)){
            $vaccineArr = json_decode($vaccine);
            // d($vaccineArr);
            for ($i=0; $i < $vacArr_length; $i++) { 
                
                $getvaccine = Vaccine::where('id', $vaccineArr[$i])->first();

                $shopvac = array(
                    "arrID"         => $i,
                    "vaccine_ID"    => $getvaccine->id,
                    "vaccine_name"  => $getvaccine->vaccine_name,
                    "drug_name"     => $getvaccine->drug_name,
                    "properties"    => $getvaccine->properties,
                    "vaccine_type"  => $getvaccine->vaccine_type,
                    "img_vaccine"   => $getvaccine->img_vaccine,
                    "company"       => $getvaccine->company,
                );

                array_push($vacArr, $shopvac);

            }
        }
        // d($vacArr);
        // sd($vaccine);

        $shop->vaccine      = empty($vacArr) ? "" : json_encode($vacArr);
        $shop->vaccineArr   = empty($vaccine) ? "" : json_encode($vaccine);
        $shop->save();

        return ['status' => 'success', 'data' => $vacArr, 'dataVac' => $vaccine];
    }


    public function postRemove()
    {
        $shop_id    = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $shop       = Shop::where('id_shop', $shop_id)->first();

        if(empty($shop)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $path = base_path();
        if(!empty($shop->shop_img)){
            if(file_exists($path.$shop->shop_img)){
                unlink($path.$shop->shop_img);
            }
        }

        $shop->delete();

        return ['status' => 'success'];
    }

    public function postRemoveManageService()
    {
        $service_id    = \Input::has('service_id') ? \Input::get('service_id') : '';

        $service       = ShopService::where('id', $service_id)->first();

        if(empty($service)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];


        $service->delete();

        return ['status' => 'success'];
    }



    public function ajaxCenter()
    {
        $method         = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'getVaccineList':

                $id = \Input::has('id') ? \Input::get('id') : '';

                $getVaccine = Shop::where('id_shop', $id)->first();

                if(!empty($getVaccine)){
                    return ['status' => 'success', 'data' => json_decode($getVaccine->vaccineArr)];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'getAmphur':

                $province_id = \Input::has('province_id') ? \Input::get('province_id') : '';

                $amphurs = Amphur::where('PROVINCE_ID', $province_id)->get();
                // sd($amphurs->toArray());

                if(!empty($amphurs)){
                    return ['status' => 'success', 'data' => $amphurs];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'getDistrict':

                $amphur_id = \Input::has('amphur_id') ? \Input::get('amphur_id') : '';

                $Districts = District::where('AMPHUR_ID', $amphur_id)->get();
                // sd($District->toArray());

                if(!empty($Districts)){
                    return ['status' => 'success', 'data' => $Districts];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'addFilterToSession':

                $filters                    = [];
                $filters['search_txt']      = \Input::get('search_txt');
                $filters['type_shopfil']    = \Input::get('type_shopfil');
                $filters['status_openfill'] = \Input::get('status_openfill');


                // d(\Input::get('search_txt'));
                // d(\Input::get('status_openfill'));
                // sd(\Input::get('type_shopfil'));

                \Session::put('shop_filters', $filters);

                return ['status' => 'success'];
                break;

            case 'clearFilterToSession':

                \Session::forget('shop_filters');
                
                return ['status' => 'success'];
                break;
            

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
