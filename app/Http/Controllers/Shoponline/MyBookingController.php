<?php

namespace App\Http\Controllers\Shoponline;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Shop\Shop;
use App\Services\Partner\Partner;
use App\Services\Amphur\Amphur;
use App\Services\District\District;
use App\Services\Province\Province;
use App\Services\Zipcode\Zipcode;
use App\Services\Description\Description;
use App\Services\PetService\PetService;
use App\Services\ShopService\ShopService;
use App\Services\Promotion\Promotion;
use App\Services\PetsShop\PetsShop;
use App\Services\Vaccinrecord\Vaccinrecord;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;


class MyBookingController extends Controller
{
    public function getIndex(Request $request, $id)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getPets = Description::where('id_owner', $user_id)->get();
        
        $shops = Shop::where('status_open', '!=', 0)->get();

        $shop = Shop::where('id_shop', $id)->where('status_open', '!=', 0)->first();

        $getPromotion = Promotion::with(['shop'])->where('shop_id', $id)->where('status', 'Active')->first();

        if(!empty($getPromotion)){
            if($getPromotion->enddate == date('Y-m-d')){
                $getPromotion->status = 'Inactive';
                $getPromotion->save();
            }
        }

        // sd($getPromotion);

        $courses = Course::with(['shop','user'])->where('shop_id', $id)->first();
        // d($shop->vaccine);
        // d(json_decode($shop->vaccine));
        // sd(json_decode($shop->vaccine, true));

    	return $this->view_online('shoponline.booking',compact('shop', 'shops', 'getPets', 'getPromotion', 'courses'));
    }


    public function getVaccineBooking(Request $request, $id, $petid, $data)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $vaccine_id     = $id;

        $getPets = Description::where('id_owner', $user_id)->get();
        
        $shops = Shop::where('status_open', '!=', 0)->get();

        $shop = Shop::where('id_shop', $data)->where('status_open', '!=', 0)->first();

        $getPromotion = Promotion::with(['shop'])->where('shop_id', $data)->where('status', 'Active')->first();


        // d($shop->vaccine);
        // d(json_decode($shop->vaccine));
        // sd(json_decode($shop->vaccine, true));
        // d($petid);
        // d($data);
        // sd($vaccine_id);

        return $this->view_online('shoponline.vaccinebooking',compact('shop', 'shops', 'getPets', 'getPromotion', 'vaccine_id', 'petid'));
    }


    public function getCourse(Request $request, $id)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $shop_id     = $id;

        $shop = Shop::where('id_shop', $shop_id);
        $shop = $shop->first();

        $courses = Course::with(['shop','user']);
        $courses = $courses->where('status', 'Active');
        $courses = $courses->where('shop_id', $shop_id);
        $courses = $courses->get();

        $getPets = Description::where('id_owner', $user_id)->get();

        return $this->view_online('shoponline.course',compact('courses', 'shop', 'getPets'));
    }


    public function getPostponeADate(Request $request, $id, $petid, $shopid, $bookingid)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $serviceID = $id;

        $getPets = Description::where('id_owner', $user_id)->get();
        
        $shops = Shop::where('status_open', '!=', 0)->get();

        $shop = Shop::where('id_shop', $shopid)->where('status_open', '!=', 0)->first();

        $getPromotion = Promotion::with(['shop'])->where('shop_id', $shopid)->where('status', 'Active')->first();

        $getBookingDetail = PetService::with(['user','shop','pet', 'vaccineRC']);
        $getBookingDetail = $getBookingDetail->where('id_login', $user_id);
        $getBookingDetail = $getBookingDetail->where('id_petservice', $bookingid)->first();

        $getTimes = ShopService::where('shop_id', $shopid)->get();

        // d($shop->vaccine);
        // d(json_decode($shop->vaccine));
        // sd(json_decode($shop->vaccine, true));
        // d($petid);
        // d($data);
        // sd($getBookingDetail);

        return $this->view_online('shoponline.postponeAdate',compact('shop', 'shops', 'getPets', 'getPromotion', 'petid', 'shopid', 'serviceID', 'getBookingDetail', 'getTimes'));
    }

    public function getVaccineShop(Request $request, $id, $petid)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $vaccine_id     = $id;

        $shops = Shop::where('status_open', 1);
        $shops = $shops->where(function($shops){
                        $shops->Where('name_shop', 'like', '%วัคซีน%');
                        $shops->orWhere('type_shop', 'like', '%วัคซีน%');
                        $shops->orWhere('address_shop', 'like', '%วัคซีน%');
                        $shops->orWhere('shop_description', 'like', '%วัคซีน%');
                    });
        $shops = $shops->get();



        // d($shop->vaccine);
        // d(json_decode($shop->vaccine));
        // sd(json_decode($shop->vaccine, true));

        return $this->view_online('shoponline.vaccineshop',compact('shops', 'vaccine_id', 'petid'));
    }

    public function getBookingHistory()
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        // $getPets = Description::where('id_owner', $user_id)->get();

        $getServices = PetService::with(['user','shop','pet']);
        $getServices = $getServices->where('id_login', $user_id)->orderBy('created_at', 'desc')->get();


        // sd($getServices->toArray());

        return $this->view_online('shoponline.bookinghistory',compact('getServices'));
    }


    public function getBookingDetail(Request $request, $id)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getService = PetService::with(['user','shop','pet']);
        $getService = $getService->where('id_login', $user_id);
        $getService = $getService->where('id_petservice', $id)->first();

        $getshopservices = ShopService::where('shop_id', $getService->id_shop);
        $getshopservices = $getshopservices->where('service', $getService->id_service);
        $getshopservices = $getshopservices->where('time_period', $getService->booking_time);
        $getshopservices = $getshopservices->first();


        $price = '-';

        if(!empty($getService->promo_code) && $getService->ispromotion == true){
            $getpromotion = Promotion::where('shop_id', $getService->id_shop)->where('code', 'like', $getService->promo_code)->first();
            if(!empty($getpromotion)){

                $percent = str_replace("%","",$getpromotion->discount);

                $discount_value = ($getService->shop->deposit / 100) * $percent;

                $realprice = $getService->shop->deposit-$discount_value;

                $price = $realprice.' บาท (ท่านได้รับรับส่วนลด '.$getpromotion->discount.' จากราคามัดจำ)';
            }else{
                $price = $getService->shop->deposit.' บาท';
            }
        }else{
            $price = $getService->shop->deposit.' บาท';
        }

        // $price = '100 บาท';
        // if(!empty($getService->promo_code)){
        //     $price = '50 บาท (ท่านได้รับรับส่วนลด 50%)';
        // }else{
        //     $price = '100 บาท';
        // }

        // sd($getshopservices->toArray());

        return $this->view_online('shoponline.bookingdetail',compact('getService','price','getshopservices'));
    }


    public function getBookingCourseDetail(Request $request, $id)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getService = PetService::with(['user','shop','pet','course']);
        $getService = $getService->where('id_login', $user_id);
        $getService = $getService->where('id_petservice', $id)->first();

        $getshopservices = ShopService::where('shop_id', $getService->id_shop);
        $getshopservices = $getshopservices->where('service', $getService->id_service);
        $getshopservices = $getshopservices->where('time_period', $getService->booking_time);
        $getshopservices = $getshopservices->first();


        $price = '-';
        if($getService->iscourse == true){
            $courses = Course::with(['shop','user'])->where('id', $getService->courseid)->first();

            if(!empty($getService->promo_code) && $getService->ispromotion == true){
                $getpromotion = Promotion::where('shop_id', $getService->id_shop)->where('code', 'like', $getService->promo_code)->first();
                
                if(!empty($getpromotion)){

                    $percent = str_replace("%","",$getpromotion->discount);

                    $discount_value = ($courses->price / 100) * $percent;

                    $realprice = $courses->price-$discount_value;

                    $price = $realprice.' บาท (ท่านได้รับรับส่วนลด '.$getpromotion->discount.' จากราคาคอร์ส)';
                }else{
                    $price = $courses->price.' บาท';
                }
            }else{
                $price = $courses->price.' บาท';
            }
        }

        // sd($getService->toArray());

        return $this->view_online('shoponline.bookingcoursedetail',compact('getService','price','getshopservices'));
    }

    public function postAdd()
    {
        $id_shop          = \Input::has('id_shop') ? \Input::get('id_shop') : '';
        $booking_date     = \Input::get('booking_date');
        $promo_code       = \Input::has('promo_code') ? \Input::get('promo_code') : '';
        $id_service       = \Input::has('id_service') ? \Input::get('id_service') : '';
        $booking_time     = \Input::get('booking_time');
        $pet_id           = \Input::has('pet_id') ? \Input::get('pet_id') : '';
        $note             = \Input::has('note') ? \Input::get('note') : '';
        $vaccine_val      = \Input::has('vaccine_val') ? \Input::get('vaccine_val') : '';
        $vaccine_id       = \Input::has('vaccine_id') ? \Input::get('vaccine_id') : '';
        
        $courseID       = \Input::has('courseID') ? \Input::get('courseID') : '';
        // $promo_code     = \Input::has('promo_code') ? \Input::get('promo_code') : '';
        $promostatus    = \Input::has('promostatus') ? \Input::get('promostatus') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;
        
        // sd($courseID);

        $getshop_ownerID = Shop::where('id_shop', $id_shop)->first();

        $get_pets_shop = PetsShop::where('pet_id', $pet_id);
        $get_pets_shop = $get_pets_shop->where('shop_id', $id_shop);
        $get_pets_shop = $get_pets_shop->where('user_id', $getshop_ownerID->owner_id);
        $get_pets_shop = $get_pets_shop->first();


        $getShopUsed = ShopUsed::where('shop_id', $id_shop)->where('user_id', $user_id)->first();

        if(empty($getShopUsed)){
            $newShopUsed            = new ShopUsed;
            $newShopUsed->shop_id   = $id_shop;
            $newShopUsed->user_id   = $user_id;
            $newShopUsed->save();
        }

        if($promostatus == 'error') return ['status' => 'error', 'msg' => 'โค้ดส่วนลดไม่ถูกต้อง'];

        // d($id_shop);
        // d($user_id);
        // d($get_pets_shop);

            if(empty($get_pets_shop)){
                $new_pets_shop              = new PetsShop;
                $new_pets_shop->shop_id     = $id_shop;
                $new_pets_shop->pet_id      = $pet_id;
                $new_pets_shop->user_id     = $getshop_ownerID->owner_id;
                $new_pets_shop->save();
            }

        if($courseID == ''){
            if($vaccine_val != ''){
                $new_vaccinrecord                   = new Vaccinrecord;
                $new_vaccinrecord->vaccin           = $vaccine_val;
                $new_vaccinrecord->vaccine_ID       = $vaccine_id;
                $new_vaccinrecord->veterinary_ID    = null;
                $new_vaccinrecord->pet_id           = $pet_id;
                $new_vaccinrecord->shop_id          = $id_shop;
                $new_vaccinrecord->namevaccin       = null;
                $new_vaccinrecord->veterinarian     = null;
                $new_vaccinrecord->notevaccin       = null;
                $new_vaccinrecord->img              = null;
                $new_vaccinrecord->save();
            }
        }

        $courses = Course::where('id', $courseID)->first();
        
        // d($new_vaccinrecord->id_vaccin);

        // d('$id_shop : '.$id_shop);
        // d('$booking_date : '.$booking_date);
        // d('$id_service : '.$id_service);
        // d('$booking_time : '.$booking_time);
        // d('$pet_id : '.$pet_id);
        // d('$note : '.$note);
        // d('$vaccine_val : '.$vaccine_val);
        // d('$vaccine_id : '.$vaccine_id);
        // d('$promo_code : '.$promo_code);
        // d('$promostatus : '.$promostatus);
        // sd('$courseID : '.$courseID);

        $new_service                 = new PetService;
        $new_service->id_shop        = $id_shop;
        $new_service->note           = $note;
        $new_service->id_login       = $user_id;
        $new_service->pet_id         = $pet_id;
        if($courseID == ''){
            $new_service->vaccine_ID     = $vaccine_id;
            $new_service->pets_shopID    = empty($get_pets_shop) ? $new_pets_shop->id : $get_pets_shop->id;
        }
        $new_service->id_service     = $id_service;
        $new_service->booking_time   = $booking_time;
        $new_service->booking_date   = $booking_date;
        if($promo_code != ''){
            $new_service->promo_code     = $promo_code;
            $new_service->ispromotion    = true;
        }
        if($courseID !== ''){
            $new_service->courseid     = $courseID;
            $new_service->iscourse     = true;
        }
        if($vaccine_val != ''){
            $new_service->vaccine         = $vaccine_val;
            $new_service->vaccineRC_id    = $new_vaccinrecord->id_vaccin;
        }
        $new_service->status                = 'รอชำระเงิน';
        $new_service->appoint               = false;
        $new_service->postponeAdate         = false;
        $new_service->postponeAdate_status  = null;
        $new_service->save();



        if($courseID != ''){
            if(!empty($courses)){
                for ($i=0; $i < $courses->numoftimes; $i++) { 

                    $newcourseRC            = new CourseRecord;
                    $newcourseRC->course_id = $courseID;
                    if($i == 0){
                        $newcourseRC->startdate = $booking_date;
                    }else{
                        $newcourseRC->startdate = null;
                    }
                    $newcourseRC->id_petservice   = $new_service->id_petservice;
                    $newcourseRC->user_id   = $user_id;
                    $newcourseRC->shop_id   = $id_shop;
                    $newcourseRC->pet_id    = $pet_id;
                    $newcourseRC->thetime   = $i+1;
                    $newcourseRC->isuse     = false;
                    $newcourseRC->save();   
                }
            }
        }


        return ['status' => 'success', 'serviceid' => $new_service->id_petservice];
    }


    public function postEdit()
    {
        $bookID           = \Input::has('bookID') ? \Input::get('bookID') : '';
        $id_shop          = \Input::has('id_shop') ? \Input::get('id_shop') : '';
        $booking_date     = \Input::has('booking_date') ? \Input::get('booking_date') : '';
        $promo_code       = \Input::has('promo_code') ? \Input::get('promo_code') : '';
        $id_service       = \Input::has('id_service') ? \Input::get('id_service') : '';
        $booking_time     = \Input::has('booking_time') ? \Input::get('booking_time') : '';
        $pet_id           = \Input::has('pet_id') ? \Input::get('pet_id') : '';
        $note             = \Input::has('note') ? \Input::get('note') : '';
        $vaccine_val      = \Input::has('vaccine_val') ? \Input::get('vaccine_val') : '';
        $vaccine_id       = \Input::has('vaccine_id') ? \Input::get('vaccine_id') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;
        
        $getService = PetService::where('id_login', $user_id);
        $getService = $getService->where('id_petservice', $bookID)->first();
        if(empty($getService)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        if($vaccine_val != ''){
            $EDvaccinrecord = Vaccinrecord::where('id_vaccin', $getService->vaccineRC_id)->first();
            if(empty($EDvaccinrecord)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];


            $EDvaccinrecord->vaccin           = $vaccine_val;
            $EDvaccinrecord->vaccine_ID       = $vaccine_id;
            $EDvaccinrecord->pet_id           = $pet_id;
            $EDvaccinrecord->save();
        }

        $getService->note           = $note;
        $getService->vaccine_ID     = $vaccine_id;
        $getService->id_login       = $user_id;
        $getService->booking_date   = $booking_date;
        $getService->promo_code     = $promo_code;
        $getService->id_service     = $id_service;
        $getService->booking_time   = $booking_time;
        $getService->pet_id         = $pet_id;
        if($vaccine_val != ''){
            $getService->vaccine    = $vaccine_val;
        }
        $getService->appoint        = false;
        $getService->postponeAdate = true;
        $getService->postponeAdate_status = 'รอตรวจสอบ';
        $getService->save();

        return ['status' => 'success', 'serviceid' => $bookID];
    }

    public function postSavePaySlip(){
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $date_payment   = \Input::has('date_payment') ? \Input::get('date_payment') : '';
        $time_payment   = \Input::has('time_payment') ? \Input::get('time_payment') : '';
        $bank_payment   = \Input::has('bank_payment') ? \Input::get('bank_payment') : '';
        $pic_payment    = \Input::hasFile('pic_payment') ? \Input::file('pic_payment') : '';

        $service    = PetService::where('id_petservice', $id)->first();
        if(empty($service)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        // d($date_payment);
        // sd($time_payment);

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/paySlipImg")){
            $oldmask = umask(0);
            mkdir($path."/public/paySlipImg", 0777);
            umask($oldmask);
        }

        if(!empty($pic_payment))
        {
            $image_name = "/public/paySlipImg/".$date."-".$pic_payment->getClientOriginalName();
            copy($pic_payment, $path.$image_name);
        }
        
        if(!empty($image_name)){
            $service->pic_payment = $image_name;
        }
        
        $service->status        = 'รอการตรวจสอบยอดเงิน';
        $service->date_payment  = $date_payment;
        $service->time_payment  = $time_payment;
        $service->bank_payment  = $bank_payment;
        $service->save();

        
        return ['status' => 'success'];
    }


    public function ajaxCenter()
    {
        $method         = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'checkBooing':

                $id_shop         = \Input::has('id_shop') ? \Input::get('id_shop') : '';
                $booking_date    = \Input::has('booking_date') ? \Input::get('booking_date') : '';
                $id_service      = \Input::has('id_service') ? \Input::get('id_service') : '';
                $booking_time    = \Input::has('booking_time') ? \Input::get('booking_time') : '';

                // sd($booking_date);



                $getAll_orders = PetService::where('id_shop', $id_shop)
                                    ->where('id_service', $id_service)
                                    ->where('booking_time', $booking_time)
                                    ->whereDate('booking_date', $booking_date)
                                    ->where('status', 'ชำระเงินแล้ว')
                                    ->get();

                $getservices = ShopService::where('shop_id', $id_shop)
                                    ->where('service', $id_service)
                                    ->where('time_period', $booking_time)
                                    ->first();

                // d($id_shop);
                // d($booking_date);
                // d($id_service);
                // d($booking_time);
                // d($getAll_orders->toArray());
                // sd($getservices);

                if($getAll_orders->count() != 0){
                    if(!empty($getservices)){
                        $allque     = $getservices->que;
                        $allowque   = $getservices->que - $getAll_orders->count();

                        if($getservices->que > $getAll_orders->count()){
                            $bookingallow = true;
                            $msg = "สามารถทำรายการจองได้";
                        }else{
                            $bookingallow = false;
                            $msg = "คิวเต็ม ไม่สามารถทำรายการได้";
                        }
                    }else{
                        $allque     = 0;
                        $allowque   = 0;
                        $bookingallow = false;
                        $msg = "ไม่สามารถทำรายการได้ เนื่องจากทางร้านยังไม่มีบริการนี้";
                    }
                }else{
                    if(!empty($getservices)){
                        $allque     = $getservices->que;
                        $allowque   = $getservices->que;
                        $bookingallow = true;
                        $msg = "สามารถทำรายการจองได้";
                    }else{
                        $allque     = 0;
                        $allowque   = 0;
                        $bookingallow = false;
                        $msg = "ไม่สามารถทำรายการได้ เนื่องจากทางร้านยังไม่มีบริการนี้";

                    }
                }
                
                return ['status' => 'success', 'allque' => $allque, 'allowque' => $allowque, 'bookingallow' => $bookingallow, 'msg' => $msg];

                break;

            case 'getVaccine':

                $id_shop        = \Input::has('id_shop') ? \Input::get('id_shop') : '';
                $id_service     = \Input::has('id_service') ? \Input::get('id_service') : '';

                $shop = Shop::where('id_shop', $id_shop)->where('status_open', '!=', 0)->first();
                
                // if(!empty($shop)) return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                // sd($District->toArray());

                $getservices = ShopService::where('shop_id', $id_shop)->where('service', $id_service)->get();

                // if(!empty($shop)){
                return ['status' => 'success', 'data' => json_decode($shop->vaccine), 'services' => $getservices];
                // }else{
                //     return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                // }

                
                break;


            case 'postAddBookingTime':

                $id_petservice    = \Input::has('id_petservice') ? \Input::get('id_petservice') : '';
                $booking_time     = \Input::has('booking_time') ? \Input::get('booking_time') : '';
                $booking_date     = \Input::has('booking_date') ? \Input::get('booking_date') : '';

                // sd($booking_date);

                $getPetService = PetService::where('id_petservice', $id_petservice)->first();

                if(empty($getPetService)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                if(!empty($booking_date)){
                    $getPetService->booking_date = $booking_date;
                }
                $getPetService->booking_time = $booking_time;
                $getPetService->save();
                
                
                return ['status' => 'success'];
                
                break;

            case 'checkPormoCode':

                $promo_code = \Input::has('promo_code') ? \Input::get('promo_code') : '';
                $courseID   = \Input::has('courseID') ? \Input::get('courseID') : '';
                $shopID     = \Input::has('shopID') ? \Input::get('shopID') : '';

                // d($promo_code);
                // d($shopID);
                // sd($courseID);

                $getpromotion = Promotion::where('shop_id', $shopID)->where('code', 'like', $promo_code)->first();
                if(empty($getpromotion)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $courses = Course::with(['shop','user']);
                $courses = $courses->where('status', 'Active');
                $courses = $courses->where('id', $courseID);
                $courses = $courses->first();
                if(empty($courses)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
                
                $txtalert = '';

                // if($courses->isUsePromotion == true){
                    if(!empty($getpromotion)){
                        $txtalert = 'โค้ดส่วนลดนี้สามารถใช้ได้';
                    }
                // }else{
                //     $txtalert = 'คอร์สนี้ไม่สามารถใช้โค้ดส่วนลดได้';
                // }

                
                return ['status' => 'success', 'txtalert' => $txtalert];
                
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
