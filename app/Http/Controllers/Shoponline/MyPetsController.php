<?php

namespace App\Http\Controllers\Shoponline;

use DB;
use PDF;
use QrCode;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Shop\Shop;
use App\Services\Partner\Partner;
use App\Services\Amphur\Amphur;
use App\Services\District\District;
use App\Services\Province\Province;
use App\Services\Zipcode\Zipcode;
use App\Services\Description\Description;
use App\Services\PetService\PetService;
use App\Services\ShopService\ShopService;
use App\Services\Veterinary\Veterinary;
use App\Services\Vaccinrecord\Vaccinrecord;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;



class MyPetsController extends Controller
{
    public $getID;

    public function getIndex()
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getPets = Description::where('id_owner', $user_id)->get();

        $species   = Species::where('status', 'Active')->orderBy('name', 'asc')->get();

        $pettypes  = PetType::where('status', 'Active')->orderBy('name', 'asc')->get();

        // sd($getPets->toArray());
    
        
    	return $this->view_online('shoponline.mypets', compact('getPets', 'pettypes'));
    }

    public function getPetDetail(Request $request, $id)
    {
        
        $species   = Species::where('status', 'Active')->orderBy('name', 'asc')->get();

        $pettypes  = PetType::where('status', 'Active')->orderBy('name', 'asc')->get();


        $getPet = Description::where('pet_id', $id)->first();

        $getHistories = PetService::with(['user','shop','pet']);
        $getHistories = $getHistories->where('pet_id', $id);
        $getHistories = $getHistories->where('appoint',true)->get();


        // $getVaccineReccord = Vaccinrecord::with(['vaccine', 'veterinary', 'shop', 'pet'])->where('pet_id',$id)->get();
        $getVaccineReccord = DB::table('vaccinrecord')->where('pet_id',$id)->get();

        // $getVaccineAll = Vaccine::where('vaccine_status','Active')->get();
        $getVaccineAll = DB::table('vaccine')->where('vaccine_status','Active')->get();


        $vacccineRC_arr = array();
        foreach ($getVaccineAll as $key => $vall) {
            $vaccineArr = array(
                "vaccine_id"        => $vall->id,
                "vaccine_name"      => $vall->vaccine_name,
                "drug_name"         => $vall->drug_name,
                "properties"        => $vall->properties,
                "vaccine_type"      => $vall->vaccine_type,
                "vaccine_usage"     => $vall->vaccine_usage,
                "description"       => $vall->description,
                "img_vaccine"       => $vall->img_vaccine,
                "company"           => $vall->company,
                "numoftimes"        => $vall->numoftimes,
                "timeperiod"        => $vall->timeperiod,
                "vRC_id"            => '',
                "vRC_notevaccin"    => '',
                "veterinarian"      => '',
                "vRC_created_at"    => '',
                "vRC_img"           => '',
            );

            array_push($vacccineRC_arr, $vaccineArr);
        }
       
        foreach ($getVaccineReccord as $key => $vrc) {
            foreach ($vacccineRC_arr as $keys => $vallArr) {
                if($vallArr['vaccine_id'] == $vrc->vaccine_ID){
                    $vacccineRC_arr[$keys]['vRC_id']                = $vrc->id_vaccin ;
                    $vacccineRC_arr[$keys]['vRC_notevaccin']        = $vrc->notevaccin;
                    $vacccineRC_arr[$keys]['veterinarian']          = $vrc->veterinarian;
                    $vacccineRC_arr[$keys]['vRC_created_at']        = date($vrc->created_at);
                    $vacccineRC_arr[$keys]['vRC_img']               = $vrc->img;
                    $vacccineRC_arr[$keys]['vRC_numoftimes']        = $vrc->numoftimes;
                    $vacccineRC_arr[$keys]['vRC_timeperiod']        = $vrc->timeperiod;
                    $vacccineRC_arr[$keys]['vRC_startdate']         = $vrc->startdate;
                    $vacccineRC_arr[$keys]['vRC_anewdate']          = $vrc->anewdate;

                }

            }
        }


        $getcoursedata = CourseRecord::with(['user', 'shop', 'pet', 'course']);
        // $getcoursedata = $getcoursedata->where('course_id', $getdetail->courseid);
        // $getcoursedata = $getcoursedata->where('user_id', $getdetail->id_login);
        // $getcoursedata = $getcoursedata->where('shop_id', $getdetail->id_shop);
        $getcoursedata = $getcoursedata->where('pet_id', $id);
        $getcoursedata = $getcoursedata->get();


        $getcourse = CourseRecord::with(['user', 'shop', 'pet', 'course', 'service']);
        $getcourse = $getcourse->where('startdate', null);
        $getcourse = $getcourse->where('pet_id', $id);
        $getcourse = $getcourse->first();


        // sd($getcoursedata->toArray());
        if(!empty($getcoursedata)){
            foreach ($getcoursedata as $key => $item) {
                if(!empty($item->startdate) && $item->isuse == false){
                    // $txt_startdate = empty($item->startdate) ? '' : $item->startdate;

                    $expire = strtotime($item->startdate);
                    $today  = strtotime("today midnight");
                    // $diffexpired_date = date_diff(date_create($txt_enddate),date_create(date('Y-m-d')));
                    if($today >= $expire){
                        $item->isuse = true;
                        $item->save();
                    }
                }
            }
        }

        // d($vacccineRC_arr);
        // sd($getcoursedata->toArray());


        return $this->view_online('shoponline.mypetsdetail',compact('getPet', 'getHistories', 'vacccineRC_arr', 'getVaccineAll', 'pettypes', 'getcoursedata','getcourse'));
    }

    public function postAdd()
    {
        $pet_name           = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $pet_txt_type       = \Input::has('pet_txt_type') ? \Input::get('pet_txt_type') : '';
        $pet_type           = \Input::has('pet_type') ? \Input::get('pet_type') : '';
        $breed              = \Input::has('breed') ? \Input::get('breed') : '';
        $color              = \Input::has('color') ? \Input::get('color') : '';
        $weight             = \Input::has('weight') ? \Input::get('weight') : '';
        $notedescription    = \Input::has('notedescription') ? \Input::get('notedescription') : '';
        $birthday           = \Input::has('birthday') ? \Input::get('birthday') : '';
        $gender             = \Input::has('gender') ? \Input::get('gender') : '';
        $pets_img           = \Input::hasFile('pic') ? \Input::file('pic') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        // d($birthday);
        // d($birthday);
        // d($birthday);
        // sd($birthday);

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/petImg")){
         $oldmask = umask(0);
         mkdir($path."/public/petImg", 0777);
         umask($oldmask);
        }

        if(!empty($pets_img))
        {
         $image_name = "/public/petImg/".$date."-".$pets_img->getClientOriginalName();
         copy($pets_img, $path.$image_name);
        }


        $new_pet                   = new Description;
        $new_pet->name             = $pet_name;
        $new_pet->id_owner         = $user_id;
        $new_pet->pet_txt_type     = $pet_txt_type;
        $new_pet->pic              = (empty($image_name)) ? "" : $image_name;
        $new_pet->type             = $pet_type;
        $new_pet->breed            = $breed;
        $new_pet->color            = $color;
        $new_pet->weight           = $weight;
        $new_pet->notedescription  = $notedescription;
        $new_pet->birthday         = $birthday;
        $new_pet->gender           = $gender;
        $new_pet->qrcode           = null;
        $new_pet->save();


        $pet = Description::where('pet_id', $new_pet->pet_id)->first();
        if(empty($pet)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $time = time();

        // ตรวจสอบและสร้าง folder สำหรับเก็บ QR Code
        $path = base_path();
        if(!file_exists($path."/public/QrCodeForPet")){
         $oldmask = umask(0);
         mkdir($path."/public/QrCodeForPet", 0777);
         umask($oldmask);
        }

        $path_name = $path."/public/QrCodeForPet/".$new_pet->pet_id."_".$date."_".$time."_qrcode.svg";
        $path_name_save = "/public/QrCodeForPet/".$new_pet->pet_id."_".$date."_".$time."_qrcode.svg";

        // +++EDIT PATH++++
        $urlPathQR = "https://www.petscarebusiness.com/mypet/".$new_pet->pet_id;

        $qrcode = QrCode::size(250)->backgroundColor(103,179,160)->generate($urlPathQR , $path_name);

        // $image = QrCode::format('png')->merge('http://127.0.0.1:8001/themes/image/logo.png', 0.3, true)
        //         ->size(200)->errorCorrection('H')
        //         ->generate($urlPathQR , $path_name);

        $pet->qrcode = $path_name_save;
        $pet->save();


        return ['status' => 'success'];
    }


    public function postAddSP()
    {
        $pet_txt_type       = \Input::has('pet_txt_type') ? \Input::get('pet_txt_type') : '';
        $pet_type           = \Input::has('pet_type') ? \Input::get('pet_type') : '';
        $breed              = \Input::has('breed') ? \Input::get('breed') : '';
        $info               = \Input::has('info') ? \Input::get('info') : '';
        $gender             = \Input::has('gender') ? \Input::get('gender') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $infodetail = json_decode($info, true);

        // d(count($infodetail));
        // sd(json_decode($info, true));

        if(!empty($infodetail)){
            for ($i=0; $i < count($infodetail); $i++) { 

                $new_pet                   = new Description;
                $new_pet->name             = $infodetail[$i]['name'];
                $new_pet->id_owner         = $user_id;
                $new_pet->pet_txt_type     = $pet_txt_type;
                $new_pet->pic              = null;
                $new_pet->type             = $pet_type;
                $new_pet->breed            = $breed;
                $new_pet->color            = $infodetail[$i]['color'];
                $new_pet->gender           = $gender;
                $new_pet->qrcode           = null;
                $new_pet->save();


                $pet = Description::where('pet_id', $new_pet->pet_id)->first();
                if(empty($pet)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $time = time();
                $date = date('Y-m-d');

                // ตรวจสอบและสร้าง folder สำหรับเก็บ QR Code
                $path = base_path();
                if(!file_exists($path."/public/QrCodeForPet")){
                 $oldmask = umask(0);
                 mkdir($path."/public/QrCodeForPet", 0777);
                 umask($oldmask);
                }

                $path_name = $path."/public/QrCodeForPet/".$new_pet->pet_id."_".$date."_".$time."_qrcode.svg";
                $path_name_save = "/public/QrCodeForPet/".$new_pet->pet_id."_".$date."_".$time."_qrcode.svg";

                // +++EDIT PATH++++
                $urlPathQR = "https://www.petscarebusiness.com/mypet/".$new_pet->pet_id;

                $qrcode = QrCode::size(250)->backgroundColor(103,179,160)->generate($urlPathQR , $path_name);

                // $image = QrCode::format('png')->merge('http://127.0.0.1:8001/themes/image/logo.png', 0.3, true)
                //         ->size(200)->errorCorrection('H')
                //         ->generate($urlPathQR , $path_name);

                $pet->qrcode = $path_name_save;
                $pet->save();
            }

        }



        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $pet_id             = \Input::has('pet_id') ? \Input::get('pet_id') : '';
        $pet_name           = \Input::has('pet_name') ? \Input::get('pet_name') : '';
        $pet_type           = \Input::has('pet_type') ? \Input::get('pet_type') : '';
        $breed              = \Input::has('breed') ? \Input::get('breed') : '';
        $color              = \Input::has('color') ? \Input::get('color') : '';
        $weight             = \Input::has('weight') ? \Input::get('weight') : '';
        $notedescription    = \Input::has('notedescription') ? \Input::get('notedescription') : '';
        $birthday           = \Input::has('birthday') ? \Input::get('birthday') : '';
        $gender             = \Input::has('gender') ? \Input::get('gender') : '';
        $pets_img           = \Input::hasFile('pic') ? \Input::file('pic') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        // d(date($birthday, 'Y-m-d'));
        // d(date('Y-m-d',strtotime($birthday)));
        // sd($birthday);
        $pet       = Description::where('pet_id', $pet_id)->first();
        if(empty($pet)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];


        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/petImg")){
         $oldmask = umask(0);
         mkdir($path."/public/petImg", 0777);
         umask($oldmask);
        }

        if(!empty($pets_img))
        {
         $image_name = "/public/petImg/".$date."-".$pets_img->getClientOriginalName();
         copy($pets_img, $path.$image_name);
        }


        $pet->name             = $pet_name;
        $pet->id_owner         = $user_id;
        if(!empty($image_name)){
            $pet->pic           = $image_name;
        }
        $pet->type             = $pet_type;
        $pet->breed            = $breed;
        $pet->color            = $color;
        $pet->weight           = $weight;
        $pet->notedescription  = $notedescription;
        $pet->birthday         = $birthday;
        $pet->gender           = $gender;
        $pet->save();
       
        return ['status' => 'success'];
    }



    public function postRemove()
    {
        $pet_id    = \Input::has('pet_id') ? \Input::get('pet_id') : '';
        $pet       = Description::where('pet_id', $pet_id)->first();

        if(empty($pet)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $path = base_path();
        if(!empty($pet->pic)){
            if(file_exists($path.$pet->pic)){
                unlink($path.$pet->pic);
            }
        }

        $pet->delete();
        
        return ['status' => 'success'];
    }


    public function getGenerateQRForPet(Request $request, $id){

        $getPet = Description::with(['user'])->where('pet_id', $id)->first();

        $getHistories = PetService::with(['user','shop','pet']);
        $getHistories = $getHistories->where('pet_id', $id)->get();

        $getVaccineReccord = Vaccinrecord::with(['vaccine', 'veterinary', 'shop', 'pet'])->where('pet_id',$id)->get();

        $getVaccineAll = Vaccine::where('vaccine_status','Active')->get();


        $vacccineRC_arr = array();
        foreach ($getVaccineAll as $key => $vall) {
            $vaccineArr = array(
                "vaccine_id"        => $vall->id,
                "vaccine_name"      => $vall->vaccine_name,
                "drug_name"         => $vall->drug_name,
                "properties"        => $vall->properties,
                "vaccine_type"      => $vall->vaccine_type,
                "vaccine_usage"     => $vall->vaccine_usage,
                "description"       => $vall->description,
                "img_vaccine"       => $vall->img_vaccine,
                "company"           => $vall->company,
                "vRC_id"            => '',
                "vRC_notevaccin"    => '',
                "veterinarian"      => '',
                "vRC_created_at"    => '',
                "vRC_img"           => '',
            );

            array_push($vacccineRC_arr, $vaccineArr);
        }
       
        foreach ($getVaccineReccord as $key => $vrc) {
            foreach ($vacccineRC_arr as $keys => $vallArr) {
                if($vallArr['vaccine_id'] == $vrc->vaccine_ID){
                    $vacccineRC_arr[$keys]['vRC_id']            = $vrc->id_vaccin ;
                    $vacccineRC_arr[$keys]['vRC_notevaccin']    = $vrc->notevaccin;
                    $vacccineRC_arr[$keys]['veterinarian']      = $vrc->veterinarian;
                    $vacccineRC_arr[$keys]['vRC_created_at']    = date($vrc->created_at);
                    $vacccineRC_arr[$keys]['vRC_img']           = $vrc->img;

                }

            }
        }


        // d($getPet->toArray());
        // sd($getHistories->toArray());


        return $this->view_online('mypetQR',compact('getPet','getHistories','vacccineRC_arr','getVaccineAll'));

    }


    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {

            case 'getPetData':

                $pet_id = \Input::has('pet_id') ? \Input::get('pet_id') : '';

                $pet       = Description::where('pet_id', $pet_id)->first();

                if(!empty($pet)){
                    return ['status' => 'success', 'data' => $pet];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'getVaccineRCDetail':

                $vRC_id = \Input::has('vRC_id') ? \Input::get('vRC_id') : '';

                $getVaccineReccord = Vaccinrecord::with(['vaccine', 'veterinary', 'shop', 'pet'])->where('id_vaccin',$vRC_id)->first();
                // $getVaccineReccord = DB::table('vaccinrecord')->with(['vaccine', 'veterinary', 'shop', 'pet'])
                //                         ->where('id_vaccin',$vRC_id)->first();

                if(!empty($getVaccineReccord)){
                    return ['status' => 'success', 'data' => $getVaccineReccord];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;


            case 'getSpeciesByPetType':

                $pettype = \Input::has('pettype') ? \Input::get('pettype') : '';

                $species  = Species::where('pet_type', $pettype)->orderBy('name', 'asc')->get();

                if(!empty($species)){
                    return ['status' => 'success', 'species' => $species];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;


            default:
                return ['status' => 'error'];
                break;
        }
    }
}
