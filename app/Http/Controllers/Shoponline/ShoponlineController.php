<?php

namespace App\Http\Controllers\Shoponline;

use DB;
use PDF;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Amphur\Amphur;
use App\Services\District\District;
use App\Services\Province\Province;
use App\Services\Zipcode\Zipcode;
use App\Services\Shop\Shop;
use App\Services\PetService\PetService;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;



class ShoponlineController extends Controller
{
    public $getID;

    public function getIndex()
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;


        $shops = Shop::where('status_open', '!=', 0)->where('status_premium', 1)->get();

        $date_now = date("Y-m-d");

    	return $this->view_online('shoponline.index',compact('shops'));
    }

    public function getShopNearBy()
    {

        $shops = Shop::where('status_open', '!=', 0)->get();

        return $this->view_online('shoponline.shopnearby',compact('shops'));
    }


    public function getShopNearByGoogle()
    {

        $shops = Shop::where('status_open', '!=', 0)->get();

        return $this->view_online('shoponline.shopnearbygoogle',compact('shops'));
    }


    public function getAboutus()
    {

        $shops = Shop::where('status_open', '!=', 0)->get();

        return $this->view_online('shoponline.aboutus',compact('shops'));
    }

    public function getContactus()
    {

        $shops = Shop::where('status_open', '!=', 0)->get();

        return $this->view_online('shoponline.contactus',compact('shops'));
    }

    public function postContactForm(Request $request)
    {
       $name    = \Input::get('name');
       $email   = \Input::get('email');
       $subject = \Input::get('title');
       $message = \Input::get('textmsg');

        $to_name = 'Pets Care';
        $to_email = 'petscare406@gmail.com';

        $data = [
            'name'      =>  $name ,
            'email'     =>  $email,
            'subject'   =>  $subject,
            'message'   =>   $message
        ];

        // Mail::to('todayhappyjung@gmail.com')->send(new SendMail($data));

        // Mail::send('email.mail_contact', $data, function($message) use ($to_name, $to_email) {
        //     $message->to($to_email, $to_name)
        //             ->subject('ติดต่อสอบถามข้อมูลเพิ่มเติม');
        //     $message->from($to_email,'Artisans Web');
        // });

        Mail::to('petscare406@gmail.com')->send(new SendMail($data));


        return ['status' => 'success'];

        // return response()->json(['status' => 'OK','message' => 'send email success'], 200);
        // return with('success', 'Thanks for contacting us!');
    }

    public function getProfile()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $getUser = User::where('id_owner', $user_id)->first();

        return $this->view_online('shoponline.profile',compact('getUser'));
    }


    public function getPageSearch(Request $request, $data)
    {
        // sd($data);
        $txt_search = empty($data) ? '' : $data;
        // d($txt_search);
        if($txt_search == 'อาบน้ำ'){
            $shops = Shop::where('status_open', 1);
            $shops = $shops->where(function($shops) use ($txt_search){
                            $shops->Where('name_shop', 'like', '%อาบน้ำ%');
                            $shops->orWhere('name_shop', 'like', '%ตัดขน%');
                            $shops->orWhere('type_shop', 'like', '%อาบน้ำ%');
                            $shops->orWhere('type_shop', 'like', '%ตัดขน%');
                            $shops->orWhere('address_shop', 'like', '%ตัดขน%');
                            $shops->orWhere('address_shop', 'like', '%อาบน้ำ%');
                            $shops->orWhere('shop_description', 'like', '%ตัดขน%');
                            $shops->orWhere('shop_description', 'like', '%อาบน้ำ%');
                        });
            $shops = $shops->get();

            $txt_search = 'อาบน้ำ - ตัดขน';
        }else{
            $shops = Shop::where('status_open', 1);
            $shops = $shops->where(function($shops) use ($txt_search){
                            $shops->Where('name_shop', 'like', '%'.$txt_search.'%');
                            $shops->orWhere('type_shop', 'like', '%'.$txt_search.'%');
                            $shops->orWhere('address_shop', 'like', '%'.$txt_search.'%');
                            $shops->orWhere('shop_description', 'like', '%'.$txt_search.'%');
                        });
            $shops = $shops->get();
        }
            
        // sd($shops->toArray());


        return $this->view_online('shoponline.search',compact('shops', 'txt_search'));
        
    }

    public function getPageVaccineDetail()
    {
        $filters = \Session::has('vaccine_filters') ? \Session::get('vaccine_filters') : [];

        // d($filters);
        
        // $getVaccines = Vaccine::where('vaccine_status', 'Active');
        $getVaccines = DB::table('vaccine')->where('vaccine_status', 'Active');

        if(!empty($filters['vaccine_type'])){
            $getVaccines = $getVaccines->where('vaccine_type', $filters['vaccine_type']);
        }

        if(!empty($filters['search_txt'])){
            $keyword = $filters['search_txt'];
            $getVaccines = $getVaccines->where(function($getVaccines) use ($keyword){
                                $getVaccines->where('vaccine_name', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('vaccine_type', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('vaccine_usage', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('description', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('vaccine_status', 'like', '%'.$keyword.'%');
                            });
        }

        $getVaccines = $getVaccines->get();

        // sd($getVaccines->toArray());

        return $this->view_online('shoponline.vaccinedetail', compact('getVaccines','filters'));
    }


    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {

            case 'getShopData':

                $shops = Shop::where('status_open', 1);
                $shops = $shops->get();

                if(!empty($shops)){
                    return ['status' => 'success', 'data' => $shops];
                }else{
                    return ['status' => 'error', 'msg' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'addFilterToSession':

                $filters                    = [];
                $filters['search_txt']      = \Input::get('search_txt');
                $filters['vaccine_type']    = \Input::get('vaccine_type');

                \Session::put('vaccine_filters', $filters);

                return ['status' => 'success'];
                break;

            case 'clearFilterToSession':

                \Session::forget('vaccine_filters');
                
                return ['status' => 'success'];
                break;

            default:
                return ['status' => 'error'];
                break;
        }
    }
}
