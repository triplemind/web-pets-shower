<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Stock\Stock;
use App\Services\Partner\Partner;
use App\Services\Bill\Bill;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;

class UserController extends Controller
{
    public function getIndex()
    {
        // $filters = \Session::has('user_filters') ? \Session::get('user_filters') : [];
        
        $users = User::where('id_owner', '!=', 0)->get();

        // sd($users->toArray());

    	return $this->view('user.index',compact('users'));
    }

    public function postAdd()
    {
        $name_owner     = \Input::has('name_owner') ? \Input::get('name_owner') : '';
        $username       = \Input::has('username') ? \Input::get('username') : '';
        $user_type      = \Input::has('user_type') ? \Input::get('user_type') : '';
        $email          = \Input::has('email') ? \Input::get('email') : '';
        $password       = \Input::has('password') ? \Input::get('password') : '';
        $address       = \Input::has('address') ? \Input::get('address') : '';
        $telephone       = \Input::has('telephone') ? \Input::get('telephone') : '';
        $user_img       = \Input::hasFile('user_img') ? \Input::file('user_img') : '';

        // $menu_accress = json_decode($menu_accress);

        // if(empty($menu_accress)) return ['status' => 'error', 'msg' => 'Do not allow empty menu !!!'];

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/userImg")){
         $oldmask = umask(0);
         mkdir($path."/public/userImg", 0777);
         umask($oldmask);
        }

        if(!empty($user_img))
        {
         $image_name = "/public/userImg/".$date."-".$user_img->getClientOriginalName();
         copy($user_img, $path.$image_name);
        }

        // d($user_img);
        // d($menu_accress);
        // sd($image_name);

        $new_user               = new User;
        $new_user->name_owner   = $name_owner;
        $new_user->user_img     = (empty($image_name)) ? "" : $image_name;
        $new_user->email        = $email;
        $new_user->user_type    = $user_type;
        $new_user->username     = $username;
        $new_user->password     = $password;
        $new_user->telephone    = $telephone;
        $new_user->address      = $address;
        $new_user->save();

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $user_id        = \Input::has('user_id') ? \Input::get('user_id') : '';
        $name_owner     = \Input::has('name_owner') ? \Input::get('name_owner') : '';
        $username       = \Input::has('username') ? \Input::get('username') : '';
        $user_type      = \Input::has('user_type') ? \Input::get('user_type') : '';
        $email          = \Input::has('email') ? \Input::get('email') : '';
        $password       = \Input::has('password') ? \Input::get('password') : '';
        $address        = \Input::has('address') ? \Input::get('address') : '';
        $telephone      = \Input::has('telephone') ? \Input::get('telephone') : '';
        $user_img       = \Input::hasFile('user_img') ? \Input::file('user_img') : '';

        // $menu_accress = json_decode($menu_accress);
        // if(empty($menu_accress)) return ['status' => 'error', 'msg' => 'Do not allow empty menu !!!'];

        $chk_username   = User::where('username', $username)->count();
        if($chk_username > 1) return ['status' => 'error', 'msg' => 'ชื่อผู้ใช้งานนี้มีอยู่ในระบบแล้ว'];

        $chk_email  = User::where('email', $email)->count();
        if($chk_email > 1) return ['status' => 'error', 'msg' => 'อีเมลนี้มีอยู่ในระบบแล้ว'];

        $user       = User::where('id_owner', $user_id)->first();
        if(empty($user)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/userImg")){
         $oldmask = umask(0);
         mkdir($path."/public/userImg", 0777);
         umask($oldmask);
        }

        if(!empty($user_img))
        {
         $image_name = "/public/userImg/".$date."-".$user_img->getClientOriginalName();
         copy($user_img, $path.$image_name);
        }

        // d($user_img);
        // d($menu_accress);
        // sd($image_name);

        $user->name_owner   = $name_owner;
        if(!empty($image_name)){
            $user->user_img     = $image_name;
        }
        $user->email        = $email;
        $user->user_type    = $user_type;
        $user->username     = $username;
        if(!empty($password)){
            $user->password     = md5($password);
        }
        $user->address      = $address;
        $user->telephone    = $telephone;
        $user->save();

        return ['status' => 'success'];
    }



    public function postRemove()
    {
        $user_id    = \Input::has('user_id') ? \Input::get('user_id') : '';
        $user       = User::where('id_owner', $user_id)->first();

        if(empty($user)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $path = base_path();
        if(!empty($user->user_img)){
            if(file_exists($path.$user->user_img)){
                unlink($path.$user->user_img);
            }
        }

        $user->delete();

        return ['status' => 'success'];
    }



    public function ajaxCenter()
    {
        $method         = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'getUserData':

                $user_id = \Input::has('user_id') ? \Input::get('user_id') : '';

                $user = User::where('id_owner', $user_id)->first();

                if(!empty($user)){
                    return ['status' => 'success', 'data' => $user];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
