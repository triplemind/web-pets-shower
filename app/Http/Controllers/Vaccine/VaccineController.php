<?php

namespace App\Http\Controllers\Vaccine;

use DB;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Stock\Stock;
use App\Services\Partner\Partner;
use App\Services\Bill\Bill;
use App\Services\Pos\Pos;
use App\Services\Orderpos\Orderpos;
use App\Services\Vaccine\Vaccine;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;


class VaccineController extends Controller
{
    public function getIndex()
    {
        $filters = \Session::has('report_filters') ? \Session::get('report_filters') : [];
        // $filters['search_txt']    
        // $filters['vaccine_type']    
        // $filters['vaccine_status']        

        // $getVaccines = Vaccine::where('id', '!=', 0);
        $getVaccines = DB::table('vaccine')->where('id', '!=', 0);

        if(!empty($filters['search_txt'])){
            $keyword = $filters['search_txt'];
            $getVaccines = $getVaccines->where(function($getVaccines) use ($keyword){
                                $getVaccines->where('vaccine_name', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('properties', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('vaccine_type', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('vaccine_usage', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('description', 'like', '%'.$keyword.'%');
                                $getVaccines->orWhere('vaccine_status', 'like', '%'.$keyword.'%');
                            });
        }

        if(!empty($filters['vaccine_type'])){
            $getVaccines = $getVaccines->where('vaccine_type', $filters['vaccine_type']);
        }

        if(!empty($filters['vaccine_status'])){
            $getVaccines = $getVaccines->where('vaccine_status', $filters['vaccine_status']);
        }

        $getVaccines = $getVaccines->paginate(20);

        
    	return $this->view('vaccine.index',compact('getVaccines', 'filters'));
    }


    public function getAdd()
    {

        return $this->view('vaccine.add');
    }

    public function getView(Request $request, $id)
    {
        // $vaccine = Vaccine::where('id', $id)->first();
        $vaccine = DB::table('vaccine')->where('id', $id)->first();

        return $this->view('vaccine.view',compact('vaccine'));
    }

    public function getEdit(Request $request, $id)
    {
        // $vaccine = Vaccine::where('id', $id)->first();
        $vaccine = DB::table('vaccine')->where('id', $id)->first();

        return $this->view('vaccine.edit',compact('vaccine'));
    }

    //add ตรงกล่องส่วนล่างของ admin
    public function postAdd()
    {
        $vaccine_name       = \Input::has('vaccine_name') ? \Input::get('vaccine_name') : '';
        $drug_name          = \Input::has('drug_name') ? \Input::get('drug_name') : '';
        $company            = \Input::has('company') ? \Input::get('company') : '';
        $properties         = \Input::has('properties') ? \Input::get('properties') : '';
        $vaccine_usage      = \Input::has('vaccine_usage') ? \Input::get('vaccine_usage') : '';
        $description        = \Input::has('description') ? \Input::get('description') : '';
        $vaccine_type       = \Input::has('vaccine_type') ? \Input::get('vaccine_type') : '';
        $vaccine_status     = \Input::has('vaccine_status') ? \Input::get('vaccine_status') : '';
        $timeperiod_unit    = \Input::has('timeperiod_unit') ? \Input::get('timeperiod_unit') : '';
        $numoftimes         = \Input::has('numoftimes') ? \Input::get('numoftimes') : '';
        $timeperiod         = \Input::has('timeperiod') ? \Input::get('timeperiod') : '';
        $img_vaccine        = \Input::hasFile('img_vaccine') ? \Input::file('img_vaccine') : '';

        //ประกาศตัวแปรสำหรับเก็บชื่อImg
        $image_name = "";
        $date = date('Y-m-d');

        //เช็คโฟเดอร์ว่ามีมั้ย ถ้าไม่มีให้สร้างใหม่
        $path = base_path();
        if(!file_exists($path."/public/vaccineImg")){
         $oldmask = umask(0);
         mkdir($path."/public/vaccineImg", 0777);
         umask($oldmask);
        }

        if(!empty($img_vaccine))
        {
         $image_name = "/public/vaccineImg/".$date."-".$img_vaccine->getClientOriginalName();
         copy($img_vaccine, $path.$image_name);
        }

        //บันทึกลงdatabase new record เพื่อสร้าง pk
        // $new_vaccine                    = new Vaccine;
        // $new_vaccine->vaccine_name      = $vaccine_name;
        // $new_vaccine->drug_name         = $drug_name;
        // $new_vaccine->company           = $company;
        // $new_vaccine->properties        = $properties;
        // $new_vaccine->img_vaccine       = (empty($image_name)) ? "" : $image_name;;
        // $new_vaccine->vaccine_usage     = $vaccine_usage;
        // $new_vaccine->description       = $description;
        // $new_vaccine->vaccine_type      = $vaccine_type;
        // $new_vaccine->vaccine_status    = $vaccine_status;
        // $new_vaccine->save();


        DB::table('vaccine')->insert([
                'vaccine_name'      => $vaccine_name,
                'img_vaccine'       => (empty($image_name)) ? '' : $image_name,
                'drug_name'         => $drug_name,
                'company'           => $company,
                'properties'        => $properties,
                'vaccine_usage'     => $vaccine_usage,
                'description'       => $description,
                'vaccine_type'      => $vaccine_type,
                'vaccine_status'    => $vaccine_status,
                'timeperiod_unit'   => $timeperiod_unit,
                'numoftimes'        => $numoftimes,
                'timeperiod'        => $timeperiod,
                'created_at'        => date('Y-m-d H:i:s'),
            ]);

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $vaccine_name   = \Input::has('vaccine_name') ? \Input::get('vaccine_name') : '';
        $drug_name      = \Input::has('drug_name') ? \Input::get('drug_name') : '';
        $company        = \Input::has('company') ? \Input::get('company') : '';
        $properties     = \Input::has('properties') ? \Input::get('properties') : '';
        $vaccine_usage  = \Input::has('vaccine_usage') ? \Input::get('vaccine_usage') : '';
        $description    = \Input::has('description') ? \Input::get('description') : '';
        $vaccine_type   = \Input::has('vaccine_type') ? \Input::get('vaccine_type') : '';
        $vaccine_status = \Input::has('vaccine_status') ? \Input::get('vaccine_status') : '';
        $timeperiod_unit    = \Input::has('timeperiod_unit') ? \Input::get('timeperiod_unit') : '';
        $numoftimes         = \Input::has('numoftimes') ? \Input::get('numoftimes') : '';
        $timeperiod         = \Input::has('timeperiod') ? \Input::get('timeperiod') : '';
        $img_vaccine    = \Input::hasFile('img_vaccine') ? \Input::file('img_vaccine') : '';

        // $vaccine    = Vaccine::where('id', $id)->first();
        $vaccine    = DB::table('vaccine')->where('id', $id)->first();
        if(empty($vaccine)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/vaccineImg")){
         $oldmask = umask(0);
         mkdir($path."/public/vaccineImg", 0777);
         umask($oldmask);
        }

        if(!empty($img_vaccine))
        {
         $image_name = "/public/vaccineImg/".$date."-".$img_vaccine->getClientOriginalName();
         copy($img_vaccine, $path.$image_name);
        }


        // $vaccine->vaccine_name      = $vaccine_name;
        // $vaccine->drug_name         = $drug_name;
        // $vaccine->company           = $company;
        // $vaccine->properties        = $properties;
        // if(!empty($image_name)){
        //     $vaccine->img_vaccine   = $image_name;
        // }
        // $vaccine->vaccine_usage     = $vaccine_usage;
        // $vaccine->description       = $description;
        // $vaccine->vaccine_type      = $vaccine_type;
        // $vaccine->vaccine_status    = $vaccine_status;
        // $vaccine->save();

        DB::table('vaccine')
            ->where('id', $id)
            ->update([
                'vaccine_name'      => $vaccine_name,
                'img_vaccine'       => (empty($image_name)) ? $vaccine->img_vaccine : $image_name,
                'drug_name'         => $drug_name,
                'company'           => $company,
                'properties'        => $properties,
                'vaccine_usage'     => $vaccine_usage,
                'description'       => $description,
                'vaccine_type'      => $vaccine_type,
                'vaccine_status'    => $vaccine_status,
                'timeperiod_unit'   => $timeperiod_unit,
                'numoftimes'        => $numoftimes,
                'timeperiod'        => $timeperiod,
            ]);

        return ['status' => 'success'];
    }

    //เวลาลบให้ค้นหาแล้วลบรูปในโฟลเดอร์ที่สร้างไว้ตอน add ด้วย โดยอ้างอิงจาก url ที่ภูกบันทึกลง database
    public function postRemove()
    {
        $id    = \Input::has('id') ? \Input::get('id') : '';

        // $vaccine = Vaccine::where('id', $id)->first();
        $vaccine = DB::table('vaccine')->where('id', $id)->first();
        if(empty($vaccine)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $path = base_path();
        if(!empty($vaccine->img_vaccine)){
            if(file_exists($path.$vaccine->img_vaccine)){
                unlink($path.$vaccine->img_vaccine);
            }
        }

        // $vaccine->delete();
        DB::table('vaccine')
            ->where('id', $id)
            ->delete();

        return ['status' => 'success'];
    }


    public function ajaxCenter()
    {
        $method  = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {

            //เก็บข้อมูลที่ค้นหา ไว้ใช้สำหรับการ search วัคซีน
             case 'addFilterToSession':

                $filters                    = [];
                $filters['search_txt']      = \Input::get('search_txt');
                $filters['vaccine_type']    = \Input::get('vaccine_type');
                $filters['vaccine_status']  = \Input::get('vaccine_status');

                \Session::put('report_filters', $filters);

                return ['status' => 'success'];
                break;

            //ล้างข้อมูลที่ค้นหา 
             case 'clearFilterToSession':

                \Session::forget('report_filters');
                
                return ['status' => 'success'];
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
