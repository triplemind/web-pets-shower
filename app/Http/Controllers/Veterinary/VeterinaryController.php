<?php

namespace App\Http\Controllers\Veterinary;

use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Veterinary\Veterinary;
use App\Services\Description\Description;
use App\Services\Shop\Shop;
use App\Services\Vaccinrecord\Vaccinrecord;
use App\Services\Species\Species;
use App\Services\PetType\PetType;
use App\Services\ShopUsed\ShopUsed;
use App\Services\Course\Course;
use App\Services\CourseRecord\CourseRecord;


class VeterinaryController extends Controller
{
    public function getIndex()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'){
            $veterinarys = Veterinary::with(['shop'])->where('id', '!=', 0)->get();
            // $veterinarys = DB::table('veterinary')->where('id', '!=', 0)->get();
        }else{
            $veterinarys = Veterinary::with(['shop']);
            // $veterinarys = DB::table('veterinary')->with(['shop']);
            $veterinarys = $veterinarys->where('id', '!=', 0);
            $veterinarys = $veterinarys->where('user_id', $user_id);
            $veterinarys = $veterinarys->get();
        }

        $Shopname = Shop::where('owner_id', $user_id)->first();

        $shops = Shop::where('owner_id', $user_id);
        $shops = $shops->get(); 
        // sd($users->toArray());

        return $this->view('veterinary.index',compact('veterinarys', 'shops', 'Shopname'));
    }

    public function postAdd()
    {
        $shop_id            = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $Fname              = \Input::has('Fname') ? \Input::get('Fname') : '';
        $Lname              = \Input::has('Lname') ? \Input::get('Lname') : '';
        $vaterinary_code    = \Input::has('vaterinary_code') ? \Input::get('vaterinary_code') : '';
        $remark             = \Input::has('remark') ? \Input::get('remark') : '';
        $tel                = \Input::has('tel') ? \Input::get('tel') : '';
        $status             = \Input::has('status') ? \Input::get('status') : '';
        $img                = \Input::hasFile('img') ? \Input::file('img') : '';

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;


        $getveterinary = Veterinary::where('vaterinary_code', $vaterinary_code)->count();
        if($getveterinary > 0) return ['status' => 'error', 'msg' => 'เลขที่ใบอนุญาตนี้มีอยู่ในระบบแล้ว'];

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/vaterinaryImg")){
         $oldmask = umask(0);
         mkdir($path."/public/vaterinaryImg", 0777);
         umask($oldmask);
        }

        if(!empty($img))
        {
         $image_name = "/public/vaterinaryImg/".$date."-".$img->getClientOriginalName();
         copy($img, $path.$image_name);
        }

        // d($user_img);
        // d($menu_accress);
        // sd($image_name);
        // DB::table('veterinary')->insert([
        //     'shop_id'           => $shop_id,
        //     'img'               => (empty($image_name)) ? "" : $image_name,
        //     'user_id'           => $user_id,
        //     'Fname'             => $Fname,
        //     'Lname'             => $Lname,
        //     'vaterinary_code'   => $vaterinary_code,
        //     'remark'            => $remark,
        //     'tel'               => $tel,
        //     'status'            => $status,
        //     'created_at'        => date('Y-m-d H:i:s'),
        // ]);

        $new_veterinary                     = new Veterinary;
        $new_veterinary->shop_id            = $shop_id;
        $new_veterinary->img                = (empty($image_name)) ? "" : $image_name;
        $new_veterinary->user_id            = $user_id;
        $new_veterinary->Fname              = $Fname;
        $new_veterinary->Lname              = $Lname;
        $new_veterinary->vaterinary_code    = $vaterinary_code;
        $new_veterinary->remark             = $remark;
        $new_veterinary->tel                = $tel;
        $new_veterinary->status             = $status;
        $new_veterinary->save();

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $id                 = \Input::has('id') ? \Input::get('id') : '';
        $shop_id            = \Input::has('shop_id') ? \Input::get('shop_id') : '';
        $Fname              = \Input::has('Fname') ? \Input::get('Fname') : '';
        $Lname              = \Input::has('Lname') ? \Input::get('Lname') : '';
        $vaterinary_code    = \Input::has('vaterinary_code') ? \Input::get('vaterinary_code') : '';
        $remark             = \Input::has('remark') ? \Input::get('remark') : '';
        $tel                = \Input::has('tel') ? \Input::get('tel') : '';
        $status             = \Input::has('status') ? \Input::get('status') : '';
        $img                = \Input::hasFile('img') ? \Input::file('img') : '';

        // $veterinary = Veterinary::where('id', $id)->first();
        $veterinary = DB::table('veterinary')->where('id', $id)->first();
        if(empty($veterinary)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $getveterinary = Veterinary::where('vaterinary_code', $vaterinary_code)->count();
        if($getveterinary > 1) return ['status' => 'error', 'msg' => 'เลขที่ใบอนุญาตนี้มีอยู่ในระบบแล้ว'];


        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $name_owner     = empty($user_obj->name_owner) ? "" : $user_obj->name_owner;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->id_owner) ? "" : $user_obj->id_owner;

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/vaterinaryImg")){
         $oldmask = umask(0);
         mkdir($path."/public/vaterinaryImg", 0777);
         umask($oldmask);
        }

        if(!empty($img))
        {
         $image_name = "/public/vaterinaryImg/".$date."-".$img->getClientOriginalName();
         copy($img, $path.$image_name);
        }

        // d($user_img);
        // d($menu_accress);
        // sd($image_name);

        // $veterinary->shop_id            = $shop_id;
        // if(!empty($image_name)){
        //     $veterinary->img            = $image_name;
        // }
        // $veterinary->user_id            = $user_id;
        // $veterinary->Fname              = $Fname;
        // $veterinary->Lname              = $Lname;
        // $veterinary->vaterinary_code    = $vaterinary_code;
        // $veterinary->remark             = $remark;
        // $veterinary->tel                = $tel;
        // $veterinary->status             = $status;
        // $veterinary->save();
            DB::table('veterinary')
                ->where('id', $id)
                ->update([
                    'shop_id'           => $shop_id,
                    'img'               => (empty($image_name)) ? $veterinary->img : $image_name,
                    'user_id'           => $user_id,
                    'Fname'             => $Fname,
                    'Lname'             => $Lname,
                    'vaterinary_code'   => $vaterinary_code,
                    'remark'            => $remark,
                    'tel'               => $tel,
                    'status'            => $status,
                ]);

        return ['status' => 'success'];
    }



    public function postRemove()
    {
        $id    = \Input::has('id') ? \Input::get('id') : '';

        // $veterinary = Veterinary::where('id', $id)->first();
        $veterinary = DB::table('veterinary')->where('id', $id)->first();
        if(empty($veterinary)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $path = base_path();
        if(!empty($veterinary->img)){
            if(file_exists($path.$veterinary->img)){
                unlink($path.$veterinary->img);
            }
        }

        // $veterinary->delete();
        DB::table('veterinary')
            ->where('id', $id)
            ->delete();

        return ['status' => 'success'];
    }



    public function ajaxCenter()
    {
        $method         = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'getVeterinaryData':

                $id = \Input::has('id') ? \Input::get('id') : '';

                // $veterinary = Veterinary::where('id', $id)->first();
                $veterinary = DB::table('veterinary')->where('id', $id)->first();

                if(!empty($veterinary)){
                    return ['status' => 'success', 'data' => $veterinary];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
