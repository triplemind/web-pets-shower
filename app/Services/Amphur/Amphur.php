<?php namespace App\Services\Amphur;

use Illuminate\Database\Eloquent\Model;

class Amphur extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'amphur';
 	protected $primaryKey 	= 'AMPHUR_ID';

}