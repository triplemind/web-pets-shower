<?php namespace App\Services\Auths;

use App\Services\Users\UserObject;
use App\Services\Users\User;

class UserAuth {


    public static function attempt($username, $password)
    {
        $userObject = new UserObject;

        // Query.
        $user   = User::where('username', $username)->where('password', $password);
        $user   = $user->orWhere(function($q) use ($username, $password) {
                        $q->where('email', $username)->where('password', $password);
                    });

        $userModel = $user->first(['id_owner', 'name_owner', 'address', 'telephone', 'username', 'email', 'user_type', 'user_img']);

        if (empty($userModel)) return false;

        // if($userModel->user_type == 'admin')  \session::put('id', 0);
        // Save User to Session.
        $userObject->setUp($userModel);
        // Set Lang.
        // setLang();
        //Return.
        return $userModel;
	}

    public static function check_user_type()
    {
        // \Session::forget('current_user');
        $result = false;
        // sd(\Session::has('current_user'));
        if (\Session::has('current_user')) {
            if (\Session::get('current_user')->user_type == 'ผู้ดูแลระบบ' || \Session::get('current_user')->user_type == 'Root') {

                $result = true;
            }
        }

        return $result;
    }

	public static function check()
	{
		$result = false;

        if (\Session::has('current_user')) {

        	$result = true;
        }

        return $result;
	}

    public static function clear()
    {
        \Session::forget('current_user');
        \Session::forget('count_all');
        \Session::forget('noti');
        \Session::forget('vaccine_filters');
        \Session::forget('chart_range');
        // \Session::forget('product_filters');
        // \Session::forget('locate');
        // \Session::forget('admin_id');
        // \Session::forget('user_packages');
        // \Session::forget('view_style_renew');
        // \Session::forget('view_style');
        // \Session::forget('report_filters');
        // \Session::forget('report_dashboard_filters');
        // \Session::forget('renew_filters');
    }
}