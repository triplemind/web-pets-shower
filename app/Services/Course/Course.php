<?php namespace App\Services\Course;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'manage_course';
 	protected $primaryKey 	= 'id';


 	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'user_id');
	}

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'shop_id');
	}

	
}