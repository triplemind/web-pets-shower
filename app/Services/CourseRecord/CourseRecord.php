<?php namespace App\Services\CourseRecord;

use Illuminate\Database\Eloquent\Model;

class CourseRecord extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'course_record';
 	protected $primaryKey 	= 'id';


 	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'user_id');
	}

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'shop_id');
	}


	public function pet()
	{
		return $this->belongsTo("App\Services\Description\Description", 'pet_id');
	}

	public function course()
	{
		return $this->belongsTo("App\Services\Course\Course", 'course_id');
	}

	public function service()
	{
		return $this->belongsTo("App\Services\PetService\PetService", 'id_petservice');
	}

}