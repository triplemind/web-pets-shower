<?php namespace App\Services\Description;

use Illuminate\Database\Eloquent\Model;

class Description extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'description';
 	protected $primaryKey 	= 'pet_id';

	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'id_owner');
	}
}