<?php namespace App\Services\District;

use Illuminate\Database\Eloquent\Model;

class District extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'district';
 	protected $primaryKey 	= 'DISTRICT_ID';

}