<?php namespace App\Services\PetService;

use Illuminate\Database\Eloquent\Model;

class PetService extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'pet_service';
 	protected $primaryKey 	= 'id_petservice';

 	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'id_login');
	}

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'id_shop');
	}

	public function pet()
	{
		return $this->belongsTo("App\Services\Description\Description", 'pet_id');
	}

	public function petsShop()
	{
		return $this->belongsTo("App\Services\PetsShop\PetsShop", 'pets_shopID');
	}

	public function vaccineRC()
	{
		return $this->belongsTo("App\Services\Vaccinrecord\Vaccinrecord", 'vaccineRC_id');
	}

	public function vaccine()
	{
		return $this->belongsTo("App\Services\Vaccine\Vaccine", 'id');
	}

	public function course()
	{
		return $this->belongsTo("App\Services\Course\Course", 'courseid');
	}


}