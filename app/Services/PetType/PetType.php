<?php namespace App\Services\PetType;

use Illuminate\Database\Eloquent\Model;

class PetType extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'pet_type';
 	protected $primaryKey 	= 'id';

	
}