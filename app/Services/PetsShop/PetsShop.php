<?php namespace App\Services\PetsShop;

use Illuminate\Database\Eloquent\Model;

class PetsShop extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'pets_shop';
 	protected $primaryKey 	= 'id';

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'shop_id');
	}

	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'user_id');
	}

	public function pet()
	{
		return $this->belongsTo("App\Services\Description\Description", 'pet_id');
	}
}