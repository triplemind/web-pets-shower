<?php namespace App\Services\Promotion;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'promotion';
 	protected $primaryKey 	= 'id';

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'shop_id');
	}
}