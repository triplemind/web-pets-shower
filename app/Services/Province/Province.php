<?php namespace App\Services\Province;

use Illuminate\Database\Eloquent\Model;

class Province extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'province';
 	protected $primaryKey 	= 'PROVINCE_ID';

}