<?php namespace App\Services\Shop;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'shop';
 	protected $primaryKey 	= 'id_shop';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}