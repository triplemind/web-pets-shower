<?php namespace App\Services\ShopService;

use Illuminate\Database\Eloquent\Model;

class ShopService extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'shop_service';
 	protected $primaryKey 	= 'id';

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'shop_id');
	}

}