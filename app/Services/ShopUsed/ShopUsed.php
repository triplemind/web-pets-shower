<?php namespace App\Services\ShopUsed;

use Illuminate\Database\Eloquent\Model;

class ShopUsed extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'shop_used';
 	protected $primaryKey 	= 'id';

	public function shop()
	{
		return $this->hasMany("App\Services\Shop\Shop", 'shop_id');
	}

	public function user()
	{
		return $this->hasMany("App\Services\Users\User", 'user_id');
	}
}