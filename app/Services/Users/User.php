<?php namespace App\Services\Users;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'owner';
 	protected $primaryKey 	= 'id_owner';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}