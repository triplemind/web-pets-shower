<?php namespace App\Services\Users;

use App\Services\Users\User;

class UserObject {

	protected $id_owner;
	protected $name_owner;
	protected $address;
	protected $username;
	protected $user_type;
	protected $email;
	protected $telephone;
	protected $user_img;

	public function __construct()
	{
		// Put Session to Object.
		if (\Session::has('current_user')) {

			$current_user 			= \Session::get('current_user');
			$this->id_owner			= $current_user->id_owner;
			$this->name_owner 		= $current_user->name_owner;
			$this->address 		= $current_user->address;
			$this->username 		= $current_user->username;
			$this->user_type 		= $current_user->user_type;
			$this->email 			= $current_user->email;
			$this->telephone 	= $current_user->telephone;
			$this->user_img 		= $current_user->user_img;
		}
	}

	public function setUp($userModel)
	{
		// Save to Session.
        \Session::put('current_user', $userModel);
    }

	public function getID()
	{
		return $this->id_owner;
	}
	public function getFirstname()
	{
		return $this->name_owner;
	}
	public function getLastname()
	{
		return $this->address;
	}
	public function getUsername()
	{
		return $this->username;
	}
	public function getUserType()
	{
		return $this->user_type;
	}
	public function getEmail()
	{
		return $this->email;
	}
	public function getImag()
	{
		return $this->user_img;
	}
	public function getTel()
	{
		return $this->telephone;
	}
	

}