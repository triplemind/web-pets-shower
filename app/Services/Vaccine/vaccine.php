<?php namespace App\Services\Vaccine;

use Illuminate\Database\Eloquent\Model;

class Vaccine extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'vaccine';
 	protected $primaryKey 	= 'id';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}