<?php namespace App\Services\Vaccinrecord;

use Illuminate\Database\Eloquent\Model;

class vaccinrecord extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'vaccinrecord';
 	protected $primaryKey 	= 'id_vaccin';


 	public function vaccine()
	{
		return $this->belongsTo("App\Services\Vaccine\Vaccine", 'vaccine_ID');
	}

	public function veterinary()
	{
		return $this->belongsTo("App\Services\Veterinary\Veterinary", 'veterinary_ID');
	}

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'shop_id');
	}

	public function pet()
	{
		return $this->belongsTo("App\Services\Description\Description", 'pet_id');
	}
	
}