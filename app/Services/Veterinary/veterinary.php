<?php namespace App\Services\Veterinary;

use Illuminate\Database\Eloquent\Model;

class Veterinary extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'veterinary';
 	protected $primaryKey 	= 'id';

	public function shop()
	{
		return $this->belongsTo("App\Services\Shop\Shop", 'shop_id');
	}
}