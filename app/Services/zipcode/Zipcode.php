<?php namespace App\Services\Zipcode;

use Illuminate\Database\Eloquent\Model;

class Zipcode extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'zipcode';
 	protected $primaryKey 	= 'ZIPCODE_ID';

}