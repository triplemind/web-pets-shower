let time_payment;
let ed_time_payment;
$(function(){

	console.log("dsds");
    $('#bank_payment').dropdown();
    $('#ed_bank_payment').dropdown();

    $('#date_payment').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        },
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        },
    });

    $('#ed_date_payment').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        },
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        },
    });

    var check_payment = $('#check_payment').val();

    // $('#time_payment').calendar({
    //     type: 'time',
    //     popupOptions: {
    //         observeChanges: false
    //     }
    // });

    if(check_payment == 'empty'){
        time_payment = new Picker(document.querySelector('#time_payment'), {
            // container: document.querySelector('.docs-picker-container'),
            format: 'HH:mm',
            headers: true,
            controls: true,
            inline: true,
            rows:3,
            text: {
                title: 'เลือกเวลา',
            },
        });
    }

    if(check_payment == 'not empty'){
        ed_time_payment = new Picker(document.querySelector('#ed_time_payment'), {
            // container: document.querySelector('.docs-picker-container'),
            format: 'HH:mm',
            headers: true,
            controls: true,
            inline: true,
            rows:3,
            text: {
                title: 'เลือกเวลา',
            },
        });
    }



    // ed_time_payment = new Picker(document.querySelector('#ed_time_payment'), {
    //     // container: document.querySelector('.docs-picker-container'),
    //     format: 'HH:mm',
    //     headers: true,
    //     controls: true,
    //     inline: true,
    //     rows:3,
    //     text: {
    //         title: 'เลือกเวลา',
    //     },
    // });

    // $('#ed_time_payment').calendar({
    //     type: 'time',
    //     popupOptions: {
    //         observeChanges: false
    //     }
    // });

    $('.btn-save-payment').on('click', function(){
        var addslip_url     = $('#addslip-url').data('url');
        var id              = $('#id').val();
        var date_payment    = $('#date_payment').calendar('get date');
        // var time_payment    = $('#time_payment').calendar('get date');
        var time_payment_val = time_payment.getDate(true);
        var bank_payment    = $('#bank_payment').dropdown('get value');

        var data        = new FormData();
        if (typeof($('#pic_payment')[0]) !== 'undefined') {

            jQuery.each(jQuery('#pic_payment')[0].files, function(i, file) {
                data.append('pic_payment', file);
                // console.log(file);
            });
        }
        data.append('id',id);
        data.append('date_payment',moment(date_payment).locale('th').format('YYYY-MM-DD'));
        data.append('time_payment',time_payment_val);
        data.append('bank_payment',bank_payment);

        if(time_payment_val != null || date_payment != null || bank_payment != ''){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: addslip_url,
                data: data,
                contentType: false,
                processData:false,
                cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                       setTimeout(function(){ 
                            $("body").toast({
                                class: "success",
                                position: 'bottom right',
                                message: `บันทึกเสร็จสิ้น`
                            });
                            setTimeout(function(){ 
                                window.location.reload();
                            }, 1000);
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }else{
            $("body").toast({
                class: "error",
                position: 'bottom right',
                message: 'กรุณากรอกข้อมูลให้ครบ'
            });
        }

    });



	$('.btn-edit-payment').on('click', function(){
        var addslip_url     = $('#editslip-url').data('url');
        var id 			    = $('#id').val();
        var date_payment    = $('#ed_date_payment').calendar('get date');
        // var time_payment    = $('#ed_time_payment').calendar('get date');
        var time_payment_val = ed_time_payment.getDate(true);
        var bank_payment    = $('#ed_bank_payment').dropdown('get value');

        var data        = new FormData();
	    if (typeof($('#ed_pic_payment')[0]) !== 'undefined') {

	        jQuery.each(jQuery('#ed_pic_payment')[0].files, function(i, file) {
	            data.append('pic_payment', file);
	            // console.log(file);
	        });
	    }
		data.append('id',id);
        data.append('date_payment',moment(date_payment).locale('th').format('YYYY-MM-DD'));
        data.append('time_payment',time_payment_val);
        data.append('bank_payment',bank_payment);

        if(time_payment != null || date_payment != null || bank_payment != ''){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: addslip_url,
               	data: data,
    	        contentType: false,
    	        processData:false,
    	        cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                       setTimeout(function(){ 
    	                    $("body").toast({
    	                        class: "success",
    	                        position: 'bottom right',
    	                        message: `บันทึกเสร็จสิ้น`
    	                    });
    	                    setTimeout(function(){ 
    	                        window.location.reload();
    	                    }, 1000);
    	                });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }else{
            $("body").toast({
                class: "error",
                position: 'bottom right',
                message: 'กรุณากรอกข้อมูลให้ครบ'
            });
        }

    });


    $('.btn-cancel').on('click', function(){
        Swal.fire({
            title: "คุณต้องการยกเลิกรายการจองนี้ใช่หรือไม่ ?",
            showCancelButton: true,
            confirmButtonColor: "#db1818",
            cancelButtonText: "ไม่ใช่",
            cancelButtonColor: "",
            confirmButtonText: "ใช่"
        }).then(result => {
            if (result.value) {
                CancelOrder();
            }
        });
    });

});


function ShowimgModal() {
    $('.ui.basic.modal').modal('show');
}



function CancelOrder() {
    var pos_id          = $('#id').val();

    console.log(pos_id);

    var method      = 'CancelOrder';
    var ajax_url    = $('#ajax-center-url').data('url');

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
             'pos_id' : pos_id,
        },
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `ยกเลิกรายการเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.reload();
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
        }
    });
}