$(function(){

	// $('.btn-book-time').on('click', function(){
	// 	$('.tiny.modal').modal({
	// 		closable: false,
	// 		onDeny: function() {
	// 		},
	// 		onApprove: function() {
	// 			var id_petservice  	= $('#id_petservice').val();
	// 			// addNewMyPets();
	// 		}
	// 	}).modal("show");
	// });


	// $('#booking_time').dropdown('clear').dropdown({
	// 	onChange: function (value, text) {
	// 		console.log(value);
	// 		if(value){
	// 			var id_petservice  	= $('#id_petservice').val();
	// 			var id_shop  		= $('#id_shop').val();
	// 			var id_service  	= $('#id_service').val();
	// 			var booking_date  	= $('#booking_date').val();
	// 			var booking_time 	= value;

	// 			checkBooing(booking_date, id_service, booking_time, id_shop);							
	// 		}
	// 	}
	// });

});


function addBookingTime(booking_date, id_service, id_shop, id_petservice){
	var booking_time;

    console.log(booking_date, id_service, id_shop, id_petservice);

	$('.tiny.modal').modal({
		closable: false,
		onDeny: function() {
		},
		onApprove: function() {
			// var bookTime = $('#booking_time').dropdown('get value');
			postAddBookingTime(id_petservice, booking_time);
		}
	}).modal("show");

    var method      = 'getVaccine';
    var ajax_url    = $('#ajax-center-url').data('url');
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'id_shop' : id_shop,
            'id_service' : id_service,
        },
        success: function(result) {
            if(result.status == 'success'){
                var txt_time = "";
                console.log("sss:",result.services);
                setTimeout(function(){ 
                    if(result.services.length != 0){
                        $('#booking_time').empty();
                        $('#booking_time')
                            .append($("<option></option>")
                            .text('เลือกช่วงเวลา'));
                        $.each(result.services, function(key, value) {  
                            if(value.time_period == 'เช้า'){
                                txt_time = "เช้า : "+((!value.startTime) ? "09.00น.- 12.30น." : value.startTime);
                            }else if(value.time_period == 'บ่าย'){
                                txt_time = "บ่าย : "+((!value.endTime) ? "13.30น.-19.00น." : value.endTime);
                            }

                            $('#booking_time')
                            .append($("<option></option>")
                            .attr("value", value.time_period)
                            .text(txt_time)); 
                        });
                    }else{
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: 'ไม่สามารถทำรายการได้ กรุณาติดต่อร้านค้า'
                        });
                    }
                   
                }, 1000);

            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.data
                });
            }
        }
    });


	$('#booking_time').dropdown('clear').dropdown({
		onChange: function (value, text) {
			console.log(value);
			if(value){
				booking_time 	= value;
				checkBooing(booking_date, id_service, booking_time, id_shop);							
			}
		}
	});

	
}


//เช็คว่ามีการจองรึยัง
function checkBooing(date, id_service, booking_time, id_shop){
	var id_shop           = id_shop;
    var booking_date      = date;
    var id_service        = id_service;
    var booking_time      = booking_time;

    booking_date = booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

    var ajax_url        = $('#ajax-center-url').data('url');
    var method        	= 'checkBooing';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
        	'method' : method,
        	'id_shop' : id_shop,
        	'booking_date' : booking_date,
        	'id_service' : id_service,
        	'booking_time' : booking_time,
        },
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 

                	$txt_que = "รับ "+result.allque+" คิว || ว่าง "+result.allowque+" คิว";

                	$("#que_txt").text($txt_que);

                	if(result.bookingallow){
                		$("body").toast({
	                        class: "success",
	                        position: 'bottom right',
	                        message: result.msg
	                    });

	                    $('.btn-save').removeClass('disabled');
                	}else{
                		$("body").toast({
	                        class: "error",
	                        position: 'bottom right',
	                        message: result.msg
	                    });
                		
	                    $('.btn-save').addClass('disabled');
                	}

                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
}



function postAddBookingTime(id_petservice, booking_time){
	// var bookTime = $('#booking_time').dropdown('get value');

	console.log(id_petservice, booking_time);

	var ajax_url        = $('#ajax-center-url').data('url');
    var method        	= 'postAddBookingTime';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
        	'method' : method,
        	'id_petservice' : id_petservice,
        	'booking_time' : booking_time,
        },
        success: function(result) {
            if(result.status == 'success'){
            	$("body").toast({
                    class: "success",
                    position: 'bottom right',
                    message: 'บันทึกเสร็จสิ้น'
                });
                setTimeout(function(){ 
                	window.location.reload();
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
}