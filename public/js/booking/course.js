var _promostatus;
$(function(){
	console.log('dddd');
	$('#coursedetail').accordion({
        selector: {
          trigger: '.title .icon'
        }
  	});

    $('#pet_id').dropdown();

	$('.btn-clickcourse').on('click', function(){
		var price 		= $(this).data('price');
		var id 			= $(this).data('id');
		var numoftimes 	= $(this).data('numoftimes');
		var name 		= $(this).data('name');
		var name_shop 	= $(this).data('name_shop');
		var shopid 	    = $(this).data('shopid');

		$('#courseID').val();
		$('#shopID').val();
		$('#courseName').text('');
		$('#md_numoftimes').text('');
		$('#md_price').text('');
		$('#shopName').text('');

		getCourse(price,id,numoftimes,name,name_shop,shopid);

	});

	$('.btn-apply-code').on('click', function(){
		var promo_code 		= $('#promo_code').val();
		var courseID 		= $('#courseID').val();
		var shopID 			= $('#shopID').val();

		checkPormoCode(promo_code,courseID,shopID);

	});


    $('.btn-check-time').on('click', function(){
        var shopID          = $('#shopID').val();

        $('.bookingdate').css('display', 'unset');
        // PG0001

        checkBookingTime(shopID);

    });


});

function getCourse(price,id,numoftimes,name,name_shop,shopid){

	console.log(id, price, numoftimes, name, name_shop,shopid);

	$('#shopID').val(shopid);
	$('#courseID').val(id);
	$('#courseName').text(name);
	$('#md_numoftimes').text(numoftimes);
	$('#md_price').text(price);
	$('#shopName').text(name_shop);

    $('#booking_date').calendar({
        type: 'date',
        minDate: new Date(),
        monthFirst: false,
        popup:'show',
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        }
    });

  	$('.small.modal').modal({
		closable: false,
        autofocus: false,
        detachable: false,
		onDeny: function() {
		},
		onApprove: function() {
			// var bookTime = $('#booking_time').dropdown('get value');
			postAddBooking();
		}  
	}).modal('show');

}


function checkPormoCode(promo_code,courseID,shopID){
	console.log('checkPormoCode',promo_code,courseID,shopID);

	$('.thismsgcheckpro').css('display', 'none');
	$('#msgcheckpro').text('');

	var method      = 'checkPormoCode';
    var ajax_url    = $('#ajax-center-url').data('url');
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'promo_code' : promo_code,
            'courseID' : courseID,
            'shopID' : shopID,
        },
        success: function(result) {
        	_promostatus = result.status;
            if(result.status == 'success'){
                console.log(result.txtalert);
                // console.log(JSON.parse(result.data));
                $('.thismsgcheckpro').css('display', 'unset');
                $('#msgcheckpro').text(result.txtalert);
                
            } // End if check s tatus success.

            if(result.status == 'error'){

            	$('.thismsgcheckpro').css('display', 'unset');
            	$('#msgcheckpro').text(result.msg);
                // $("body").toast({
                //     class: "error",
                //     position: 'bottom right',
                //     message: result.data
                // });
            }
        }
    });

}


function postAddBooking(){
	var promo_code 		= $('#promo_code').val();
	var courseID 		= $('#courseID').val();
	var shopID 			= $('#shopID').val();
    var pet_id          = $('#pet_id').dropdown('get value');
    var booking_time    = $('#booking_time').dropdown('get value');
    var booking_date    = $('#booking_date').calendar('get date');

	console.log('postAddBooking',promo_code,courseID,shopID,pet_id);

    if(pet_id){
        var ajax_url    = $('#add_url').data('url');
        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: 'post',
            url: ajax_url,
            data: {
                'promo_code' : promo_code,
                'courseID' : courseID,
                'id_shop' : shopID,
                'promostatus' : _promostatus,
                'pet_id' : pet_id,
                'id_service' : "PG0001",
                'booking_time' : booking_time ? booking_time : null,
                'booking_date' : booking_date ? moment(booking_date).format('YYYY-MM-DD') : null,
            },
            success: function(result) {
                if(result.status == 'success'){
                   $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: 'ทำรายการสำเร็จ'
                    });

                   	setTimeout(function(){ 
                        window.location.href = '/bookingcoursedetail/'+result.serviceid;
                    }, 1000);
                    
                } // End if check s tatus success.

                if(result.status == 'error'){
                    $("body").toast({
                        class: "error",
                        position: 'bottom right',
                        message: result.msg
                    });
                }
            }
        });
    }else{
        $("body").toast({
            class: "error",
            position: 'bottom right',
            message: 'กรุณาเลือกสัตว์เลี้ยงก่อนทำรายการ'
        });
    }


}



function checkBookingTime(shopID){

    var method      = 'getVaccine';
    var ajax_url    = $('#ajax-center-url').data('url');
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'id_shop' : shopID,
            // 'id_service' : id_service,
            'id_service' : "PG0001",
        },
        success: function(result) {
            if(result.status == 'success'){
                var txt_time = "";
                console.log("sss:",result.services);
                setTimeout(function(){ 
                    if(result.services.length != 0){
                        $('#booking_time').empty();
                        $('#booking_time')
                            .append($("<option></option>")
                            .text('เลือกช่วงเวลา'));
                        $.each(result.services, function(key, value) {  
                            if(value.time_period == 'เช้า'){
                                txt_time = "เช้า : "+((!value.startTime) ? "09.00น.- 12.30น." : value.startTime);
                            }else if(value.time_period == 'บ่าย'){
                                txt_time = "บ่าย : "+((!value.endTime) ? "13.30น.-19.00น." : value.endTime);
                            }

                            $('#booking_time')
                            .append($("<option></option>")
                            .attr("value", value.time_period)
                            .text(txt_time)); 
                        });
                    }else{
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: 'ไม่สามารถทำรายการได้ กรุณาติดต่อร้านค้า'
                        });
                    }
                   
                }, 1000);

            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.data
                });
            }
        }
    });


    $('#booking_time').dropdown('clear').dropdown({
        onChange: function (value, text) {
            console.log(value);
            if(value){
                booking_time    = value;
                checkBooing("PG0001", booking_time, shopID);                           
            }
        }
    });

}



//เช็คว่ามีการจองรึยัง
function checkBooing(id_service, booking_time, id_shop){
    var id_shop           = id_shop;
    var booking_date      = $('#booking_date').calendar('get date');
    var id_service        = id_service;
    var booking_time      = booking_time;

    booking_date = booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

    var ajax_url        = $('#ajax-center-url').data('url');
    var method          = 'checkBooing';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'id_shop' : id_shop,
            'booking_date' : booking_date,
            'id_service' : id_service,
            'booking_time' : booking_time,
        },
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 

                    $txt_que = "รับ "+result.allque+" คิว || ว่าง "+result.allowque+" คิว";

                    $("#que_txt").text($txt_que);

                    if(result.bookingallow){
                        $("body").toast({
                            class: "success",
                            position: 'bottom right',
                            message: result.msg
                        });

                        $('.btn-save').removeClass('disabled');
                    }else{
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.msg
                        });
                        
                        $('.btn-save').addClass('disabled');
                    }

                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
}