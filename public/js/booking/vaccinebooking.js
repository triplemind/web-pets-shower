var id_service;
var vaccine_id;
var petidU;
$(function(){
	vaccine_id 	= $('#vaccine_idU').val();
	petidU 		= $('#petidU').val();
	id_service	= 'PV0001';

	var method      = 'getVaccine';
    var ajax_url    = $('#ajax-center-url').data('url');
	$.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'id_shop' : $('#id_shop').val(),
            'id_service' : 'PV0001',
        },
        success: function(result) {
            if(result.status == 'success'){
                console.log(result.data);
                // console.log(JSON.parse(result.data));
				$('.vaccine_display').css('display', 'unset');
				$('#vaccine').dropdown();
                if(result.data){
                    let arr_vac = sort_by_key(result.data, 'vaccine_name');
                    
                    $.each(arr_vac, function(key, value) {   
                        $('#vaccine')
                        .append($("<option></option>")
                        .attr("value", value.vaccine_ID)
    					.text(value.vaccine_name+" : "+value.properties)); 
                    });

                    setTimeout(function(){ 
                    	var allowvaccine = _.find(arr_vac, function(item){ 
                            // console.log(item.vaccine_ID);
                    		if(item.vaccine_ID == vaccine_id){
                    			return true; 
                    		}else{
                    			return false; 
                    		}
                    	});
                    	console.log(vaccine_id);
                        console.log(allowvaccine);
                    	if(allowvaccine){
                        	$("#vaccine").dropdown("refresh").dropdown("set selected", vaccine_id);
                    	}else{
                    		$("body").toast({
		                        class: "error",
		                        position: 'bottom right',
		                        message: 'ร้านค้านี้ไม่มีวัคซีนที่ท่านต้องการ กรุณาติดต่อร้านค้า'
		                    });
		                    setTimeout(function(){ 
		                    	window.location.href = '/';
	                    	}, 1500);
                    	}
                    }, 1000);
                }
				
                var txt_time = "";

            	console.log("sss:",result.services);
                if(result.services.length != 0){
                	$('#booking_time').empty();

                    $('#booking_time')
                        .append($("<option></option>")
                        .text('เลือกช่วงเวลา')); 
                        
                	$.each(result.services, function(key, value) {  
                		if(value.time_period == 'เช้า'){
							txt_time = "เช้า : "+((!value.startTime) ? "09.00น.- 12.30น." : value.startTime);
                		}else if(value.time_period == 'บ่าย'){
                			txt_time = "บ่าย : "+((!value.endTime) ? "13.30น.-19.00น." : value.endTime);
                		}

                        $('#booking_time')
                        .append($("<option></option>")
                        .attr("value", value.time_period)
    					.text(txt_time)); 
                    });
                }else{
                	$("body").toast({
                        class: "error",
                        position: 'bottom right',
                        message: 'ไม่สามารถทำรายการได้ กรุณาติดต่อร้านค้า'
                    });
                }
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.data
                });
            }
        }
    });



	$('#booking_date').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		onChange: function (date) {
			//จะขึ้นให้เลือก time
			$('#booking_time').dropdown('clear').dropdown({
				onChange: function (value1, text1) {
					console.log(value1);
					if(value1){
						var id_shop  = $('#id_shop').val();
						var booking_time = value1;
						checkBooing(date, id_service, booking_time, id_shop);							
					}
				}
			});
		}
	});


	$('#id_service').dropdown({
		onChange: function (value, text) {
			id_service = value;
			// $('#vaccine').dropdown();
			// $('#vaccine').empty();

			var method      = 'getVaccine';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'id_shop' : $('#id_shop').val(),
                    'id_service' : id_service,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        console.log(result.data);
                        // console.log(JSON.parse(result.data));
						if(text == 'วัคซีน' || text == 'โปรโมชันวัคซีน'){
							$('.vaccine_display').css('display', 'unset');
	                        if(result.data){
		                        let arr_vac = sort_by_key(result.data, 'vaccine_name');
		                        
		                        $.each(arr_vac, function(key, value) {   
		                            $('#vaccine')
		                            .append($("<option></option>")
		                            .attr("value", value.vaccine_ID)
                					.text(value.vaccine_name+" : "+value.properties)); 
		                        });
		                    }
						}else{
							$('.vaccine_display').css('display', 'none');
						}

                        var txt_time = "";

                    	console.log("sss:",result.services);
                        if(result.services){
                        	$('#booking_time').empty();
                        	$.each(result.services, function(key, value) {  
                        		if(value.time_period == 'เช้า'){
									txt_time = "เช้า : "+((!value.startTime) ? "09.00น.- 12.30น." : value.startTime);
                        		}else if(value.time_period == 'บ่าย'){
                        			txt_time = "บ่าย : "+((!value.endTime) ? "13.30น.-19.00น." : value.endTime);
                        		}

	                            $('#booking_time')
	                            .append($("<option></option>")
	                            .attr("value", value.time_period)
            					.text(txt_time)); 
	                        });
                        }
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            });

		}
	});

    // $('#pet_id').dropdown();
    $("#pet_id").dropdown("refresh").dropdown("set selected", petidU);

    $('.btn-save').on('click', function(){
        postAdd();
    });

    setTimeout(function(){ 
	    var hasPro = $('#hasPro').val();

	    if(hasPro == 'has'){
	    	$('.ui.large.modal').modal('show');
	    }

	});


});


function sort_by_key(array, key)
{
	return array.sort(function(a, b)
	{
		var x = a[key]; var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}

//เช็คว่ามีการจองรึยัง
function checkBooing(date, id_service, booking_time, id_shop){
	var id_shop           = id_shop;
    var booking_date      = date;
    var id_service        = id_service;
    var booking_time      = booking_time;

    booking_date = booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

    var ajax_url        = $('#ajax-center-url').data('url');
    var method        	= 'checkBooing';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
        	'method' : method,
        	'id_shop' : id_shop,
        	'booking_date' : booking_date,
        	'id_service' : id_service,
        	'booking_time' : booking_time,
        },
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 

                	$txt_que = "รับ "+result.allque+" คิว || ว่าง "+result.allowque+" คิว";

                	$("#que_txt").text($txt_que);

                	if(result.bookingallow){
                		$("body").toast({
	                        class: "success",
	                        position: 'bottom right',
	                        message: result.msg
	                    });

	                    $('.btn-save').removeClass('disabled');
                	}else{
                		$("body").toast({
	                        class: "error",
	                        position: 'bottom right',
	                        message: result.msg
	                    });
                		
	                    $('.btn-save').addClass('disabled');
                	}

                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
}


function postAdd() {
    var id_shop           = $('#id_shop').val();
    var note           	  = $('#note').val();
    var booking_date      = $('#booking_date').calendar('get date');
    var promo_code        = $('#promo_code').val();
    var id_service        = $('#id_service').dropdown('get value');
    var booking_time      = $('#booking_time').dropdown('get value');
    var pet_id            = $('#pet_id').dropdown('get value');
    var vaccine_val 	  = '';
    var vaccine_id 	      = '';

    if(id_service == 'PV0001' || id_service == 'PV0002'){
    	vaccine_val = $('#vaccine').dropdown('get text');
    	vaccine_id = $('#vaccine').dropdown('get value');
    }else{
    	vaccine_val = '';
    	vaccine_id = '';
    }


    booking_date = booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

    var add_url         = $('#add_url').data('url');
    var data            = new FormData();

    data.append('id_shop', id_shop);
    data.append('note', note);
    data.append('booking_date',booking_date);
    data.append('promo_code',promo_code);
    data.append('id_service',id_service);
    data.append('booking_time',booking_time);
    data.append('pet_id',pet_id);
    data.append('vaccine_val',vaccine_val);
    data.append('vaccine_id',vaccine_id);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: add_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.href = '/bookingdetail/'+result.serviceid;
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}

let pos;
let map1;
let bounds;
let infoWindow;
let currentInfoWindow;
let service;
let infoPane;
function initMap() {
    // Styles a map in night mode.
    setTimeout(function(){ 
        console.log(_getlat);
        console.log(_getlong);

        _getlat = parseFloat($('#shop_lat').val());
        _getlong = parseFloat($('#shop_long').val());
        _name_shop = $('#name_shop').val();

        setTimeout(function(){ 
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: _getlat, 
                    lng: _getlong
                },
                zoom: 16,
                mapTypeId: "roadmap",
                
            });

            const infowindow = new google.maps.InfoWindow({
                content: "<h2>"+_name_shop+"</h2>",
            });


            var marker = new window.google.maps.Marker({
                map: map,
                position: {
                    lat: _getlat, 
                    lng: _getlong
                },
                icon: "http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png",
                // title: _name_shop,
            });
            marker.addListener("click", () => {
                infowindow.open(map, marker);
            });
        });
    });


    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
        pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        map1 = new google.maps.Map(document.getElementById('map1'), {
            center: pos,
            zoom: 15
        });

        getNearbyPlaces(pos);
       
       console.log(pos);

        /* TODO: Step 3B2, Call the Places Nearby Search */
    }, () => {
        // Browser supports geolocation, but user has denied permission
        handleLocationError(true, infoWindow);
    });
    } else {
    // Browser doesn't support geolocation
    handleLocationError(false, infoWindow);
    }
}


// Handle a geolocation error
function handleLocationError(browserHasGeolocation, infoWindow) {
    // Set default location to Sydney, Australia

    pos = {lat: 13.8040211, lng: 100.547996};
    map1 = new google.maps.Map(document.getElementById('map1'), {
        center: pos,
        zoom: 15
    });

    getNearbyPlaces(pos);

    /* TODO: Step 3B3, Call the Places Nearby Search */
}


function getNearbyPlaces(position) {
    console.log(position);
    let request = {
        location: position,
        // rankBy: google.maps.places.RankBy.DISTANCE,
        keyword: 'สัตวแพทย์',
        radius: 10000, //5000 = 5KM
    };

    service = new google.maps.places.PlacesService(map1);
    service.nearbySearch(request, nearbyCallback);

}

// Handle the results (up to 20) of the Nearby Search
function nearbyCallback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
    // createMarkers(results);
        console.log(results);
        genCardForShow(results);

    }
}



function genCardForShow(results){

    console.log(results);

    $('#addcard-nearby').html('');
    var txtnear;
    $.each(results, function(key, value) {   
        
        txtnear = '<div class="card">'+
                        '<div class="image">'+
                            '<img class="ui huge image" src="'+(value.photos ? value.photos[0].getUrl() : "https://www.petscarebusiness.com/public/themes/image/logo.png")+'" style="max-height: 165px;">'+
                        '</div>'+
                        '<div class="content">'+
                            '<a class="header" href="#">'+value.name+'</a>'+
                        '</div>'+
                    '</div>';

        console.log(key);
        if(key < 4){
            $('#addcard-nearby').append(txtnear); 
        }
    });

}