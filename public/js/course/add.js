$(function(){
	CKEDITOR.replace( 'description' );
    
    $('#status').dropdown();

	$('.btn-back').on('click', function(){
		window.location.href = "/admin/course";
	});

	
	$('.btn-save').on('click', function(){
        postAdd();
    });

    $('#enddate').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        }
    });

    $('#startdate').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        }
    });

});


function postAdd() {
    var name    		= $('#name').val();
    var price           = $('#price').val();
    var times    	    = $('#times').val();
    var description     = CKEDITOR.instances.description.getData();
    var status  		= $('#status').dropdown('get value');
    var enddate         = $('#enddate').calendar('get date');
    var startdate       = $('#startdate').calendar('get date');

    var add_url         = $('#add_url').data('url');
    var data            = new FormData();

    data.append('name', name);
    data.append('price', price);
    data.append('times', times);
    data.append('status', status);
    data.append('description', description);
    data.append('enddate', moment(enddate).format('YYYY-MM-DD'));
    data.append('startdate', moment(startdate).format('YYYY-MM-DD'));

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: add_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.href = '/admin/course';
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}

