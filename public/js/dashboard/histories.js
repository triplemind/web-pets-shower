$(function(){

	DTRender('#TBL_onlinelist', true);

	$('#order_status').dropdown();
	$('#rangestart').calendar({
		type: 'date',
		monthFirst: false,
        ampm: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		endCalendar: $('#rangeend')
	});
	$('#rangeend').calendar({
		type: 'date',
		monthFirst: false,
        ampm: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		startCalendar: $('#rangestart')
	});

	$('.btn-online-detail').on('click', function(){
		var order_id = $(this).data('id');
		window.location.href = "/admin/dashboard/orderdetail/"+order_id;
	});

	$('.btn-search').on('click', function(){
		msg_waiting();
        addFilterToSession();
    });

    $('.btn-clear').on('click', function(){
        msg_waiting();
    	var ajax_url 		= $('#ajax-center-url').data('url');
    	var method 			= 'clearFilterToSession'
    	$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: ajax_url,
	        data: {
	            'method' : method,
	        },
	        success: function(result) {
	            if (result.status === 'success')       window.location.reload();
	        },
	    });
    });

});


const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}

function msg_waiting() {
	let timerInterval;
	Swal.fire({
		title: '',
		html: 'LOADING',
		timer: 2000,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading()
			timerInterval = setInterval(() => {
				const content = Swal.getContent()
				if (content) {
					const b = content.querySelector('b')
					if (b) {
						b.textContent = Swal.getTimerLeft()
					}
				}
			}, 100)
		},
		onClose: () => {
			clearInterval(timerInterval)
		}
	}).then((result) => {
		/* Read more about handling dismissals below */
		if (result.dismiss === Swal.DismissReason.timer) {
			console.log('I was closed by the timer')
		}
	});
}

function addFilterToSession() {
    var rangestart   = $('#rangestart').calendar('get date');
    var rangeend     = $('#rangeend').calendar('get date');
    var order_status = $('#order_status').dropdown('get value');

    console.log(moment(rangestart).format('YYYY-MM-DD'), moment(rangeend).format('YYYY-MM-DD'));
    console.log(rangestart, rangeend);
    console.log(order_status);

    var ajax_url        = $('#ajax-center-url').data('url');
    var method          = 'addFilterToSession'

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'rangestart' : rangestart ? moment(rangestart).format('YYYY-MM-DD') : '',
            'rangeend' : rangeend ? moment(rangeend).format('YYYY-MM-DD') : '',
            'order_status' : (order_status != 'all') ? order_status : '',
        },
        success: function(result) {
            if (result.status === 'success')       window.location.reload();
        },
    });
}