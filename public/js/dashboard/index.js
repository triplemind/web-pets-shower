$(function(){

	$('#stat_range').dropdown({
		onChange: function (value1, text1) {
			console.log(value1);
			if(value1){
				msg_waiting();
	        	addFilterToSession();				
			}
		}
	});

	$('#rangestart').calendar({
		type: 'date',
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		onChange: function (date1) {

		},
		endCalendar: $('#rangedateend')
	});
	$('#rangedateend').calendar({
		type: 'date',
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		onChange: function (date2) {
			var date1 = $('#rangestart').calendar("get date");
			// var date2 = $('#rangedateend').calendar("get date");

			console.log(date1, date2);
			if(date2 && date1){
				msg_waiting();
				setTimeout(function(){ 
		        	addFilterToSession();
		        });
			}
		},
		startCalendar: $('#rangestart')
	});


	$('.btn-clear').on('click', function(){
        msg_waiting();
    	var ajax_url 		= $('#ajax-center-url').data('url');
    	var method 			= 'clearFilterToSession'
    	$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: ajax_url,
	        data: {
	            'method' : method,
	        },
	        success: function(result) {
	            if (result.status === 'success')       window.location.reload();
	        },
	    });
    });


	var ajax_url 		= $('#ajax-center-url').data('url');
	var method 			= 'getDataTochart'
	$.ajax({
	    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	    type: 'post',
	    url: ajax_url,
	    data: {
	        'method' : method,
	    },
	    success: function(result) {
	        if (result.status === 'success'){

	        	var ctx = document.getElementById('myChart');
				var myChart = new Chart(ctx, {
				    type: 'doughnut',
				    data: {
				        labels: result.txtstat_vac,
				        datasets: [{
				            label: '# of Votes',
				            data: result.valstat_vac,
				            backgroundColor: [
				                'rgba(255, 99, 132, 0.5)',
				                'rgba(54, 162, 235, 0.5)',
				                'rgba(255, 206, 86, 0.5)',
				                'rgba(45, 229, 89, 0.5)',
				                'rgba(153, 102, 255, 0.5)',
				            ],
				            borderColor: [
				                'rgba(255, 99, 132, 1)',
				                'rgba(54, 162, 235, 1)',
				                'rgba(255, 206, 86, 1)',
				                'rgba(75, 192, 192, 1)',
				                'rgba(153, 102, 255, 1)',
				            ],
				            borderWidth: 1
				        }]
				    },
				    options: {
				        legend: {
				            display: true,
				            labels: {
				                fontColor: '#000',
				                fontSize: 28,
				                padding: 32,
				            },
				            position: 'top',
				            align: 'center',
				        },
				        tooltips: {
				        	titleFontSize: 32,
			                bodyFontSize: 32,
			                caretSize: 50,
			                caretPadding: 12,
			                cornerRadius: 1,
			                position: 'average',
				        },
				        title:{
				        	display: true,
				        	text:'วัคซีนยอดนิยม',
				        	position: 'bottom',
				        	fontSize: 45,
				        }
				    }
				});



				var ctx1 = document.getElementById('myChart1');
				var myChart = new Chart(ctx1, {
				    type: 'doughnut',
				    data: {
				        labels: result.txttstat_ser_vac,
				        datasets: [{
				            label: '# of Votes',
				            data: result.getstat_ser_vac,
				            backgroundColor: [
				                'rgba(255, 99, 132, 0.5)',
				                'rgba(54, 162, 235, 0.5)',
				            ],
				            borderColor: [
				                'rgba(255, 99, 132, 1)',
				                'rgba(54, 162, 235, 1)',
				            ],
				            borderWidth: 1
				        }]
				    },
				    options: {
				        legend: {
				            display: true,
				            labels: {
				                fontColor: '#000',
				                fontSize: 16,
				                padding: 32,
				            },
				            position: 'right',
				            align: 'center',
				        },
				        tooltips: {
				        	titleFontSize: 15,
			                bodyFontSize: 15,
			                caretSize: 26,
			                caretPadding: 2,
			                cornerRadius: 1,
			                position: 'average',
				        },
				        title:{
				        	display: true,
				        	text:'ช่วงเวลาใช้บริการวัคซีน',
				        	position: 'bottom',
				        	fontSize: 20,
				        }
				    }
				});


				var ctx2 = document.getElementById('myChart2');
				var myChart = new Chart(ctx2, {
				    type: 'doughnut',
				    data: {
				        labels: result.txttstat_ser_bath,
				        datasets: [{
				            label: '# of Votes',
				            data: result.getstat_ser_bath,
				            backgroundColor: [
				                'rgba(255, 99, 132, 0.5)',
				                'rgba(54, 162, 235, 0.5)',
				            ],
				            borderColor: [
				                'rgba(255, 99, 132, 1)',
				                'rgba(54, 162, 235, 1)',
				            ],
				            borderWidth: 1
				        }]
				    },
				    options: {
				        legend: {
				            display: true,
				            labels: {
				                fontColor: '#000',
				                fontSize: 16,
				                padding: 32,
				            },
				            position: 'right',
				            align: 'center',
				        },
				        tooltips: {
				        	titleFontSize: 15,
			                bodyFontSize: 15,
			                caretSize: 26,
			                caretPadding: 2,
			                cornerRadius: 1,
			                position: 'average',
				        },
				        title:{
				        	display: true,
				        	text:'ช่วงเวลาใช้บริการอาบน้ำ-ตัดขน',
				        	position: 'bottom',
				        	fontSize: 20,
				        }
				    }
				});

	        }
	    },
	});



	DTRender('#TBL_onlinelist', true);

	$('.btn-online-detail').on('click', function(){
		var order_id = $(this).data('id');
		window.location.href = "/admin/dashboard/orderdetail/"+order_id;
	});

});


const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}


function msg_waiting() {
	let timerInterval;
	Swal.fire({
		title: '',
		html: 'LOADING',
		timer: 2000,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading()
			timerInterval = setInterval(() => {
				const content = Swal.getContent()
				if (content) {
					const b = content.querySelector('b')
					if (b) {
						b.textContent = Swal.getTimerLeft()
					}
				}
			}, 100)
		},
		onClose: () => {
			clearInterval(timerInterval)
		}
	}).then((result) => {
		/* Read more about handling dismissals below */
		if (result.dismiss === Swal.DismissReason.timer) {
			console.log('I was closed by the timer')
		}
	});
}

function addFilterToSession() {
    var stat_range 	= $('#stat_range').dropdown('get value');
    var rangestart 	= $('#rangestart').calendar("get date");
    var rangeend 	= $('#rangedateend').calendar("get date");

    // console.log(rangestart,rangeend);

    var ajax_url        = $('#ajax-center-url').data('url');
    var method          = 'addFilterToSessionChart'

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'stat_range' : stat_range,
            'rangestart' : rangestart ? moment(rangestart).format('YYYY-MM-DD') : '',
            'rangeend' : rangeend ? moment(rangeend).format('YYYY-MM-DD') : '',
        },
        success: function(result) {
            if (result.status === 'success')       window.location.reload();
        },
    });
}