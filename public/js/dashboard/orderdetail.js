let _vacList = [];

$(function() {

	$('#bank_payment').dropdown();
	// $('#namevaccin').dropdown();
	$('#veterinarian').dropdown();

	$('#startdate').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		}
	});


	var method      = 'getVaccine';
    var ajax_url    = $('#ajax-center-url').data('url');
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'id_shop' : $('#shop_id').val(),
        },
        success: function(result) {
            if(result.status == 'success'){
                console.log(result.data);
                // console.log(JSON.parse(result.data));
                _vacList = result.data;
                let arr_vac = sort_by_key(_vacList, 'vaccine_name');
                
                $.each(arr_vac, function(key, value) {   
                    $('#namevaccin')
                    .append($("<option></option>")
                    .attr("value", value.vaccine_ID)
                    .text(value.vaccine_name+" : "+value.properties)); 

                    if($('#vaccine_id').val() == value.vaccine_ID){
                		$('#companytxt').val(value.company);
                		$('#companytxtshow').text(value.company);
                		$('#vaccin').val(value.properties+" ("+value.vaccine_type+")");
                		// $('#namevaccin').dropdown('set selected', value.vaccine_ID);
                		setTimeout(function(){ 
		                    $('#namevaccin').dropdown('set selected', value.vaccine_ID).dropdown({
								onChange: function (value,text) {
									console.log(value);
									console.log(text);
						            console.log(_vacList);
									angular.forEach(_vacList, function (value2) {
										if (value == value2.vaccine_ID) {
											$('#companytxt').val(value2.company);
											$('#vaccin').val(value2.properties+" ("+value2.vaccine_type+")");
										}
									});
								},
							});
						});
                    }else{
                    	$('#namevaccin').dropdown({
							onChange: function (value,text) {
								console.log(value);
								console.log(text);
					            console.log(_vacList);
								angular.forEach(_vacList, function (value2) {
									if (value == value2.vaccine_ID) {
										$('#companytxt').val(value2.company);
										$('#companytxtshow').text(value2.company);
										$('#vaccin').val(value2.properties+" ("+value2.vaccine_type+")");
									}
								});
							},
						});
                    }

                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.data
                });
            }
        }
    });

    console.log($('#vaccine_id').val());


    $('#date_payment').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        },
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        },
    });

    $('#time_payment').calendar({
        type: 'time',
        popupOptions: {
            observeChanges: false
        }
    });

	$('.btn-saveCourseRC').on('click', function(){
		postAddCourseRecord();
	});

	$('.btn-back').on('click', function(){
		window.location.href = '/admin/dashboard/histories';
	});

	$('.btn-saveVaccineRC').on('click', function(){
		postsaveVaccineRC();
	});

	$('.btn-edit-payslip').on('click', function(){
		$('.pic_payment_F').css('display', 'none');
		$('.pic_payment_Edit').css('display', 'unset');
	});

	$('.btn-cancel-payment').on('click', function(){
		$('.pic_payment_Edit').css('display', 'none');
		$('.pic_payment_F').css('display', 'unset');
	});

	$('.btn-confirm').on('click', function(){
		Swal.fire({
	        title: "คุณต้องการยืนยันการชำระเงินของรายการจองนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#18db32",
	        cancelButtonText: "ไม่ใช่",
	        cancelButtonColor: "",
	        confirmButtonText: "ใช่"
	    }).then(result => {
	        if (result.value) {
	            CompletedOrder();
	        }
	    });
	});

	$('.btn-finish').on('click', function(){
		Swal.fire({
	        title: "การบริการนี้เสร็จสิ้นแล้วใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#18db32",
	        cancelButtonText: "ไม่ใช่",
	        cancelButtonColor: "",
	        confirmButtonText: "ใช่"
	    }).then(result => {
	        if (result.value) {
	            FinishOrder();
	        }
	    });
	});

	$('.btn-cancel').on('click', function(){
		Swal.fire({
	        title: "คุณต้องการยกเลิกรายการจองนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ไม่ใช่",
	        cancelButtonColor: "",
	        confirmButtonText: "ใช่"
	    }).then(result => {
	        if (result.value) {
	            CancelOrder();
	        }
	    });
	});

	$('.btn-cf_postponeAdate').on('click', function(){
		Swal.fire({
	        icon: 'info',
	        title: "ยืนยันการขอเลื่อนนัดใช่หรือไม่",
	        text: "หากยืนยัน ระบบจะทำการตรวจสอบและปรับปรุงข้อมูลการจอง",
	        showCancelButton: true,
	        confirmButtonColor: "#21ba45",
	        cancelButtonText: "ปิด",
	        cancelButtonColor: "",
	        confirmButtonText: "ยืนยัน"
	    }).then(result => {
	        if (result.value) {
	        	console.log('sds');
	            ConfirmPostponeADate();
	        }
	    });
	});


	$('.btn-save-payment').on('click', function(){
        var addslip_url     = $('#addslip-url').data('url');
        var id 				= $('#pos_id').val();
        var date_payment    = $('#date_payment').calendar('get date');
        var time_payment    = $('#time_payment').calendar('get date');
        var bank_payment    = $('#bank_payment').dropdown('get value');

        if(date_payment != null){
        	date_payment = moment(date_payment).locale('th').format('YYYY-MM-DD');
        }else{
        	date_payment = '';
        }

        if(time_payment != null){
        	time_payment = moment(time_payment).locale('th').format('LT');
        }else{
        	time_payment = '';
        }


        console.log(date_payment);
        console.log(time_payment);

        var data        = new FormData();
	    if (typeof($('#pic_payment')[0]) !== 'undefined') {

	        jQuery.each(jQuery('#pic_payment')[0].files, function(i, file) {
	            data.append('pic_payment', file);
	            // console.log(file);
	        });
	    }
		data.append('id',id);
        data.append('date_payment',date_payment);
        data.append('time_payment',time_payment);
        data.append('bank_payment',bank_payment);

        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: 'post',
            url: addslip_url,
           	data: data,
	        contentType: false,
	        processData:false,
	        cache: false,
            success: function(result) {
                if(result.status == 'success'){
                   setTimeout(function(){ 
	                    $("body").toast({
	                        class: "success",
	                        position: 'bottom right',
	                        message: `บันทึกเสร็จสิ้น`
	                    });
	                    setTimeout(function(){ 
	                        window.location.reload();
	                    }, 1000);
	                });
                } // End if check s tatus success.

                if(result.status == 'error'){
                    $("body").toast({
                        class: "error",
                        position: 'bottom right',
                        message: result.data
                    });
                }
            }
        }); 
    });

});


function sort_by_key(array, key)
{
	return array.sort(function(a, b)
	{
		var x = a[key]; var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}



function ShowimgModal() {
	$('.ui.basic.modal').modal('show');
}

function ShowVaccineImgModal() {
	$('.vaccineImgModal').modal('show');
}

function postsaveVaccineRC() {
	var addVaccineRC_url    = $('#addVaccineRC-url').data('url');
    
    var vaccineRC_id 		= $('#vaccineRC_id').val();
    var vaccin 				= $('#vaccin').val();
    var namevaccin    		= $('#namevaccin').dropdown('get text');
    var vaccine_ID    		= $('#namevaccin').dropdown('get value');
    var veterinarian    	= $('#veterinarian').dropdown('get text');
    var veterinary_ID    	= $('#veterinarian').dropdown('get value');
    var notevaccin    		= $('#notevaccin').val();

    console.log(veterinarian);
    console.log(veterinary_ID);

    var data        = new FormData();
    // if (typeof($('#img')[0]) !== 'undefined') {

    //     jQuery.each(jQuery('#img')[0].files, function(i, file) {
    //         data.append('img[]', file);
    //         console.log(file);
    //     });
    // }

    console.log(vaccineRC_id);
	data.append('vaccineRC_id',vaccineRC_id);
	data.append('vaccin',vaccin);
    data.append('namevaccin',namevaccin);
    data.append('vaccine_ID',vaccine_ID);
    data.append('veterinarian',veterinarian);
    data.append('veterinary_ID',veterinary_ID);
    data.append('notevaccin',notevaccin);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: addVaccineRC_url,
       	data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
               setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.reload();
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        }
    }); 

}


function CancelOrder() {
	var pos_id = $('#pos_id').val();

	var method 		= 'CancelOrder';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
		  	 'pos_id' : pos_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ยกเลิกรายการเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

function CompletedOrder() {
	var pos_id 		= $('#pos_id').val();
	var status 		= 'ชำระเงินแล้ว';

	var method 		= 'CompletedOrder';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
	  	 	'pos_id' : pos_id,
	  	 	'status' : status,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ดำเนินการจัดคิวเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

function FinishOrder() {
	var pos_id 		= $('#pos_id').val();
	var status 		= 'การใช้บริการเสร็จสิ้น';

	var method 		= 'FinishOrder';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
	  	 	'pos_id' : pos_id,
	  	 	'status' : status,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ดำเนินการเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}


function ConfirmPostponeADate(){
	var pos_id 		= $('#pos_id').val();

	var method 		= 'ConfirmPostponeADate';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
	  	 	'pos_id' : pos_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						// message: `ดำเนินการจัดคิวเสร็จสิ้น`
						message: result.msg
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}


function postAddCourseRecord(){
	var pos_id 			= $('#pos_id').val();
	var thetime 		= $('#thetime').val();
	var startdate 		= $('#startdate').calendar('get date');

	var method 		= 'postAddCourseRecord';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
	  	 	'pos_id' : pos_id,
	  	 	'thetime' : thetime,
	  	 	'startdate' : startdate ? moment(startdate).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'),
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
						// message: result.msg
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}