let _SPpetsArr = [];
var x = 0;
var txtshow;
$(function(){

	$('.btn-add-pet').on('click', function(){
		$('#gender').dropdown();
		
		$('#birthday').calendar({
			type: 'date',
			popupOptions: {
				observeChanges: false
			}
		});

		$("#addMyPetModal").modal({
			closable: false,
			onDeny: function() {
				$('#addpetform').form('clear');
			},
			onApprove: function() {
				addNewMyPets();
			}
		}).modal("show");
	});

	$('.btn-add-sp-pet').on('click', function(){
		$('#SP_gender').dropdown();

		$("#addMySPPetModal").modal({
			closable: false,
			onDeny: function() {
				$('#addSPpetform').form('clear');
				_SPpetsArr = [];

				$('#sppets_show').html('');

				console.log(_SPpetsArr);

			},
			onApprove: function() {
				addNewMySPPets();
			}
		}).modal("show");
	});


	$('#pet_type').dropdown({
		onChange: function(value, text) {
            console.log(value);
            console.log(text);

            $('#breed').empty();

            var method      = 'getSpeciesByPetType';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'pettype' : value,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // console.log(result.data);
                        $('#breed')
                            .append($("<option></option>")
                            .text('เลือกสายพันธุ์สัตว์เลี้ยง')); 

                        $.each(result.species, function(key, value) {   
                            $('#breed')
                            .append($("<option></option>")
                            .attr("value", value.name)
                            .text(value.name)); 
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }
	});

	$('#breed').dropdown();


	$('#SP_pet_type').dropdown({
		onChange: function(value, text) {
            console.log(value);
            console.log(text);

            $('#SP_breed').empty();

            var method      = 'getSpeciesByPetType';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'pettype' : value,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // console.log(result.data);
                        $('#SP_breed')
                            .append($("<option></option>")
                            .text('เลือกสายพันธุ์สัตว์เลี้ยง')); 

                        $.each(result.species, function(key, value) {   
                            $('#SP_breed')
                            .append($("<option></option>")
                            .attr("value", value.name)
                            .text(value.name)); 
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }
	});

	$('#SP_breed').dropdown();



	$('.btn-addnew').on('click', function(){
		$('#sppets_show').html('');
		var addlist = {
						arr_id: x,
						name: $('#SP_pet_name').val(),
						color: $('#SP_color').val(),
					};

		_SPpetsArr.push(addlist);

		console.log(_SPpetsArr);

		setTimeout(function(){
			$.each(_SPpetsArr, function(key, value) {   
                
				txtshow = '<div class="three fields">'+				
				            '<div class="seven wide field">'+
				                '<label>'+value.name+'</label>'+
				            '</div>'+
				            '<div class="seven wide field">'+
				                '<label>'+value.color+'</label>'+
				            '</div>'+
				            '<div class="two wide field">'+
				                '<i class="large window close link icon" onclick="RemoveArr('+value.arr_id+')"></i>'+
				            '</div>'+
			            '</div>';

				$('#sppets_show').append(txtshow);
            });

            $('#SP_pet_name').val('');
            $('#SP_color').val('');
		});

		x++

	});


});

function RemoveArr(_arrid){
	console.log(_arrid);

	var check_delrow = _SPpetsArr.filter(c => c.arr_id == _arrid);

	if(check_delrow.length != 0){
		
		_SPpetsArr.splice(_arrid, 1);

		x--;
		
		console.log(_SPpetsArr);
		setTimeout(function(){
			$("#sppets_show").html("");
			$.each(_SPpetsArr, function(key, value) {   
                
				txtshow = '<div class="three fields">'+				
				            '<div class="seven wide field">'+
				                '<label>'+value.name+'</label>'+
				            '</div>'+
				            '<div class="seven wide field">'+
				                '<label>'+value.name+'</label>'+
				            '</div>'+
				            '<div class="two wide field">'+
				                '<i class="large window close link icon" onclick="RemoveArr('+value.arr_id+')"></i>'+
				            '</div>'+
			            '</div>';

				$('#sppets_show').append(txtshow);
            });
	    });

	    console.log(x);

	}


}



function addNewMyPets() {
	var pet_name 		= $('#pet_name').val();
	// var pet_type 		= $('#pet_type').val();
	var pet_txt_type	= "สัตว์เลี้ยง";
	var breed 			= $('#breed').val();
	var color 			= $('#color').val();
	var weight 			= $('#weight').val();
	var notedescription = $('#notedescription').val();
	var birthday 		= $('#birthday').calendar('get date');
	var gender			= $('#gender').dropdown('get value');
	var pet_type		= $('#pet_type').dropdown('get value');

	console.log(birthday);
	console.log(moment(birthday).format('YYYY-MM-DD'));

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	if (typeof($('#pic')[0]) !== 'undefined') {

        jQuery.each(jQuery('#pic')[0].files, function(i, file) {
            data.append('pic', file);
            // console.log(file);
        });
    }

	data.append('pet_txt_type', pet_txt_type);
	data.append('pet_name', pet_name);
	data.append('pet_type',pet_type);
	data.append('breed',breed);
	data.append('color',color);
	data.append('weight',weight);
	data.append('notedescription',notedescription);
	data.append('birthday',moment(birthday).format('YYYY-MM-DD'));
	data.append('gender',gender);

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/mypets';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}


function addNewMySPPets() {
	var pet_txt_type	= "สัตว์จรจัด";
	var breed 			= $('#SP_breed').val();
	var gender			= $('#SP_gender').dropdown('get value');
	var pet_type		= $('#SP_pet_type').dropdown('get value');

	console.log(_SPpetsArr);

	var add_url			= $('#addsp-url').data('url');
	var data  			= new FormData();

	data.append('info', JSON.stringify(_SPpetsArr));
	data.append('pet_txt_type', pet_txt_type);
	data.append('pet_name', pet_name);
	data.append('pet_type',pet_type);
	data.append('breed',breed);
	data.append('color',color);
	data.append('gender',gender);

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/mypets';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}