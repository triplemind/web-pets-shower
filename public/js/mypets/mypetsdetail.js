$(function(){

	// DTRender('#bookingVaccineTBL', true);
	// DTRender('#vaccineTBL', true);

	$('.btn-saveCourseRC').on('click', function(){
		postAddCourseRecord();
	});

	// $('#startdate').calendar({
	// 	type: 'date',
	// 	minDate: new Date(),
	// 	monthFirst: false,
	// 	formatter: {
	// 		date: function (date, settings) {
	// 			if (!date) return '';
	// 				var day = date.getDate();
	// 				var month = date.getMonth() + 1;
	// 				var year = date.getFullYear();
	// 			return day + '/' + month + '/' + year;
	// 		}
	// 	}
	// });

	$('#startdate').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		onChange: function (date) {

			console.log($('#id_shop').val());
			
			var method      = 'getVaccine';
            var ajax_url    = $('#Booking-ajax-center-url').data('url');
	        $.ajax({
	            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	            type: 'post',
	            url: ajax_url,
	            data: {
	                'method' : method,
	                'id_shop' : $('#id_shop').val(),
	                'id_service' : 'PG0001',
	            },
	            success: function(result) {
	                if(result.status == 'success'){
	                    console.log(result.data);

	                    var txt_time = "";

	                	console.log("sss:",result.services);
	                    if(result.services){
	                    	$('#booking_time').empty();

	                    	$('#booking_time')
	                            .append($("<option></option>")
	        					.text('เลือกช่วงเวลา')); 

	                    	$.each(result.services, function(key, value) {  
	                    		if(value.time_period == 'เช้า'){
									txt_time = "เช้า : "+((!value.startTime) ? "09.00น.- 12.30น." : value.startTime);
	                    		}else if(value.time_period == 'บ่าย'){
	                    			txt_time = "บ่าย : "+((!value.endTime) ? "13.30น.-19.00น." : value.endTime);
	                    		}

	                            $('#booking_time')
	                            .append($("<option></option>")
	                            .attr("value", value.time_period)
	        					.text(txt_time)); 
	                        });
	                    }
	                } // End if check s tatus success.

	                if(result.status == 'error'){
	                    $("body").toast({
	                        class: "error",
	                        position: 'bottom right',
	                        message: result.data
	                    });
	                }
	            }
	        });

			//จะขึ้นให้เลือก time
			$('#booking_time').dropdown('clear').dropdown({
				onChange: function (value1, text1) {
					console.log(value1);
					if(value1){
						var id_shop  = $('#id_shop').val();
						var booking_time = value1;
						checkBooing(date, 'PG0001', booking_time, id_shop);							
					}
				}
			});
		}
	});

	$('.accordion').accordion();
	
	$('#pet_type').dropdown({
		onChange: function(value, text) {
            console.log(value);
            console.log(text);

            $('#breed').empty();

            var method      = 'getSpeciesByPetType';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'pettype' : value,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // console.log(result.data);
                        $.each(result.species, function(key, value) {   
                            $('#breed')
                            .append($("<option></option>")
                            .attr("value", value.name)
                            .text(value.name)); 
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }
	});

	$('#breed').dropdown();

	$('.btn-edit-pet').on('click', function(){
		$('#gender').dropdown();
		
		$('#birthday').calendar({
			type: 'date',
			popupOptions: {
				observeChanges: false
			}
		});

		var pet_id = $(this).data('pet_id');

		var method 		= 'getPetData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'pet_id' : pet_id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$('#pet_id').val(result.data.pet_id);
					$('#pet_name').val(result.data.name);
					// $('#pet_type').val(result.data.type);
					$('#breed').val(result.data.breed);
					$('#gender').dropdown('set selected', result.data.gender);
					$('#pet_type').dropdown('set selected', result.data.type);
					$('#weight').val(result.data.weight);
					$('#color').val(result.data.color);
					$('#notedescription').val(result.data.notedescription);
					$('#birthday').calendar('set date', result.data.birthday);

					setTimeout(function(){
						$("#editMyPetModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editNewMyPets();
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});
	});



	$('.btn-emove-pet').on('click', function(){
		console.log($(this).data('pet_id'));
		var pet_id = $(this).data('pet_id');
		Swal.fire({
	        title: "คุณต้องการลบข้อมูลสัตว์เลี้ยงนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deletePet(pet_id);
	        }
	    });
	});


});

function editNewMyPets() {
	var pet_name 		= $('#pet_name').val();
	var pet_id 			= $('#pet_id').val();
	// var pet_type 		= $('#pet_type').val();
	var breed 			= $('#breed').val();
	var color 			= $('#color').val();
	var weight 			= $('#weight').val();
	var notedescription = $('#notedescription').val();
	var birthday 		= $('#birthday').calendar('get date');
	var gender			= $('#gender').dropdown('get value');
	var pet_type		= $('#pet_type').dropdown('get value');

	console.log(birthday);
	console.log(moment(birthday).format('YYYY-MM-DD'));

	var edit_url			= $('#edit-url').data('url');
	var data  			= new FormData();

	if (typeof($('#pic')[0]) !== 'undefined') {

        jQuery.each(jQuery('#pic')[0].files, function(i, file) {
            data.append('pic', file);
            // console.log(file);
        });
    }

	data.append('pet_id', pet_id);
	data.append('pet_name', pet_name);
	data.append('pet_type',pet_type);
	data.append('breed',breed);
	data.append('color',color);
	data.append('weight',weight);
	data.append('notedescription',notedescription);
	data.append('birthday',moment(birthday).format('YYYY-MM-DD'));
	data.append('gender',gender);

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: edit_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}


function deletePet(pet_id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'pet_id' : pet_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/mypets';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}



function addBookingTime(id_service, id_shop, id_petservice){
	var booking_time;
	var booking_date;

	$('.tiny.modal').modal({
		closable: false,
		onDeny: function() {
		},
		onApprove: function() {
			// var bookTime = $('#booking_time').dropdown('get value');
			postAddBookingTime(id_petservice, booking_time, booking_date);
		}
	}).modal("show");

	console.log(id_service, id_shop, id_petservice);

	$('#booking_date').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		onChange: function (date) {

			booking_date = date;

			setTimeout(function(){ 
				$('#booking_time').dropdown('clear').dropdown({
					onChange: function (value, text) {
						console.log(value);
						console.log(booking_date);
						if(value){
							booking_time 	= value;
							checkBooing(booking_date, id_service, booking_time, id_shop);							
						}
					}
				});
			});
		}
	});
	
}


//เช็คว่ามีการจองรึยัง
function checkBooing(date, id_service, booking_time, id_shop){
	console.log(date, id_service, booking_time, id_shop);
	var id_shop           = id_shop;
    var booking_date      = date;
    var id_service        = id_service;
    var booking_time      = booking_time;

    booking_date = booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

    var ajax_url        = $('#Booking-ajax-center-url').data('url');
    var method        	= 'checkBooing';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
        	'method' : method,
        	'id_shop' : id_shop,
        	'booking_date' : booking_date,
        	'id_service' : id_service,
        	'booking_time' : booking_time,
        },
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 

                	$txt_que = "รับ "+result.allque+" คิว || ว่าง "+result.allowque+" คิว";

                	$("#que_txt").text($txt_que);

                	if(result.bookingallow){
                		$("body").toast({
	                        class: "success",
	                        position: 'bottom right',
	                        message: result.msg
	                    });

	                    $('.btn-save').removeClass('disabled');
                	}else{
                		$("body").toast({
	                        class: "error",
	                        position: 'bottom right',
	                        message: result.msg
	                    });
                		
	                    $('.btn-save').addClass('disabled');
                	}

                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
}



function postAddBookingTime(id_petservice, booking_time, booking_date){
	// var bookTime = $('#booking_time').dropdown('get value');

	console.log(id_petservice, booking_time, booking_date);

	var ajax_url        = $('#Booking-ajax-center-url').data('url');
    var method        	= 'postAddBookingTime';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
        	'method' : method,
        	'id_petservice' : id_petservice,
        	'booking_time' : booking_time,
        	'booking_date' : booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'),
        },
        success: function(result) {
            if(result.status == 'success'){
            	$("body").toast({
                    class: "success",
                    position: 'bottom right',
                    message: 'บันทึกเสร็จสิ้น'
                });
                setTimeout(function(){ 
                	window.location.reload();
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
}


function getModalVaccineDetail(vRC_id, envimg){

	$('.vaccine_name').html('');
	$('.properties').html('');
	$('.company').html('');
	$('.veterinarian').html('');
	$('.description').html('');
	$('.img_vaccine').html('');


	console.log(vRC_id,envimg);

	var ajax_url    = $('#ajax-center-url').data('url');
    var method      = 'getVaccineRCDetail';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
        	'method' : method,
        	'vRC_id' : vRC_id,
        },
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 

                	console.log(result.data);

                	if(result.data){

	                	$('.vaccine_name').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.vaccine_name+" ("+result.data.vaccine.drug_name+")");
	                	$('.properties').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.properties);
	                	$('.company').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.company);
	                	$('.veterinarian').append('<span><i class="dog icon"></i></span>'+result.data.veterinarian);
	                	$('.description').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.description);
	                	$('.img_vaccine').append('<img class="ui small image" src="'+result.data.vaccine.img_vaccine.replace("/public", envimg)+'">');


						$('.small.modal').modal({
							closable: false,
						}).modal("show");
                	}

                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
	
	
}



const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		responsive: true,
		rowReorder: {
            selector: 'td:nth-child(2)'
        },
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}

function postAddCourseRecord(){
	var course_id 		= $('#course_id').val();
	var id_petservice 	= $('#id_petservice').val();
	var booking_time 	= $('#booking_time').dropdown('get value');
	var startdate 		= $('#startdate').calendar('get date');

	var method 		= 'postAddCourseRecord';
	var ajax_url	= $('#dashboard-ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
	  	 	'id_petservice' : id_petservice,
	  	 	'course_id' : course_id,
	  	 	'booking_time' : booking_time,
	  	 	'startdate' : startdate ? moment(startdate).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'),
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
						// message: result.msg
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}


let pos;
let map;
let bounds;
let infoWindow;
let currentInfoWindow;
let service;
let infoPane;
function initMap() {
    // Try HTML5 geolocation
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
        pos = {
	        lat: position.coords.latitude,
	        lng: position.coords.longitude
        };

        map = new google.maps.Map(document.getElementById('map'), {
	        center: pos,
	        zoom: 15
        });

        getNearbyPlaces(pos);
       
       console.log(pos);

        /* TODO: Step 3B2, Call the Places Nearby Search */
    }, () => {
        // Browser supports geolocation, but user has denied permission
        handleLocationError(true, infoWindow);
    });
    } else {
    // Browser doesn't support geolocation
    handleLocationError(false, infoWindow);
    }
}

// Handle a geolocation error
function handleLocationError(browserHasGeolocation, infoWindow) {
    // Set default location to Sydney, Australia

    pos = {lat: 13.8040211, lng: 100.547996};
    map = new google.maps.Map(document.getElementById('map'), {
	    center: pos,
	    zoom: 15
    });

    getNearbyPlaces(pos);

    /* TODO: Step 3B3, Call the Places Nearby Search */
}


function getNearbyPlaces(position) {
	console.log(position);
    let request = {
	    location: position,
	    // rankBy: google.maps.places.RankBy.DISTANCE,
	    keyword: 'สัตวแพทย์',
	    radius: 10000, //5000 = 5KM
    };

    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, nearbyCallback);

}

// Handle the results (up to 20) of the Nearby Search
function nearbyCallback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
    // createMarkers(results);
	    console.log(results);
	    genCardForShow(results);

    }
}

function genCardForShow(results){

    console.log(results);

    $('#addcard-nearby').html('');
    var txtnear;
    $.each(results, function(key, value) {   
        
    	txtnear = '<div class="card">'+
						'<div class="image">'+
							'<img class="ui huge image" src="'+(value.photos ? value.photos[0].getUrl() : "https://www.petscarebusiness.com/public/themes/image/logo.png")+'" style="max-height: 165px;">'+
						'</div>'+
						'<div class="content">'+
							'<a class="header" href="#">'+value.name+'</a>'+
						'</div>'+
					'</div>';

		console.log(key);
		if(key < 4){
   	 		$('#addcard-nearby').append(txtnear); 
		}
    });

}
