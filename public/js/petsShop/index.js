$(function(){

	DTRender('#TBL_petsShop', true);

	$('.viewpet').on('click', function(){
		var id = $(this).data('id');

		window.location.href = "/admin/petsShop/view/"+id;
	});

	$('.btn-managespecies').on('click', function(){
		window.location.href = "/admin/petsShop/managespecies";
	});

	$('.btn-managepettype').on('click', function(){
		window.location.href = "/admin/petsShop/managepettype";
	});

	// console.log('ssss');
	$('.btn-addpet').on('click', function(){
		$('#gender').dropdown();
		$('#shop_id').dropdown();
		
		$('#birthday').calendar({
			type: 'date',
			popupOptions: {
				observeChanges: false
			}
		});

		$("#addMyPetModal").modal({
			closable: false,
			onDeny: function() {
				$('#addpetform').form('clear');
			},
			onApprove: function() {
				addNewMyPets();
			}
		}).modal("show");
	});
	
	$('#pet_type').dropdown({
		onChange: function(value, text) {
            console.log(value);
            console.log(text);

            $('#breed').empty();

            var method      = 'getSpeciesByPetType';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'pettype' : value,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // console.log(result.data);
                        $('#breed')
                            .append($("<option></option>")
                            .text('เลือกสายพันธุ์สัตว์เลี้ยง')); 

                        $.each(result.species, function(key, value) {   
                            $('#breed')
                            .append($("<option></option>")
                            .attr("value", value.name)
                            .text(value.name)); 
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }
	});

	$('#breed').dropdown();


	$('#ed_pet_type').dropdown({
		onChange: function(value, text) {
            console.log(value);
            console.log(text);

            $('#ed_breed').empty();

            var method      = 'getSpeciesByPetType';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'pettype' : value,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // console.log(result.data);
                        $.each(result.species, function(key, value) {   
                            $('#ed_breed')
                            .append($("<option></option>")
                            .attr("value", value.name)
                            .text(value.name)); 
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }
	});

	$('#ed_breed').dropdown();


	$('.editpet').on('click', function(){
		$('#ed_gender').dropdown();
		$('#ed_shop_id').dropdown();
		
		$('#ed_birthday').calendar({
			type: 'date',
			popupOptions: {
				observeChanges: false
			}
		});

		var id = $(this).data('id');

		var method 		= 'getPetData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'id' : id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$.each(result.species, function(key, value) {   
                        $('#ed_breed')
                        .append($("<option></option>")
                        .attr("value", value.name)
                        .text(value.name)); 
                    });

					$('#ed_petsShop_id').val(result.data.id);
					$('#ed_pet_name').val(result.data.pet.name);
					$('#ed_pet_type').dropdown('set selected', result.data.pet.type);
					$('#ed_breed').dropdown('set selected', result.data.pet.breed);
					$('#ed_gender').dropdown('set selected', result.data.pet.gender);
					$('#ed_shop_id').dropdown('set selected', result.data.shop_id);
					$('#ed_weight').val(result.data.pet.weight);
					$('#ed_color').val(result.data.pet.color);
					$('#ed_notedescription').val(result.data.pet.notedescription);
					$('#ed_birthday').calendar('set date', result.data.pet.birthday);

					setTimeout(function(){
						$("#editMyPetModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editNewMyPets();
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});
	});


	$('.deletepet').on('click', function(){
		var id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบข้อมูลสัตว์เลี้ยงนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deletePet(id);
	        }
	    });
	});

});

function addNewMyPets() {
	var shop_id 		= $('#shop_id').dropdown('get value');
	var pet_name 		= $('#pet_name').val();
	// var pet_type 		= $('#pet_type').val();
	var breed 			= $('#breed').val();
	var color 			= $('#color').val();
	var weight 			= $('#weight').val();
	var notedescription = $('#notedescription').val();
	var birthday 		= $('#birthday').calendar('get date');
	var gender			= $('#gender').dropdown('get value');
	var pet_type		= $('#pet_type').dropdown('get value');

	console.log(birthday);
	console.log(moment(birthday).format('YYYY-MM-DD'));

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	if (typeof($('#pic')[0]) !== 'undefined') {

        jQuery.each(jQuery('#pic')[0].files, function(i, file) {
            data.append('pic', file);
            // console.log(file);
        });
    }

	data.append('shop_id', shop_id);
	data.append('pet_name', pet_name);
	data.append('pet_type',pet_type);
	data.append('breed',breed);
	data.append('color',color);
	data.append('weight',weight);
	data.append('notedescription',notedescription);
	data.append('birthday',moment(birthday).format('YYYY-MM-DD'));
	data.append('gender',gender);

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/petsShop';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}

function editNewMyPets() {
	var pet_name 		= $('#ed_pet_name').val();
	var petshop_id 		= $('#ed_petsShop_id').val();
	// var pet_type 		= $('#ed_pet_type').val();
	var breed 			= $('#ed_breed').val();
	var color 			= $('#ed_color').val();
	var weight 			= $('#ed_weight').val();
	var notedescription = $('#ed_notedescription').val();
	var birthday 		= $('#ed_birthday').calendar('get date');
	var gender			= $('#ed_gender').dropdown('get value');
	var pet_type		= $('#ed_pet_type').dropdown('get value');

	console.log(birthday);
	console.log(moment(birthday).format('YYYY-MM-DD'));

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	if (typeof($('#ed_pic')[0]) !== 'undefined') {

        jQuery.each(jQuery('#ed_pic')[0].files, function(i, file) {
            data.append('pic', file);
            // console.log(file);
        });
    }

	data.append('petshop_id', petshop_id);
	data.append('pet_name', pet_name);
	data.append('pet_type',pet_type);
	data.append('breed',breed);
	data.append('color',color);
	data.append('weight',weight);
	data.append('notedescription',notedescription);
	data.append('birthday',moment(birthday).format('YYYY-MM-DD'));
	data.append('gender',gender);

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: edit_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}

function deletePet(id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'id' : id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}


const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}