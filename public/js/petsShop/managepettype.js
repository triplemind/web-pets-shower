$(function(){

	DTRender('#TBL_managepettype', true);

	// console.log('ssss');
	$('.btn-addpettype').on('click', function(){
		$('#status').dropdown();
		
		$("#addPetTypeModal").modal({
			closable: false,
			onDeny: function() {
			},
			onApprove: function() {
				addPetType();
			}
		}).modal("show");
	});


	$('.editpettype').on('click', function(){
		$('#ed_status').dropdown();
		

		var id = $(this).data('id');

		var method 		= 'getPetTypeData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'id' : id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$('#ed_id').val(result.data.id);
					$('#ed_name').val(result.data.name);
					$('#ed_status').dropdown('set selected', result.data.status);

					setTimeout(function(){
						$("#editPetTypeModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editPetType();
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});
	});


	$('.deletepettype').on('click', function(){
		var id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบประเภทสัตว์เลี้ยงนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deletePetType(id);
	        }
	    });
	});

});

function addPetType() {
	var status 		= $('#status').dropdown('get value');
	var name 		= $('#name').val();

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	data.append('status', status);
	data.append('name', name);

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}

function editPetType() {
	var id 			= $('#ed_id').val();
	var status 		= $('#ed_status').dropdown('get value');
	var name 		= $('#ed_name').val();

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	data.append('id', id);
	data.append('status', status);
	data.append('name', name);

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: edit_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}

function deletePetType(id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'id' : id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}


const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}