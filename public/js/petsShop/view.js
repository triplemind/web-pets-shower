$(function() {

	DTRender('#TBL_vaccineRC', true);

	$('.btn-back').on('click', function(){
		window.location.href = '/admin/petsShop';
	});

	$('.btn-viewImg').on('click', function(){
		console.log($(this).data('id'));
		var VRC_id = $(this).data('id');
		window.location.href = "/admin/petsShop/viewImg/"+VRC_id;
	});


	$('#booking_date').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
		formatter: {
			date: function (date, settings) {
				if (!date) return '';
					var day = date.getDate();
					var month = date.getMonth() + 1;
					var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		onChange: function (date) {
			$('#id_service').dropdown('clear').dropdown({
				onChange: function (value, text) {
					var id_service = value;
					$('#vaccine').dropdown();
					// $('#vaccine').empty();
					if(text == 'วัคซีน' || text == 'โปรโมชันวัคซีน'){
						$('.vaccine_display').css('display', 'unset');

						var method      = 'getVaccine';
			            var ajax_url    = $('#bookingajax-center-url').data('url');
			            $.ajax({
			                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			                type: 'post',
			                url: ajax_url,
			                data: {
			                    'method' : method,
			                    'id_shop' : $('#id_shop').val(),
			                },
			                success: function(result) {
			                    if(result.status == 'success'){
			                        console.log(result.data);
			                        // console.log(JSON.parse(result.data));
			                        let arr_vac = sort_by_key(result.data, 'vaccine_name');
			                        
			                        $.each(arr_vac, function(key, value) { 
			                            $('#vaccine')
			                            .append($("<option></option>")
			                            .attr("value", value.vaccine_ID)
                    					.text(value.vaccine_name+" : "+value.properties));
			                        });
			                    } // End if check s tatus success.

			                    if(result.status == 'error'){
			                        $("body").toast({
			                            class: "error",
			                            position: 'bottom right',
			                            message: result.data
			                        });
			                    }
			                }
			            });
					}else{
						$('.vaccine_display').css('display', 'none');
					}
					// $('#booking_time').dropdown('clear').dropdown({
					// 	onChange: function (value1, text1) {
					// 		console.log(value1);
					// 		if(value1){
					// 			var id_shop  = $('#id_shop').val();
					// 			var booking_time = value1;
					// 			checkBooing(date, id_service, booking_time, id_shop);							
					// 		}
					// 	}
					// });
				}
			});
		}
	});

	$('.btn-save').on('click', function(){
        postAddBooking();
    });

});

function sort_by_key(array, key)
{
	return array.sort(function(a, b)
	{
		var x = a[key]; var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}

// function checkBooing(date, id_service, booking_time, id_shop){
// 	var id_shop           = id_shop;
//     var booking_date      = date;
//     var id_service        = id_service;
//     var booking_time      = booking_time;

//     booking_date = booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

//     var ajax_url        = $('#bookingajax-center-url').data('url');
//     var method        	= 'checkBooing';
    
//     $.ajax({
//         headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
//         type: 'post',
//         url: ajax_url,
//         data: {
//         	'method' : method,
//         	'id_shop' : id_shop,
//         	'booking_date' : booking_date,
//         	'id_service' : id_service,
//         	'booking_time' : booking_time,
//         },
//         success: function(result) {
//             if(result.status == 'success'){
//                 setTimeout(function(){ 

//                 	$txt_que = "รับ "+result.allque+" คิว || ว่าง "+result.allowque+" คิว";

//                 	$("#que_txt").text($txt_que);

//                 	if(result.bookingallow){
//                 		$("body").toast({
// 	                        class: "success",
// 	                        position: 'bottom right',
// 	                        message: result.msg
// 	                    });

// 	                    $('.btn-save').removeClass('disabled');
//                 	}else{
//                 		$("body").toast({
// 	                        class: "error",
// 	                        position: 'bottom right',
// 	                        message: result.msg
// 	                    });
                		
// 	                    $('.btn-save').addClass('disabled');
//                 	}

//                 });
//             } // End if check s tatus success.

//             if(result.status == 'error'){
//                 $("body").toast({
//                     class: "error",
//                     position: 'bottom right',
//                     message: result.msg
//                 });
//             }
//         },
//         error : function(error) {
//             // showForm(form, action, error, data);
//         }
//     });
// }

function postAddBooking() {
    var id           		= $('#id').val();
    var id_shop           = $('#id_shop').val();
    var pet_id            = $('#pet_id').val();
    var note           	  = $('#note').val();
    var booking_date      = $('#booking_date').calendar('get date');
    // var promo_code        = $('#promo_code').val();
    var id_service        = $('#id_service').dropdown('get value');
    // var booking_time      = $('#booking_time').dropdown('get value');
    var vaccine_val 	  = '';
    var vaccine_id 	  	  = '';

    if(id_service == 'PV0001' || id_service == 'PV0002'){
    	vaccine_val = $('#vaccine').dropdown('get text');
    	vaccine_id = $('#vaccine').dropdown('get value');
    }else{
    	vaccine_val = '';
    	vaccine_id = '';
    }


    booking_date = booking_date ? moment(booking_date).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD');

    var add_url         = $('#add_url').data('url');
    var data            = new FormData();

    data.append('id', id);
    data.append('id_shop', id_shop);
    data.append('note', note);
    data.append('booking_date',booking_date);
    // data.append('promo_code',promo_code);
    data.append('id_service',id_service);
    // data.append('booking_time',booking_time);
    data.append('pet_id',pet_id);
    data.append('vaccine_val',vaccine_val);
    data.append('vaccine_id',vaccine_id);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: add_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        // window.location.reload();
                        window.location.href = '/admin/dashboard';
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}


const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}