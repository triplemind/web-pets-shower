let time_payment;
let ed_time_payment;
$(function(){
    $('.viewshop').on('click', function(){

        $(this).data('id');

        $('.shopName').text('');
        $('.pre_status').text('');
        $('.shop_status').text('');
        $('.pre_startdate').text('');
        $('.pre_enddate').text('');
        $('.paydate').text('');
        $('.paytime').text('');
        $('.paybank').text('');
        $(".addslip-src").removeAttr("src");
        $(".shopimg").removeAttr("src");

        console.log($(this).data('name_shop'));

        $('.shopName').text($(this).data('name_shop'));
        $('.pre_status').text('สถานะพรีเมี่ยม : '+$(this).data('pre_status'));
        $('.shop_status').text('สถานะร้าน : '+$(this).data('shop_status'));
        $('.pre_startdate').text('วันที่เริ่มต้นสมาชิก : '+$(this).data('pre_startdate'));
        $('.pre_enddate').text('วันที่สิ้นสุดสมาชิก : '+$(this).data('pre_enddate'));
        $('.paydate').text('วันที่ชำระเงิน : '+$(this).data('paydate'));
        $('.paytime').text('เวลาที่ชำระเงิน : '+$(this).data('paytime')+' น.');
        $('.paybank').text('ธนาคาร : '+$(this).data('paybank'));
        $(".addslip-src").attr("src", $(this).data('addslip-src'));
        $(".shopimg").attr("src", $(this).data('shopimg'));


        $('.coupled.modal').modal({
            allowMultiple: true
        });
        // attach events to buttons
        $('.second.modal').modal('attach events', '.first.modal .slipimg');
        // show first now
        $('.first.modal').modal('show');
    });



    // Change Status banner [Active|Inactive].
    $('.on-off-status_premium').on('change',  function(){
        var shop_obj      = $(this);
        var shop_id       = $(this).val();
        var status_change  = $(this).is(":checked");

        var ajax_url        = $('#ajax-center-url').data('url');
        var method          = 'activeOrInactivePremium';
        Swal.fire({
            title: "คุณต้องการแก้ไขสถานะสมาชิก Premium ใช่หรือไม่?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "ใช่",
            cancelButtonText: "ไม่ใช่",
            closeOnConfirm: true,
            closeOnCancel: true
        }).then(result => {
            if (result.value) {
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: 'post',
                    url: ajax_url,
                    data: {
                        'method' : method,
                        'shop_id' : shop_id,
                        'status_change' : status_change,
                    },
                    success: function(result) {
                        if (result.status === 'success')
                           setTimeout(function(){ 
                            $("body").toast({
                                class: "success",
                                position: 'bottom right',
                                message: `บันทึกเสร็จสิ้น`
                            });
                            setTimeout(function(){ 
                                window.location.reload();
                            }, 1000);
                        });
                    },
                });
            }else{
                if(status_change){
                    shop_obj.removeAttr('checked');
                }else{
                    shop_obj.click();
                }
            }
        });
    });


    $('.btn-regitPre').on('click', function(){
        $('#regist-pre').css('display', 'none');
        $('#payment-prement').css('display', 'unset');
    });

    $('.btn-cancel-regitPre').on('click', function(){
        $('#regist-pre').css('display', 'unset');
        $('#payment-prement').css('display', 'none');
    });

	console.log("dsds");
    $('#bank_payment').dropdown();
    $('#ed_bank_payment').dropdown();

    $('#date_payment').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        },
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        },
    });

    $('#ed_date_payment').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        },
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        },
    });

    var check_payment = $('#check_payment').val();
    if(check_payment == 'empty'){
        time_payment = new Picker(document.querySelector('#time_payment'), {
            // container: document.querySelector('.docs-picker-container'),
            format: 'HH:mm',
            headers: true,
            controls: true,
            inline: true,
            rows:3,
            text: {
                title: 'เลือกเวลา',
            },
        });
    }

    if(check_payment == 'not empty'){
        ed_time_payment = new Picker(document.querySelector('#ed_time_payment'), {
            // container: document.querySelector('.docs-picker-container'),
            format: 'HH:mm',
            headers: true,
            controls: true,
            inline: true,
            rows:3,
            text: {
                title: 'เลือกเวลา',
            },
        });
    }

    $('.btn-save-payment').on('click', function(){
        var addslip_url     = $('#addslip-url').data('url');

        var price           = 300;
        var pre_startdate   = moment().locale('th').format('YYYY-MM-DD');
        var pre_enddate     = moment().add(1, 'months');
        var id              = $('#id').val();
        var date_payment    = $('#date_payment').calendar('get date');
        var time_payment_val = time_payment.getDate(true);
        var bank_payment    = $('#bank_payment').dropdown('get value');

        console.log(pre_startdate);
        console.log(moment(pre_enddate).locale('th').format('YYYY-MM-DD'));

        var data        = new FormData();
        if (typeof($('#pic_payment')[0]) !== 'undefined') {

            jQuery.each(jQuery('#pic_payment')[0].files, function(i, file) {
                data.append('pic_payment', file);
                // console.log(file);
            });
        }
        data.append('id',id);
        data.append('price',price);
        data.append('pre_startdate',pre_startdate);
        data.append('pre_enddate',moment(pre_enddate).locale('th').format('YYYY-MM-DD'));
        data.append('date_payment',moment(date_payment).locale('th').format('YYYY-MM-DD'));
        data.append('time_payment',time_payment_val);
        data.append('bank_payment',bank_payment);

        if(time_payment_val != null || date_payment != null || bank_payment != ''){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: addslip_url,
                data: data,
                contentType: false,
                processData:false,
                cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                       setTimeout(function(){ 
                            $("body").toast({
                                class: "success",
                                position: 'bottom right',
                                message: `บันทึกเสร็จสิ้น`
                            });
                            setTimeout(function(){ 
                                window.location.reload();
                            }, 1000);
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }else{
            $("body").toast({
                class: "error",
                position: 'bottom right',
                message: 'กรุณากรอกข้อมูลให้ครบ'
            });
        }

    });



	$('.btn-edit-payment').on('click', function(){
        var addslip_url     = $('#editslip-url').data('url');
       
        var id 			    = $('#id').val();
        var date_payment    = $('#ed_date_payment').calendar('get date');
        var time_payment_val = ed_time_payment.getDate(true);
        var bank_payment    = $('#ed_bank_payment').dropdown('get value');

        var data        = new FormData();
	    if (typeof($('#ed_pic_payment')[0]) !== 'undefined') {

	        jQuery.each(jQuery('#ed_pic_payment')[0].files, function(i, file) {
	            data.append('pic_payment', file);
	            // console.log(file);
	        });
	    }
		data.append('id',id);
        data.append('date_payment',moment(date_payment).locale('th').format('YYYY-MM-DD'));
        data.append('time_payment',time_payment_val);
        data.append('bank_payment',bank_payment);

        if(time_payment != null || date_payment != null || bank_payment != ''){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: addslip_url,
               	data: data,
    	        contentType: false,
    	        processData:false,
    	        cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                       setTimeout(function(){ 
    	                    $("body").toast({
    	                        class: "success",
    	                        position: 'bottom right',
    	                        message: `บันทึกเสร็จสิ้น`
    	                    });
    	                    setTimeout(function(){ 
    	                        window.location.reload();
    	                    }, 1000);
    	                });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }else{
            $("body").toast({
                class: "error",
                position: 'bottom right',
                message: 'กรุณากรอกข้อมูลให้ครบ'
            });
        }

    });

});


