$(function(){
	CKEDITOR.replace( 'description' );
    
    $('#service_type').dropdown();
    $('#status').dropdown();
    $('#shop_id').dropdown();

	$('.btn-back').on('click', function(){
		window.location.href = "/admin/promotion";
	});

	
	$('.btn-save').on('click', function(){
        postAdd();
    });

    $('#enddate').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        }
    });

    $('#startdate').calendar({
        type: 'date',
        popupOptions: {
            observeChanges: false
        }
    });

});


function postAdd() {
    var title    		= $('#title').val();
    var enddate    		= $('#enddate').calendar('get date');
    var startdate       = $('#startdate').calendar('get date');
    var code            = $('#code').val();
    var discount    	= $('#discount').val();
    var link    		= $('#link').val();
    var description     = CKEDITOR.instances.description.getData();
    var service_type 	= $('#service_type').dropdown('get value');
    var status  		= $('#status').dropdown('get value');
    // var shop_id  		= $('#shop_id').dropdown('get value');

    var add_url         = $('#add_url').data('url');
    var data            = new FormData();

    if (typeof($('#img')[0]) !== 'undefined') {

        jQuery.each(jQuery('#img')[0].files, function(i, file) {
            data.append('img', file);
            // console.log(file);
        });
    }

    data.append('title', title);
    data.append('enddate', moment(enddate).format('YYYY-MM-DD'));
    data.append('startdate', moment(startdate).format('YYYY-MM-DD'));
    data.append('code', code);
    data.append('discount', discount);
    data.append('link', link);
    data.append('description',description);
    data.append('service_type',service_type);
    data.append('status',status);
    // data.append('shop_id',shop_id);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: add_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.href = '/admin/promotion';
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}

