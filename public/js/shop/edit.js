$(function(){
	CKEDITOR.replace( 'shop_description' );

    var cleave = new Cleave('#tel_shop', {
        // numericOnly: true,
        phone: true,
        phoneRegionCode: 'th'
        // blocks: [3, 3, 4],
        // delimiters: ["(", ")", " ", "-"]
    });

    // var province_shop   = $('#province_shop').val();
    // var amphur_shop     = $('#amphur_shop').val();
    // var district_shop   = $('#district_shop').val();

    // console.log(province_shop, amphur_shop, district_shop);
	
    $('#status_open').dropdown();
    $('#type_shop').dropdown();

	$('.btn-back').on('click', function(){
		window.location.href = "/admin/shop";
	});

	$('#province').dropdown({
        onChange: function(value, text) {
            console.log(value);
            console.log(text);

            $('#amphur').empty();
            $('#district').empty();

            var method      = 'getAmphur';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'province_id' : value,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // console.log(result.data);
                        $.each(result.data, function(key, value) {   
                            $('#amphur')
                            .append($("<option></option>")
                            .attr("value", value.AMPHUR_ID)
                            .text(value.AMPHUR_NAME)); 
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }
    });
    
    $('#amphur').dropdown({
        onChange: function(value, text) {
            console.log(value);
            console.log(text);

            $('#district').empty();

            var method      = 'getDistrict';
            var ajax_url    = $('#ajax-center-url').data('url');
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: ajax_url,
                data: {
                    'method' : method,
                    'amphur_id' : value,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // console.log(result.data);
                        $.each(result.data, function(key, value) {   
                            $('#district')
                            .append($("<option></option>")
                            .attr("value", value.DISTRICT_ID )
                            .text(value.DISTRICT_NAME)); 
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }
    });

    $('#district').dropdown();

	$('.btn-save').on('click', function(){
        postEdit();
    });

});


function postEdit() {
    var id_shop             = $('#id_shop').val();
    var name_shop           = $('#name_shop').val();
    var shop_lat            = $('#shop_lat').val();
    var shop_long           = $('#shop_long').val();
    var deposit             = $('#deposit').val();
    var type_shop           = $('#type_shop').dropdown('get value');
    var licenesNo           = $('#licenesNo').val();
    var tel_shop            = $('#tel_shop').val();
    var address_shop        = $('#address_shop').val();
    var zip                 = $('#zip').val();
    var shop_description    = CKEDITOR.instances.shop_description.getData();
    var province            = $('#province').dropdown('get value');
    var amphur              = $('#amphur').dropdown('get value');
    var district            = $('#district').dropdown('get value');
    var status_open         = $('#status_open').dropdown('get value');

    var edit_url            = $('#edit_url').data('url');
    var data                = new FormData();

    if (typeof($('#shop_img')[0]) !== 'undefined') {

        jQuery.each(jQuery('#shop_img')[0].files, function(i, file) {
            data.append('shop_img', file);
            // console.log(file);
        });
    }

    data.append('id_shop', id_shop);
    data.append('shop_lat', shop_lat);
    data.append('shop_long', shop_long);
    data.append('name_shop', name_shop);
    data.append('deposit', deposit);
    data.append('type_shop',type_shop);
    data.append('licenesNo',licenesNo);
    data.append('tel_shop',tel_shop);
    data.append('zip',zip);
    data.append('address_shop',address_shop);
    data.append('shop_description',shop_description);
    data.append('province',province);
    data.append('amphur',amphur);
    data.append('district',district);
    data.append('status_open',status_open);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: edit_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.href = '/admin/shop';
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}



// Function สำหรับ Map ทั้งหมด  
function initMap() {
    const input = document.getElementById("pac-input");
    const searchBox = new google.maps.places.SearchBox(input);

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // For each place, get the icon, name and location.
        const bounds = new google.maps.LatLngBounds();
        places.forEach((place) => {
            console.log(place);
            console.log(place.geometry);
            console.log(place.geometry.location);
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
           
            console.log(latitude);
            console.log(longitude);
            
            $('#shop_lat').val(latitude);
            $('#shop_long').val(longitude);
            
           

        });
        // map.fitBounds(bounds);
    });



}