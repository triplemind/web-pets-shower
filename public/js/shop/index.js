let _multimenu;

$(function(){

	DTRender('#TBL_user', true);

	$('#type_shopfil').dropdown();
	$('#status_openfill').dropdown();

	$('.btn-addshop').on('click', function(){
		window.location.href = "/admin/shop/add";
	});

	$('.btn-manage_service').on('click', function(){
		console.log($(this).data('id'));
		var shop_id = $(this).data('id');
		window.location.href = "/admin/shop/manageservice/"+shop_id;
	});

	// 
	$('.editshop').on('click', function(){
		console.log($(this).data('id'));
		var shop_id = $(this).data('id');

		window.location.href = "/admin/shop/edit/"+shop_id;
	});


	$('.viewshop').on('click', function(){
		console.log($(this).data('id'));
		var shop_id = $(this).data('id');

		window.location.href = "/admin/shop/view/"+shop_id;
	});

	$('.deleteshop').on('click', function(){
		console.log($(this).data('id'));
		var shop_id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการร้านนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deleteShop(shop_id);
	        }
	    });
	});


	$('.btn-search').on('click', function(){
		msg_waiting();
        addFilterToSession();
    });

    $('.btn-clear').on('click', function(){
        msg_waiting();
    	var ajax_url 		= $('#ajax-center-url').data('url');
    	var method 			= 'clearFilterToSession'
    	$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: ajax_url,
	        data: {
	            'method' : method,
	        },
	        success: function(result) {
	            if (result.status === 'success')       window.location.reload();
	        },
	    });
    });

});

function deleteShop(shop_id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'shop_id' : shop_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/shop';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}


function msg_waiting() {
	let timerInterval;
	Swal.fire({
		title: '',
		html: 'LOADING',
		timer: 2000,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading()
			timerInterval = setInterval(() => {
				const content = Swal.getContent()
				if (content) {
					const b = content.querySelector('b')
					if (b) {
						b.textContent = Swal.getTimerLeft()
					}
				}
			}, 100)
		},
		onClose: () => {
			clearInterval(timerInterval)
		}
	}).then((result) => {
		/* Read more about handling dismissals below */
		if (result.dismiss === Swal.DismissReason.timer) {
			console.log('I was closed by the timer')
		}
	});
}

function addFilterToSession() {
	var search_txt   	= $('#search_txt').val();
    var type_shopfil 	= $('#type_shopfil').dropdown('get value');
    var status_openfill = $('#status_openfill').dropdown('get value');

    // console.log(moment(rangestart).format('YYYY-MM-DD'), moment(rangeend).format('YYYY-MM-DD'));
    console.log(type_shopfil);
    console.log(status_openfill);

    var ajax_url        = $('#ajax-center-url').data('url');
    var method          = 'addFilterToSession'

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'search_txt' : search_txt,
            'type_shopfil' : (type_shopfil != 'all') ? type_shopfil : '',
            'status_openfill' : (status_openfill != 'all') ? status_openfill : '',
        },
        success: function(result) {
            if (result.status === 'success')       window.location.reload();
        },
    });
}