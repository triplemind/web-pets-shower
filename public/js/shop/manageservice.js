
let start_time_morning;
let end_time_morning;
let end_time_afternoon;
let start_time_afternoon;

$(function(){

	DTRender('#TBL_user', true);

	getVaccineList();

    start_time_morning = new Picker(document.querySelector('#start_time_morning'), {
        // container: document.querySelector('.docs-picker-container'),
        format: 'HH:mm',
        headers: true,
        controls: true,
        inline: true,
        rows:3,
        text: {
            title: 'เลือกเวลา',
        },
    });

    end_time_morning = new Picker(document.querySelector('#end_time_morning'), {
        format: 'HH:mm',
        headers: true,
        controls: true,
        inline: true,
        rows:3,
        text: {
            title: 'เลือกเวลา',
        },
    });

    end_time_afternoon = new Picker(document.querySelector('#end_time_afternoon'), {
        // container: document.querySelector('.docs-picker-container'),
        format: 'HH:mm',
        headers: true,
        controls: true,
        inline: true,
        rows:3,
        text: {
            title: 'เลือกเวลา',
        },
    });

    start_time_afternoon = new Picker(document.querySelector('#start_time_afternoon'), {
        format: 'HH:mm',
        headers: true,
        controls: true,
        inline: true,
        rows:3,
        text: {
            title: 'เลือกเวลา',
        },
    });



	$('#id_service').dropdown();
	$('#booking_time').dropdown({
        onChange: function (value,text) {
            if(value == 'บ่าย'){
                $('.endTime').css('display', 'unset');
                $('.btn-saveservice').css('display', 'unset');

                $('.startTime').css('display', 'none');

            }else if(value == 'เช้า'){
                $('.startTime').css('display', 'unset');
                $('.btn-saveservice').css('display', 'unset');
                
                $('.endTime').css('display', 'none');
            }


        }
    });
	$('#id_vaccine').dropdown();

	$('.btn-back').on('click', function(){
		window.location.href = "/admin/shop";
	});

	$('.btn-manage_service').on('click', function(){
		console.log($(this).data('id'));
		var service_id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบบริการนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ไม่ใช่",
	        cancelButtonColor: "",
	        confirmButtonText: "ใช่"
	    }).then(result => {
	        if (result.value) {
				postRemoveManageService(service_id);
	        }
	    });
	});

	$('.btn-save').on('click', function(){
		postAddManageService();
	});

	$('.btn-savevaccine').on('click', function(){
		postAddVaccine();
	});

});

function getVaccineList(){
    var ajax_url  = $('#ajax-center-url').data('url');
    var method    = 'getVaccineList';

    var path 		= window.location.pathname;
    var path_val 	= path.replace("/admin/shop/manageservice/", "");

    console.log(path_val);

     $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {'method' : method,'id' : path_val},
        success: function(result) {
            if(result.status == 'success'){
            	console.log(result.data);
            	console.log(JSON.parse(result.data));
                setTimeout(function(){ 
	            	$("#id_vaccine").dropdown("refresh").dropdown("set selected", JSON.parse(result.data));
	          	});
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}


function postAddManageService() {
    var Que                 = $('#Que').val();
    var id_service          = $('#id_service').dropdown('get value');
    var booking_time        = $('#booking_time').dropdown('get value');
    var shop_id        		= $('#shop_id').val();

    var start_time_morning_val      = start_time_morning.getDate(true);
    var end_time_morning_val        = end_time_morning.getDate(true);
    var end_time_afternoon_val      = end_time_afternoon.getDate(true);
    var start_time_afternoon_val    = start_time_afternoon.getDate(true);
    var startTime               = '';
    var endTime                 = '';

    if(booking_time == 'เช้า'){
        startTime   = start_time_morning_val+" น. - "+end_time_morning_val+" น.";
    }else if(booking_time == 'บ่าย'){
        endTime     = start_time_afternoon_val+" น. - "+end_time_afternoon_val+" น.";
    }


    console.log(startTime);
    console.log(endTime);
    console.log(start_time_morning.getDate(true));
    console.log(end_time_morning.getDate(true));
    console.log(end_time_afternoon.getDate(true));
    console.log(start_time_afternoon.getDate(true));

    var add_url         = $('#add_url').data('url');
    var data            = new FormData();

    data.append('Que', Que);
    data.append('id_service',id_service);
    data.append('booking_time',booking_time);
    data.append('shop_id',shop_id);
    data.append('startTime',startTime);
    data.append('endTime',endTime);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: add_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.reload();
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
}


function postAddVaccine() {
    var id_vaccine          = JSON.stringify($('#id_vaccine').dropdown('get value'));
    var vacArr_length       = $('#id_vaccine').dropdown('get value').length;
    var shop_id        		= $('#shop_id').val();

    console.log(vacArr_length);
    console.log(id_vaccine,shop_id);

    var add_url         = $('#add_vaccineurl').data('url');
    var data            = new FormData();

    data.append('id_vaccine',id_vaccine);
    data.append('shop_id',shop_id);
    data.append('vacArr_length',vacArr_length);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: add_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.reload();
                    	
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}


function postRemoveManageService(service_id) {
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'service_id' : service_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}



const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}