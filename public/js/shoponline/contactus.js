

function getFormToSendMail() {
	var name 		= $('#name').val();
	var email 		= $('#email').val();
	var title 		= $('#title').val();
	var textmsg 	= $('#textmsg').val();

	var add_url 	= $('#sendmail').data('url');
    var data 		= new FormData();

    data.append('name', name);
    data.append('email',email);
    data.append('title',title);
    data.append('textmsg',textmsg);

	var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

	
	if(name && email && title && textmsg){
		if (testEmail.test(email)){
            msg_waiting();
            
			$.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: add_url,
                data: data,
                contentType: false,
                processData:false,
                cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                        setTimeout(function(){ 
                            $("body").toast({
                                class: "success",
                                position: 'bottom right',
                                message: `ส่งข้อความสำเร็จ กรุณารอการติดต่อกลับ`
                            });
                            setTimeout(function(){ 
                                window.location.href = '/';
                            }, 1000);
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.msg
                        });
                    }
                },
                error : function(error) {
                    // showForm(form, action, error, data);
                }
            });

		}else{
	        $("body").toast({
	            class: "error",
	            displayTime: 10000,
	            position: 'bottom right',
	            message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
	        });
	    }
	}else{
        $("body").toast({
            class: "error",
            displayTime: 10000,
            position: 'bottom right',
            message: `กรุณากรอกข้อมูลให้ครบถ้วน`
        });
    }


}



function msg_waiting() {
    let timerInterval;
    Swal.fire({
        title: '',
        html: 'LOADING',
        timer: 2000,
        timerProgressBar: true,
        onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                const content = Swal.getContent()
                if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                        b.textContent = Swal.getTimerLeft()
                    }
                }
            }, 100)
        },
        onClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer')
        }
    });
}