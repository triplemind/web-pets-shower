var _countOfcart;
$(function(){
	console.log('sssss');

	$('.slider-nav').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 600,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});

});

function addToCart(id) {
    console.log(id);

    var method 		= 'addToCart';
	var ajax_url	= $('#ajax-center-url').data('url');
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
		  	'method' : method,
		  	'stock_id' : id,
		},
		success: function(result) {
			if(result.status == 'success'){
				console.log(result.data);

				setTimeout(function(){
					_countOfcart = result.numOfcount;
					$("#total_cart").html("");
    				$("#total_cart").html(result.numOfcount);
		        });
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		}
	});
}



let pos;
let map;
let bounds;
let infoWindow;
let currentInfoWindow;
let service;
let infoPane;
function initMap() {
    // Try HTML5 geolocation
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
        pos = {
	        lat: position.coords.latitude,
	        lng: position.coords.longitude
        };

        map = new google.maps.Map(document.getElementById('map'), {
	        center: pos,
	        zoom: 15
        });

        getNearbyPlaces(pos);
       
       console.log(pos);

        /* TODO: Step 3B2, Call the Places Nearby Search */
    }, () => {
        // Browser supports geolocation, but user has denied permission
        handleLocationError(true, infoWindow);
    });
    } else {
    // Browser doesn't support geolocation
    handleLocationError(false, infoWindow);
    }
}

// Handle a geolocation error
function handleLocationError(browserHasGeolocation, infoWindow) {
    // Set default location to Sydney, Australia

    pos = {lat: 13.8040211, lng: 100.547996};
    map = new google.maps.Map(document.getElementById('map'), {
	    center: pos,
	    zoom: 15
    });

    getNearbyPlaces(pos);

    /* TODO: Step 3B3, Call the Places Nearby Search */
}


function getNearbyPlaces(position) {
	console.log(position);
    let request = {
	    location: position,
	    // rankBy: google.maps.places.RankBy.DISTANCE,
	    keyword: 'สัตวแพทย์',
	    radius: 10000, //5000 = 5KM
    };

    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, nearbyCallback);

}

// Handle the results (up to 20) of the Nearby Search
function nearbyCallback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
    // createMarkers(results);
	    console.log(results);
	    genCardForShow(results);

    }
}

function genCardForShow(results){

    console.log(results);

    $('#addcard-nearby').html('');
    var txtnear;
    $.each(results, function(key, value) {   
        
    	txtnear = '<div class="card">'+
						'<div class="image">'+
							'<img class="ui huge image" src="'+(value.photos ? value.photos[0].getUrl() : "https://www.petscarebusiness.com/public/themes/image/logo.png")+'" style="max-height: 165px;">'+
						'</div>'+
						'<div class="content">'+
							'<a class="header" href="#">'+value.name+'</a>'+
						'</div>'+
					'</div>';

		console.log(key);
		if(key < 4){
   	 		$('#addcard-nearby').append(txtnear); 
		}
    });

}
