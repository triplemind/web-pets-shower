$(function(){

	var cleave = new Cleave('#ed_telephone', {
			        numericOnly: true,
			        blocks: [3, 3, 4],
			        // delimiters: ["(", ")", " ", "-"]
			    });

	$(".toggle-password").click(function() {
        $(this).toggleClass("slash");
        var input = $($(this).attr("toggle"));

        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });


    $('.password, .confirm_pass').on('keyup', function () {
        $('.err-pass').addClass('alert-danger');
        $('#message').css('font-weight', 'bold');
        $('#message').css('font-size', '18px');
        if ($('.password').val() == $('.confirm_pass').val()) {
            $('#message').html('รหัสผ่านตรงกัน').css('color', 'green');
            
            $('.passwordtxt').removeClass('error');
            $('.confirm_passtxt').removeClass('error');

            $('.passwordtxt').addClass('success');
            $('.confirm_passtxt').addClass('success');

            console.log('success');

        } else 
            $('#message').html('รหัสผ่านไม่ตรงกัน').css('color', 'red');

            console.log('error');

            $('.passwordtxt').addClass('error');
            $('.confirm_passtxt').addClass('error');
    });


	
	$('.btn-save').on('click', function(){
		if ($('.password').val() == $('.confirm_pass').val()) {
			editUser();
		}else{
			$("body").toast({
				class: "error",
				position: 'bottom right',
				message: `กรุณาตรวจสอบรหัสผ่านอีกครั้ง`
			});
		}
	});


});

function postAction(action) {
	if(action == 'edit'){
		$('.editIcon').css('display', 'none');
		$('.closeIcon').css('display', 'unset');
		$('#edit-form').css('display', 'unset');
	}

	if(action == 'close'){
		$('.editIcon').css('display', 'unset');
		$('.closeIcon').css('display', 'none');
		$('#edit-form').css('display', 'none');
	}
}

function editUser() {
	var id_owner 		= $('#id_owner').val();
	var user_type 		= $('#ed_user_type').val();
	var username 		= $('#ed_username').val();
	var password 		= $('#password').val();
	var name_owner 		= $('#ed_name_owner').val();
	var email 			= $('#ed_email').val();
	var address 		= $('#ed_address').val();
	var telephone 		= $('#ed_telephone').val();
	var telVal          = telephone.replace(" ", "");

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	if (typeof($('#ed_user_img')[0]) !== 'undefined') {

        jQuery.each(jQuery('#ed_user_img')[0].files, function(i, file) {
            data.append('user_img', file);
            console.log(file);
        });
    }

	data.append('user_id', id_owner);
	data.append('username', username);
	data.append('password',password);
	data.append('name_owner',name_owner);
	data.append('email',email);
	data.append('user_type',user_type);
	data.append('address',address);
	data.append('telephone',telVal.replace(" ", ""));

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));

	// check format email
	var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

	if (testEmail.test(email)){
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: edit_url,
			data: data,
			contentType: false,
	        processData:false,
	        cache: false,
			success: function(result) {
				if(result.status == 'success'){
					setTimeout(function(){ 
						$("body").toast({
							class: "success",
							position: 'bottom right',
							message: `บันทึกเสร็จสิ้น`
						});
						setTimeout(function(){ 
			                window.location.reload();
			            }, 1000);
					});
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.msg
					});
				}
			},
			error : function(error) {
				// showForm(form, action, error, data);
			}
		});
	}else{
		$("body").toast({
			class: "error",
			displayTime: 10000,
			position: 'bottom right',
			message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
		});
	}

}