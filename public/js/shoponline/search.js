var _shopAllData = [];
$(function(){
	$('#drop_search').dropdown();


	var method      = 'getShopData';
    var ajax_url    = $('#ajax-center-url').data('url');
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
        },
        success: function(result) {
            if(result.status == 'success'){
                console.log(result.data);
                _shopAllData = result.data;
                setTimeout(function(){
                    _shopAllData.forEach(element => {
                        element['results'] = element.name_shop;
                        element['category'] = element.type_shop;
                        element['title'] = element.name_shop;
                    });

                    console.log(_shopAllData);

                    $('#shop_search').search({
                        ignoreDiacritics: true,
                        fullTextSearch:'exact',
                        type: 'category',
                        source: _shopAllData,
                        onSelect: (result, response) => {
                        	window.location.href = '/search/'+result.name_shop;
                        },
                        error: 
                            {
                                source:"ไม่สามารถค้นหา ไม่ได้ใช้แหล่งที่มาและโมดูล Semantic API ไม่รวมอยู่",
                                noResultsHeader: "ไม่มีผลลัพธ์",
                                noResults: "การค้นหาของคุณไม่เจอผลลัพธ์",
                                logging: "ข้อผิดพลาดในการบันทึกการดีบักกำลังออก",
                                noTemplate: "ไม่ได้ระบุชื่อเทมเพลตที่ถูกต้อง",
                                serverError: "มีปัญหาในการสอบถามเซิร์ฟเวอร์",
                                maxResults: "ผลลัพธ์จะต้องเป็นอาร์เรย์เพื่อใช้การตั้งค่า maxResults",
                                method: "วิธีการที่คุณเรียก ไม่ได้กำหนดไว้"
                            },
                    });

                });
            } 

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        }
    });


});
