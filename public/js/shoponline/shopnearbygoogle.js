

let pos;
let map;
let bounds;
let infoWindow;
let currentInfoWindow;
let service;
let infoPane;
function initMap() {
    // Try HTML5 geolocation
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(position => {
        pos = {
	        lat: position.coords.latitude,
	        lng: position.coords.longitude
        };

        // pos = {
        //     lat: 13.8017808,
        //     lng: 100.5780864
        // };

        map = new google.maps.Map(document.getElementById('map'), {
	        center: pos,
	        zoom: 15
        });

        getNearbyPlaces(pos);
       
       console.log(pos);

        /* TODO: Step 3B2, Call the Places Nearby Search */
    }, () => {
        // Browser supports geolocation, but user has denied permission
        handleLocationError(true, infoWindow);
    });
    } else {
    // Browser doesn't support geolocation
    handleLocationError(false, infoWindow);
    }
}

// Handle a geolocation error
function handleLocationError(browserHasGeolocation, infoWindow) {
    // Set default location to Sydney, Australia

    pos = {lat: 13.8040211, lng: 100.547996};
    map = new google.maps.Map(document.getElementById('map'), {
	    center: pos,
	    zoom: 15
    });

    getNearbyPlaces(pos);

    /* TODO: Step 3B3, Call the Places Nearby Search */
}


function getNearbyPlaces(position) {
	console.log(position);
    let request = {
	    location: position,
	    // rankBy: google.maps.places.RankBy.DISTANCE,
	    keyword: 'สัตวแพทย์',
        radius: 10000, //5000 = 5KM
        // type:'veterinary_care',
    };

    service = new google.maps.places.PlacesService(map);
    service.nearbySearch(request, nearbyCallback);

}

// Handle the results (up to 20) of the Nearby Search
function nearbyCallback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
    // createMarkers(results);
	    console.log(results);
	    genCardForShow(results);

    }
}

function genCardForShow(results){

    console.log(results);

    $('#addcard-nearby').html('');
    var txtnear;
    $.each(results, function(key, value) {   
        
    	txtnear = '<div class="card">'+
						'<div class="image">'+
							'<img class="ui huge image" src="'+(value.photos ? value.photos[0].getUrl() : "https://www.petscarebusiness.com/public/themes/image/logo.png")+'" style="max-height: 165px;">'+
						'</div>'+
						'<div class="content">'+
							'<a class="header">'+value.name+'</a>'+
                            '<p>'+value.name+'</p>'+
						'</div>'+
					'</div>';

		// console.log((value.opening_hours ? value.opening_hours.isOpen() : ''));
		// if(key < 4){
 		$('#addcard-nearby').append(txtnear); 
		// }
    });

}
