$(function(){
	$('.ui.accordion').accordion({
        selector: {
          trigger: '.title .icon'
        }
  	});

	$('#vaccine_type').dropdown();

	$('.btn-search').on('click', function(){
		msg_waiting();
        addFilterToSession();
    });

    $('.btn-clear').on('click', function(){
        msg_waiting();
    	var ajax_url 		= $('#ajax-center-url').data('url');
    	var method 			= 'clearFilterToSession'
    	$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: ajax_url,
	        data: {
	            'method' : method,
	        },
	        success: function(result) {
	            if (result.status === 'success')       window.location.reload();
	        },
	    });
    });

});

function msg_waiting() {
	let timerInterval;
	Swal.fire({
		title: '',
		html: 'LOADING',
		timer: 2000,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading()
			timerInterval = setInterval(() => {
				const content = Swal.getContent()
				if (content) {
					const b = content.querySelector('b')
					if (b) {
						b.textContent = Swal.getTimerLeft()
					}
				}
			}, 100)
		},
		onClose: () => {
			clearInterval(timerInterval)
		}
	}).then((result) => {
		/* Read more about handling dismissals below */
		if (result.dismiss === Swal.DismissReason.timer) {
			console.log('I was closed by the timer')
		}
	});
}


function addFilterToSession() {
    var search_txt 		= $('#search_txt').val();
    var vaccine_type 	= $('#vaccine_type').dropdown('get value');

    var ajax_url        = $('#ajax-center-url').data('url');
    var method          = 'addFilterToSession'

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' 		: method,
            'search_txt' 	: search_txt,
            'vaccine_type' 	: (vaccine_type != 'all') ? vaccine_type : '',
        },
        success: function(result) {
            if (result.status === 'success')       window.location.reload();
        },
    });
}