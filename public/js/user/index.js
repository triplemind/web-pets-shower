let _multimenu;

$(function(){

	DTRender('#TBL_user', true);

	// console.log('ssss');
	$('.btn-adduser').on('click', function(){
		$('#user_type').dropdown();
		$("#addUserModal").modal({
			closable: false,
			onDeny: function() {
			},
			onApprove: function() {
				addNewUser();
			}
		}).modal("show");
	});


	// ทำ edit กับ delete ต่อ
	$('.edituser').on('click', function(){
		var user_id = $(this).data('id');

		var method 		= 'getUserData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'user_id' : user_id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$('#user_id').val(result.data.id_owner);
					$('#ed_username').val(result.data.username);
					// $('#ed_password').val(result.data.password);
					$('#ed_name_owner').val(result.data.name_owner);
					$('#ed_email').val(result.data.email);
					$('#ed_address').val(result.data.address);
					$('#ed_telephone').val(result.data.telephone);
					$('#ed_user_type').dropdown('set selected', result.data.user_type);

					setTimeout(function(){
						// $('#ed_user_type').dropdown();
						// $('#menu_accress').dropdown({
						// 	onChange: function (val) {
						// 		_multimenu = val;
						// 		console.log(val);
						// 	}
						// });

						$("#EditUserModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editUser(user_id);
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});


		
	});

	$('.deleteuser').on('click', function(){
		console.log($(this).data('id'));
		var user_id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบผู้ใช้งานนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deleteUser(user_id);
	        }
	    });
	});

});


function addNewUser() {
	var username 		= $('#username').val();
	var password 		= $('#password').val();
	var name_owner 		= $('#name_owner').val();
	var email 			= $('#email').val();
	var address 		= $('#address').val();
	var telephone 		= $('#telephone').val();
	var user_type		= $('#user_type').dropdown('get value');
	// var menu_accress 	= _multimenu

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	if (typeof($('#user_img')[0]) !== 'undefined') {

        jQuery.each(jQuery('#user_img')[0].files, function(i, file) {
            data.append('user_img', file);
            // console.log(file);
        });
    }

	data.append('username', username);
	data.append('password',password);
	data.append('name_owner',name_owner);
	data.append('email',email);
	data.append('user_type',user_type);
	data.append('address',address);
	data.append('telephone',telephone);
	// data.append('menu_accress',JSON.stringify(menu_accress));

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));

	// check format email
	var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

	if (testEmail.test(email)){
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: add_url,
			data: data,
			contentType: false,
	        processData:false,
	        cache: false,
			success: function(result) {
				if(result.status == 'success'){
					setTimeout(function(){ 
						$("body").toast({
							class: "success",
							position: 'bottom right',
							message: `บันทึกเสร็จสิ้น`
						});
						setTimeout(function(){ 
			                window.location.href = '/admin/user';
			            }, 1000);
					});
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.msg
					});
				}
			},
			error : function(error) {
				// showForm(form, action, error, data);
			}
		});
	}else{
		$("body").toast({
			class: "error",
			displayTime: 10000,
			position: 'bottom right',
			message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
		});
	}

}




function editUser(user_id) {
	var user_id 		= $('#user_id').val();
	var username 		= $('#ed_username').val();
	var password 		= $('#ed_password').val();
	var name_owner 		= $('#ed_name_owner').val();
	var email 			= $('#ed_email').val();
	var address 		= $('#ed_address').val();
	var telephone 		= $('#ed_telephone').val();
	var user_type		= $('#ed_user_type').dropdown('get value');
	// var menu_accress 	= _multimenu

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	if (typeof($('#ed_user_img')[0]) !== 'undefined') {

        jQuery.each(jQuery('#ed_user_img')[0].files, function(i, file) {
            data.append('user_img', file);
            console.log(file);
        });
    }

	data.append('user_id', user_id);
	data.append('username', username);
	data.append('password',password);
	data.append('name_owner',name_owner);
	data.append('email',email);
	data.append('user_type',user_type);
	data.append('address',address);
	data.append('telephone',telephone);
	// data.append('menu_accress',JSON.stringify(menu_accress));

    // console.log(data);
	// console.log(JSON.stringify(menu_accress));

	// check format email
	var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

	if (testEmail.test(email)){
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: edit_url,
			data: data,
			contentType: false,
	        processData:false,
	        cache: false,
			success: function(result) {
				if(result.status == 'success'){
					setTimeout(function(){ 
						$("body").toast({
							class: "success",
							position: 'bottom right',
							message: `บันทึกเสร็จสิ้น`
						});
						setTimeout(function(){ 
			                window.location.href = '/admin/user';
			            }, 1000);
					});
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.msg
					});
				}
			},
			error : function(error) {
				// showForm(form, action, error, data);
			}
		});
	}else{
		$("body").toast({
			class: "error",
			displayTime: 10000,
			position: 'bottom right',
			message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
		});
	}

}



function deleteUser(user_id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'user_id' : user_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/user';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}