$(function(){
	CKEDITOR.replace( 'vaccine_usage' );
    CKEDITOR.replace( 'description' );
    
    $('#vaccine_type').dropdown();
    $('#vaccine_status').dropdown();
    $('#timeperiod_unit').dropdown();

	$('.btn-back').on('click', function(){
		window.location.href = "/admin/vaccine";
	});

	
	$('.btn-save').on('click', function(){
        postAdd();
    });

});

//get ข้อมูลจาก form เพื่อส่งไปบันทึกที่ function postAdd ใน VaccineController
function postAdd() {
    var vaccine_name    = $('#vaccine_name').val();
    var drug_name       = $('#drug_name').val();
    var company         = $('#company').val();
    var properties      = $('#properties').val();
    var vaccine_usage   = CKEDITOR.instances.vaccine_usage.getData();
    var description     = CKEDITOR.instances.description.getData();
    var vaccine_type    = $('#vaccine_type').dropdown('get value');
    var vaccine_status  = $('#vaccine_status').dropdown('get value');
    var timeperiod_unit = $('#timeperiod_unit').dropdown('get value');
    var numoftimes      = $('#numoftimes').val();
    var timeperiod      = $('#timeperiod').val();

    var add_url         = $('#add_url').data('url');
    var data            = new FormData();

    //get รูป
    if (typeof($('#img_vaccine')[0]) !== 'undefined') {
        jQuery.each(jQuery('#img_vaccine')[0].files, function(i, file) {
            data.append('img_vaccine', file);
            // console.log(file);
        });
    }

    data.append('vaccine_name', vaccine_name);
    data.append('drug_name', drug_name);
    data.append('company', company);
    data.append('properties', properties);
    data.append('vaccine_usage',vaccine_usage);
    data.append('description',description);
    data.append('vaccine_type',vaccine_type);
    data.append('vaccine_status',vaccine_status);
    data.append('timeperiod_unit',timeperiod_unit);
    data.append('numoftimes',numoftimes);
    data.append('timeperiod',timeperiod);

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: add_url,
        data: data,
        contentType: false,
        processData:false,
        cache: false,
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 
                    $("body").toast({
                        class: "success",
                        position: 'bottom right',
                        message: `บันทึกเสร็จสิ้น`
                    });
                    setTimeout(function(){ 
                        window.location.href = '/admin/vaccine';
                    }, 1000);
                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });

}

