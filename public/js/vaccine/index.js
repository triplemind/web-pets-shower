$(function(){

	// DTRender('#TBL_report', true);

	$('.add-page .pagination').addClass('ui');
	$('.add-page .pagination').addClass('menu');

	$('.add-page .pagination').css('padding',0);

	$('.add-page .pagination .page-item ').addClass('item');

	$('.btn-addvaccine').on('click', function(){
		window.location.href = "/admin/vaccine/add";
	});

	$('.editvaccine').on('click', function(){
		console.log($(this).data('id'));
		var id = $(this).data('id');

		window.location.href = "/admin/vaccine/edit/"+id;
	});


	$('.viewvaccine').on('click', function(){
		console.log($(this).data('id'));
		var id = $(this).data('id');

		window.location.href = "/admin/vaccine/view/"+id;
	});


	$('.deletevaccine').on('click', function(){
		console.log($(this).data('id'));
		var id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการวัคซีนนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deleteShop(id);
	        }
	    });
	});

	$('#vaccine_type').dropdown();
	$('#pets_each').dropdown();
	$('#vaccine_status').dropdown();

	$('.btn-search').on('click', function(){
		msg_waiting();
        addFilterToSession();
    });

    $('.btn-clear').on('click', function(){
        msg_waiting();
    	var ajax_url 		= $('#ajax-center-url').data('url');
    	var method 			= 'clearFilterToSession'
    	$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: ajax_url,
	        data: {
	            'method' : method,
	        },
	        success: function(result) {
	            if (result.status === 'success')       window.location.reload();
	        },
	    });
    });
	
});

function deleteShop(id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'id' : id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/vaccine';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}

function msg_waiting() {
	let timerInterval;
	Swal.fire({
		title: '',
		html: 'LOADING',
		timer: 2000,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading()
			timerInterval = setInterval(() => {
				const content = Swal.getContent()
				if (content) {
					const b = content.querySelector('b')
					if (b) {
						b.textContent = Swal.getTimerLeft()
					}
				}
			}, 100)
		},
		onClose: () => {
			clearInterval(timerInterval)
		}
	}).then((result) => {
		/* Read more about handling dismissals below */
		if (result.dismiss === Swal.DismissReason.timer) {
			console.log('I was closed by the timer')
		}
	});
}


function addFilterToSession() {
    var search_txt 		= $('#search_txt').val();
    var vaccine_type 	= $('#vaccine_type').dropdown('get value');
    var vaccine_status  = $('#vaccine_status').dropdown('get value');

    var ajax_url        = $('#ajax-center-url').data('url');
    var method          = 'addFilterToSession'

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' 		: method,
            'vaccine_type' 	: (vaccine_type != 'all') ? vaccine_type : '',
            'vaccine_status': (vaccine_status != 'all') ? vaccine_status : '',
        },
        success: function(result) {
            if (result.status === 'success')       window.location.reload();
        },
    });
}