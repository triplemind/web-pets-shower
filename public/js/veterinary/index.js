$(function(){

	DTRender('#TBL_veterinary', true);

	// console.log('ssss');
	$('.btn-addveterinary').on('click', function(){
		$('#shop_id').dropdown();
		$('#status').dropdown();
		$("#addVeterinaryodal").modal({
			closable: false,
			onDeny: function() {
			},
			onApprove: function() {
				addNewVeterinaryodal();
			}
		}).modal("show");
	});


	$('.editveterinary').on('click', function(){
		var id = $(this).data('id');

		var method 		= 'getVeterinaryData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'id' : id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$('#ed_Fname').val(result.data.Fname);
					$('#ed_Lname').val(result.data.Lname);
					$('#ed_vaterinary_code').val(result.data.vaterinary_code);
					$('#ed_tel').val(result.data.tel);
					$('#ed_remark').val(result.data.remark);
					$('#ed_shop_id').dropdown('set selected', result.data.shop_id);
					$('#ed_status').dropdown('set selected', result.data.status);

					setTimeout(function(){
						$("#EditVeterinaryModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editVeterinary(id);
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});


		
	});

	$('.deletveterinary').on('click', function(){
		console.log($(this).data('id'));
		var id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบข้อมูลสัตวแพทย์คนนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deleteVeterinary(id);
	        }
	    });
	});

});


function addNewVeterinaryodal() {
	var Fname 				= $('#Fname').val();
	var Lname 				= $('#Lname').val();
	var vaterinary_code 	= $('#vaterinary_code').val();
	var tel 				= $('#tel').val();
	var remark 				= $('#remark').val();
	var shop_id				= $('#shop_id').dropdown('get value');
	var status				= $('#status').dropdown('get value');

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	if (typeof($('#img')[0]) !== 'undefined') {

        jQuery.each(jQuery('#img')[0].files, function(i, file) {
            data.append('img', file);
            // console.log(file);
        });
    }

	data.append('Fname', Fname);
	data.append('Lname',Lname);
	data.append('vaterinary_code',vaterinary_code);
	data.append('tel',tel);
	data.append('status',status);
	data.append('shop_id',shop_id);
	data.append('remark',remark);
	
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/veterinary';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});

}

function editVeterinary(id) {
	var Fname 				= $('#ed_Fname').val();
	var Lname 				= $('#ed_Lname').val();
	var vaterinary_code 	= $('#ed_vaterinary_code').val();
	var tel 				= $('#ed_tel').val();
	var remark 				= $('#ed_remark').val();
	var shop_id				= $('#ed_shop_id').dropdown('get value');
	var status				= $('#ed_status').dropdown('get value');

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	if (typeof($('#ed_img')[0]) !== 'undefined') {

        jQuery.each(jQuery('#ed_img')[0].files, function(i, file) {
            data.append('img', file);
            console.log(file);
        });
    }

	data.append('id', id);
	data.append('Fname', Fname);
	data.append('Lname',Lname);
	data.append('vaterinary_code',vaterinary_code);
	data.append('tel',tel);
	data.append('status',status);
	data.append('shop_id',shop_id);
	data.append('remark',remark);

    
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: edit_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/veterinary';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});
}

function deleteVeterinary(id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	'id' : id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/veterinary';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}