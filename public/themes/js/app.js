var app = angular.module("myApp", ["ngRoute", "ngAnimate"]);
var isMobile = device.mobile();
var body = $("body");
app.run(function($rootScope, $http, $timeout, sphttp, $filter, $route) {
  moment.locale("th");
  $rootScope.PROVINCE = PROVINCE;
  $rootScope.CurrentUser = {};
  $rootScope.$route = $route; 
  $rootScope.loadingapp = "active";
  sphttp.getCurrentUser().then(function(response) {
    var Email = response.Email;
    $rootScope.CurrentUser = response;
    sphttp
      .getItems("UserAdmin", `$filter=EMail eq '${Email}'`)
      .then(function(resAdmin) {
        $timeout(function() {
          if (resAdmin.data.d.results.length) {
            $rootScope.isAdmin = true;
          } else {
            $rootScope.isAdmin = false;
          }
          console.log($rootScope.isAdmin);
          sphttp.getDataUser(Email).then(function(responseUser) {
            $timeout(function() {
              $rootScope.CurrentUser.info = responseUser;
              $rootScope.loadingapp = "";
              console.log($rootScope.CurrentUser);
            });
          });
        });
      });
  });
});
app.config(function($routeProvider) {
  $routeProvider
    .when("/", {
      redirectTo: "/home"
    })
    .when("/home", {
      templateUrl: model.templateUrl + "/HomeCon/HomeCon.htm",
      controller: "HomeCon",
      activetab: "home"
    })
    .when("/Booking", {
      templateUrl: model.templateUrl + "/BookCon/BookCon.htm",
      controller: "BookCon",
      activetab: "Booking"
    })
    .when("/Settings", {
      templateUrl: model.templateUrl + "/SettingsCon/SettingsCon.htm",
      controller: "SettingsCon",
      activetab: "Settings"
    })
    .when("/Settings/Admin", {
      templateUrl: model.templateUrl + "/AdminCon/AdminCon.htm",
      controller: "AdminCon"
    })
    .when("/Settings/Cartype", {
      templateUrl: model.templateUrl + "/CarTypeCon/CarTypeCon.htm",
      controller: "CarTypeCon"
    })
    .when("/Settings/RentalCars", {
      templateUrl: model.templateUrl + "/RentalCarCon/RentalCarCon.htm",
      controller: "RentalCarCon"
    })
    .when("/Settings/Drivers", {
      templateUrl: model.templateUrl + "/DriverCon/DriverCon.htm",
      controller: "DriverCon"
    })
    .when("/Settings/Drivers/Add", {
      templateUrl: model.templateUrl + "/AddDriverCon/AddDriverCon.htm",
      controller: "AddDriverCon"
    })
    .when("/Settings/Drivers/:id/Edit", {
      templateUrl: model.templateUrl + "/EditDriverCon/EditDriverCon.htm",
      controller: "EditDriverCon"
    })
    .when("/Settings/DayOff", {
      templateUrl: model.templateUrl + "/DayOffCon/DayOffCon.htm",
      controller: "DayOffCon"
    })
    .when("/Settings/CarCentralManage", {
      templateUrl: model.templateUrl + "/CarCentralManageCon/CarCentralManageCon.htm",
      controller: "CarCentralManageCon"
    })
    .when("/Settings/CarCentralManage/Add", {
      templateUrl: model.templateUrl + "/AddCarCentralManageCon/AddCarCentralManageCon.htm",
      controller: "AddCarCentralManageCon"
    })
    .when("/Settings/CarCentralManage/:id/Edit", {
      templateUrl: model.templateUrl + "/EditCarCentralManageCon/EditCarCentralManageCon.htm",
      controller: "EditCarCentralManageCon"
    })
    .when("/Settings/CarCentralManage/:id/Detail", {
      templateUrl: model.templateUrl + "/DetailCarCentralManageCon/DetailCarCentralManageCon.htm",
      controller: "DetailCarCentralManageCon"
    })
    .when("/Settings/RentalCar/Add", {
      templateUrl: model.templateUrl + "/AddRentalCarCon/AddRentalCarCon.htm",
      controller: "AddRentalCarCon"
    })
    .when("/Settings/RentalCar/:id/Edit", {
      templateUrl: model.templateUrl + "/EditRentalCarCon/EditRentalCarCon.htm",
      controller: "EditRentalCarCon"
    })
    .when("/CarResView/:id", {
      templateUrl: model.templateUrl + "/CarResViewCon/CarResViewCon.htm",
      controller: "CarResViewCon"
    })
    .when("/ApproveList", {
      templateUrl: model.templateUrl + "/ApproveListCon/ApproveListCon.htm",
      controller: "ApproveListCon",
      activetab: "ApproveList"
    })
    .when("/DriverView", {
      templateUrl: model.templateUrl + "/DriverViewCon/DriverViewCon.htm",
      controller: "DriverViewCon",
      activetab: "DriverView"
    })
    .when("/Rental", {
      templateUrl: model.templateUrl + "/RentCon/RentCon.htm",
      controller: "RentCon",
      activetab: "Rental"
    })
    .when("/RentApprove/:id/form", {
      templateUrl: model.templateUrl + "/RentApproveCon/RentApproveCon.htm",
      controller: "RentApproveCon"
    })
    .when("/ApproveBooking", {
      templateUrl: model.templateUrl + "/ApproveBookingCon/ApproveBookingCon.htm",
      controller: "ApproveBookingCon"
    })
    .when("/permission", {
      templateUrl: model.templateUrl + "/permission/permission.htm"
    })
    .when("/Settings/Vendor", {
      templateUrl: model.templateUrl + "/VendorsCon/VendorsCon.htm",
      controller: "VendorsCon"
    })
    .when("/approve/rent", {
      templateUrl: model.templateUrl + "/ApproveRentCarCon/ApproveRentCarCon.htm",
      controller: "ApproveRentCarCon"
    })
    .otherwise({
      template: "<h1>None</h1><p>Nothing has been selected</p>"
    });
});
app.controller("control", function($scope, $rootScope, $timeout, sphttp) {
  angular.element(() => {});
});
var DataTableOption = {
  //   "searching": false,
  // "buttons": [
  //     'copy', 'excel', 'pdf'
  // ],
  retrieve: true,
  dom:
    "<'ui stackable grid'" +
    "<'row dt-table'" +
    "<'sixteen wide column'tr>" +
    ">" +
    "<'row'" +
    "<'seven wide column'i>" +
    "<'right aligned nine wide column'p>" +
    ">" +
    ">",
  ordering: false,
  lengthChange: false,
  pageLength: 10,
  language: {
    decimal: "",
    emptyTable: "ไม่มีข้อมูลในตาราง",
    info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
    infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
    infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
    infoPostFix: "",
    thousands: ",",
    lengthMenu: "แสดง _MENU_ รายการ",
    loadingRecords: "กำลังโหลด...",
    processing: "กำลังคำนวน...",
    search: "ค้นหา:",
    zeroRecords: "ไม่เจอรายการที่ค้นหา",
    paginate: {
      first: "หน้าแรก",
      last: "หน้าสุดท้าย",
      next: ">",
      previous: "<"
    },
    aria: {
      sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
      sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
    }
  }
};
