<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PETs CARE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.css">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/dataTables.semanticui.min.css">
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />

    <style type="text/css">
        .body-background-image{
            position: absolute;
            z-index: -1;
            width: 100%;
            height: 100%;
            /*background-image: url('http://127.0.0.1:8001/public/themes/image/bgnow.png');*/
            background-size: cover;
            background-color: #fff;
            /*opacity: 0.7;*/
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
    </style>
    
</head>

<body>
    <div class="ui" style="">
        <div class="ui grid" style="">
            <div class="computer only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui small menu" style="background-color: #FEE265;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <img class="ui mini image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png'; ?>">
                            </a>
                            <div class="ui dropdown <?php echo (helperGetAction() == 'search') ? 'active' : '' ?> item" id="drop_search">
                                บริการของเรา <i class="dropdown icon"></i>
                                <div class="menu">
                                    <a class="item" href="/search/อาบน้ำ">อาบน้ำ</a>
                                    <a class="item" href="/search/ตัดขน">ตัดขน</a>
                                    <a class="item" href="/search/ฉีดวัคซีน">ฉีดวัคซีน</a>
                                </div>
                            </div>
                            <a class="<?php echo (helperGetAction() == 'shopnearby') ? 'active' : '' ?> item" href="/shopnearby">
                                ร้านค้า
                            </a>
                            <a class="<?php echo (helperGetAction() == 'vaccinedetail') ? 'active' : '' ?> item" href="/vaccinedetail">
                                ข้อมูลวัคซีน
                            </a>
                            <a class="<?php echo (helperGetAction() == 'contactus') ? 'active' : '' ?> item" href="/contactus">
                                ติดต่อเรา
                            </a>
                            <a class="<?php echo (helperGetAction() == 'aboutus') ? 'active' : '' ?> item" href="/aboutus">
                                เกี่ยวกับเรา
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tablet mobile only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui small menu" style="background-color: #FEE265;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <img class="ui mini image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png'; ?>">
                            </a>
                            <!-- <div class="ui dropdown <?php echo (helperGetAction() == 'search') ? 'active' : '' ?> item" id="drop_search_mobile">
                                <i class="sidebar icon"></i>
                                <div class="menu">
                                    <a class="item" href="/search/อาบน้ำ">อาบน้ำ</a>
                                    <a class="item" href="/search/ตัดขน">ตัดขน</a>
                                    <a class="item" href="/search/ฉีดวัคซีน">ฉีดวัคซีน</a>
                                    <a class="item" href="/shopnearby">
                                        ร้านค้า
                                    </a>
                                    <a class="item" href="/vaccinedetail">
                                        ข้อมูลวัคซีน
                                    </a>
                                    <a class="item" href="/contactus">
                                        ติดต่อเรา
                                    </a>
                                    <a class="item" href="/aboutus">
                                        เกี่ยวกับเรา
                                    </a>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
    <div class="body-background-image"></div>
    <div class="pusher view-animate-container">
        <div class="ui"  style="padding-left: 0%;padding-right: 0%;">
            <!-- <div class="ui divider"></div> -->
            <div class="ui two column grid">
                <div class="row">
                    <div class="sixteen wide column" style="text-align: center;">
                        <div class="ui grid" style="">
                            <div class="computer only row">
                                <div class="column">
                                    <img class="ui centered medium image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/logo.png"; ?>">
                                </div>
                            </div>

                            <div class="tablet mobile only row">
                                <div class="column">
                                    <img class="ui centered small image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/logo.png"; ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="four wide middle aligned content column" style="text-align: center;">
                        
                    </div>
                    <div class="eight wide column" style="text-align: center;">
                        <div class="ui grid" style="">
                            <div class="computer only row">
                                <div class="column">
                                    <div class="ui segment" style="background-color: #FFEEBA;border-radius: 5px;">
                                        <p><span class="ui large text" style="color: #5DACBD;font-weight: 700;">ลงชื่อเข้าใช้</span></p>
                                        <form class="ui form" id="sign_in" method="POST" action="<?php echo \URL::route('auth.login.post'); ?>">
                                            <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
                                            <div class="field">
                                                <!-- <label>First Name</label> -->
                                                <input type="text" name="username" placeholder="ชื่อผู้ใช้งานหรืออีเมล">
                                            </div>
                                            <div class="field">
                                                <!-- <label>Last Name</label> -->
                                                <!-- <input type="password" name="password" placeholder="รหัสผ่าน"> -->
                                                <div class="ui icon input">
                                                    <input type="password" placeholder="รหัสผ่าน" name="password" id="password" maxlength="8">
                                                    <i class="eye link icon toggle-password" toggle="#password"></i>
                                                </div>
                                            </div>
                                            <div class="two fields">
                                                <div class="field">
                                                    <a class="ui mini teal button" href="<?php echo \URL::route('auth.register.get'); ?>" style="border-radius: 30px;"><span class="ui medium text" style="color: #000;">ลงทะเบียนสำหรับผู้ใช้ทั่วไป</span></a>
                                                </div>
                                                <div class="field">
                                                    <a class="ui mini teal button" href="<?php echo \URL::route('auth.registerowner.get'); ?>" style="border-radius: 30px;"><span class="ui medium text" style="color: #000;">ลงทะเบียนสำหรับเจ้าของกิจการ</span></a>
                                                </div>
                                            </div>
                                            <div class="field">
                                                <a href="<?php echo \URL::route('auth.forgotpassword.get'); ?>"><span class="ui medium text" style="color: #e41414;">ลืมรหัสผ่าน</span></a>
                                            </div>
                                            <button class="ui primary button" style="border-radius: 30px;background-color: #FEE265;color: #000" type="submit" id="btn_login">เข้าสู่ระบบ</button>
                                        </form>

                                        <div class="field">
                                            <?php if (count($errors) > 0): ?>
                                                <div class="alert alert-danger alert-dismissible text-center" style="color: red;font-weight: 700;">
                                                    <?php foreach ($errors->all() as $error): ?>
                                                        <?php echo $error ?>
                                                    <?php endforeach ?>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tablet mobile only row">
                                <div class="column">
                                    <div class="ui segment" style="background-color: #FFEEBA;border-radius: 5px;">
                                        <p><span class="ui large text" style="color: #5DACBD;font-weight: 700;">ลงชื่อเข้าใช้</span></p>
                                        <form class="ui form" id="sign_in" method="POST" action="<?php echo \URL::route('auth.login.post'); ?>">
                                            <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
                                            <div class="field">
                                                <!-- <label>First Name</label> -->
                                                <input type="text" name="username" placeholder="ชื่อผู้ใช้งานหรืออีเมล">
                                            </div>
                                            <div class="field">
                                                <!-- <label>Last Name</label> -->
                                                <!-- <input type="password" name="password" placeholder="รหัสผ่าน"> -->
                                                <div class="ui icon input">
                                                    <input type="password" placeholder="รหัสผ่าน" name="password" id="password" maxlength="8">
                                                    <i class="eye link icon toggle-password" toggle="#password"></i>
                                                </div>
                                            </div>
                                            <div class="two fields">
                                                <div class="field">
                                                    <a class="ui mini teal button" href="<?php echo \URL::route('auth.register.get'); ?>" style="border-radius: 30px;"><span class="ui medium text" style="color: #000;">ลงทะเบียนสำหรับผู้ใช้ทั่วไป</span></a>
                                                </div>
                                                <div class="field">
                                                    <a class="ui mini teal button" href="<?php echo \URL::route('auth.registerowner.get'); ?>" style="border-radius: 30px;"><span class="ui medium text" style="color: #000;">ลงทะเบียนสำหรับเจ้าของกิจการ</span></a>
                                                </div>
                                            </div>
                                            <div class="field">
                                                <a href="<?php echo \URL::route('auth.forgotpassword.get'); ?>"><span class="ui medium text" style="color: #e41414;">ลืมรหัสผ่าน</span></a>
                                            </div>
                                            <button class="ui primary button" style="border-radius: 30px;background-color: #FEE265;color: #000" type="submit" id="btn_login">เข้าสู่ระบบ</button>
                                        </form>

                                        <div class="field">
                                            <?php if (count($errors) > 0): ?>
                                                <div class="alert alert-danger alert-dismissible text-center" style="color: red;font-weight: 700;">
                                                    <?php foreach ($errors->all() as $error): ?>
                                                        <?php echo $error ?>
                                                    <?php endforeach ?>
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>




    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/current-device.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/calendarTH.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment-with-locales.js"></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/interaction/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-common/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/sweetalert2.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/lodash.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.dataTables.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/dataTables.semanticui.min.js'></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-route.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-animate.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/cleave.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/addons/cleave-phone.th.js"></script>


    <script type="text/javascript">
        $(function(){
            // var arr_bg      = ['bg_1.jpg', 'bg_2.jpg', 'bg_3.jpg'];
            // var no_random   = Math.floor(Math.random() * 3);
            // console.log(arr_bg[no_random]);
            // $('.body-background-image').css('background-image', 'url(http://127.0.0.1:8001/public/themes/image/bgnow.png)');
           

            $(".toggle-password").click(function() {
                $(this).toggleClass("slash");
                var input = $($(this).attr("toggle"));

                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });


            $('#drop_search').dropdown();
            $('#drop_search_mobile').dropdown();
        });
    </script>
</body>

</html>