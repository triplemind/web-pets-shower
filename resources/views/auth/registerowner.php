<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PETs CARE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.css">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/dataTables.semanticui.min.css">
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />

    <style type="text/css">
        .body-background-image{
            position: absolute;
            z-index: -1;
            width: 100%;
            height: 100%;
            /*background-image: url('http://127.0.0.1:8000/public/themes/image/bg.png');*/
            background-size: cover;
            background-color: #fff;
            /*background-image: linear-gradient(315deg, #7ee8fa 0%, #80ff72 74%);*/
            /*background-color: #fff;
            background-image: linear-gradient(315deg, #fff 0%, #fff 74%);*/
            /*opacity: 0.7;*/
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            /*min-height: 900px;*/
        }
    </style>



    
</head>

<body>
       
    <div class="ui" style="">

        <div class="ui grid" style="">
            <div class="computer only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui small menu" style="background-color: #FEE265;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <img class="ui mini image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png'; ?>">
                            </a>
                            <div class="ui dropdown <?php echo (helperGetAction() == 'search') ? 'active' : '' ?> item" id="drop_search">
                                บริการของเรา <i class="dropdown icon"></i>
                                <div class="menu">
                                    <a class="item" href="/search/อาบน้ำ">อาบน้ำ</a>
                                    <a class="item" href="/search/ตัดขน">ตัดขน</a>
                                    <a class="item" href="/search/ฉีดวัคซีน">ฉีดวัคซีน</a>
                                </div>
                            </div>
                            <a class="<?php echo (helperGetAction() == 'shopnearby') ? 'active' : '' ?> item" href="/shopnearby">
                                ร้านค้า
                            </a>
                            <a class="<?php echo (helperGetAction() == 'vaccinedetail') ? 'active' : '' ?> item" href="/vaccinedetail">
                                ข้อมูลวัคซีน
                            </a>
                            <a class="<?php echo (helperGetAction() == 'contactus') ? 'active' : '' ?> item" href="/contactus">
                                ติดต่อเรา
                            </a>
                            <a class="<?php echo (helperGetAction() == 'aboutus') ? 'active' : '' ?> item" href="/aboutus">
                                เกี่ยวกับเรา
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="tablet mobile only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui small menu" style="background-color: #FEE265;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <img class="ui mini image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png'; ?>">
                            </a>
                            <!-- <div class="ui dropdown <?php echo (helperGetAction() == 'search') ? 'active' : '' ?> item" id="drop_search_mobile">
                                <i class="sidebar icon"></i>
                                <div class="menu">
                                    <a class="item" href="/search/อาบน้ำ">อาบน้ำ</a>
                                    <a class="item" href="/search/ตัดขน">ตัดขน</a>
                                    <a class="item" href="/search/ฉีดวัคซีน">ฉีดวัคซีน</a>
                                    <a class="item" href="/shopnearby">
                                        ร้านค้า
                                    </a>
                                    <a class="item" href="/vaccinedetail">
                                        ข้อมูลวัคซีน
                                    </a>
                                    <a class="item" href="/contactus">
                                        ติดต่อเรา
                                    </a>
                                    <a class="item" href="/aboutus">
                                        เกี่ยวกับเรา
                                    </a>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="body-background-image"></div>
    <div class="pusher view-animate-container">
        <div class="ui container"  style="padding-left: 0%;padding-right: 0%;">
            <!-- <div class="ui divider"></div> -->
            <div class="ui two column grid">
                <!-- <div class="row">
                    <div class="sixteen wide column" style="text-align: center;">
                        <img class="ui centered small image" src="http://127.0.0.1:8000/public/themes/image/ccc.png">
                        <br>
                    </div>
                </div> -->
                <div class="row">
                    <div class="two wide middle aligned content column">
                        
                    </div>
                    <div class="twelve wide middle aligned content column">
                        <div class="ui segment" style="background-color: #FFEEBA;border-radius: 5px;margin-top: 3rem;">
                            <img class="ui centered small image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/logo.png"; ?>">
                            <br>
                            <p style="text-align: center;"><span class="ui large text" style="color: #000;font-weight: 700;">ลงทะเบียนสำหรับเจ้าของกิจการ</span></p>
                            <div class="ui form">
                                <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
                                <div class="required field usernametxt">
                                    <label>ชื่อผู้ใช้งาน</label>
                                    <input type="text" placeholder="" name="username" id="username">
                                </div>
                                <div class="two fields">
                                    <div class="required field passwordtxt">
                                        <label>รหัสผ่าน</label>
                                        <!-- <input type="text" placeholder="" class="password" name="password" id="password"> -->
                                        <div class="ui icon input">
                                            <input type="password" placeholder="รหัสผ่าน" class="password" name="password" id="password" maxlength="8">
                                            <i class="eye link icon toggle-password" toggle="#password"></i>
                                        </div>
                                    </div>
                                    <div class="required field confirm_passtxt">
                                        <label>ยืนยันรหัสผ่าน</label>
                                        <div class="ui icon input">
                                            <input type="password" class="confirm_pass" placeholder="ยืนยันรหัสผ่าน" name="cfnewpassword" id="cfnewpassword" maxlength="8">
                                            <i class="eye link icon toggle-password" toggle="#cfnewpassword"></i>
                                        </div>
                                        <!-- <input type="password" class="confirm_pass" placeholder="ยืนยันรหัสผ่าน" name="cfnewpassword" id="cfnewpassword"> -->
                                    </div>
                                </div>
                                <div class="two fields required">
                                    <div class="field first_nametxt">
                                        <label>ชื่อ</label>
                                        <input type="text" placeholder="" name="first_name" id="first_name">
                                    </div>
                                    <div class="field last_nametxt">
                                        <label>นามสกุล</label>
                                        <input type="text" placeholder="" name="last_name" id="last_name">
                                    </div>
                                </div>
                                <div class="two fields required">
                                    <div class="field emailtxt">
                                        <label>อีเมล</label>
                                        <input type="text" placeholder="" name="email" id="email">
                                    </div>
                                    <div class="field teltxt">
                                        <label>เบอร์โทรศัพท์</label>
                                        <input type="text" placeholder="" name="tel" id="tel">
                                    </div>
                                </div>
                                <!-- <div class="field">
                                   <label>ที่อยู่อาศัย</label>
                                    <textarea rows="3" name="address" id="address"></textarea>
                                </div> -->
                                <div class="field">
                                    <!-- <label>รูปภาพ</label>
                                    <input type="file" placeholder="" name="user_img" id="user_img"> -->
                                    <div class="ui fluid action input">
                                        <label for="user_img" class="ui teal labeled icon button">
                                            <i class="upload icon"></i>
                                            เลือกรูปภาพ
                                        </label>
                                        <input type="text" id="txtimg" readonly placeholder="รูปภาพของคุณ">
                                    </div>
                                    <input name="user_img" id="user_img" style="visibility:hidden;" type="file">
                                </div>
                                <div class="field" style="text-align: center;">
                                    <div class="two fields">
                                        <div class="field">
                                            <button class="ui primary button" style="background-color: #FEE265;color: #000" type="submit" id="btn-register">ลงทะเบียน</button>
                                        </div>
                                        <div  class="field">
                                            <button class="ui primary button" style="background-color: #808080;" type="submit" id="btn_back">ย้อนกลับ</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="field">
                                <div class="alert alert-dismissible text-center err-pass" style="text-align: center;">
                                    <span id='message'></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two wide middle aligned content column">
                        
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- URL สำหรับ ส่งข้อมูลไป check login -->
    <!-- <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" /> -->
    <div id="register_url" data-url="<?php echo \URL::route('auth.register.post'); ?>"></div>



    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/current-device.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/calendarTH.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment-with-locales.js"></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/interaction/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-common/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/sweetalert2.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/lodash.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.dataTables.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/dataTables.semanticui.min.js'></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-route.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-animate.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/cleave.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/addons/cleave-phone.th.js"></script>


    <script type="text/javascript">
        $(function(){

            var cleave = new Cleave('#tel', {
                // numericOnly: true,
                phone: true,
                phoneRegionCode: 'th'
                // blocks: [3, 3, 4],
                // delimiters: ["(", ")", " ", "-"]
            });

            // var arr_bg      = ['bg_1.jpg', 'bg_2.jpg', 'bg_3.jpg'];
            // var no_random   = Math.floor(Math.random() * 3);
            // console.log(arr_bg[no_random]);
            // $('.body-background-image').css('background-image', 'url(http://127.0.0.1:8000/public/themes/image/bg.png)');

            $("#user_img").change(function() {
                console.log(this.files);
                if(this.files.length !== 0){
                    filename = this.files[0].name
                    console.log(filename);
                    $('#txtimg').val(filename);
                }
            });

            $(".toggle-password").click(function() {
                $(this).toggleClass("slash");
                var input = $($(this).attr("toggle"));

                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
            });

            $('.password, .confirm_pass').on('keyup', function () {
                $('.err-pass').addClass('alert-danger');
                $('#message').css('font-weight', 'bold');
                $('#message').css('font-size', '18px');
                if ($('.password').val() == $('.confirm_pass').val()) {
                    $('#message').html('รหัสผ่านตรงกัน').css('color', 'green');
                    
                    $('.passwordtxt').removeClass('error');
                    $('.confirm_passtxt').removeClass('error');

                    $('.passwordtxt').addClass('success');
                    $('.confirm_passtxt').addClass('success');

                    console.log('success');

                } else 
                    $('#message').html('รหัสผ่านไม่ตรงกัน').css('color', 'red');

                    console.log('error');

                    $('.passwordtxt').addClass('error');
                    $('.confirm_passtxt').addClass('error');
            });
            

            $('#drop_search').dropdown();

            $('#btn-register').on('click', function(){
                addNewUser();
            });
            

            $('#btn_back').on('click', function(){
                window.location.href = '/auth/login';
            });
        });


        function addNewUser() {
            var username        = $('#username').val();
            var password        = $('#password').val();
            var first_name      = $('#first_name').val();
            var last_name       = $('#last_name').val();
            var email           = $('#email').val();
            var address         = null;
            var tel             = $('#tel').val();
            var user_type       = 'เจ้าของกิจการ';
            var telVal          = tel.replace(" ", "");


            var add_url         = $('#register_url').data('url');
            var data            = new FormData();
            console.log("mind@owner.com", add_url);

            if (typeof($('#user_img')[0]) !== 'undefined') {

                jQuery.each(jQuery('#user_img')[0].files, function(i, file) {
                    data.append('user_img', file);
                    // console.log(file);
                });
            }

            data.append('username', username);
            data.append('password',password);
            data.append('first_name',first_name);
            data.append('last_name',last_name);
            data.append('email',email);
            data.append('address',address);
            data.append('tel',telVal.replace(" ", ""));
            data.append('user_type',user_type);

            console.log(telVal.replace(" ", ""));
            // console.log(JSON.stringify(menu_accress));

            // check format email
            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

            if(username || password || first_name || last_name || email || address || tel){
                if (testEmail.test(email)){
                    $.ajax({
                        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                        type: 'post',
                        url: add_url,
                        data: data,
                        contentType: false,
                        processData:false,
                        cache: false,
                        success: function(result) {
                            if(result.status == 'success'){
                                setTimeout(function(){ 
                                    $("body").toast({
                                        class: "success",
                                        position: 'bottom right',
                                        message: `บันทึกเสร็จสิ้น`
                                    });
                                    setTimeout(function(){ 
                                        window.location.href = '/auth/login';
                                    }, 1000);
                                });
                            } // End if check s tatus success.

                            if(result.status == 'error'){
                                $("body").toast({
                                    class: "error",
                                    position: 'bottom right',
                                    message: result.msg
                                });
                            }
                        },
                        error : function(error) {
                            // showForm(form, action, error, data);
                        }
                    });
                }else{
                    $("body").toast({
                        class: "error",
                        displayTime: 10000,
                        position: 'bottom right',
                        message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
                    });
                }
            }else{
                $("body").toast({
                    class: "error",
                    displayTime: 10000,
                    position: 'bottom right',
                    message: `กรุณากรอกข้อมูลให้ครบถ้วน`
                });
            }

        }

    </script>
</body>

</html>