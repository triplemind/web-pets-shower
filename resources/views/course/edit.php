<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="ad icon"></i>
                <div class="content">
                    แก้ไขข้อมูลคอร์ส
                </div>
            </h3>
        </div>
        <div class="right floated column">
           
        </div>
    </div>

	<div class="ui form segment">
		<form class="ui form">
			<input type="hidden" name="id" id="id" value="<?php echo $courses->id; ?>">
			<div class="two fields">
				<div class="field">
					<label>ชื่อคอร์ส</label>
					<input type="text" name="name" id="name" placeholder="ชื่อคอร์ส" value="<?php echo $courses->name; ?>">
				</div>
				<div class="field">
					<label>สถานะ</label>
					<select class="ui fluid search selection dropdown" name="status" id="status" >
	                    <option value="Active" <?php echo $courses->status == "Active" ? "selected" : ""; ?>>Active</option>
	                    <option value="Inactive" <?php echo $courses->status == "Inactive" ? "selected" : ""; ?>>Inactive</option>
	                </select>
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>วันที่เริ่มคอร์ส</label>
					<div class="ui calendar" id="startdate">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="วันที่เริ่มคอร์ส" readonly name="startdate" value="<?php echo $courses->startdate; ?>">
                        </div>
                    </div>
				</div>
				<div class="field">
					<label>วันหมดอายุคอร์ส</label>
					<div class="ui calendar" id="enddate">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="วันหมดอายุคอร์ส" readonly name="enddate" value="<?php echo $courses->enddate; ?>">
                        </div>
                    </div>
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>จำนวนครั้งในการใช้บริการ</label>
					<input type="number" name="times" id="times" placeholder="จำนวนครั้งในการใช้บริการ" value="<?php echo $courses->numoftimes; ?>">
				</div>
				<div class="field">
					<label>ราคา</label>
					<input type="number" name="price" id="price" placeholder="ราคา" value="<?php echo $courses->price; ?>">
				</div>
			</div>
            <br>
            <br>
            <div class="field">
                <label>รายละเอียดเพิ่มเติม</label>
                <textarea name="description" id="description" rows="10" cols="80"><?php echo $courses->description; ?>
                </textarea>
            </div>
		</form>

        <br>
        <br>

        <div class="ui two column grid">
            <div class="column">
                <button class="ui fluid big red button btn-back" type="submit" >ยกเลิก</button>
            </div>
            <div class="column">
                <button class="ui fluid big blue button btn-save" type="submit">บันทึก</button>
            </div>
        </div>
	</div>

</div>



<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="add_url" data-url="<?php echo \URL::route('course.edit.post'); ?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('course.ajax_center.post');?>"></div>
