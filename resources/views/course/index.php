<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="book icon"></i>
                <div class="content">
                    หน้าจัดการคอร์ส
                </div>
            </h3>
        </div>

        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big green button btn-addpromotion" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลคอร์ส</button>
                </div>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_report">
                    <thead>
                        <tr>
                            <th>ชื่อคอร์ส</th>
                            <th>ราคา</th>
                            <th>จำนวนครั้งในการใช้บริการ</th>
                            <th>ร้านค้า</th>
                            <th>วันที่เริ่มคอร์ส</th>
                            <th>วันหมดอายุคอร์ส</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php if(!empty($courses)): ?>
                            <?php foreach ($courses as $key => $course):?>
                                <tr>
                                    <td>
                                        <?php echo empty($course->name) ? '-' : $course->name ?>
                                    </td>
                                    <td>
                                        <?php echo empty($course->price) ? '-' : $course->price; ?>
                                    </td>
                                    <td>
                                        <?php echo empty($course->numoftimes) ? '-' : $course->numoftimes; ?>
                                    </td>
                                    <td>
                                        <?php echo empty($course->shop) ? '-' : $course->shop->name_shop; ?>
                                    </td>
                                    <td>
                                        <?php echo empty($course->startdate) ? '-' : DateThai($course->startdate, true, false); ?>
                                    </td>
                                    <td>
                                        <?php echo empty($course->enddate) ? '-' : DateThai($course->enddate, true, false); ?>
                                    </td>
                                    <td>
                                        <?php echo empty($course->status) ? '-' : $course->status; ?>
                                    </td>
                                    <td>
                                        <i class="large eye outline icon viewpromotion" data-id="<?php echo $course->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large edit outline icon editpromotion" data-id="<?php echo $course->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deletepromotion" data-id="<?php echo $course->id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr> 
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <?php echo "รายการทั้งหมด  ".number_format($courses->total())."  รายการ"; ?>
                <div class="add-page" style="float: right;">
                    <?php echo $courses->render(); ?>
                </div>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('course.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('course.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('course.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('course.delete.post');?>"></div>

