<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="syringe icon"></i>
                <div class="content">
                    รายละเอียดคอร์ส
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>

    <?php 
        $lb_color = "";
        $status_txt = "";
        if($courses->status == "Active"){
            $lb_color = 'green';
            $status_txt = "Active";
        }else if($courses->status == "Inactive"){
            $lb_color = 'red';
            $status_txt = "Inactive";
        }
    ?>



    <div class="ui form segment">
        <div class="top right attached large ui label">
            <div class="ui <?php echo $lb_color; ?> empty circular label"></div> <?php echo $status_txt ?>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui table">
                    <tbody>
                        <tr>
                            <td class="two wide active">ชื่อคอร์ส</td>
                            <td class="six wide"><?php echo $courses->name ?></td>
                            <td class="two wide active">ร้านค้า</td>
                            <td class="six wide"><?php echo empty($courses->shop) ? "-" :$courses->shop->name_shop; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">จำนวนครั้งในการใช้บริการ</td>
                            <td class="six wide"><?php echo $courses->numoftimes ?></td>
                            <td class="two wide active">ราคา</td>
                            <td class="six wide"><?php echo $courses->price; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">รายละเอียดเพิ่มเติม</td>
                            <td class="six wide" colspan="3"><?php echo $courses->description ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    
</div>
