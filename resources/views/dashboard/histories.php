<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="clipboard outline icon"></i>
                <div class="content">
                    ประวัติรายการจองทั้งหมด
                </div>
            </h3>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <h4 class="ui header">ค้นหารายการจอง</h4>
        <div class="fields">
            <div class="six wide field">
                <label>วันที่ทำการจองเริ่มต้น</label>
                <div class="ui calendar" id="rangestart">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" placeholder="วันที่เริ่มต้น" value="<?php echo isset($filters['rangestart'])? date('d-m-Y',strtotime($filters['rangestart'])) : '' ?>">
                    </div>
                </div>
            </div>
            <div class="six wide field">
                <label>วันที่ทำการจองสิ้นสุด</label>
                <div class="ui calendar" id="rangeend">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" placeholder="วันที่สิ้นสุด" value="<?php echo isset($filters['rangeend'])? date('d-m-Y',strtotime($filters['rangeend'])) : '' ?>">
                    </div>
                </div>
            </div>
            <div class="four wide field">
                <?php $filters['order_status'] = isset($filters['order_status']) ? $filters['order_status'] : '' ?>
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="order_status" id="order_status" >
                    <option value="all" <?php echo $filters['order_status'] == 'all' ? 'selected' : '' ?>>ทั้งหมด</option>
                    <option value="เสร็จสิ้น" <?php echo $filters['order_status'] == 'เสร็จสิ้น' ? 'selected' : '' ?>>เสร็จสิ้น</option>
                    <option value="ชำระเงินแล้ว" <?php echo $filters['order_status'] == 'ชำระเงินแล้ว' ? 'selected' : '' ?>>ชำระเงินแล้ว</option>
                    <option value="รอชำระเงิน" <?php echo $filters['order_status'] == 'รอชำระเงิน' ? 'selected' : '' ?>>รอชำระเงิน</option>
                    <option value="รอการตรวจสอบยอดเงิน" <?php echo $filters['order_status'] == 'รอการตรวจสอบยอดเงิน' ? 'selected' : '' ?>>รอการตรวจสอบยอดเงิน</option>
                    <option value="ยกเลิกรายการ" <?php echo $filters['order_status'] == 'ยกเลิกรายการ' ? 'selected' : '' ?>>ยกเลิกรายการ</option>
                </select>
            </div>
        </div>
        <div class="ui unstackable four column grid ">
            <div class="column">
            </div>
            <div class="right floated column">
                <div class="fields">
                    <div class="eight wide field">
                        <button class="ui fluid small blue button btn-search" type="submit" style="border-radius: 30px;">ค้นหา</button>
                    </div>
                    <div class="eight wide field">
                        <button class="ui fluid small red button btn-clear" type="submit" style="border-radius: 30px;">ล้าง</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                 <table class="ui teal table" id="TBL_onlinelist">
                    <thead>
                        <tr>
                            <th>วันที่ทำรายการ</th>
                            <th>วันที่นัด</th>
                            <th>ช่วงเวลา</th>
                            <th>สัตว์เลี้ยง</th>
                            <th>ร้านค้า</th>
                            <th>บริการ</th>
                            <th>สถานะ</th>
                            <th>สถานะการเลื่อนนัด</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($getServices)): ?>
                            <?php if($getServices->count() != 0): ?>
                                <?php foreach ($getServices as $key => $getService): ?>
                                    <tr>
                                        <td>
                                            <?php
                                                // $datetoday = date('Y-m-d H:i:s');
                                                $createddate = DateThai($getService->created_at, true, false);
                                            ?>
                                            <?php echo $createddate ?>
                                                
                                        </td>
                                        <td>
                                            <?php
                                                // $datetoday = date('Y-m-d H:i:s');
                                                $datetoday = DateThai($getService->booking_date, true, false);
                                            ?>
                                            <?php echo $datetoday ?>
                                                
                                        </td>
                                        <td>
                                            <?php 
                                                $booking_time_txt = "";
                                                if($getService->booking_time == "บ่าย"){
                                                    // $booking_time_txt = "บ่าย 13.30น.-19.00น.";
                                                    $booking_time_txt = "บ่าย : ".(empty($getService->endTime) ? "13.30น.-19.00น." : $getService->endTime);
                                                }else if($getService->booking_time == "เช้า"){
                                                    // $booking_time_txt = "เช้า 09.00น.-12.30น.";
                                                    $booking_time_txt = "เช้า : ".(empty($getService->startTime) ? "09.00น.-12.30น." : $getService->startTime);
                                                }
                                            ?>
                                            <?php echo $booking_time_txt; ?>
                                        </td>
                                        <td><?php echo empty($getService->pet) ? '' : $getService->pet->name ?></td>
                                        <td><?php echo empty($getService->shop) ? '' : $getService->shop->name_shop ?></td>
                                        <td>
                                            <?php 
                                                $service_txt = "";
                                                if($getService->id_service == "PG0001"){
                                                    $service_txt = "อาบน้ำ-ตัดขน";
                                                }else if($getService->id_service == "PG0002"){
                                                    $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
                                                }else if($getService->id_service == "PV0001"){
                                                    $service_txt = "วัคซีน";
                                                }else if($getService->id_service == "PV0002"){
                                                    $service_txt = "โปรโมชันวัคซีน";
                                                }
                                            ?>
                                            <?php echo $service_txt; ?>
                                        </td>
                                        <td>
                                            <?php 
                                                $service_statustxt = "";
                                                if($getService->status == "รอชำระเงิน"){
                                                    $service_statustxt = "red";
                                                }else if($getService->status == "รอการตรวจสอบยอดเงิน"){
                                                    $service_statustxt = "orange";
                                                }else if($getService->status == "การใช้บริการเสร็จสิ้น"){
                                                    $service_statustxt = "green";
                                                }else if($getService->status == "ชำระเงินแล้ว"){ 
                                                    $service_statustxt = "blue";
                                                }
                                            ?>

                                            
                                            <span class="ui <?php echo $service_statustxt ?> label"><?php echo $getService->status ?></span>
                                        </td>
                                        <td>
                                            <?php 
                                                $postponeAdate_statustxt = "";
                                                if($getService->postponeAdate_status == "รอตรวจสอบ"){
                                                    $postponeAdate_statustxt = "blue";
                                                }else if($getService->postponeAdate_status == "ไม่สามารถเลื่อนนัดได้"){
                                                    $postponeAdate_statustxt = "red";
                                                }else if($getService->postponeAdate_status == "เลื่อนนัดสำเร็จ"){
                                                    $postponeAdate_statustxt = "yellow";
                                                }
                                            ?>
                                            
                                            <?php if($getService->postponeAdate == 1): ?>
                                                <span class="ui <?php echo $postponeAdate_statustxt ?> label"><?php echo $getService->postponeAdate_status ?></span>
                                            <?php else: ?>
                                                -
                                            <?php endif ?>
                                        </td>
                                        <td>
                                            <button class="ui teal button btn-view" onclick="window.location.href = '/admin/dashboard/orderdetail/<?php echo $getService->id_petservice; ?>'" type="submit">ดูรายละเอียด</button>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>
                        <?php endif ?>
                    </tbody>
                </table>

                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('dashboard.ajax_center.post');?>"></div>

