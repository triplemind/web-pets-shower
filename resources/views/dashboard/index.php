<style type="text/css">
    #myChart {
        width: 550px !important;
        height: 550px !important; 
    }

    #myChart1 {
        width: 550px !important;
        height: 550px !important; 
    }

    #myChart2 {
        width: 550px !important;
        height: 550px !important; 
    }
</style>

<div class="ui basic segment">

    <?php if(!empty($getshop)): ?>
        <br>
        <br>
        
        <div class="ui form segment">
                <h2><?php echo $getshop->name_shop;  ?></h2>
        </div>
        
        <br>
        <br>
    <?php endif ?>

    <div class="ui form segment">
        <div class="fields">
            <div class="eight wide field">
                 <h4 class="ui header">ประจำวันที่ <?php echo $datetoday; ?></h4>
            </div>
            <div class="eight wide field">
                <button class="ui blue right floated button" onclick="window.location = '/admin/dashboard/histories';">รายการจองทั้งหมด</button>
            </div>
        </div>
        <div class="fields">
            <div class="two wide field" style="margin-top: .4rem;text-align: center;">
                <p style="font-size: 16px;">เลือกค้นหาแบบ</p>
            </div>
            <div class="four wide field">
                <?php $filters['stat_range'] = isset($filters['stat_range']) ? $filters['stat_range'] : '' ?>
               <select class="ui fluid search selection dropdown" name="stat_range" id="stat_range" >
                    <option>เลือกการค้นหา</option>
                    <option value="day" <?php echo $filters['stat_range'] == 'day' ? 'selected' : '' ?>>วันนี้</option>
                    <!-- <option value="week">รายสัปดาห์</option> -->
                    <option value="month" <?php echo $filters['stat_range'] == 'month' ? 'selected' : '' ?>>รายเดือน</option>
                    <option value="year" <?php echo $filters['stat_range'] == 'year' ? 'selected' : '' ?>>รายปี</option>
                </select>
            </div>
            <div class="two wide field" style="margin-top: .4rem;text-align: center;">
                <p style="font-size: 16px;">หรือค้นหาแบบ</p>
            </div>
            <div class="three wide field">
                <div class="ui calendar" id="rangestart">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" placeholder="วันเริ่มต้นค้นหา" value="<?php echo isset($filters['rangestart']) ? $filters['rangestart'] : '' ?>">
                    </div>
                </div>
            </div>
            <div class="three wide field">
                <div class="ui calendar" id="rangedateend">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" placeholder="วันสิ้นสุดค้นหา" value="<?php echo isset($filters['rangeend']) ? $filters['rangeend'] : '' ?>">
                    </div>
                </div>
            </div>
            <div class="two wide field">
                <button class="ui red right fluid floated button btn-clear">ล้าง</button>
            </div>
        </div>
    </div>
    <br>
    <br>

    <div class="ui form segment">
        <?php if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'): ?>
            <div class="three fields">
                <div class="field">
                    <div class="ui yellow massive message">
                        <i class="user alternate icon"></i>
                        สมาชิกลูกค้า   <?php echo $numofUsernormal; ?>
                    </div>
                </div>
                <div class="field">
                    <div class="ui green massive message" style="text-align: center;">
                        <i class="store icon"></i>
                        สมาชิกร้านค้า   <?php echo $numofShop; ?>
                    </div>
                </div>
                <div class="field">
                    <div class="ui violet massive message" style="text-align: center;">
                        <i class="chess queen icon"></i>
                        สมาชิกพรีเมี่ยม   <?php echo $numofPremuim; ?> 
                    </div>
                </div>
            </div>
        <?php endif ?>

        <div class="three fields">
            <div class="field">
                <div class="ui pink massive message">
                    <i class="user alternate icon"></i>
                    การนัดหมายวัคซีน   <?php echo $numofVaccineBook; ?>
                </div>
            </div>
            <div class="field">
                <div class="ui teal massive message" style="text-align: center;">
                    <i class="store icon"></i>
                    การนัดหมายอาบน้ำ   <?php echo $numofShowerBook; ?>
                </div>
            </div>
            <div class="field">
                <div class="ui orange massive message">
                    <i class="paw icon"></i>
                    สัตว์เลี้ยงที่เข้ามาใช้บริการ   <?php echo $numofPetUse; ?>
                </div>
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <div class="ui olive massive message" style="text-align: center;">
                    <i class="chess queen icon"></i>
                    การนัดหมายช่วงเวลาเช้า  <?php echo $numofServiceMor; ?> 
                </div>
            </div>
            <div class="field">
                <div class="ui olive massive message" style="text-align: center;">
                    <i class="chess queen icon"></i>
                    การนัดหมายช่วงเวลาบ่าย   <?php echo $numofServiceNoon; ?> 
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>
    <div class="ui form segment">
        <div class="fields">
            <div class="sixteen wide field" style="padding: 2rem;text-align: -webkit-center;">
                <canvas id="myChart" width="400" height="400"></canvas>
            </div>
        </div>

        <div class="fields">
            <div class="eight wide field" style="padding: 2rem;">
                <canvas id="myChart1" width="400" height="400"></canvas>
            </div>
            <div class="eight wide field" style="padding: 2rem;">
                <canvas id="myChart2" width="400" height="400"></canvas>
            </div>
        </div>
    </div>

    <br>
    <br>

    <div class="ui form segment">
        <br>
        <br>

        <?php if(!empty($getvaccine_stat)): ?>
            <?php $sumall_vaccine = 0; ?>
            <div class="sixteen wide field">
                <table class="ui celled table">
                    <thead>
                        <tr>
                            <th colspan="3" style="text-align: center;">
                                วัคซีนยอดนิยม
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($getvaccine_stat as $key => $getvaccinestat): ?>
                            <tr>
                                <td><?php echo $getvaccinestat->vaccine->vaccine_name; ?></td>
                                <td><?php echo $getvaccinestat->count_vac; ?></td>
                                <td class="selectable">
                                    <a href="/admin/vaccine/view/<?php echo $getvaccinestat->vaccine_ID; ?>">รายละเอียด</a>
                                </td>
                            </tr>
                            <?php $sumall_vaccine += $getvaccinestat->count_vac; ?>
                        <?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>รวม</th>
                            <th><?php echo $sumall_vaccine; ?></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <?php endif ?>

        <br>

        <?php if(!empty($getstat_ser_vac)): ?>
            <?php $sumall_vac = 0; ?>
            <div class="sixteen wide field">
                <table class="ui table">
                    <thead>
                        <tr>
                            <th colspan="3" style="text-align: center;">
                                ช่วงเวลาใช้บริการวัคซีน
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($getstat_ser_vac as $key => $getstat_servac): ?>
                            <tr>
                                <td><?php echo $getstat_servac->booking_time; ?></td>
                                <td><?php echo $getstat_servac->count_booking_time; ?></td>
                            </tr>
                            <?php $sumall_vac += $getstat_servac->count_booking_time; ?>
                        <?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>รวม</th>
                            <th><?php echo $sumall_vac; ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <?php endif ?>

        <br>

        <?php if(!empty($getstat_ser_bath)): ?>
            <?php $sumall_bath = 0; ?>
            <div class="sixteen wide field">
                <table class="ui table">
                    <thead>
                        <tr>
                            <th colspan="3" style="text-align: center;">
                                ช่วงเวลาใช้บริการอาบน้ำ-ตัดขน
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($getstat_ser_bath as $key => $getstat_serbath): ?>
                            <tr>
                                <td><?php echo $getstat_serbath->booking_time; ?></td>
                                <td><?php echo $getstat_serbath->count_booking_time; ?></td>
                            </tr>

                            <?php $sumall_bath += $getstat_serbath->count_booking_time; ?>
                        <?php endforeach ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>รวม</th>
                            <th><?php echo $sumall_bath; ?></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        <?php endif ?>

    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('dashboard.ajax_center.post');?>"></div>