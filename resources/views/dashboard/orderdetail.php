<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="receipt icon"></i>
                <div class="content">
                    รายละเอียดการจอง
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>
    <?php 
        $lb_color = "";
        if($getdetail->status == 'รอการตรวจสอบยอดเงิน'){
            $lb_color = 'orange';
        }else if($getdetail->status == 'ยกเลิกรายการ' || $getdetail->status == 'รอชำระเงิน'){
            $lb_color = 'red';
        }else{
            $lb_color = 'green';
        }
    ?>

    <div class="ui form segment">
        <div class="top right attached big ui label">
            <div class="ui <?php echo $lb_color; ?> empty circular label"></div> <?php echo $getdetail->status ?>
        </div>

        <div class="ui grid">
            <div class="four wide column">
                <img class="ui small circular image" src="<?php echo empty($getdetail->pet) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : (empty($getdetail->pet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getdetail->pet->pic)); ?>" style="max-width: 150px;max-height: 150px">
            </div>
            <div class="four wide column">
                <div style="margin-top: 7rem;"><span style="font-weight: 900;font-size: 45px;color: #565656;"><?php echo empty($getdetail->pet) ? '-' : $getdetail->pet->name; ?></span></div>
            </div>
            <div class="eight wide column">
                <!-- <img class="ui small image" src="<?php //echo url('').'/themes/image/QR.png'; ?>"> -->
            </div>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui single line table">
                    <tbody>
                        <!-- <tr>
                            <td class="active" colspan="4">รายละเอียดคำสั่งซื้อ</td>
                        </tr> -->
                        <tr>
                            <td class="two wide active">เจ้าของ</td>
                            <td class="six wide"><?php echo !empty($getdetail->user) ? $getdetail->user->name_owner : '-'; ?></td>
                            <td class="two wide active">ร้านที่ให้บริการ</td>
                            <td class="six wide"><?php echo !empty($getdetail->shop) ? $getdetail->shop->name_shop : '-'; ?></td>
                        </tr>
                        <tr>
                            <?php if($getdetail->iscourse == false): ?>
                                <td class="two wide active">บริการ</td>
                                <?php 
                                    $service_txt = "";
                                    if($getdetail->id_service == "PG0001"){
                                        $service_txt = "อาบน้ำ-ตัดขน";
                                    }else if($getdetail->id_service == "PG0002"){
                                        $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
                                    }else if($getdetail->id_service == "PV0001"){
                                        $service_txt = "วัคซีน";
                                    }else if($getdetail->id_service == "PV0002"){
                                        $service_txt = "โปรโมชันวัคซีน";
                                    }
                                ?>
                                <td class="six wide"  colspan="3"><?php echo $service_txt; ?></td>
                            <?php else: ?>
                                <td class="three wide active">คอร์ส</td>
                                <td class="six wide"  colspan="3"><?php echo !empty($getdetail->course) ? $getdetail->course->name." ( ราคา ".$getdetail->course->price." บาท )" : '-'; ?></td>
                            <?php endif ?>
                        </tr>
                        <?php if(!empty($getdetail->vaccine)): ?>
                            <tr>
                                <td class="two wide active">วัคซีน</td>
                                <td class="six wide"  colspan="3"><?php echo $getdetail->vaccine; ?></td>
                            </tr>
                        <?php endif ?>
                        <tr>
                            <td class="two wide active">วันที่นัดหมาย</td>
                            <?php
                                $datetoday = DateThai($getdetail->booking_date, true, false);
                            ?>
                            <td class="six wide"><?php echo $datetoday; ?></td>
                            <td class="two wide active">ช่วงเวลา</td>
                            <?php 
                                $booking_time_txt = "";
                                if($getdetail->booking_time == "บ่าย"){
                                    // $booking_time_txt = "บ่าย 13.30น.-19.00น.";
                                    $booking_time_txt = "บ่าย : ".(empty($getdetail->endTime) ? "13.30น.-19.00น." : $getdetail->endTime);
                                }else if($getdetail->booking_time == "เช้า"){
                                    // $booking_time_txt = "เช้า 09.00น.-12.30น.";
                                    $booking_time_txt = "เช้า : ".(empty($getdetail->startTime) ? "09.00น.-12.30น." : $getdetail->startTime);
                                }
                            ?>
                            <td class="six wide"><?php echo $getdetail->booking_time." ".$getdetail->service_time; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">คิว</td>
                            <td class="six wide"><?php echo empty($getdetail->service_order) ? 'อยู่ระหว่างการตรวจสอบคิว' : $getdetail->service_order; ?></td>
                            <td class="two wide active">การชำระเงิน (มัดจำ)</td>
                            <td class="six wide"><?php echo $price; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">หมายเหตุ</td>
                            <td class="six wide"  colspan="3"><?php echo $getdetail->note; ?></td>
                        </tr>
                        <?php if(!empty($getdetail->pic_payment)): ?>
                            <tr>
                                <td class="two wide active">วันที่ชำระเงิน</td>
                                <td class="six wide"><?php echo DateThai($getdetail->date_payment, true, false); ?></td>
                                <td class="two wide active">เวลาที่ชำระเงิน</td>
                                <td class="six wide"><?php echo $getdetail->time_payment; ?></td>
                            </tr>
                            <tr>
                                <td class="two wide active">ธนาคาร</td>
                                <td class="six wide"  colspan="3"><?php echo $getdetail->bank_payment; ?></td>
                            </tr>
                        <?php endif ?>
                        <tr class="pic_payment_F">
                            <td class="two wide active">หลักฐานการชำระเงิน</td>
                            <td class="six wide"  colspan="3">
                                <?php if(empty($getdetail->pic_payment)): ?>
                                    รอชำระเงิน
                                <?php else: ?>
                                    <span style="font-size: 12px;color: red;">คลิกที่รูปเพื่อดูรูปภาพขนาดใหญ่</span>
                                    <img class="ui small image" style="cursor: pointer;" onclick="ShowimgModal()" src="<?php echo empty($getdetail->pic_payment) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getdetail->pic_payment); ?>">
                                            
                                <?php endif ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <input type="hidden" placeholder="" value="<?php echo $getdetail->id_petservice ?>" name="pos_id" id="pos_id">

        <input type="hidden" name="shop_id" id="shop_id" value="<?php echo $getdetail->id_shop; ?>">

        <input type="hidden" name="vaccineRC_id" id="vaccineRC_id" value="<?php echo $getdetail->vaccineRC_id; ?>">

        <input type="hidden" name="vaccine_id" id="vaccine_id" value="<?php echo empty($getdetail->vaccineRC) ? '' : $getdetail->vaccineRC->vaccine_ID; ?>">

        <?php if(!empty($getdetail->vaccine) && ($getdetail->status == 'ชำระเงินแล้ว' || $getdetail->status == 'การใช้บริการเสร็จสิ้น')): ?>
            <div class="ui section divider"></div>

            <h3 class="ui header">เพิ่มข้อมูลวัคซีน</h3>
            <div class="ui form">
                <div class="sixteen wide field">
                    <label>สรรพคุณของวัคซีน</label>
                    <input type="text" placeholder="" name="vaccin" id="vaccin" readonly value="<?php echo empty($getdetail->vaccineRC) ? '-' : $getdetail->vaccineRC->vaccin; ?>">
                </div>
                <div class="three fields">
                    <div class="field">
                        <label>ชื่อวัคซีน</label>
                        <select class="ui fluid search selection dropdown" name="namevaccin" id="namevaccin" >
                            <!-- <option value='' selected>--- กรุณาเลือกวัคซีนที่ใช้ ---</option> -->
                            <!-- <?php //if(!empty($vaccines)): ?>
                                <?php //foreach ($vaccines as $key => $vaccine): ?>
                                    <option value='<?php //echo $vaccine->id ?>'><?php //echo $vaccine->vaccine_name." (".$vaccine->company.")"; ?></option>
                                <?php //endforeach ?>
                            <?php //endif ?> -->
                            <?php //if(!empty($vaccines->vaccine)): ?>
                                <?php //$vaccinelist = json_decode($vaccines->vaccine, true); ?>
                                <?php //for ($i=0; $i < count($vaccinelist); $i++): ?>
                                    <!-- <option value="<?php //echo $vaccinelist[$i] ?>"><?php //echo $vaccinelist[$i] ?></option> -->
                                <?php //endfor ?>
                            <?php //endif ?>
                        </select>
                    </div>
                    <div class="field">
                        <label>บริษัทผู้ผลิตวัคซีน</label>
                        <input type="text" placeholder="" name="companytxt" id="companytxt" readonly >
                    </div>
                    <div class="field">
                        <label>สัตว์แพทย์</label>
                        <select class="ui fluid search selection dropdown" name="veterinarian" id="veterinarian" >
                            <!-- <option value='' selected>--- กรุณาเลือกสัตว์แพทย์ ---</option> -->
                            <?php if(!empty($veterinarys)): ?>
                                <?php foreach ($veterinarys as $key => $veterinary): ?>
                                    <option value='<?php echo $veterinary->id ?>'><?php echo $veterinary->Fname." ".$veterinary->Lname." (".$veterinary->vaterinary_code.")"; ?></option>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>
                   <!--  <div class="six wide field">
                        <label>เฉพาะไฟล์ .png, .jpg  จำนวน ไม่เกิน 20 รูป</label>
                        <input type="file" placeholder="" name="img" id="img" multiple="">
                    </div> -->
                </div>
                <div class="two fields">
                    <div class="fourteen wide field">
                        <label>รายละเอียด</label>
                        <input type="text" placeholder="" name="notevaccin" id="notevaccin" >
                    </div>
                    <div class="two wide field" style="margin-top: 1.7rem;">
                        <button class="ui fluid blue button btn-saveVaccineRC" type="submit">บันทึก</button>
                    </div>
                </div>
            </div>

            <br>
            <?php if(!empty($getdetail->vaccineRC)): ?>
            <h3 class="ui header">ข้อมูลวัคซีน</h3>
            <div class="ui relaxed divided list">
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">ชื่อวัคซีน</a>
                        <div class="description"><?php echo empty($getdetail->vaccineRC) ? '-' : $getdetail->vaccineRC->namevaccin; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">บริษัทผู้ผลิตวัคซีน</a>
                        <div class="description" id='companytxtshow'></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">สรรพคุณของวัคซีน</a>
                        <div class="description"><?php echo empty($getdetail->vaccineRC) ? '-' : $getdetail->vaccineRC->vaccin; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">สัตว์แพทย์</a>
                        <div class="description"><?php echo empty($getdetail->vaccineRC) ? '-' : $getdetail->vaccineRC->veterinarian; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">ฉีดครั้งต่อไป</a>
                        <div class="description"><?php echo empty($getdetail->vaccineRC) ? '-' : DateThai($getdetail->vaccineRC->anewdate, true, false); ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">รายละเอียด</a>
                        <div class="description"><?php echo empty($getdetail->vaccineRC) ? '-' : $getdetail->vaccineRC->notevaccin; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">ฉลากวัคซีน</a>
                    </div>
                </div>
            </div>
            <div class="ui form">
                <span style="font-size: 12px;color: red;">คลิกที่รูปเพื่อดูรูปภาพขนาดใหญ่</span>
                <div class="fields">
                    <div class="ui stackable" id="img-card">
                        <?php if(!empty($getdetail->vaccineRC->img)): ?>
                            <?php $getimg = $getdetail->vaccineRC->img; ?>
                            <!-- <img class="ui small image" style="cursor: pointer;" src="<?php //echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png'; ?>" onclick="ShowVaccineImgModal()"> -->
                            <img class="ui small image" style="cursor: pointer;" src="<?php echo empty($getimg) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getimg); ?>" onclick="ShowVaccineImgModal()">
                                    
                        <?php endif ?>
                    </div>
                </div>
            </div>
            <?php endif ?>

            <br>
            <br>

        <?php endif ?>


        <?php if(!empty($getdetail->course)): ?> 
            <div class="ui section divider"></div>

            <h3 class="ui header">เพิ่มข้อมูลการใช้คอร์ส</h3>
            
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_onlinelist">
                    <thead>
                        <tr>
                            <th>ชื่อคอร์ส</th>
                            <th>สัตว์เลี้ยง</th>
                            <th>ครั้งที่ใช้</th>
                            <th>วันที่ใช้งาน</th>
                            <th>จำนวนครั้งคงเหลือที่ใช้ได้</th>
                            <th>สถานะการใช้งาน</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($getcoursedata)): ?>
                            <?php if($getcoursedata->count() != 0): ?>
                                <?php foreach ($getcoursedata as $key => $getcourses): ?>
                                    <tr>
                                        <td><?php echo empty($getcourses->course) ? "-" : $getcourses->course->name; ?></td>
                                        <td><?php echo empty($getcourses->pet) ? "-" : $getcourses->pet->name; ?></td>
                                        <td><?php echo empty($getcourses->thetime) ? "-" : $getcourses->thetime; ?></td>
                                        <td>
                                            <?php
                                                // $datetoday = date('Y-m-d H:i:s');
                                                $datetoday = empty($getcourses->startdate) ? '-' : DateThai($getcourses->startdate, true, false);
                                            ?>
                                            <?php echo $datetoday ?>
                                                
                                        </td>
                                        <td>
                                            <!-- <?php //if($getcourses->isuse == true): ?> -->
                                            <?php if(!empty($getcourses->startdate)): ?>
                                                <?php echo empty($getcourses->thetime) ? "-" : (empty($getcourses->course) ? "-" : $getcourses->course->numoftimes-$getcourses->thetime); ?>
                                            <?php else: ?>
                                                -
                                            <?php endif ?>
                                        </td>
                                        <td><span class="ui <?php echo $getcourses->isuse == false ? "red" : "green"; ?> label"><?php echo $getcourses->isuse == false ? "รอใช้บริการ" : "ใช้บริการเสร็จสิ้น"; ?></span></td>
                                    </tr>
                                <?php endforeach ?>
                            <?php endif ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>

            <div class="ui section divider"></div>
            <br>
        <?php endif ?> 






        <?php if(!empty($user_type)): ?>
            <?php if($user_type == 'Root' || $user_type == 'ผู้ดูแลระบบ' || $user_type == 'เจ้าของกิจการ'): ?>
                <div class="sixteen wide field">
                    <div class="ui unstackable four column grid ">
                        <div class="column">
                            <!-- <?php //if($getdetail->status == 'รอการตรวจสอบยอดเงิน'): ?>
                                <div class="field">
                                    <button class="ui fluid orange button btn-edit-payslip <?php //echo (($getdetail->status == 'ยกเลิกรายการ') || ($getdetail->status == 'การใช้บริการเสร็จสิ้น') || ($getdetail->status == 'ชำระเงินแล้ว') || ($getdetail->status == 'รอชำระเงิน')) ? 'disabled' : '' ?>" type="submit" >แก้ไขหลักฐานการชำระเงิน</button>
                                </div>
                            <?php //endif ?> -->
                            <?php if($getdetail->status == 'ชำระเงินแล้ว' || $getdetail->status == 'การใช้บริการเสร็จสิ้น'): ?>
                                <div class="field">
                                    <button class="ui fluid teal button" type="submit" onclick="window.location.href = '/admin/petsShop/view/<?php echo $getdetail->pets_shopID; ?>';">ทำนัดครั้งถัดไป</button>
                                </div>
                            <?php endif ?>
                            <?php if($getdetail->postponeAdate == 1 && $getdetail->postponeAdate_status == 'รอตรวจสอบ'): ?>
                                <div class="field">
                                    <button class="ui fluid orange button btn-cf_postponeAdate" type="submit" >ยืนยันการขอเลื่อนนัด</button>
                                </div>
                            <?php endif ?>
                        </div>
                        <div class="right floated column">
                            <div class="fields">
                                <?php if($getdetail->status == 'รอการตรวจสอบยอดเงิน'): ?>
                                    <div class="field">
                                        <button class="ui fluid green button btn-confirm <?php echo (($getdetail->status == 'ยกเลิกรายการ') || ($getdetail->status == 'การใช้บริการเสร็จสิ้น') || ($getdetail->status == 'ชำระเงินแล้ว') || ($getdetail->status == 'รอชำระเงิน')) ? 'disabled' : '' ?>" type="submit" >ยืนยันการชำระเงิน</button>
                                    </div>
                                <?php endif ?>
                                <?php if($getdetail->status == 'ชำระเงินแล้ว'): ?>
                                    <div class="field">
                                        <button class="ui fluid green button btn-finish <?php echo (($getdetail->status == 'ยกเลิกรายการ')) ? 'disabled' : '' ?>" type="submit" >การใช้บริการเสร็จสิ้น</button>
                                    </div>
                                <?php endif ?>
                                <?php if($getdetail->status !== 'ยกเลิกรายการ' && $getdetail->status !== 'การใช้บริการเสร็จสิ้น'): ?>
                                    <div class="field">
                                        <button class="ui fluid red button btn-cancel" type="submit" >ยกเลิกรายการ</button>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        <?php endif ?>
        
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('dashboard.ajax_center.post');?>"></div>
    <div id='addslip-url' data-url="<?php echo \URL::route('dashboard.addslip.post');?>"></div>
    <div id='addVaccineRC-url' data-url="<?php echo \URL::route('dashboard.addVaccineRC.post');?>"></div>


<div class="ui basic modal">
    <div class="image content">
        <img class="image" src="<?php echo empty($getdetail->pic_payment) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getdetail->pic_payment); ?>">
    </div>
    <div class="actions">
        <div class="ui red basic cancel inverted button">
            <i class="remove icon"></i>
            ปิด
        </div>
  </div>
</div>


<div class="ui basic modal vaccineImgModal">
    <div class="image content">
        <?php if(!empty($getdetail->vaccineRC)): ?>
            <?php $getimg = $getdetail->vaccineRC->img; ?>
            <img class="image" src="<?php echo empty($getimg) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getimg); ?>">
        <?php endif ?>
    </div>
    <div class="actions">
        <div class="ui red basic cancel inverted button">
            <i class="remove icon"></i>
            ปิด
        </div>
  </div>
</div>