<!DOCTYPE>
<html>
<head>
<title></title>
<style>
    @import url('https://fonts.googleapis.com/css?family=Kanit');
*{
    font-family: 'Kanit', sans-serif;
}
</style>
<style>
    .card {
      box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
      transition: 0.3s;
      width: 80%;
      font-size: 16px
    }

    .card:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }

    .container {
      padding: 2px 16px;
    }

    .test {
        white-space: pre-wrap;       /* Since CSS 2.1 */
        white-space: -moz-pre-wrap;  /* Mozilla, since 1999 */
        white-space: -pre-wrap;      /* Opera 4-6 */
        white-space: -o-pre-wrap;    /* Opera 7 */
        word-wrap: break-word;       /* Internet Explorer 5.5+ */
    }
</style>
</head>
<body>

<table style="background-color:#BCBEC0;width:100%;" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td style="padding-top:20px;padding-bottom:20px;box-sizing:border-box;vertical-align: top">
                <table style="max-width:none;min-width:800px;width:800px;margin: auto;background-color:#ffffff" cellspacing="0" cellpadding="0" border="0">
                    <tbody style="">
                        <tr style="">
                            <td style="font-family:'Open Sans', sans-serif;">
                                <div style="padding: 20px 30px;height: 70px;">
                                    <img style="width:auto;height: 80px; float:left;" src="https://www.petscarebusiness.com/public/themes/image/logo.png" />
                                    <div style="font-size:24px; margin-left: 10%;    padding-top: 3%;">Pets Care</div>
                                </div>

                                <div style="height: 10px;background-color:#FEE265;margin-bottom: 30px;"></div>

                                <!-- Content -->
                                <div style="min-height: 700px;word-break: break-all;padding: 10px 51px;">
                                    <div style="font-size: 18px;">
                                    เรียน เจ้าหน้าที่ Pets Care </div>
                                    <!-- Insert Content in this div -->
                                    <div style="margin: 10px;"></div>
                                    <div style="font-size: 16px;padding-left: 6%;">
                                        มีผู้สนใจสอบถามข้อมูลเพิ่มเติมและต้องการให้ทาง Pets Care ติดต่อกลับโดยด่วน 
                                    </div>
                                    <div style="font-size: 16px;padding-left: 6%;">ได้แนบข้อมูลมาดังนี้
                                    </div>

                                    <div style="margin: 30px;"></div>

                                    <div style="margin-left: 15%;">
                                        <div class="card">
                                            <div class="container">
                                                <p style="font-weight: 600;font-size: 16px;">ข้อมูลผู้สนใจสอบถามเพิ่มเติม</p> 
                                                <p><span style="font-weight: 600">หัวข้อที่สนใจสอบถามเพิ่มเติม : </span> <?php echo isset($data['subject']) ? $data['subject'] : "-" ?></p> 
                                                <p><span style="font-weight: 600">ชื่อผู้สนใจ : </span> <?php echo isset($data['name']) ? $data['name'] : "-" ?> </p> 
                                                <p><span style="font-weight: 600">อีเมลที่ต้องการให้ติดต่อกลับ : </span> <?php echo isset($data['email']) ? $data['email'] : "-" ?> </p> 
                                                <p class="test"><span style="font-weight: 600">ข้อความ : </span> <?php echo isset($data['message']) ? $data['message'] : "-" ?></p> 
                                            </div>
                                        </div>
                                    </div>


                                    <div style="padding: 20px 30px;">
                                        
                                    </div>
                                    <div style="color:#000000;padding: 5px 30px;text-align: center;background-color: #FEE265;font-weight: bold;">
                                        กรุณาติดต่อกลับทางช่องทางที่แนบมาโดยด่วน 
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

</body>
</html>


