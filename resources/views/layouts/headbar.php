
<div class="ui massive top fixed sticky attached blue demo inverted menu" style="margin-top:unset;background-color: #FEE265;">
    <div class="item active">
        <img class="ui mini circular image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png'; ?>">
        <span class="ui small" style="margin-left: 1em">PETs CARE</span>
    </div>
    <div class="item active bartxx">
        <div style="cursor: pointer;">
            <i class="sidebar icon"></i>
            <span class="text">เมนู</span>
        </div>
    </div>
   
    <div class="right menu">
        <div class="item">
            <img class="ui mini circular image" src="<?php echo empty($user_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $user_img); ?>">
            <span class="ui small" style="margin-left: 1em"><?php echo (empty($name_owner))  ? '' : $name_owner; ?></span>
            <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
        </div>
    </div>
</div>


