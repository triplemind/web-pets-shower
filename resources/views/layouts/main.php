<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PETs CARE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.css">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/dataTables.semanticui.min.css">
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />
    <link  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/pickerjs/dist/picker.css" rel="stylesheet">
    <link  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/chart.js-2.9.4/dist/Chart.css" rel="stylesheet">
    <link  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/chart.js-2.9.4/dist/Chart.min.css" rel="stylesheet">
    

    <?php if (file_exists(base_path().'/public/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo (env('APP_ENV') == 'production' ? '/public' : '').'/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>
</head>

<body style="background-color: #e2e2e2;">

    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/current-device.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/calendarTH.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment-with-locales.js"></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/interaction/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-common/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/sweetalert2.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/lodash.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.dataTables.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/dataTables.semanticui.min.js'></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-route.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-animate.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/cleave.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/addons/cleave-phone.th.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/ckeditor/ckeditor.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/pickerjs/dist/picker.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/chart.js-2.9.4/dist/Chart.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/chart.js-2.9.4/dist/Chart.min.js"></script>

    <?php if (file_exists(base_path().'/public/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo (env('APP_ENV') == 'production' ? '/public' : '').'/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>
        <?php //sd('/js/'.helperGetModule().'/'.helperGetAction().'.js?t='); ?>
    <?php endif ?>

    <!-- Header.php -->
        <?php echo view('layouts.headbar', compact('name_owner', 'username', 'user_type', 'user_img', 'user_id'));?>

        <br>
        <br>
        <br>
        
        <div class="ui bottom attached segment" style="height: auto;background:#e2e2e2;border: unset;">
            <?php echo view('layouts.sidebar', compact('name_owner', 'username', 'user_type', 'user_img', 'user_id', 'check_premium'));?>
            <div class="pusher">
                <?php echo view($page, $data);?> 


                <br>
                <br>

                <div class="ui basic segment" style="text-align: center;">
                    <p>Copyright © 2020 PETCARE.COM</p>
                </div>

            </div>

        </div>

    <!-- Data -->
    <div id="logout-url"  data-url="<?php echo \URL::route('auth.logout.get');?>">
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />

<script type="text/javascript">
    $(function(){

        // $('.left.demo.sidebar').first().sidebar('attach events', '.fixed');
        // $('.fixed').removeClass('disabled');

        $('.ui.sidebar').sidebar({
            context: $('.bottom.segment')
        }).sidebar('attach events', '.menu .item.bartxx');



        $('.btn-logout').on('click', function(){
            Swal.fire({
                title: "คุณต้องการออกจากระบบหรือไม่ ?",
                showCancelButton: true,
                confirmButtonColor: "#59B855",
                cancelButtonText: "ยกเลิก",
                cancelButtonColor: "",
                confirmButtonText: "ตกลง"
            }).then(result => {
                if (result.value) {
                    logout_url = $('#logout-url').data('url');
                    console.log(logout_url);
                    window.location = logout_url;
                }
            });


        });

    });
</script>
</body>

</html>