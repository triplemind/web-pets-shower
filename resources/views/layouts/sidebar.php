<div class="ui stackable inverted labeled icon left inline vertical demo sidebar menu">
<?php //sd($user_type); ?>
    <?php if(empty($user_type)): ?>
        <a class="item" href="<?php echo \URL::route('dashboard.index.get'); ?>">
            <i class="home icon"></i>
            หน้าหลัก
        </a>
        <!-- <a class="item" href="<?php //echo \URL::route('report.index.get'); ?>">
            <i class="clipboard outline icon"></i>
            รายงาน
        </a> -->
    <?php elseif(!empty($user_type)): ?>
        <?php if($user_type == 'ผู้ดูแลระบบ'): ?>
            <a class="item" href="<?php echo \URL::route('dashboard.index.get'); ?>">
                <i class="home icon"></i>
                หน้าหลัก
            </a>
            <a class="item" href="<?php echo \URL::route('vaccine.index.get'); ?>">
                <i class="syringe icon"></i>
                หน้าจัดการวัคซีน
            </a>
            <?php if(!empty($check_premium)): ?>
                <?php if($check_premium->status_premium == true): ?>
                    <a class="item" href="<?php echo \URL::route('promotion.index.get'); ?>">
                        <i class="ad icon"></i>
                        จัดการโปรโมชั่น
                    </a> 
                    <a class="item" href="<?php echo \URL::route('course.index.get'); ?>">
                        <i class="book icon"></i>
                        จัดการคอร์ส
                    </a>
                <?php endif ?>
            <?php endif ?>
            <a class="item" href="<?php echo \URL::route('veterinary.index.get'); ?>">
                <i class="stethoscope icon"></i>
                จัดการสัตวแพทย์
            </a> 
            <a class="item" href="<?php echo \URL::route('petsShop.index.get'); ?>">
                <i class="paw icon"></i>
                จัดการสัตว์เลี้ยง
            </a> 
            <a class="item" href="<?php echo \URL::route('premium.index.get'); ?>">
                <i class="chess queen icon"></i>
                Premuim
            </a>
        <?php elseif($user_type == 'Root'): ?>
            <a class="item" href="<?php echo \URL::route('dashboard.index.get'); ?>">
                <i class="home icon"></i>
                หน้าหลัก
            </a>
            <a class="item" href="<?php echo \URL::route('user.index.get'); ?>">
                <i class="user alternate icon"></i>
                จัดการผู้ใช้งานระบบ
            </a>
            <a class="item" href="<?php echo \URL::route('shop.index.get'); ?>">
                <i class="store icon"></i>
                จัดการร้านค้า
            </a>
            <a class="item" href="<?php echo \URL::route('vaccine.index.get'); ?>">
                <i class="syringe icon"></i>
                จัดการวัคซีน
            </a> 
           <?php //if(!empty($check_premium)): ?>
                <?php //if($check_premium->status_premium == true): ?>
                    <a class="item" href="<?php echo \URL::route('promotion.index.get'); ?>">
                        <i class="ad icon"></i>
                        จัดการโปรโมชั่น
                    </a> 
                    <a class="item" href="<?php echo \URL::route('course.index.get'); ?>">
                        <i class="book icon"></i>
                        จัดการคอร์ส
                    </a>
                <?php //endif ?>
            <?php //endif ?>
            <a class="item" href="<?php echo \URL::route('veterinary.index.get'); ?>">
                <i class="stethoscope icon"></i>
                จัดการสัตวแพทย์
            </a> 
            <a class="item" href="<?php echo \URL::route('petsShop.index.get'); ?>">
                <i class="paw icon"></i>
                จัดการสัตว์เลี้ยง
            </a> 
            <a class="item" href="<?php echo \URL::route('premium.index.get'); ?>">
                <i class="chess queen icon"></i>
                Premuim
            </a> 
        <?php elseif($user_type == 'ผู้ใช้ทั่วไป'): ?>
            <a class="item" href="<?php echo \URL::route('dashboard.index.get'); ?>">
                <i class="home icon"></i>
                หน้าหลัก
            </a>
            <!-- <a class="item" href="<?php //echo \URL::route('vaccine.index.get'); ?>">
                <i class="clipboard outline icon"></i>
                รายงาน
            </a> -->
        <?php elseif($user_type == 'เจ้าของกิจการ'): ?>
            <a class="item" href="<?php echo \URL::route('dashboard.index.get'); ?>">
                <i class="home icon"></i>
                หน้าหลัก
            </a>
            <a class="item" href="<?php echo \URL::route('shop.index.get'); ?>">
                <i class="store icon"></i>
                จัดการร้านค้า
            </a>
            <?php if(!empty($check_premium)): ?>
                <?php if($check_premium->status_premium == true): ?>
                    <a class="item" href="<?php echo \URL::route('promotion.index.get'); ?>">
                        <i class="ad icon"></i>
                        จัดการโปรโมชั่น
                    </a> 
                    <a class="item" href="<?php echo \URL::route('course.index.get'); ?>">
                        <i class="book icon"></i>
                        จัดการคอร์ส
                    </a>
                <?php endif ?>
            <?php endif ?>
            <a class="item" href="<?php echo \URL::route('veterinary.index.get'); ?>">
                <i class="stethoscope icon"></i>
                จัดการสัตวแพทย์
            </a> 
            <a class="item" href="<?php echo \URL::route('petsShop.index.get'); ?>">
                <i class="paw icon"></i>
                จัดการสัตว์เลี้ยง
            </a> 
            <a class="item" href="<?php echo \URL::route('premium.index.get'); ?>">
                <i class="chess queen icon"></i>
                Premuim
            </a> 
        <?php endif ?>
    <?php endif ?>
</div>