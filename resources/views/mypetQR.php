
<div class="ui grid" style="">
    <div class="computer only row">
        <div class="column">
            <div class="ui stackable segment" style="background-color: #ffe597;margin-left: 6rem;margin-right: 6rem;">

                <div class="ui grid">
                    <div class="six wide column">
                        <img class="ui medium circular image" src="<?php echo empty($getPet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->pic); ?>" style="max-width: 300px;max-height: 300px;">
                    </div>
                    <div class="six wide column">
                        <div style="margin-top: 18rem;"><span style="font-weight: 900;font-size: 45px;color: #565656;"><?php echo !empty($getPet) ? $getPet->name : '-'; ?></span></div>
                    </div>
                    <div class="four wide column">
                        <?php if(!empty($getPet->qrcode)): ?>
                            <a href="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>" download>
                                <img class="ui centered small image" src="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>">
                            </a>
                            <label style="color: red">*คลิกที่ Qr Code เพื่อทำการดาวน์โหลด</label>
                        <?php endif ?>
                    </div>
                </div>

                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>


                <div class="fields" style="margin-top: 1em;">
                    <div class="sixteen wide field">
                        <table class="ui single line table">
                            <tbody>
                                <tr>
                                    <td class="two wide active">ประเภทสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->type : '-'; ?></td>
                                    <td class="two wide active">สายพันธุ์สัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->breed : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">เพศสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->gender : '-'; ?></td>
                                    <td class="two wide active">สีของสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->color : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">วันเกิดสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? DateThai($getPet->birthday, true, false) : '-'; ?></td>
                                    <td class="two wide active">น้ำหนัก (กิโลกรัม)</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->weight : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">รายละเอียดเพิ่มเติม</td>
                                    <td class="six wide"  colspan="3"><?php echo !empty($getPet) ? $getPet->notedescription : '-'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>


                <div class="fields" style="margin-top: 1em;">
                    <div class="sixteen wide field">
                        <table class="ui single line table">
                            <tbody>
                                <tr>
                                    <td class="two wide active">ชื่อเจ้าของ</td>
                                    <td class="six wide"><?php echo !empty($getPet->user) ? $getPet->user->name_owner : '-'; ?></td>
                                    <td class="two wide active">เบอร์โทรศัพท์ติดต่อ</td>
                                    <td class="six wide"><?php echo !empty($getPet->user) ? $getPet->user->telephone : '-'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>

                <div class="ui grid">
                    <div class="sixteen wide column">
                        <div style="margin-top: 5rem;text-align: center;">
                            <span style="font-weight: 900;font-size: 28px;color: #5DACBD;">ประวัติ การฉีดวัคซีน</span>
                        </div>
                    </div>
                    <div class="sixteen wide column">
                        <table class="ui striped definition table">
                            <thead  class="full-width" >
                                <tr>
                                    <th style="background-color: #ffffff;text-align: center;">วัคซีนป้องกันโรค</th>
                                    <th style="background-color: #ffffff;text-align: center;">วันที่ฉีด</th>
                                    <th style="background-color: #ffffff;text-align: center;">รูปภาพวัคซีน</th>
                                    <th style="background-color: #ffffff;text-align: center;">สัตวแพทย์/เลขที่ใบอนุญาต</th>
                                    <th style="background-color: #ffffff;text-align: center;">รายละเอียด</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(count($vacccineRC_arr) != 0): ?>
                                    <?php foreach ($vacccineRC_arr as $key => $vaccineall): ?>
                                        <tr>
                                            <td class="four wide" style="background-color: #5dacbd;color: #fff">
                                                <?php echo empty($vaccineall['vaccine_name']) ? '' : $vaccineall['vaccine_name']; ?>
                                            </td>
                                            <td>
                                                <?php echo empty($vaccineall['vRC_created_at']) ? '' : DateThai($vaccineall['vRC_created_at'], true, false); ?>
                                            </td>
                                            <td> 
                                                <?php if(!empty($vaccineall['vRC_created_at'])): ?>
                                                    <img class="ui tiny image" src="<?php echo empty($vaccineall['img_vaccine']) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $vaccineall['img_vaccine']); ?>">
                                                <?php endif ?>
                                            </td>
                                            <td>
                                                <?php echo empty($vaccineall['veterinarian']) ? '' : $vaccineall['veterinarian']; ?>
                                            </td>
                                            <td>
                                                <?php if(!empty($vaccineall['vRC_id'])):?>
                                                    <button class="ui fluid button btn-view" style="background-color: #ec4c2d;color: #fff;border-radius: 10px;" onclick="getModalVaccineDetail(<?php echo $vaccineall['vRC_id'].", '".(env('APP_ENV') == 'production' ? '/public' : '')."'"; ?>)" type="submit">เพิ่มเติม</button>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>



                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>

                <div class="ui grid">
                    <div class="sixteen wide column">
                        <div style="margin-top: 5rem;"><span style="font-weight: 900;font-size: 28px;color: #565656;">ตารางประวัติการใช้บริการ</span></div>
                    </div>
                    <div class="sixteen wide column">
                        <table class="ui teal table">
                            <thead>
                                <tr>
                                    <th>วันที่ใช้บริการ</th>
                                    <th>ช่วงเวลา</th>
                                    <th>ร้านค้า</th>
                                    <th>บริการ</th>
                                    <!-- <th>สัตว์แพทย์ผู้ฉีด</th> -->
                                    <th>หมายเหตุ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if($getHistories->count() != 0): ?>
                                    <?php foreach ($getHistories as $key => $getHistory): ?>
                                        <tr>
                                            <td>
                                                <?php
                                                    $datetoday = DateThai($getHistory->booking_date, true, false);
                                                ?>
                                                <?php echo $datetoday ?>
                                            </td>
                                            <td>
                                                <?php 
                                                    $booking_time_txt = "";
                                                    if($getHistory->booking_time == "บ่าย"){
                                                        $booking_time_txt = "บ่าย 13.30น.-19.00น.";
                                                    }else if($getHistory->booking_time == "เช้า"){
                                                        $booking_time_txt = "เช้า 09.00น.-12.30น.";
                                                    }
                                                ?>
                                                <?php echo $booking_time_txt; ?>
                                            </td>
                                            <td><?php echo empty($getHistory->shop) ? '' : $getHistory->shop->name_shop ?></td>
                                            <td>
                                                <?php 
                                                    $service_txt = "";
                                                    if($getHistory->id_service == "PG0001"){
                                                        $service_txt = "อาบน้ำ-ตัดขน";
                                                    }else if($getHistory->id_service == "PG0002"){
                                                        $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
                                                    }else if($getHistory->id_service == "PV0001"){
                                                        $service_txt = "วัคซีน";
                                                    }else if($getHistory->id_service == "PV0002"){
                                                        $service_txt = "โปรโมชันวัคซีน";
                                                    }
                                                ?>
                                                <?php echo $service_txt; ?>
                                            </td>
                                            <td><?php echo $getHistory->note  ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="tablet mobile only row">
        <div class="column">
            <div class="ui stackable segment" style="background-color: #ffe597;margin-left: 2rem;margin-right: 2rem;">

                <div class="ui grid">
                    <div class="sixteen wide column">
                        <img class="ui centered medium circular image" src="<?php echo empty($getPet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->pic); ?>" style="max-width: 300px;max-height: 300px;">
                    </div>
                    <div class="sixteen wide column">
                        <?php if(!empty($getPet->qrcode)): ?>
                            <a href="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>" download>
                                <img class="ui centered small image" src="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>">
                            </a>
                            <label style="color: red">*คลิกที่ Qr Code เพื่อทำการดาวน์โหลด</label>
                        <?php endif ?>
                    </div>
                    <div class="sixteen wide column">
                        <div style="text-align: center;"><span style="font-weight: 900;font-size: 24px;color: #565656;"><?php echo !empty($getPet) ? $getPet->name : '-'; ?></span></div>
                    </div>
                </div>

                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>


                <div class="fields" style="margin-top: 1em;">
                    <div class="sixteen wide field">
                        <table class="ui single line table">
                            <tbody>
                                <tr>
                                    <td class="two wide active">ประเภทสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->type : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">สายพันธุ์สัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->breed : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">สีของสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->color : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">เพศสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->gender : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">วันเกิดสัตว์เลี้ยง</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? DateThai($getPet->birthday, true, false) : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">น้ำหนัก (กิโลกรัม)</td>
                                    <td class="six wide"><?php echo !empty($getPet) ? $getPet->weight : '-'; ?></td>
                                </tr>
                                <tr>
                                    <td class="two wide active">รายละเอียดเพิ่มเติม</td>
                                    <td class="six wide"  colspan="3"><?php echo !empty($getPet) ? $getPet->notedescription : '-'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>


                <div class="fields" style="margin-top: 1em;">
                    <div class="sixteen wide field">
                        <table class="ui single line table">
                            <tbody>
                                <tr>
                                    <td class="two wide active">ชื่อเจ้าของ</td>
                                    <td class="six wide"><?php echo !empty($getPet->user) ? $getPet->user->name_owner : '-'; ?></td>
                                    <td class="two wide active">เบอร์โทรศัพท์ติดต่อ</td>
                                </tr>
                                <tr>
                                    <td class="two wide active">เบอร์โทรศัพท์ติดต่อ</td>
                                    <td class="six wide"><?php echo !empty($getPet->user) ? $getPet->user->telephone : '-'; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>

                <div class="sixteen wide column">
                    <div style="margin-top: 5rem;text-align: center;">
                        <span style="font-weight: 900;font-size: 20px;color: #5DACBD;">ประวัติ การฉีดวัคซีน</span>
                    </div>
                </div>

                <div class="sixteen wide column">
                    <div class="ui two doubling link cards">
                        <?php if(count($vacccineRC_arr) != 0): ?>
                            <?php foreach ($vacccineRC_arr as $key => $vaccineall): ?>
                                <div class="card">
                                    <div class="image">
                                        <?php if(!empty($vaccineall['vRC_created_at'])): ?>
                                            <img src="<?php echo empty($vaccineall['img_vaccine']) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $vaccineall['img_vaccine']); ?>">
                                        <?php endif ?>
                                    </div>
                                    <div class="content">
                                        <div class="description">
                                            <div class="header"><?php echo empty($vaccineall['vaccine_name']) ? '-' : $vaccineall['vaccine_name']; ?></div>
                                            วันที่ฉีด : <span> <?php echo empty($vaccineall['vRC_created_at']) ? '-' : DateThai($vaccineall['vRC_created_at'], true, false); ?> </span><br>
                                            สัตวแพทย์/เลขที่ใบอนุญาต : <span> <?php echo empty($vaccineall['veterinarian']) ? '-' : $vaccineall['veterinarian']; ?> </span><br>
                                        </div>
                                    </div>
                                    <div class="extra content">
                                        <!-- div class="ui one buttons" style="padding-top: 1rem;">
                                            <?php //if(!empty($vaccineall['vRC_id'])):?>
                                                <div class="ui green button btn-view" style="background-color: #ec4c2d;color: #fff;" onclick="getModalVaccineDetail(<?php //echo $vaccineall['vRC_id']; ?>)" type="submit">เพิ่มเติม</div>
                                            <?php //endif ?>
                                        </div> -->
                                        <div class="ui accordion">
                                            <div class="title">
                                                <i class="dropdown icon"></i>
                                                เพิ่มเติม
                                            </div>
                                            <div class="content">
                                                <div class="ui form sixteen wide column">
                                                    <div class="field">
                                                        <h4  style="color: #5DACBD;">ชื่อวัคซีน</h4>
                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['vaccine_name']) ? '-' : $vaccineall['vaccine_name']; ?></div>
                                                    </div>
                                                    <div class="field">
                                                        <h4  style="color: #5DACBD;">บริษัทผู้ผลิตวัคซีน</h4>
                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['company']) ? '-' : $vaccineall['company']; ?></div>
                                                    </div>
                                                    <div class="field">
                                                        <h4  style="color: #5DACBD;">สรรพคุณของวัคซีน</h4>
                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['properties']) ? '-' : $vaccineall['properties']; ?> </div>
                                                    </div>
                                                    <div class="field">
                                                        <h4  style="color: #5DACBD;">สัตวแพทย์</h4>
                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['veterinarian']) ? '-' : $vaccineall['veterinarian']; ?> </div>
                                                    </div>
                                                    <div class="field">
                                                        <h4  style="color: #5DACBD;">รายละเอียด</h4>
                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['description']) ? '-' : $vaccineall['description']; ?> </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                </div>


                <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>

                <div class="ui stackable column grid" style="padding: 15px;">
                    <div class="sixteen wide column">
                        <div style="margin-top: 5rem;"><span style="font-weight: 900;font-size: 24px;color: #565656;">ตารางประวัติการใช้บริการ</span></div>
                    </div>
                    <div class="sixteen wide column">
                        <div class="ui two doubling link cards">
                            <?php if($getHistories->count() != 0): ?>
                                <?php foreach ($getHistories as $key => $getHistory): ?>
                                    <div class="card">
                                        <div class="content">
                                            <div class="description">
                                                วันที่ใช้บริการ : <span> <?php echo DateThai($getHistory->booking_date, true, false); ?> </span><br>
                                                <?php 
                                                    $booking_time_txt = "";
                                                    if($getHistory->booking_time == "บ่าย"){
                                                        $booking_time_txt = "บ่าย 13.30น.-19.00น.";
                                                    }else if($getHistory->booking_time == "เช้า"){
                                                        $booking_time_txt = "เช้า 09.00น.-12.30น.";
                                                    }
                                                ?>
                                                ช่วงเวลา : <span> <?php echo $booking_time_txt; ?> </span><br>
                                                ร้านค้า : <span> <?php echo empty($getHistory->shop) ? '' : $getHistory->shop->name_shop; ?> </span><br>
                                                 <?php 
                                                    $service_txt = "";
                                                    if($getHistory->id_service == "PG0001"){
                                                        $service_txt = "อาบน้ำ-ตัดขน";
                                                    }else if($getHistory->id_service == "PG0002"){
                                                        $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
                                                    }else if($getHistory->id_service == "PV0001"){
                                                        $service_txt = "วัคซีน";
                                                    }else if($getHistory->id_service == "PV0002"){
                                                        $service_txt = "โปรโมชันวัคซีน";
                                                    }
                                                ?>
                                                บริการ : <span> <?php echo $service_txt; ?> </span><br>
                                                หมายเหตุ : <span> <?php echo $getHistory->note; ?> </span>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            <?php else: ?>
                                <div class="sixteen wide column">
                                    <h3 style="color: red;font-weight: 500;">ไม่พบข้อมูลในระบบ</h3>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>

</div>


<!-- Data -->
    <!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('mypets.ajax_center.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('mypets.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('mypets.delete.post');?>"></div>



<div class="ui small modal">
    <div class="header" style="color: #5DACBD;">ข้อมูลวัคซีน</div>
    <div class="scrolling content">
        <div class="ui form sixteen wide column">
            <div class="field">
                <h4  style="color: #5DACBD;">ชื่อวัคซีน</h4>
                <div class="vaccine_name"></div>
            </div>
            <div class="field">
                <h4  style="color: #5DACBD;">บริษัทผู้ผลิตวัคซีน</h4>
                <div class="company"></div>
            </div>
            <div class="field">
                <h4  style="color: #5DACBD;">สรรพคุณของวัคซีน</h4>
                <div class="properties"></div>
            </div>
            <div class="field">
                <h4  style="color: #5DACBD;">สัตวแพทย์</h4>
                <div class="veterinarian"></div>
            </div>
            <div class="field">
                <h4  style="color: #5DACBD;">รายละเอียด</h4>
                <div class="description"></div>
            </div>
            <div class="field">
                <h4  style="color: #5DACBD;">ฉลากวัคซีน</h4>
                <div class="img_vaccine"></div>
            </div>
        </div>

        <br>
        <br>

        
    </div>
    <div class="actions">
        <div class="ui blue cancel button">ปิด</div>
    </div>
</div>



<script type="text/javascript">

    $('.accordion').accordion();

    function getModalVaccineDetail(vRC_id,envimg){

    $('.vaccine_name').html('');
    $('.properties').html('');
    $('.company').html('');
    $('.veterinarian').html('');
    $('.description').html('');
    $('.img_vaccine').html('');


    console.log(vRC_id);

    var ajax_url    = $('#ajax-center-url').data('url');
    var method      = 'getVaccineRCDetail';
    
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'vRC_id' : vRC_id,
        },
        success: function(result) {
            if(result.status == 'success'){
                setTimeout(function(){ 

                    console.log(result.data);

                    if(result.data){

                        $('.vaccine_name').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.vaccine_name+" ("+result.data.vaccine.drug_name+")");
                        $('.properties').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.properties);
                        $('.company').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.company);
                        $('.veterinarian').append('<span><i class="dog icon"></i></span>'+result.data.veterinarian);
                        $('.description').append('<span><i class="dog icon"></i></span>'+result.data.vaccine.description);
                        $('.img_vaccine').append('<img class="ui small image" src="'+result.data.vaccine.img_vaccine.replace("/public", envimg)+'">');


                        $('.small.modal').modal({
                            closable: false,
                        }).modal("show");
                    }

                });
            } // End if check s tatus success.

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        },
        error : function(error) {
            // showForm(form, action, error, data);
        }
    });
    
}
</script>