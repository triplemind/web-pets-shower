<div class="ui basic segment">
    <br>
    <div class="ui stackable column grid segment">
        <div class="eight wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="paw icon"></i>
                <div class="content">
                    จัดการสัตว์เลี้ยง
                </div>
            </h3>
        </div>

        <div class="four wide column">
            <?php if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'): ?>
            <button class="ui fluid blue button btn-managespecies" type="submit" style="border-radius: 30px;">จัดการสายพันธุ์สัตว์เลี้ยง</button>
            <?php endif ?>
        </div>

        <div class="four wide column">
            <?php if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'): ?>
            <button class="ui fluid blue button btn-managepettype" type="submit" style="border-radius: 30px;">จัดการประเภทสัตว์เลี้ยง</button>
            <?php endif ?>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <div class="field"  style="text-align: end;">
            <button class="ui green button btn-addpet" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลสัตว์เลี้ยง</button>
        </div>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_petsShop">
                    <thead>
                        <tr>
                            <th>รูปภาพ</th>
                            <th>ชื่อ</th>
                            <th>ประเภทสัตว์เลี้ยง</th>
                            <th>สายพันธุ์สัตว์เลี้ยง</th>
                            <th>เพศสัตว์เลี้ยง</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($petsShops)): ?>
                            <?php foreach ($petsShops as $key => $petsShop):?>
                                <tr>
                                    <td class="wide two">
                                        <img class="ui circular image" src="<?php echo empty($petsShop->pet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $petsShop->pet->pic); ?>" height="80px" width="80px">
                                    </td>
                                    <td class="wide two"><?php echo empty($petsShop->pet) ? '-' : $petsShop->pet->name ?></td>
                                    <td class="wide two"><?php echo empty($petsShop->pet) ? '-' : $petsShop->pet->type ?></td>
                                    <td class="wide two"><?php echo empty($petsShop->pet) ? '-' : $petsShop->pet->breed ?></td>
                                    <td class="wide two"><?php echo empty($petsShop->pet) ? '-' : $petsShop->pet->gender ?></td>
                                    <td class="wide two">
                                        <i class="large eye outline icon viewpet" data-id="<?php echo $petsShop->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large edit outline icon editpet" data-id="<?php echo $petsShop->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deletepet" data-id="<?php echo $petsShop->id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('petsShop.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('petsShop.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('petsShop.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('petsShop.delete.post');?>"></div>


<div class="ui medium modal" id="addMyPetModal">
    <div class="header">เพิ่มข้อมูลสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form"  id="addpetform">
            <div class="field">
                <label>ชื่อสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="pet_name" id="pet_name">
            </div>
            <div class="field">
                <label>ร้านค้า</label>
                <select class="ui fluid dropdown" name="shop_id" id="shop_id" >
                    <?php if(!empty($shops)): ?>
                        <?php foreach ($shops as $key => $shop): ?>
                            <option value="<?php echo $shop->id_shop ?>"><?php echo $shop->name_shop ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>ประเภทสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="pet_type" id="pet_type" >
                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                    <?php if(!empty($pettypes)): ?>
                        <?php foreach ($pettypes as $key => $pettype): ?>
                            <option value="<?php echo $pettype->name; ?>"><?php echo $pettype->name; ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สายพันธุ์สัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="breed" id="breed" >
                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                </select>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>เพศสัตว์เลี้ยง</label>
                    <select class="ui fluid dropdown" name="gender" id="gender" >
                        <option value="">เลือกเพศสัตว์เลี้ยง</option>
                        <option value="เพศผู้">เพศผู้</option>
                        <option value="เพศเมีย">เพศเมีย</option>
                    </select>
                </div>
                <div class="field">
                    <label>สีของสัตว์เลี้ยง</label>
                    <input type="text" placeholder="" name="color" id="color">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>น้ำหนัก (กิโลกรัม)</label>
                    <!-- <input type="number" placeholder="" name="weight" id="weight" min="0"> -->
                    <input type="number" placeholder="" name="weight" id="weight" min="0" oninput="javascript: if (this.value.length > 11) this.value = this.value.slice(0, 11);">
                </div>
                <div class="field">
                    <label>วันเกิดสัตว์เลี้ยง</label>
                    <div class="ui calendar" id="birthday">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="Date/Time" readonly name="birthday">
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>รายละเอียดเพิ่มเติม</label>
                <textarea name="notedescription" id="notedescription" rows="3"></textarea>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="pic" id="pic">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="editMyPetModal">
    <div class="header">เพิ่มข้อมูลสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
            <input type="hidden" placeholder="" name="ed_petsShop_id" id="ed_petsShop_id">
            <div class="field">
                <label>ชื่อสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="ed_pet_name" id="ed_pet_name">
            </div>
            <div class="field">
                <label>ร้านค้า</label>
                <select class="ui fluid dropdown" name="ed_shop_id" id="ed_shop_id" >
                    <?php if(!empty($shops)): ?>
                        <?php foreach ($shops as $key => $shop): ?>
                            <option value="<?php echo $shop->id_shop ?>"><?php echo $shop->name_shop ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>ประเภทสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="ed_pet_type" id="ed_pet_type" >
                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                    <?php if(!empty($pettypes)): ?>
                        <?php foreach ($pettypes as $key => $pettype): ?>
                            <option value="<?php echo $pettype->name; ?>"><?php echo $pettype->name; ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สายพันธุ์สัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="ed_breed" id="ed_breed" >
                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                </select>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>เพศสัตว์เลี้ยง</label>
                    <select class="ui fluid dropdown" name="ed_gender" id="ed_gender" >
                        <option value="">เลือกเพศสัตว์เลี้ยง</option>
                        <option value="เพศผู้">เพศผู้</option>
                        <option value="เพศเมีย">เพศเมีย</option>
                    </select>
                </div>
                <div class="field">
                    <label>สีของสัตว์เลี้ยง</label>
                    <input type="text" placeholder="" name="ed_color" id="ed_color">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>น้ำหนัก (กิโลกรัม)</label>
                    <!-- <input type="number" placeholder="" name="ed_weight" id="ed_weight" min="0"> -->
                    <input type="number" placeholder="" name="ed_weight" id="ed_weight" min="0" oninput="javascript: if (this.value.length > 11) this.value = this.value.slice(0, 11);">
                </div>
                <div class="field">
                    <label>วันเกิดสัตว์เลี้ยง</label>
                    <div class="ui calendar" id="ed_birthday">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="Date/Time" readonly name="ed_birthday">
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>รายละเอียดเพิ่มเติม</label>
                <textarea name="ed_notedescription" id="ed_notedescription" rows="3"></textarea>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="ed_pic" id="ed_pic">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>