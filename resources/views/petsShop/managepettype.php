<div class="ui basic segment">
    <br>
    <div class="ui stackable column grid segment">
        <div class="eight wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="paw icon"></i>
                <div class="content">
                    จัดการประเภทสัตว์เลี้ยง
                </div>
            </h3>
        </div>

        <div class="four wide column">
            <!-- <button class="ui fluid big blue button btn-addpet" type="submit" style="border-radius: 30px;">จัดการสายพันธุ์สัตว์เลี้ยง</button> -->
        </div>

        <div class="four wide column">
            <button class="ui fluid big button" type="submit" onclick="window.location.href = '/admin/petsShop';" style="border-radius: 30px;">ย้อนกลับ</button>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <div class="field" style="text-align: end;">
            <button class="ui green button btn-addpettype" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลประเภทสัตว์เลี้ยง</button>
        </div>
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_managepettype">
                    <thead>
                        <tr>
                            <th>ชื่อประเภทสัตว์เลี้ยง</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($pettypes)): ?>
                            <?php foreach ($pettypes as $key => $pettype):?>
                                <tr>
                                    <td><?php echo empty($pettype->name) ? '-' : $pettype->name ?></td>
                                    <td>
                                        <?php 
                                            $lb_color = "";
                                            if($pettype->status == 'Active'){
                                                $lb_color = 'green';
                                            }else{
                                                $lb_color = 'red';
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $pettype->status ?></a>
                                    </td>
                                    <td>
                                        <i class="large edit outline icon editpettype" data-id="<?php echo $pettype->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deletepettype" data-id="<?php echo $pettype->id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('petsShop.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('petsShop.managepettypeadd.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('petsShop.managepettypeedit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('petsShop.managepettypedelete.post');?>"></div>


<div class="ui medium modal" id="addPetTypeModal">
    <div class="header">เพิ่มข้อมูลประเภทสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
            <div class="field">
                <label>ชื่อประเภทสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="name" id="name">
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="status" id="status">
                    <option value="">เลือกสถานะ</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="editPetTypeModal">
    <div class="header">เพิ่มข้อมูลประเภทสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
            <input type="hidden" placeholder="" name="ed_id" id="ed_id">
            <div class="field">
                <label>ชื่อประเภทสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="ed_name" id="ed_name">
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="ed_status" id="ed_status">
                    <option value="">เลือกสถานะ</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>