<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="receipt icon"></i>
                <div class="content">
                    ข้อมูลสัตว์เลี้ยง
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="ui form segment">
        <div class="ui grid">
            <div class="four wide column">
                <img class="ui small circular image" src="<?php echo empty($petsShop->pet) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : (empty($petsShop->pet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $petsShop->pet->pic)); ?>" style="max-width: 150px;max-height: 150px">
            </div>
            <div class="four wide column">
                <div style="margin-top: 7rem;"><span style="font-weight: 900;font-size: 45px;color: #565656;"><?php echo empty($petsShop->pet) ? '-' : $petsShop->pet->name; ?></span></div>
            </div>
            <div class="eight wide column">
                <!-- <img class="ui small image" src="<?php //echo url('').'/themes/image/QR.png'; ?>"> -->
            </div>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui single line table">
                    <tbody>
                        <!-- <tr>
                            <td class="active" colspan="4">รายละเอียดคำสั่งซื้อ</td>
                        </tr> -->
                        <tr>
                            <td class="two wide active">เจ้าของ</td>
                            <td class="six wide"><?php echo !empty($petsShop->user) ? $petsShop->user->name_owner : '-'; ?></td>
                            <td class="two wide active">ร้านที่ให้บริการ</td>
                            <td class="six wide"><?php echo !empty($petsShop->shop) ? $petsShop->shop->name_shop : '-'; ?></td>
                        </tr>
                        
                        <tr>
                            <td class="two wide active">ประเภทสัตว์เลี้ยง</td>
                            <td class="six wide"><?php echo !empty($petsShop->pet) ? $petsShop->pet->type : '-'; ?></td>
                            <td class="two wide active">สายพันธุ์สัตว์เลี้ยง</td>
                            <td class="six wide"><?php echo !empty($petsShop->pet) ? $petsShop->pet->breed : '-'; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">เพศสัตว์เลี้ยง</td>
                            <td class="six wide"><?php echo !empty($petsShop->pet) ? $petsShop->pet->gender : '-'; ?></td>
                            <td class="two wide active">สีของสัตว์เลี้ยง</td>
                            <td class="six wide"><?php echo !empty($petsShop->pet) ? $petsShop->pet->color : '-'; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">วันเกิดสัตว์เลี้ยง</td>
                            <td class="six wide"><?php echo !empty($petsShop->pet) ? $petsShop->pet->birthday : '-'; ?></td>
                            <td class="two wide active">น้ำหนัก (กิโลกรัม)</td>
                            <td class="six wide"><?php echo !empty($petsShop->pet) ? $petsShop->pet->weight : '-'; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">รายละเอียดเพิ่มเติม</td>
                            <td class="six wide"  colspan="3"><?php echo !empty($petsShop->pet) ? $petsShop->pet->notedescription : '-'; ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <input type="hidden" placeholder="" value="<?php echo $petsShop->id ?>" name="id" id="id">
        <input type="hidden" placeholder="" value="<?php echo $petsShop->shop_id ?>" name="id_shop" id="id_shop">
        <input type="hidden" placeholder="" value="<?php echo $petsShop->pet_id ?>" name="pet_id" id="pet_id">

        <div class="ui grid">
            <div class="sixteen wide column">
                <div style="margin-top: 5rem;"><span style="font-weight: 900;font-size: 20px;color: #565656;">ทำนัดครั้งถัดไป</span></div>
            </div>
            <div class="ui form sixteen wide column">
                <div class="two fields">
                    <div class="field">
                        <label>วันที่นัดหมาย</label>
                        <div class="ui calendar" id="booking_date">
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" placeholder="วันที่นัดหมาย" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>บริการ</label>
                        <select class="ui fluid search selection dropdown" name="id_service" id="id_service" >
                            <option>เลือกบริการ</option>
                            <option value="PV0001">วัคซีน</option>
                            <!-- <option value="PV0002">โปรโมชันวัคซีน</option> -->
                        </select>
                    </div>
                   <!--  <div class="field">
                        <label>ช่วงเวลา</label>
                        <select class="ui fluid search selection dropdown" name="booking_time" id="booking_time" >
                            <option>เลือกช่วงเวลา</option>
                            <option value="เช้า">เช้า 09.00น.-12.30น.</option>
                            <option value="บ่าย">บ่าย 13.30น.-19.00น.</option>
                        </select>
                    </div> -->
                </div>
                <!-- <div class="field" style="text-align: end;">
                    <a class="ui violet basic tag label" id="que_txt"></a>
                </div> -->
                <div class="field vaccine_display" style="display: none;">
                    <label>เลือกวัคซีน</label>
                    <?php if(!empty($petsShop->shop->vaccine)): ?>
                    <select class="ui fluid search selection dropdown" name="vaccine" id="vaccine" >
                        <option value="" selected disabled>กรุณาเลือกวัคซีน</option>
                    </select>
                    <?php else: ?>
                        <div class="ui red large message">
                            <p>ไม่พบรายชื่อวัคซีน กรุณาติดต่อทางร้านค้า</p>
                        </div>
                    <?php endif ?>
                    <a href="/vaccinedetail" target="_blank" style="float: right;margin-bottom: 1rem;margin-top: 1rem;">คลิกเพื่อดูรายละเอียดวัคซีนต่างๆ</a>
                </div>
               <!--  <div class="field">
                    <label>โค้ดส่วนลด</label>
                    <input type="text" name="promo_code" id="promo_code" placeholder="">
                </div> -->
                <div class="field">
                    <label>หมายเตุ</label>
                    <textarea name="note" id="note" rows="4"></textarea> 
                </div>
            </div>
        </div>

        <div class="ui unstackable four column grid">
            <div class="column"></div>
            <div class="right floated column">
                <button class="ui fluid yellow button btn-save" type="submit">บันทึก</button>
            </div>
        </div>


        <div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>

        <div class="ui grid">
            <div class="sixteen wide column">
                <div style="margin-top: 5rem;"><span style="font-weight: 900;font-size: 20px;color: #565656;">ตารางบันทึกการฉีดวัคซีนที่ผ่านมา</span></div>
            </div>
            <div class="sixteen wide column">
                <table class="ui teal table" id="TBL_vaccineRC">
                    <thead>
                        <tr>
                            <th>ชื่อวัคซีน</th>
                            <th>ชนิดวัคซีน</th>
                            <th>วันที่ฉีด</th>
                            <th>สัตว์แพทย์ผู้ฉีด</th>
                            <th>เพิ่มเติม</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($getvaccineRCs)): ?>
                            <?php foreach ($getvaccineRCs as $key => $getvaccineRC):?>
                                <?php if(!empty($getvaccineRC->img)): ?>
                                <tr>
                                    <td><?php echo $getvaccineRC->namevaccin ?></td>
                                    <td><?php echo empty($getvaccineRC->vaccine) ? '-' : $getvaccineRC->vaccine->vaccine_type ?></td>
                                    <!-- <td><?php //echo $getvaccineRC->updated_at ?></td> -->
                                    <td><?php echo DateThai($getvaccineRC->updated_at, true, false); ?></td>
                                    <td><?php echo $getvaccineRC->veterinarian ?></td>
                                    <td><?php echo $getvaccineRC->notevaccin ?></td>
                                    <td class="wide two">
                                        <button class="ui teal button btn-viewImg" type="submit" data-id="<?php echo $getvaccineRC->id_vaccin ; ?>">ดูฉลากวัคซีน</button>
                                    </td>
                                </tr>
                                <?php endif ?>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>



      
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('petsShop.ajax_center.post');?>"></div>
    <div id='bookingajax-center-url' data-url="<?php echo \URL::route('booking.ajax_center.post');?>"></div>
    <div id='add_url' data-url="<?php echo \URL::route('petsShop.addBooking.post');?>"></div>

