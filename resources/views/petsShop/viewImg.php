<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <!-- <i class="receipt icon"></i> -->
                <div class="content">
                    รายละเอียดการฉีดวัคซีน
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>
    

    <div class="ui form segment">
        <?php if(!empty($getvaccineRC)): ?>
            <h3 class="ui header">ข้อมูลวัคซีน</h3>
            <div class="ui relaxed divided list">
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">ชื่อวัคซีน</a>
                        <div class="description"><?php echo $getvaccineRC->namevaccin; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">สรรพคุณของวัคซีน</a>
                        <div class="description"><?php echo $getvaccineRC->vaccin; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">สัตว์แพทย์</a>
                        <div class="description"><?php echo $getvaccineRC->veterinarian; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">รายละเอียด</a>
                        <div class="description"><?php echo $getvaccineRC->notevaccin; ?></div>
                    </div>
                </div>
                <div class="item">
                    <i class="circle icon"></i>
                    <div class="content">
                        <a class="header">ฉลากวัคซีน</a>
                    </div>
                </div>
            </div>
            <div class="ui form">
                <div class="fields">
                    <div class="ui stackable" id="img-card">
                        <?php //if(!empty($getvaccineRC->img)): ?>
                            <?php //$imgList = json_decode($getvaccineRC->img, true); ?>
                            <?php //sd($imgList); ?>
                            <?php //foreach ($imgList as $key => $getimg):?>
                               <!--  <div class="yellow  card">
                                    <div class="blurring dimmable image"> -->
                                <img class="ui large image" src="<?php echo empty($getvaccineRC->img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getvaccineRC->img); ?>">
                                    <!-- </div>
                                </div> -->
                            <?php //endforeach ?>
                        <?php //endif ?>
                    </div>
                </div>
            </div>
        <?php endif ?>


    </div>
</div>
