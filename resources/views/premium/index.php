<?php if($user_type == 'เจ้าของกิจการ'): ?>
<div class="ui basic segment">
    <br>
    <?php if((empty($shops->pre_startdate) && empty($shops->pre_enddate)) || ($diffexpired_date == 'expire')): ?>
	    <div id="regist-pre">
		    <div class="ui segment" style="padding: 3rem;">
		        <div style="margin-top: 0px;font-size: 28px;text-align: center;">
		            <i class="chess queen icon"></i> Premium
		        </div>
			    
			    <br>

		        <h4 style="margin-top: 0px;font-size: 18px;text-align: center;">สมัครสมาชิกแบบ Premium เพียง 300/เดือน เท่านั้น</h4>

				
			    <br>
			    <br>
			    

			    <div class="ui unstackable three column grid">
			    	<div class="two wide column"></div>
			    	<div class="twelve wide column"> 
						<button class="fluid massive ui black button btn-regitPre" style="border-radius: 15px;">
							สมัครสมาชิก Premium
						</button>
			    	</div>
			    	<div class="two wide column"></div>
			    </div>

		    </div>
	    </div>
    <?php endif ?>

    <?php if((!empty($shops->pre_startdate) && !empty($shops->pre_enddate)) && ($diffexpired_date != 'expire')): ?>
	    <div class="ui segment" style="padding: 3rem;">
	    	<div style="margin-top: 0px;font-size: 28px;text-align: center;">
	            <i class="chess queen icon"></i> สมาชิก Premium
	        </div>
	        <br>
	    	<h4 style="margin-top: 0px;font-size: 18px;text-align: center;">วันเริ่มต้นสมาชิก Premium : <?php echo DateThai($shops->pre_startdate, true, false); ?></h4>
	    	<h4 style="margin-top: 0px;font-size: 18px;text-align: center;">วันสิ้นสุดสมาชิก Premium : <?php echo DateThai($shops->pre_enddate, true, false); ?></h4>
	    	<?php $diffcount_date = date_diff(date_create($shops->pre_enddate),date_create(date('Y-m-d'))); ?>
	    	<h4 style="margin-top: 0px;font-size: 18px;text-align: center;">ระยะเวลาที่เหลือ : <?php echo $diffcount_date->days; ?> วัน</h4>
	    </div>
	    <br>
	    <br>
	<?php endif ?>

	<?php if(!empty($shops)): ?>
    <div <?php echo ($shops->status_premium == 0) ? 'style="display: none;"' : (($diffexpired_date == 'expire') ? 'style="display: none;"' : ''); ?> id="payment-prement">
	    <div class="ui segment" style="padding: 3rem;">

	    	<input type="hidden" name="check_payment" id="check_payment" value="<?php echo empty($shops->pic_payment) ? 'empty' : 'not empty'; ?>">

	    	<input type="hidden" placeholder="" name="id" id="id" value="<?php echo $shops->id_shop; ?>">

	        <h4 style="margin-top: 0px;font-size: 18px;text-align: center;">ชำระเงินและแจ้งโอนเงิน</h4>
	        <br>
	        <img class="ui centered medium image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/pay.png'; ?>">
		    <br>
		    <?php if(empty($shops->pic_payment)): ?>
			    <div class="ui form">
			    	<h4>แนบหลักฐานการชำระเงิน</h4>
					<div class="three fields">
						<div class="field">
							<label>วันที่ชำระเงิน</label>
							<div class="ui calendar" id="date_payment">
								<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" placeholder="วันที่ชำระเงิน" readonly>
								</div>
							</div>
						</div>
						<div class="field">
							<label>เวลาที่ชำระเงิน</label>
							<input type="text" class="form-control js-time-picker" readonly  name="time_payment" id="time_payment">
		                    <div class="docs-picker-container" style="display: block;"></div>
						</div>
						<div class="field">
							<label>ธนาคาร</label>
							<select class="ui fluid search selection dropdown" name="bank_payment" id="bank_payment" >
								<option value='' selected>--- กรุณาเลือกธนาคาร ---</option>
			                    <option value='ธนาคารกรุงเทพ'>ธนาคารกรุงเทพ</option>
			                    <option value='ธนาคารกสิกรไทย'>ธนาคารกสิกรไทย</option>
			                    <option value='ธนาคารกรุงไทย'>ธนาคารกรุงไทย</option>
			                    <option value='ธนาคารทหารไทย'>ธนาคารทหารไทย</option>
			                    <option value='ธนาคารไทยพาณิชย์'>ธนาคารไทยพาณิชย์</option>
			                    <option value='ธนาคารกรุงศรีอยุธยา'>ธนาคารกรุงศรีอยุธยา</option>
			                    <option value='ธนาคารเกียรตินาคิน'>ธนาคารเกียรตินาคิน</option>
			                    <option value='ธนาคารซีไอเอ็มบีไทย'>ธนาคารซีไอเอ็มบีไทย</option>
			                    <option value='ธนาคารทิสโก้'>ธนาคารทิสโก้</option>
			                    <option value='ธนาคารธนชาต'>ธนาคารธนชาต</option>
			                    <option value='ธนาคารยูโอบี'>ธนาคารยูโอบี</option>
			                    <option value='ธนาคารออมสิน'>ธนาคารออมสิน</option>
			                    <option value='ธนาคารอาคารสงเคราะห์'>ธนาคารอาคารสงเคราะห์</option>
			                    <option value='ธนาคารอิสลามแห่งประเทศไทย'>ธนาคารอิสลามแห่งประเทศไทย</option>
			                </select>
						</div>
					</div>
					<div class="field">
						<label>หลักฐานการชำระเงิน</label>
						<input type="file" placeholder="" name="pic_payment" id="pic_payment">
					</div>
					<div class="sixteen wide field">
						<div class="ui unstackable three column grid">
				            <div class="right floated column">
				            	<div class="fields">
					            	<div class="field">
										<button class="ui blue button btn-save-payment" type="submit">ยืนยันการชำระเงิน</button>
									</div>
						            <div class="field">
										<button class="ui red button btn-cancel-regitPre" type="submit">ยกเลิก</button>
						            </div>
								</div>
				            </div>
				        </div>
			        </div>
				</div>
			<?php else: ?>
				<div class="ui form">
			    	<h4>แก้ไขหลักฐานการชำระเงิน</h4>
					<div class="three fields">
						<div class="field">
							<label>วันที่ชำระเงิน</label>
							<div class="ui calendar" id="ed_date_payment">
								<div class="ui input left icon">
									<i class="calendar icon"></i>
									<input type="text" placeholder="วันที่ชำระเงิน" readonly value="<?php echo $shops->date_payment; ?>">
								</div>
							</div>
						</div>
						<div class="field">
							<label>เวลาที่ชำระเงิน</label>
							<input type="text" class="form-control js-time-picker" readonly  name="ed_time_payment" id="ed_time_payment" value="<?php echo $shops->time_payment; ?>">
		                    <div class="docs-picker-container" style="display: block;"></div>
						</div>
						<div class="field">
							<label>ธนาคาร</label>
							<select class="ui fluid search selection dropdown" name="ed_bank_payment" id="ed_bank_payment" >
								<option value='' selected>--- กรุณาเลือกธนาคาร ---</option>
			                    <option value='ธนาคารกรุงเทพ' <?php echo ($shops->bank_payment == 'ธนาคารกรุงเทพ') ? 'selected' : ''; ?> >ธนาคารกรุงเทพ</option>
			                    <option value='ธนาคารกสิกรไทย' <?php echo ($shops->bank_payment == 'ธนาคารกสิกรไทย') ? 'selected' : ''; ?> >ธนาคารกสิกรไทย</option>
			                    <option value='ธนาคารกรุงไทย' <?php echo ($shops->bank_payment == 'ธนาคารกรุงไทย') ? 'selected' : ''; ?> >ธนาคารกรุงไทย</option>
			                    <option value='ธนาคารทหารไทย' <?php echo ($shops->bank_payment == 'ธนาคารทหารไทย') ? 'selected' : ''; ?> >ธนาคารทหารไทย</option>
			                    <option value='ธนาคารไทยพาณิชย์' <?php echo ($shops->bank_payment == 'ธนาคารไทยพาณิชย์') ? 'selected' : ''; ?> >ธนาคารไทยพาณิชย์</option>
			                    <option value='ธนาคารกรุงศรีอยุธยา' <?php echo ($shops->bank_payment == 'ธนาคารกรุงศรีอยุธยา') ? 'selected' : ''; ?> >ธนาคารกรุงศรีอยุธยา</option>
			                    <option value='ธนาคารเกียรตินาคิน' <?php echo ($shops->bank_payment == 'ธนาคารเกียรตินาคิน') ? 'selected' : ''; ?> >ธนาคารเกียรตินาคิน</option>
			                    <option value='ธนาคารซีไอเอ็มบีไทย' <?php echo ($shops->bank_payment == 'ธนาคารซีไอเอ็มบีไทย') ? 'selected' : ''; ?> >ธนาคารซีไอเอ็มบีไทย</option>
			                    <option value='ธนาคารทิสโก้' <?php echo ($shops->bank_payment == 'ธนาคารทิสโก้') ? 'selected' : ''; ?> >ธนาคารทิสโก้</option>
			                    <option value='ธนาคารธนชาต' <?php echo ($shops->bank_payment == 'ธนาคารธนชาต') ? 'selected' : ''; ?> >ธนาคารธนชาต</option>
			                    <option value='ธนาคารยูโอบี' <?php echo ($shops->bank_payment == 'ธนาคารยูโอบี') ? 'selected' : ''; ?> >ธนาคารยูโอบี</option>
			                    <option value='ธนาคารออมสิน' <?php echo ($shops->bank_payment == 'ธนาคารออมสิน') ? 'selected' : ''; ?> >ธนาคารออมสิน</option>
			                    <option value='ธนาคารอาคารสงเคราะห์' <?php echo ($shops->bank_payment == 'ธนาคารอาคารสงเคราะห์') ? 'selected' : ''; ?> >ธนาคารอาคารสงเคราะห์</option>
			                    <option value='ธนาคารอิสลามแห่งประเทศไทย' <?php echo ($shops->bank_payment == 'ธนาคารอิสลามแห่งประเทศไทย') ? 'selected' : ''; ?> >ธนาคารอิสลามแห่งประเทศไทย</option>
			                </select>
						</div>
					</div>
					<div class="field">
						<label>หลักฐานการชำระเงิน</label>
						<input type="file" placeholder="" name="ed_pic_payment" id="ed_pic_payment">
					</div>
					<div class="ui column grid">
			            <div class="column" style="text-align: end;">
							<button class="ui blue button btn-edit-payment" type="submit">บันทึก</button>
			            </div>
			        </div>
				</div>
			<?php endif ?>
			
	    </div>
    </div>
    <?php endif ?>
    <br>
</div>

<?php endif ?>


<?php if($user_type == 'ผู้ดูแลระบบ' || $user_type == 'Root'): ?>
<div class="ui basic segment">
    <br>
    <div class="ui unstackable ten column grid segment">
        <div class="column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="chess queen icon"></i> 
                <div class="content">
                    Premium
                </div>
            </h3>
        </div>
    </div>

    <br>

    <div class="ui form segment">
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_user">
                    <thead>
                        <tr>
                            <th>รูปภาพ</th>
                            <th>ชื่อร้าน</th>
                            <th>วันที่เริ่มต้นสมาชิก</th>
                            <th>วันที่สิ้นสุดสมาชิก</th>
                            <th>สถานะ</th>
                            <th>สถานะพรีเมี่ยม</th>
                            <th>รายละเอียด</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($shops_admin)): ?>
                            <?php foreach ($shops_admin as $key => $shop):?>
                                <tr>
                                    <td class="wide two">
                                        <img class="ui tiny image" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>">
                                    </td>
                                    <td class="wide two"><?php echo $shop->name_shop ?></td>
                                    <td class="wide two"><?php echo empty($shop->pre_startdate) ? '-' : DateThai($shop->pre_startdate, true, false); ?></td>
                                    <td class="wide two"><?php echo empty($shop->pre_enddate) ? '-' : DateThai($shop->pre_enddate, true, false); ?></td>
                                    <td class="wide two">
                                        <?php 
                                            $lb_color = "";
                                            $status_txt = "";
                                            if($shop->status_open == 1){
                                                $lb_color = 'green';
                                                $status_txt = "เปิดให้บริการ";
                                            }else if($shop->status_open == 0){
                                                $lb_color = 'red';
                                                $status_txt = "ปิดให้บริการ";
                                            }else{
                                                $lb_color = 'yellow';
                                                $status_txt = "ไม่ระบุ";
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $status_txt ?></a>
                                            
                                    </td>
                                    <td class="wide two">
                                    	<?php 
                                    		$diffExp_date = date_diff(date_create($shop->pre_enddate),date_create(date('Y-m-d')));

                                    		$expireadmin = strtotime($shop->pre_enddate);
							                $todayadmin  = strtotime("today midnight");
							                
                                		?>
                                    	<?php if((!empty($shop->pre_startdate) && !empty($shop->pre_enddate))): ?>

                                			<?php if($todayadmin < $expireadmin): ?>
												<div class="ui toggle checkbox">
													<input class="on-off-status_premium" type="checkbox" name="status_premium" <?php echo ($shop->status_premium == 1) ? 'checked' : '' ?> value="<?php echo $shop->id_shop ; ?>">
													<label><?php echo ($shop->status_premium == 1) ? 'ใช้งานอยู่' : 'หยุดใช้งานชั่วคราว' ?></label>
												</div>
											<?php else: ?>
												<div class="ui toggle checkbox">
													<input class="on-off-status_premium" type="checkbox" name="status_premium" <?php echo ($todayadmin < $expireadmin) ? 'checked' : '' ?>  value="<?php echo $shop->id_shop ; ?>">
													<label>สิ้นสุดการใช้งาน</label>
												</div>
											<?php endif ?>

										<?php else: ?>
											<label>ไม่ใช่สมาชิกพรีเมี่ยม</label>
										<?php endif ?>
                                    </td>
                                    <td class="wide two">
                                    	<?php if((!empty($shop->pre_startdate) && !empty($shop->pre_enddate))): ?>
	                                        <i class="large eye outline icon viewshop" data-id="<?php echo $shop->id_shop; ?>"  style="cursor: pointer;" 
	                                        	data-name_shop="<?php echo $shop->name_shop; ?>" 
	                                        	data-pre_status="<?php echo ($shop->status_premium == 1) ? 'ใช้งานอยู่' : 'ยกเลิก'; ?>" 
	                                        	data-shop_status="<?php echo $status_txt; ?>" 
	                                        	data-pre_startdate="<?php echo empty($shop->pre_startdate) ? '-' : DateThai($shop->pre_startdate, true, false); ?>" 
	                                        	data-pre_enddate="<?php echo empty($shop->pre_enddate) ? '-' : DateThai($shop->pre_enddate, true, false); ?>" 
	                                        	data-paydate="<?php echo empty($shop->date_payment) ? '-' : DateThai($shop->date_payment, true, false); ?>" 
	                                        	data-paytime="<?php echo $shop->time_payment; ?>" 
	                                        	data-paybank="<?php echo $shop->bank_payment; ?>" 
	                                        	data-addslip-src="<?php echo empty($shop->pic_payment) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->pic_payment); ?>" 
	                                        	data-shopimg="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>"></i>
	                                    <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>
<?php endif ?>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='addslip-url' data-url="<?php echo \URL::route('premium.add.post');?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('premium.ajax_center.post');?>"></div>
<div id='editslip-url' data-url="<?php echo \URL::route('premium.edit.post');?>"></div>


<div class="ui second coupled modal">
	<div class="header">
		หลักฐานการชำระเงิน
	</div>
	<div class="image content">
		<img class="ui centered big image addslip-src" >
	</div>
	<div class="actions">
		<div class="ui black deny button">
			ปิด
		</div>
	</div>
</div>



<div class="ui first coupled modal">
	<div class="header">
		รายละเอียดร้านค้า
	</div>
	<div class="image scrolling content">
		<div class="ui large image">
			<img class="ui centered big image shopimg">
		</div>
		<div class="description">
			<h2 class="shopName"></h2>
			<p class="pre_status"></p>
			<p class="shop_status"></p>
			<p class="pre_startdate"></p>
			<p class="pre_enddate"></p>
			<p>รายละเอียดการชำระเงิน :</p>
			<ol class="ui list">
				<li value="*"><a class="slipimg" style="cursor: pointer;">หลักฐานการชำระเงิน</a></li>
				<li value="*" class="paydate"></li>
				<li value="*" class="paytime"></li>
				<li value="*" class="paybank"></li>
			</ol>
		</div>
	</div>
	<div class="actions">
		<div class="ui black deny button">ปิด</div>
	</div>
</div>