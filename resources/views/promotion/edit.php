<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="ad icon"></i>
                <div class="content">
                    แก้ไขข้อมูลโปรโมชั่นประจำร้าน <?php echo $shop->name_shop; ?>
                </div>
            </h3>
        </div>
        <div class="right floated column">
           
        </div>
    </div>

	<div class="ui form segment">
		<form class="ui form">
			<input type="hidden" name="id" id="id" value="<?php echo $promotion->id; ?>">
			<div class="field">
				<label>ชื่อโปรโมชั่น</label>
				<input type="text" name="title" id="title" placeholder="ชื่อโปรโมชั่น" value="<?php echo $promotion->title; ?>">
			</div>
			<div class="three fields">
				<div class="field">
					<label>ประเภทบริการ</label>
					<select class="ui fluid dropdown" name="service_type" id="service_type" >
	                    <option value="อาบน้ำ-ตัดขน" <?php echo $promotion->service_type == "อาบน้ำ-ตัดขน" ? "selected" : ""; ?>>อาบน้ำ-ตัดขน</option>
	                    <option value="ฉีดวัคซีน" <?php echo $promotion->service_type == "ฉีดวัคซีน" ? "selected" : ""; ?>>ฉีดวัคซีน</option>
	                </select>
				</div>
				<div class="field">
					<label>ส่วนลด</label>
					<input type="text" name="discount" id="discount" placeholder="ส่วนลด" value="<?php echo $promotion->discount; ?>">
				</div>
				<div class="field">
					<label>สถานะ</label>
					<select class="ui fluid search selection dropdown" name="status" id="status" >
	                    <option value="Active" <?php echo $promotion->status == "Active" ? "selected" : ""; ?>>Active</option>
	                    <option value="Inactive" <?php echo $promotion->status == "Inactive" ? "selected" : ""; ?>>Inactive</option>
	                </select>
				</div>
			</div>
			<div class="two fields">
				<div class="field">
					<label>วันเริ่มต้น</label>
					<div class="ui calendar" id="startdate">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="Date" readonly name="startdate" value="<?php echo $promotion->startdate; ?>">
                        </div>
                    </div>
				</div>
				<div class="field">
					<label>วันสิ้นสุด</label>
					<div class="ui calendar" id="enddate">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="Date" readonly name="enddate" value="<?php echo $promotion->enddate; ?>">
                        </div>
                    </div>
				</div>
			</div>
			<!-- <div class="two fields">
				<div class="field">
					<label>ร้านค้า</label>
					<select class="ui fluid search selection dropdown" name="shop_id" id="shop_id" >
	                    <option value="">เลือกร้านค้า</option>
	                    <?php if($getShops->count() != 0): ?>
		                    <?php foreach ($getShops as $key => $getShop): ?>
		                    	<option value="<?php echo $getShop->id_shop; ?>" <?php echo $promotion->shop_id == $getShop->id_shop ? "selected" : ""; ?>><?php echo $getShop->name_shop; ?></option>
		                    <?php endforeach ?>
	                    <?php endif ?>
	                </select>
				</div>
				<div class="field">
					<label>ส่วนลด</label>
					<input type="text" name="discount" id="discount" placeholder="ส่วนลด" value="<?php echo $promotion->discount; ?>">
				</div>
			</div> -->
			<div class="two fields">
				<div class="field">
					<label>Code</label>
					<input type="text" name="code" id="code" placeholder="Code" value="<?php echo $promotion->code; ?>">
				</div>
				<div class="field">
					<label>ลิ้งค์</label>
					<input type="text" name="link" id="link" placeholder="ลิ้งค์" value="<?php echo $promotion->link; ?>">
				</div>
			</div>
			<div class="field">
				<label>รูปภาพ</label>
				<input type="file" name="img" id="img" placeholder="รูปภาพ">
			</div>
            <br>
            <br>
            <div class="field">
                <label>รายละเอียดเพิ่มเติม</label>
                <textarea name="description" id="description" rows="10" cols="80"><?php echo $promotion->description; ?>
                </textarea>
            </div>
		</form>

        <br>
        <br>

        <div class="ui two column grid">
            <div class="column">
                <button class="ui fluid big red button btn-back" type="submit" >ยกเลิก</button>
            </div>
            <div class="column">
                <button class="ui fluid big blue button btn-save" type="submit">บันทึก</button>
            </div>
        </div>
	</div>

</div>



<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="add_url" data-url="<?php echo \URL::route('promotion.edit.post'); ?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('promotion.ajax_center.post');?>"></div>
