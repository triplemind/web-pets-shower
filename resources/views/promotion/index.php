<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="ad icon"></i>
                <div class="content">
                    หน้าจัดการโปรโมชั่น
                </div>
            </h3>
        </div>

        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big green button btn-addpromotion" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลโปรโมชั่น</button>
                </div>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_report">
                    <thead>
                        <tr>
                            <th>รูปภาพ</th>
                            <th>ชื่อโปรโมชั่น</th>
                            <th>ประเภทบริการ</th>
                            <th>code</th>
                            <th>ส่วนลด</th>
                            <th>วันเริ่มต้น</th>
                            <th>วันสิ้นสุด</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php if(!empty($getPromotions)): ?>
                            <?php foreach ($getPromotions as $key => $getPromotion):?>
                                <tr>
                                    <td>
                                        <img class="ui tiny image" src="<?php echo empty($getPromotion->img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPromotion->img); ?>">
                                    </td>
                                    <td>
                                        <?php echo empty($getPromotion->title) ? '-' : $getPromotion->title ?>
                                    </td>
                                    <td>
                                        <?php echo empty($getPromotion->service_type) ? '-' : $getPromotion->service_type; ?>
                                    </td>
                                    <td>
                                        <?php echo empty($getPromotion->code) ? '-' : $getPromotion->code; ?>
                                    </td>
                                    <td>
                                        <?php echo empty($getPromotion->discount) ? '-' : $getPromotion->discount; ?>
                                    </td>
                                    <td>
                                        <?php echo empty($getPromotion->startdate) ? '-' : DateThai($getPromotion->startdate, true, false); ?>
                                    </td>
                                    <td>
                                        <?php echo empty($getPromotion->enddate) ? '-' : DateThai($getPromotion->enddate, true, false); ?>
                                    </td>
                                    <td>
                                        <?php echo empty($getPromotion->status) ? '-' : $getPromotion->status; ?>
                                    </td>
                                    <td>
                                        <i class="large eye outline icon viewpromotion" data-id="<?php echo $getPromotion->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large edit outline icon editpromotion" data-id="<?php echo $getPromotion->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deletepromotion" data-id="<?php echo $getPromotion->id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr> 
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <?php echo "รายการทั้งหมด  ".number_format($getPromotions->total())."  รายการ"; ?>
                <div class="add-page" style="float: right;">
                    <?php echo $getPromotions->render(); ?>
                </div>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('promotion.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('promotion.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('promotion.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('promotion.delete.post');?>"></div>