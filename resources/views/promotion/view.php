<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="syringe icon"></i>
                <div class="content">
                    รายละเอียดโปรโมชั่น
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>

    <?php 
        $lb_color = "";
        $status_txt = "";
        if($promotion->status == "Active"){
            $lb_color = 'green';
            $status_txt = "Active";
        }else if($promotion->status == "Inactive"){
            $lb_color = 'red';
            $status_txt = "Inactive";
        }
    ?>



    <div class="ui form segment">
        <div class="top right attached large ui label">
            <div class="ui <?php echo $lb_color; ?> empty circular label"></div> <?php echo $status_txt ?>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui table">
                    <tbody>
                    	<tr>
                            <td class="" colspan="4">
                                <img class="ui centered medium image" src="<?php echo empty($promotion->img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $promotion->img); ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">ชื่อโปรโมชั่น</td>
                            <td class="six wide" colspan="3"><?php echo $promotion->title ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ประเภทบริการ</td>
                            <td class="six wide"><?php echo $promotion->service_type ?></td>
                            <td class="two wide active">ร้านค้า</td>
                            <td class="six wide"><?php echo $promotion->shop->name_shop; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">วันเริ่มต้น</td>
                            <td class="six wide"><?php echo DateThai($promotion->startdate, true, false) ?></td>
                            <td class="two wide active">วันสิ้นสุด</td>
                            <td class="six wide"><?php echo DateThai($promotion->enddate, true, false); ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ส่วนลด</td>
                            <td class="six wide"><?php echo $promotion->discount ?></td>
                            <td class="two wide active">Code</td>
                            <td class="six wide"><?php echo $promotion->code ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ลิ้งค์</td>
                            <td class="six wide" colspan="3"><?php echo $promotion->link ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">รายละเอียดเพิ่มเติม</td>
                            <td class="six wide" colspan="3"><?php echo $promotion->description ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    
</div>
