<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="receipt icon"></i>
                <div class="content">
                    แก้ไขข้อมูลร้านค้า
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <!-- <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div> -->
        </div>
    </div>

    <div class="ui form segment">
        <div class="ui form">
            <div class="field">
                <label>ค้นหาร้านค้าเพื่อเก็บข้อมูล Latitude และ Longitude</label>
                <input id="pac-input" class="controls" type="text" placeholder="ค้นหาร้านค้าเพื่อเก็บข้อมูล Latitude และ Longitude"  />
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Latitude</label>
                    <input type="text" name="shop_lat" id="shop_lat" placeholder="Latitude" readonly value="<?php echo $shop->shop_lat; ?>">
                </div>
                <div class="field">
                    <label>Longitude</label>
                    <input type="text" name="shop_long" id="shop_long" placeholder="Longitude" readonly value="<?php echo $shop->shop_long; ?>">
                </div>
            </div>
        </div>
    </div>


    <div class="ui form segment">
        <form class="ui form">
            <input type="hidden" placeholder="" name="id_shop" id="id_shop" value="<?php echo $shop->id_shop; ?>">
           <!--  <input type="hidden" name="province_shop" id="province_shop" value="<?php echo $shop->province; ?>">
            <input type="hidden" name="amphur_shop" id="amphur_shop" value="<?php echo $shop->amphur; ?>">
            <input type="hidden" name="district_shop" id="district_shop" value="<?php echo $shop->district; ?>"> -->
            <div class="two fields">
                <div class="field">
                    <label>ชื่อร้าน</label>
                    <input type="text" name="name_shop" value="<?php echo $shop->name_shop; ?>" id="name_shop" placeholder="ชื่อร้าน">
                </div>
                <div class="field">
                    <label>ประเภทร้าน</label>
                    <!-- <input type="text" name="type_shop" id="type_shop" placeholder="ประเภทร้าน" value="<?php //echo $shop->type_shop; ?>"> -->
                    <select class="ui fluid search selection dropdown" name="type_shop" id="type_shop">
                        <option value="อาบน้ำ-ตัดขน" <?php echo $shop->type_shop == 'อาบน้ำ-ตัดขน' ? "selected" : ""; ?>>อาบน้ำ - ตัดขน</option>
                        <option value="ฉีดวัคซีน" <?php echo $shop->type_shop == 'ฉีดวัคซีน' ? "selected" : ""; ?>>ฉีดวัคซีน</option>
                        <option value="อาบน้ำ-ตัดขนและฉีดวัคซีน" <?php echo $shop->type_shop == 'อาบน้ำ-ตัดขนและฉีดวัคซีน' ? "selected" : ""; ?>>อาบน้ำ-ตัดขนและฉีดวัคซีน</option>
                    </select>
                </div>
            </div>
            <div class="four fields">
                <div class="field">
                    <label>จำนวนเงินมัดจำ</label>
                    <input type="number" name="deposit" id="deposit" placeholder="จำนวนเงินมัดจำ" value="<?php echo $shop->deposit; ?>">
                </div>
                <div class="field">
                    <label>เลขที่</label>
                    <input type="text" name="licenesNo" id="licenesNo" placeholder="เลขที่" value="<?php echo $shop->licenesNo; ?>">
                </div>
                <div class="field">
                    <label>เบอร์โทรศัพท์</label>
                    <input type="text" name="tel_shop" id="tel_shop" placeholder="เบอร์โทรศัพท์" value="<?php echo $shop->tel_shop; ?>" maxlength="10">
                </div>
                <div class="field">
                    <label>สถานะ</label>
                    <select class="ui fluid search selection dropdown" name="status_open" id="status_open" >
                        <option value="1" <?php echo $shop->status_open == 1 ? "selected" : ""; ?>>เปิดให้บริการ</option>
                        <option value="0" <?php echo $shop->status_open == 0 ? "selected" : ""; ?>>ปิดให้บริการ</option>
                    </select>
                </div>
            </div>
            <div class="field">
                <label>ที่อยู่</label>
                <textarea name="address_shop" id="address_shop" rows="4"><?php echo $shop->address_shop; ?></textarea>
            </div>
            <div class="four fields">
                <div class="field">
                    <label>จังหวัด</label>
                    <select class="ui fluid search selection dropdown" name="province" id="province" >
                        <?php if(!empty($provinces)): ?>
                            <?php foreach($provinces as $key => $province): ?>
                                <option value="<?php echo $province->PROVINCE_ID; ?>" <?php echo ($shop->province == $province->PROVINCE_ID) ? "selected" : ""; ?>><?php echo $province->PROVINCE_NAME; ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="field">
                    <label>อำเภอ/เขต</label>
                    <select class="ui fluid search selection  dropdown" name="amphur" id="amphur" >
                        <?php if(!empty($amphurs)): ?>
                            <?php foreach($amphurs as $key => $amphur): ?>
                                <option value="<?php echo $amphur->AMPHUR_ID; ?>" <?php echo ($shop->amphur == $amphur->AMPHUR_ID) ? "selected" : ""; ?>><?php echo $amphur->AMPHUR_NAME; ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="field">
                    <label>ตำบล/แขวง</label>
                    <select class="ui fluid search selection  dropdown" name="district" id="district" >
                        <?php if(!empty($districts)): ?>
                            <?php foreach($districts as $key => $district): ?>
                                <option value="<?php echo $district->DISTRICT_ID ; ?>" <?php echo ($shop->district == $district->DISTRICT_ID) ? "selected" : ""; ?>><?php echo $district->DISTRICT_NAME; ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="field">
                    <label>รหัสไปรษณีย์</label>
                    <input type="text" name="zip" id="zip" placeholder="รหัสไปรษณีย์" value="<?php echo $shop->zip; ?>">
                </div>
            </div>
            <div class="field">
                <label>รูปภาพร้าน</label>
                <input type="file" placeholder="" name="shop_img" id="shop_img">
            </div>
            <br>
            <br>
            <div class="field">
                <label>รายละเอียดร้านค้า</label>
                <textarea name="shop_description" id="shop_description" rows="10" cols="80"><?php echo $shop->shop_description; ?>
                </textarea>
            </div>
        </form>
        <br>
        <br>
        <br>

        <div class="ui two column grid">
            <div class="column">
                <button class="ui fluid big red button btn-back" type="submit" >ยกเลิก</button>
            </div>
            <div class="column">
                  <button class="ui fluid big blue button btn-save" type="submit">บันทึก</button>
            </div>
        </div>
    </div>

</div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="edit_url" data-url="<?php echo \URL::route('shop.edit.post'); ?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('shop.ajax_center.post');?>"></div>

<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGetgbZ37IeA0eTGFVGnq5T2u3wcQsfBM&libraries=places&callback=initMap&language=th&region=TH"
        async defer
    ></script>