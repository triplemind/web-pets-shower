<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="user alternate icon"></i>
                <div class="content">
                    จัดการร้านค้า
                </div>
            </h3>
        </div>

        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big green button btn-addshop" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลร้านค้า</button>
                </div>
            </div>
        </div>

    </div>

    <br>
    <?php //sd($filters); ?>
    <div class="ui form segment">
        <h4 class="ui header">ค้นหารายการจอง</h4>
        <div class="fields">
            <div class="eight wide field">
                <label>คำค้นหา</label>
                <input type="text" placeholder="" name="search_txt" id="search_txt" value="<?php echo isset($filters['search_txt']) ? $filters['search_txt'] : '' ?>">
            </div>
            <div class="four wide field">
                <?php $filters_type_shopfil = isset($filters['type_shopfil']) ? $filters['type_shopfil'] : '' ?>
                <label>ค้นหาจากประเภทร้าน</label>
                <select class="ui fluid dropdown" name="type_shopfil" id="type_shopfil" >
                    <option value="all" <?php echo $filters_type_shopfil == 'all' ? 'selected' : '' ?>>ทั้งหมด</option>
                    <option value="อาบน้ำ-ตัดขน" <?php echo $filters_type_shopfil == 'อาบน้ำ-ตัดขน' ? 'selected' : '' ?>>อาบน้ำ - ตัดขน</option>
                    <option value="ฉีดวัคซีน" <?php echo $filters_type_shopfil == 'ฉีดวัคซีน' ? 'selected' : '' ?>>ฉีดวัคซีน</option>
                    <option value="อาบน้ำ-ตัดขนและฉีดวัคซีน" <?php echo $filters_type_shopfil == 'อาบน้ำ-ตัดขนและฉีดวัคซีน' ? 'selected' : '' ?>>อาบน้ำ-ตัดขนและฉีดวัคซีน</option>
                </select>
            </div>
            <div class="four wide field">
                <?php $filters_status_openfill = isset($filters['status_openfill']) ? $filters['status_openfill'] : '' ?>
                <label>ค้นหาจากสถานะ</label>
                <select class="ui fluid dropdown" name="status_openfill" id="status_openfill" >
                    <option value="all" <?php echo $filters_status_openfill == 'all' ? 'selected' : '' ?>>ทั้งหมด</option>
                    <option value="เปิด" <?php echo $filters_status_openfill == 'เปิด' ? 'selected' : '' ?>>เปิดให้บริการ</option>
                    <option value="ปิด" <?php echo $filters_status_openfill == 'ปิด' ? 'selected' : '' ?>>ปิดให้บริการ</option>
                </select>
            </div>
        </div>
        <div class="ui unstackable four column grid ">
            <div class="column">
            </div>
            <div class="right floated column">
                <div class="fields">
                    <div class="eight wide field">
                        <button class="ui fluid small blue button btn-search" type="submit" style="border-radius: 30px;">ค้นหา</button>
                    </div>
                    <div class="eight wide field">
                        <button class="ui fluid small red button btn-clear" type="submit" style="border-radius: 30px;">ล้าง</button>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_user">
                    <thead>
                        <tr>
                            <th>รูปภาพ</th>
                            <th>เลขที่</th>
                            <th>ชื่อร้าน</th>
                            <th>ประเภทร้าน</th>
                            <th>สถานะ</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($shops)): ?>
                            <?php foreach ($shops as $key => $shop):?>
                                <tr>
                                    <td class="wide two">
                                        <img class="ui tiny image" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>">
                                    </td>
                                    <td class="wide two"><?php echo $shop->licenesNo ?></td>
                                    <td class="wide two"><?php echo $shop->name_shop ?></td>
                                    <td class="wide two"><?php echo $shop->type_shop; ?></td>
                                    <td class="wide two">
                                        <?php 
                                            $lb_color = "";
                                            $status_txt = "";
                                            if($shop->status_open == 1){
                                                $lb_color = 'green';
                                                $status_txt = "เปิดให้บริการ";
                                            }else if($shop->status_open == 0){
                                                $lb_color = 'red';
                                                $status_txt = "ปิดให้บริการ";
                                            }else{
                                                $lb_color = 'yellow';
                                                $status_txt = "ไม่ระบุ";
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $status_txt ?></a>
                                            
                                    </td>
                                    <td class="wide two">
                                        <i class="large eye outline icon viewshop" data-id="<?php echo $shop->id_shop ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large edit outline icon editshop" data-id="<?php echo $shop->id_shop ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deleteshop" data-id="<?php echo $shop->id_shop ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                    <td class="wide two">
                                        <button class="ui teal button btn-manage_service" type="submit" data-id="<?php echo $shop->id_shop ; ?>">จัดการการให้บริการ</button>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('shop.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('shop.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('shop.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('shop.delete.post');?>"></div>
