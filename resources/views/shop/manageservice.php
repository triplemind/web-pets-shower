<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="receipt icon"></i>
                <div class="content">
                    รายละเอียดการให้บริการ
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>

    <?php 
        $lb_color = "";
        $status_txt = "";
        if($shop->status_open == 1){
            $lb_color = 'green';
            $status_txt = "เปิดให้บริการ";
        }else if($shop->status_open == 0){
            $lb_color = 'red';
            $status_txt = "ปิดให้บริการ";
        }else{
            $lb_color = 'yellow';
            $status_txt = "ไม่ระบุ";
        }
    ?>

	<div class="ui form segment">
        <div class="top right attached large ui label">
            <div class="ui <?php echo $lb_color; ?> empty circular label"></div> <?php echo $status_txt ?>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui single line teal table">
                    <tbody>
                        <tr>
                            <td class="" colspan="4">
                                <img class="ui centered medium image" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">ชื่อร้าน</td>
                            <td class="six wide"><?php echo $shop->name_shop ?></td>
                            <td class="two wide active">ประเภทร้าน</td>
                            <td class="six wide"><?php echo $shop->type_shop ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <br>

        <div class="ui section divider"></div>

        <div class="ui form">
            <div class="fields">
                <div class="twelve wide field">
                    <label>วัคซีน</label>
                    <select class="ui fluid search selection dropdown" name="id_vaccine" id="id_vaccine" multiple="">
                        <?php if($vaccines->count() != 0): ?>
                            <?php foreach ($vaccines as $key => $vaccine): ?>
                                <option value="<?php echo $vaccine->id; ?>"><?php echo $vaccine->vaccine_name." : ".$vaccine->properties." (".$vaccine->vaccine_type.")" ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="four wide field" style="margin-top: 1.7rem;">
                    <button class="ui fluid blue button btn-savevaccine" type="submit">บันทึก</button>
                </div>
            </div>

        </div>

        <div class="ui section divider"></div>
        <br>

		<div class="ui form">
            <div class="three fields">
                <input type="hidden" name="shop_id" id="shop_id" value="<?php echo $shop->id_shop ?>">
                <div class="field">
                    <label>บริการ</label>
                    <select class="ui fluid search selection dropdown" name="id_service" id="id_service" >
                        <option value="PG0001">อาบน้ำ-ตัดขน</option>
                        <!-- <option value="PG0002">โปรโมชันอาบน้ำ-ตัดขน</option> -->
                        <option value="PV0001">วัคซีน</option>
                        <!-- <option value="PV0002">โปรโมชันวัคซีน</option> -->
                    </select>
                </div>
                <div class="field">
                    <label>ช่วงเวลา</label>
                    <select class="ui fluid search selection dropdown" name="booking_time" id="booking_time" >
                        <option selected disabled>เลือกช่วงเวลา</option>
                        <option value="เช้า">เช้า</option>
                        <option value="บ่าย">บ่าย</option>
                    </select>
                </div>
                <div class="field">
                    <label>จำนวนคิว</label>
                    <input type="text" name="Que" id="Que" placeholder="จำนวนคิว">
                </div>
            </div>
            <div class="three fields">
                <div class="field startTime"  style="display: none;">
                    <label>ช่วงเช้า ตั้งแต่เวลา</label>
                    <input type="text" class="form-control js-time-picker" readonly  name="start_time_morning" id="start_time_morning">
                    <div class="docs-picker-container" style="display: block;"></div>
                </div>
                <div class="field startTime" style="display: none;">
                    <label>ถึงเวลา</label>
                    <input type="text" class="form-control js-time-picker" readonly name="end_time_morning" id="end_time_morning">
                    <div class="docs-picker-container" style="display: block;"></div>
                </div>
                <div class="field endTime" style="display: none;">
                    <label>ช่วงบ่าย ตั้งแต่เวลา</label>
                    <input type="text" class="form-control js-time-picker" readonly name="start_time_afternoon" id="start_time_afternoon">
                    <div class="docs-picker-container" style="display: block;"></div>
                </div>
                <div class="field endTime" style="display: none;">
                    <label>ถึงเวลา</label>
                    <input type="text" class="form-control js-time-picker" readonly  name="end_time_afternoon" id="end_time_afternoon">
                    <div class="docs-picker-container" style="display: block;"></div>
                </div>
                <div class="field btn-saveservice" style="margin-top: 1.7rem;display: none;">
                    <button class="ui fluid blue button btn-save" type="submit">บันทึก</button>
                </div>
            </div>
            

		</div>

        <br>
        <br>
        <br>

        <table class="ui teal table" id="TBL_user">
            <thead>
                <tr style="text-align: center;">
                    <th>บริการ</th>
                    <th>ช่วงเวลา</th>
                    <th>จำนวนคิว</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php if(!empty($shop_services)): ?>
                    <?php foreach ($shop_services as $key => $shop_service):?>
                        <tr style="text-align: center;">
                            <td class="wide two">
                                <?php 
                                    $service_txt = "";
                                    if($shop_service->service == "PG0001"){
                                        $service_txt = "อาบน้ำ-ตัดขน";
                                    }else if($shop_service->service == "PG0002"){
                                        $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
                                    }else if($shop_service->service == "PV0001"){
                                        $service_txt = "วัคซีน";
                                    }else if($shop_service->service == "PV0002"){
                                        $service_txt = "โปรโมชันวัคซีน";
                                    }
                                ?>
                                <?php echo $service_txt ?></td>
                            <td class="wide two">
                                <?php 
                                    $lb_color = "";
                                    if($shop_service->time_period == 'เช้า'){
                                        $lb_color = 'blue';
                                    }else if($shop_service->time_period == 'บ่าย'){
                                        $lb_color = 'orange';
                                    }
                                ?>
                                <label class="ui <?php echo $lb_color; ?> label"><?php echo $shop_service->time_period ?></label>
                            </td>
                            <td class="wide two"><?php echo $shop_service->que ?></td>
                            <td class="wide two">
                                <button class="ui red button btn-manage_service" type="submit" data-id="<?php echo $shop_service->id ; ?>">ลบ</button>
                            </td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
            </tbody>
        </table>

        

	</div>

</div>



<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="add_url" data-url="<?php echo \URL::route('shop.manageservice.post'); ?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('shop.ajax_center.post');?>"></div>
<div id='delete-url' data-url="<?php echo \URL::route('shop.deleteservice.post');?>"></div>
<div id="add_vaccineurl" data-url="<?php echo \URL::route('shop.vaccine.post'); ?>"></div>
