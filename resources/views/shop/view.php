<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="receipt icon"></i>
                <div class="content">
                    รายละเอียดร้านค้า
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>

    <?php 
        $lb_color = "";
        $status_txt = "";
        if($shop->status_open == 1){
            $lb_color = 'green';
            $status_txt = "เปิดให้บริการ";
        }else if($shop->status_open == 0){
            $lb_color = 'red';
            $status_txt = "ปิดให้บริการ";
        }else{
            $lb_color = 'yellow';
            $status_txt = "ไม่ระบุ";
        }
    ?>



    <div class="ui form segment">
        <div class="top right attached large ui label">
            <div class="ui <?php echo $lb_color; ?> empty circular label"></div> <?php echo $status_txt ?>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui single line table">
                    <tbody>
                        <tr>
                            <td class="" colspan="4">
                                <img class="ui centered medium image" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">ชื่อร้าน</td>
                            <td class="six wide"><?php echo $shop->name_shop ?></td>
                            <td class="two wide active">ประเภทร้าน</td>
                            <td class="six wide"><?php echo $shop->type_shop ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">Latitude</td>
                            <td class="six wide"><?php echo $shop->shop_lat ?></td>
                            <td class="two wide active">Longitude</td>
                            <td class="six wide"><?php echo $shop->shop_long ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">จำนวนเงินมัดจำ</td>
                            <td class="six wide" colspan="3"><?php echo $shop->deposit ?> บาท</td>
                        </tr>
                        <tr>
                            <td class="two wide active">เลขที่</td>
                            <td class="six wide"><?php echo $shop->licenesNo ?></td>
                            <td class="two wide active">เบอร์โทรศัพท์</td>
                            <td class="six wide"><?php echo $shop->tel_shop ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ที่อยู่</td>
                            <td class="six wide" colspan="3"><?php echo $shop->address_shop ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">จังหวัด</td>
                            <td class="six wide"><?php echo !empty($province) ? $province->PROVINCE_NAME : '-'; ?></td>
                            <td class="two wide active">อำเภอ/เขต</td>
                            <td class="six wide"><?php echo !empty($amphur) ? $amphur->AMPHUR_NAME : '-'; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ตำบล/แขวง</td>
                            <td class="six wide"><?php echo !empty($district) ? $district->DISTRICT_NAME : '-'; ?></td>
                            <td class="two wide active">รหัสไปรษณีย์</td>
                            <td class="six wide"><?php echo $shop->zip ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">รายละเอียดร้านค้า</td>
                            <td class="six wide" colspan="3"><?php echo $shop->shop_description ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    
</div>
