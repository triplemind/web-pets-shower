
<div class="ui stackable" style="padding-bottom: 5rem;
padding-left: 0%; padding-right: 0%; background-color: #FEE265;padding-top: 5rem;text-align: center;">
	<h1>เกี่ยวกับเรา</h1>
</div>


<div class="ui stackable container" style="padding-bottom: 15rem;
padding-left: 0%; padding-right: 0%;">

	<div class="ui segment">
		<p style="text-align: center;font-size: 16px;">เว็บไซต์ Pets Care เป็นเว็บไซต์ตัวกลางที่ช่วยดูแลการนัดหมายให้กับสัตว์เลี้ยง เราเป็นส่วนหนึ่งที่จะทำให้การดูแลสัตว์เลี้ยงที่คุณรักได้อย่างง่ายดายและสะดวกมากขึ้น </p>
	</div>


	<div class="ui styled fluid accordion">
		<div class="active title">
			<i class="dropdown icon"></i>
			<i class="handshake icon"></i> ผู้ประกอบการ
		</div>
		<div class="active content">
			<p>เงื่อนไขผู้ใช้งาน (ผู้ประกอบการ)</p>

			<ul class="ui list">
				<li>ธุรกิจสัตว์เลี้ยงที่รองรับ
					<ul>
						<li>อาบน้ำตัดขน</li>
						<li>คลินิกสัตว์ (ฉีดวัคซีน)</li>
					</ul>
				</li>
				<li>ซึ่งร้านค้าข้างต้นที่อยู่ในกลุ่มบริการหลัก สามารถสมัครและลงประกาศกับทางเราได้เอง</li>
			</ul>

			<ol class="ui suffixed list">
				<li>การสร้างร้านค้า
					<ol>
						<li>ท่านสามารถจะต้องจัดการการนัดหมายภายในร้านของตนเองได้ การรับคิวผ่านบนเว็บไซต์ Pets Care</li>
						<li>บริการในร้านค้าที่ท่านมี เช่น บริการอาบน้ำ-ตัดขน บริการฉีดวัคซีน เป็นต้น
							<ol>
								<li>ในกรณีที่เป็นคลินิกที่จะต้องมีสัตวแพทย์ทำการฉีดวัคซีน จะต้องมีการลงทะเบียนข้อมูลของสัตวแพทย์ </li>
							</ol>
						</li>
					</ol>
				</li>
				<li>การจัดการนัดหมาย
					<ol>
						<li>เมื่อมีลูกค้าสนใจบริการของร้านท่าน ลูกค้าสามารถติดต่อท่านได้ผ่าน 2 ช่องทาง ติดต่อผ่านเว็บไซต์ www.petscarebusiness.com ในกรณีที่ลูกค้าติดต่อผ่านทางเว็บไซต์ เช่น ส่งข้อความ หรือทำรายการจอง จะมีการแจ้งเตือนบนเว็บไซต์เมื่อท่านล็อคอิน และท่านสามารถล็อคอินเข้ามาเพื่อตรวจสอบข้อความหรือรายการจองของลูกค้า เพื่อตอบกลับในขั้นตอนถัดไป</li>
					</ol>
				</li>
				<li>การชำระเงินสมัคร Member Premium 
					<ol>
						<li>หากท่านได้ชำระค่า Member Premium ผ่านการโอนเงินทางธนาคาร ท่านจะต้องติดต่อเราเพื่อแจ้งถึงการโอนเงินดังกล่าว พร้อมส่งหลักฐานการโอนเงิน (เช่น ใบสลิปโอนเงิน) และระบุรหัสการทำรายการ (Order ID) ของท่าน เพื่อที่เราจะสามารถดำเนินการตรวจสอบรายการของท่านได้อย่างทันท่วงที</li>
					</ol>
				</li>
			</ol>

			<p>สำหรับการแจ้งถึงการโอนเงินดังกล่าว ท่านสามารถติดต่อเราได้ เมื่อเราได้รับแจ้งโอน และยืนยันการชำระเงินค่าสินค้าของท่านเป็นที่เรียบร้อยแล้ว จะดำเนินการอนุมัติประกาศให้แสดงร้านค้าแนะนำบนหน้าเว็บไซต์</p>

		</div>
		<div class="title">
			<i class="dropdown icon"></i>
			<i class="user alternate icon"></i> สมาชิกทั่วไป
		</div>
		<div class="content">
			<p>เงื่อนไขผู้ใช้งาน (สมาชิกทั่วไป)</p>

			<ol class="ui suffixed list">
				<li>การนัดหมาย
					<ol>
						<li>ในการนัดหมายบริการให้กับสัตว์เลี้ยง สามารถนัดหมายได้ครั้งละ 1 ตัว และได้กำหนดไว้ในช่องทางการเลือกสัตว์เลี้ยงในการเข้าใช้บริการ 
							<ol>
								<li>ในกรณืที่ผู้ใช้งานเลือกบริการฉีดวัคซีน สามารถเลือกวัคซีนที่ยังไม่ได้ซีนผ่านหน้าข้อมูลสัตว์เลี้ยง เพื่อให้ผู้ใช้งานได้ตรวจสอบการวัคซีนก่อนการนัดหมาย</li>
							</ol>
						</li>
						<li>การนัดหมายจะสำเร็จก็ต่อเมื่อมีการชำระค่ามัดจำในการยืนยันการนัดหมาย </li>
						<li>หลังจากนัดหมายสำเร็จ รอร้านอนุมัติ และเข้ามาใช้บริการได้เลย</li>
					</ol>
				</li>
				<li>การเลื่อนนัดหมาย
					<ol>
						<li>หลังจากชำระเงินมัดจำ ในกรณีที่ผู้ใช้งานไม่สะดวกเข้าใช้บริการ สามารถทำการเลื่อนบริการได้ก่อนถึงวันนัดหมาย</li>
						<li>**ในกรณีเมื่อถึงวันที่นัดหมายผู้ใช้งานจะมาไม่สามารถแก้ไขหรือเลื่อนการนัดหมายบริการได้**</li>
					</ol>
				</li>
				<li>QR Code ประวัติสัตว์เลี้ยง 
					<ol>
						<li>เมื่อท่านมีการลงทะเบียนสัตว์เลี้ยง ก็จะมีการเก็บข้อมูลผ่าน QR Code มีข้อมูลประวัติสัตว์สัตว์เลี้ยง ชื่อ อายุ สายพันธุ์ ชื่อเจ้าของ เบอร์ติดต่อ และประวัติการฉีดวัคซีนที่ได้ทำการดูแลแล้ว</li>
						<li>เป็นส่วนหนึ่งให้การช่วยประกอบการดูแลสัตว์เลี้ยงเพื่อการดูแลรักษาอย่างถูกต้องเท่านั้น ข้อมูลเป็นเพียงข้อความที่เก็บไว้เพื่อแสดงผ่าน QR Code</li>
					</ol>
				</li>
				<li>การชำระเงิน
					<ol>
						<li>สำหรับการชำระค่ามัดจำการโอนเงิน
							<ol>
								<li>หากได้ชำระเงินค่ามัดจำผ่านการโอนเงินทางธนาคารที่เป็นบัญชีของร้านค้าโดยตรง ท่านจะต้องติดต่อกับทางร้านเพื่อแจ้งถึงการโอนเงินดังกล่าว พร้อมหลักฐานการโอนเงิน (เช่น ใบสลิปโอนเงิน) และระบุรหัสการทำรายการ (Order ID) ของท่าน เพื่อที่เราจะสามารถดำเนินการตรวจสอบรายการของท่านได้อย่างทันท่วงที สำหรับการแจ้งถึงการโอนเงินดังกล่าว ท่านสามารถเช็คสถานะการนัดหมาย ผ่านช่องทางการนัดหมายของสัตว์เลี้ยง เมื่อร้านค้าได้รับแจ้งโอน และยืนยันการชำระเงินค่าสินค้าของท่านเป็นที่เรียบร้อยแล้ว ร้านค้าจะเปลี่ยนสถานะการนัดหมาย หรือ ยืนยันการทำรายการของท่าน และดำเนินการอนุมัติประกาศให้แสดงบนหน้าเว็บไซต์เป็นลำดับต่อไป</li>
							</ol>
						</li>
					</ol>
				</li>
			</ol>
		</div>
	</div>

</div>