 <style type="text/css">
	#map {
		height: 100%;
	}
	/* Optional: Makes the sample page fill the window. */
	html, body {
		height: 100%;
		margin: 0;
		padding: 0;
	}
</style>
<h2 class="ui horizontal divider header">
    <!-- <i class="center align icon"></i> -->
    <?php echo $shop->name_shop; ?>
</h2>
<br>

<h3 class="ui horizontal divider header">
    <!-- <i class="center align icon"></i> -->
    <div><span class="ui yellow label">บริการนัดหมาย </span> Queue reservation service</div>
</h3>

<input type="hidden" name="shop_lat" id="shop_lat" placeholder="Longitude" value="<?php echo $shop->shop_lat; ?>">
<input type="hidden" name="shop_long" id="shop_long" placeholder="Longitude" value="<?php echo $shop->shop_long; ?>">
<input type="hidden" name="name_shop" id="name_shop" placeholder="Longitude" value="<?php echo $shop->name_shop; ?>">

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<div class="ui stackable two column grid" style="padding: 15px;">
		<div class="eight wide column">
			<h4>รายละเอียด</h4>
            <img class="ui centered big image" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/trex.jpg' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>">
            <br>
			<div style="word-break: break-all;">
	            <?php echo $shop->shop_description; ?>
	        </div>
        </div>
        <div class="eight wide column">
        	<div class="ui form segment" style="background-color: #d8d8d8;">
        		<h3 class="ui horizontal divider header">
        			<i class="calendar plus icon"></i>
				    บริการการนัดหมาย
				</h3>
				<?php if(!empty($courses)): ?>
					<div class="ui column grid">
			            <div class="column" style="text-align: end;">
			                <button class="ui teal button btn-gotocourse" onclick="window.location.href = '/course/<?php echo $shop->id_shop  ?>';" type="submit">คอร์สประจำร้านค้า</button>
			            </div>
			        </div>
			    <?php endif ?>
				<form class="ui form">
					<input type="hidden" name="id_shop" id="id_shop" value="<?php echo $shop->id_shop  ?>">
					<div class="field">
						<label>วันที่นัดหมาย</label>
						<div class="ui calendar" id="booking_date">
							<div class="ui input left icon">
								<i class="calendar icon"></i>
								<input type="text" placeholder="วันที่นัดหมาย" readonly>
							</div>
						</div>
					</div>
					<div class="field">
						<label>บริการ</label>
						<select class="ui fluid search selection dropdown" name="id_service" id="id_service" >
							<option>เลือกบริการ</option>
	                        <option value="PG0001">อาบน้ำ-ตัดขน</option>
	                        <option value="PV0001">วัคซีน</option>
	                        
	                    </select>
					</div>
					<div class="field vaccine_display" style="display: none;">
						<label>เลือกวัคซีน</label>
						<?php if(!empty($shop->vaccine)): ?>
						<select class="ui fluid search selection dropdown" name="vaccine" id="vaccine" >
							<option value="" selected disabled>กรุณาเลือกวัคซีน</option>
	                    </select>
	                    <?php else: ?>
	                    	<div class="ui red large message">
				  				<p>ไม่พบรายชื่อวัคซีน กรุณาติดต่อทางร้านค้า</p>
							</div>
	                    <?php endif ?>
                    	<a href="/vaccinedetail" target="_blank" style="float: right;margin-bottom: 1rem;margin-top: 1rem;">คลิกเพื่อดูรายละเอียดวัคซีนต่างๆ</a>
					</div>
					<div class="field">
						<label>ช่วงเวลา</label>
						<select class="ui fluid search selection dropdown" name="booking_time" id="booking_time" >
	                        <option>เลือกช่วงเวลา</option>
	                    </select>
					</div>
					<div class="field" style="text-align: end;">
						<a class="ui violet basic tag label" id="que_txt"></a>
					</div>
					<div class="field">
						<label>เลือกสัตว์เลี้ยง</label>
						<?php if($getPets->count() != 0): ?>
						<select class="ui fluid search selection dropdown" name="pet_id" id="pet_id" >
								<?php foreach ($getPets as $key => $getPet): ?>
		                        	<option value="<?php echo $getPet->pet_id; ?>"><?php echo $getPet->name." (".$getPet->type.") "; ?></option>
		                    	<?php endforeach ?>
	                    </select>
	                    <?php else: ?>
	                    	<a href="/mypets"><span class="ui red label">กรุณาเพิ่มสัตว์เลี้ยงของท่านก่อนทำรายการ</span>  คลิกเพื่อเพิ่มสัตว์เลี้ยง</a>
	                    <?php endif ?>
					</div>
					<!-- <div class="field">
						<label>โค้ดส่วนลด</label>
						<input type="text" name="promo_code" id="promo_code" placeholder="">
					</div> -->
					<div class="field">
						<label>หมายเหตุ</label>
						<textarea name="note" id="note" rows="4"></textarea> 
					</div>
					
				</form>

		        <br>
		        <br>

		        <div class="ui column grid">
		            <div class="column">
		                <button class="ui fluid yellow button btn-save" type="submit">บันทึก</button>
		            </div>
		        </div>
			</div>
        </div>
	</div>
</div>

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<div class="ui stackable two column grid" style="padding: 15px;">
        <div class="ten wide column">
        	<div id="map"></div>
        </div>
		<div class="six wide column">
			<h4 style="text-align: end;"><a href="/shopnearbygoogle">ร้านบริเวณใกล้เคียง >> </a></h4>
			<br>
            <?php //if($shops->count() != 0): ?>
				<div class="ui two stackable centered cards"  id="addcard-nearby">
					<?php //foreach ($shops as $key => $shop): ?>
						<!-- <div class="ui centered card">
							<div class="image">
								<img class="ui small image" src="<?php //echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>" style="max-height: 140px;">
							</div>
							<div class="content">
								<a class="header" href="/booking/<?php //echo $shop->id_shop; ?>"><?php //echo $shop->name_shop; ?></a>
							</div>
						</div> -->
					<?php //endforeach ?>
				</div>
			<?php //endif ?>
        </div>
	</div>
</div>

<div id="map1" style="display: none;"></div>

<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGetgbZ37IeA0eTGFVGnq5T2u3wcQsfBM&libraries=places&callback=initMap&language=th&region=TH"
        async defer
    ></script>



<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="add_url" data-url="<?php echo \URL::route('booking.add.post'); ?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('booking.ajax_center.post');?>"></div>

<input type="hidden" name="hasPro" id="hasPro" value="<?php echo !empty($getPromotion) ? 'has' : 'not' ?>">
<input type="hidden" name="premium_shop" id="premium_shop" value="<?php echo ($shop->status_premium == 1) ?  'true' : 'false'; ?>">

<?php if(!empty($getPromotion)): ?>
	<div class="ui large modal">
		<i class="close icon"></i>
		<div class="header">
			<?php echo $getPromotion->title; ?>
		</div>
		<div class="image content">
			<div class="ui big image">
				<img class="ui centered huge image" src="<?php echo empty($getPromotion->img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/trex.jpg' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPromotion->img); ?>">
			</div>
			<div class="description">
				<div class="ui header"><?php echo empty($getPromotion->shop) ? '' : $getPromotion->shop->name_shop; ?></div>
				<p>โปรโมชั่น : <?php echo $getPromotion->service_type; ?></p>
				<p>รับส่วนลด : <?php echo $getPromotion->discount; ?></p>
				<p>กรอกโค้ดส่วนลดนี้เมื่อทำการจอง CODE : <?php echo $getPromotion->code; ?></p>
				<p>รายละเอียด : </p>
				<div><?php echo $getPromotion->description; ?></div>
			</div>
		</div>
		<div class="actions">
			<div class="ui black deny button">
				ปิด
			</div>
			<!-- <div class="ui positive right labeled icon button">
				Yep, that's me
				<i class="checkmark icon"></i>
			</div> -->
		</div>
	</div>
<?php endif ?>
