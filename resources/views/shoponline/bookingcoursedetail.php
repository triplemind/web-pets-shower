<div class="ui basic" style="margin-left: 6rem;margin-right: 6rem;text-align: center;">
	<span style="font-weight: 900;font-size: 60px;color: #5DACBD;">สรุปการจอง</span>
</div>

<div class="ui stackable segment" style="background-color: #ffe597;margin-left: 6rem;margin-right: 6rem;">
	<div class="ui grid segment">
		<div class="four wide column">
			<img class="ui small circular image" src="<?php echo empty($getService->pet) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : (empty($getService->pet->pic) ? url('').'/public/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getService->pet->pic)); ?>" style="max-width: 150px;max-height: 150px">
		</div>
		<div class="four wide column">
			<div style="margin-top: 7rem;"><span style="font-weight: 900;font-size: 45px;color: #565656;"><?php echo empty($getService->pet) ? '-' : $getService->pet->name; ?></span></div>
		</div>
		<div class="eight wide column">
			<!-- <img class="ui small image" src="<?php //echo url('').'/themes/image/QR.png'; ?>"> -->
		</div>
		
		<input type="hidden" placeholder="" name="id" id="id" value="<?php echo $getService->id_petservice; ?>">

		<div class="one wide column" style="margin-top: 1em;"></div>
		<div class="fourteen wide column" style="margin-top: 1em;">
	        <div class="sixteen wide field">
	            <table class="ui table">
	                <tbody>
	                    <tr>
	                        <td class="three wide active">เจ้าของ</td>
	                        <td class="six wide"><?php echo !empty($getService->user) ? $getService->user->name_owner : '-'; ?></td>
	                        <td class="three wide active">ร้านที่ให้บริการ</td>
	                        <td class="six wide"><?php echo !empty($getService->shop) ? $getService->shop->name_shop : '-'; ?></td>
	                    </tr>
	                    <tr>
	                        <td class="three wide active">คอร์ส</td>
	                        <td class="six wide"  colspan="3"><?php echo !empty($getService->course) ? $getService->course->name." ( ราคา ".$getService->course->price." บาท )" : '-'; ?></td>
	                    </tr>
	                    <?php if(!empty($getService->vaccine)): ?>
		                    <tr>
		                        <td class="three wide active">วัคซีน</td>
		                        <td class="six wide"  colspan="3">
		                        	<div >
		                        		<?php echo $getService->vaccine; ?>
		                        	</div>
		                        </td>
		                    </tr>
		                <?php endif ?>
	                    <!-- <tr>
	                        <td class="three wide active">วันที่นัดหมาย</td>
	                        <?php
								$datetoday = DateThai($getService->booking_date, true, false);
							?>
	                        <td class="three wide"><?php echo $datetoday; ?></td>
	                        <td class="three wide active">ช่วงเวลา</td>
	                        <?php 
								$booking_time_txt = "";
								if($getService->booking_time == "บ่าย"){
                                    // $booking_time_txt = "บ่าย 13.30น.-19.00น.";
                                    $booking_time_txt = "บ่าย : ".(empty($getshopservices->endTime) ? "13.30น.-19.00น." : $getshopservices->endTime);
                                }else if($getService->booking_time == "เช้า"){
                                    // $booking_time_txt = "เช้า 09.00น.-12.30น.";
                                    $booking_time_txt = "เช้า : ".(empty($getshopservices->startTime) ? "09.00น.-12.30น." : $getshopservices->startTime);
                                }
							?>
	                        <td class="six wide"><?php echo $booking_time_txt; ?></td>
	                    </tr> -->
	                    <tr>
	                        <!-- <td class="three wide active">คิว</td>
	                        <td class="six wide"><?php echo empty($getService->service_order) ? 'อยู่ระหว่างการตรวจสอบคิว' : $getService->service_order; ?></td> -->
	                        <td class="three wide active">การชำระเงิน</td>
	                        <td class="six wide" colspan="3"><?php echo $price; ?></td>
	                    </tr>
	                    <tr>
	                        <td class="three wide active">หมายเหตุ</td>
	                        <td class="six wide"  colspan="3"><?php echo $getService->note; ?></td>
	                    </tr>
	                    <tr class="pic_payment_F">
                            <td class="three wide active">หลักฐานการชำระเงิน</td>
                            <td class="six wide"  colspan="3">
                                <?php if(empty($getService->pic_payment)): ?>
                                    รอชำระเงิน
                                <?php else: ?>
                                    <span style="font-size: 12px;color: red;">คลิกที่รูปเพื่อดูรูปภาพขนาดใหญ่</span>
                                    <img class="ui small image" style="cursor: pointer;" onclick="ShowimgModal()" src="<?php echo empty($getService->pic_payment) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getService->pic_payment); ?>">
                                            
                                <?php endif ?>
                            </td>
                        </tr>
	                </tbody>
	            </table>
	        </div>
	    </div>
	    <div class="one wide column" style="margin-top: 1em;"></div>

	    <div class="six wide column" style="margin-top: 1em;"></div>
	    <div class="four wide column">
			<img class="ui big image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/pay.png'; ?>">
		</div>
		<div class="six wide column" style="margin-top: 1em;"></div>

		<?php if($getService->status != "ยกเลิกรายการ" && $getService->status != "การใช้บริการเสร็จสิ้น"): ?>
		    <div class="ten wide column" style="margin-top: 1em;"></div>
		    <div class="six wide column" style="margin-top: 1em;text-align: end;">
				<button class="ui red button btn-cancel" type="submit">ยกเลิกรายการ</button>
			</div>
		<?php endif ?>
	</div>

	<input type="hidden" name="check_payment" id="check_payment" value="<?php echo empty($getService->pic_payment) ? 'empty' : 'not empty'; ?>">

	<?php if(empty($getService->pic_payment)): ?>
	    <div class="ui form segment">
	    	<h4>แนบหลักฐานการชำระเงิน</h4>
			<div class="three fields">
				<div class="field">
					<label>วันที่ชำระเงิน</label>
					<div class="ui calendar" id="date_payment">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input type="text" placeholder="วันที่ชำระเงิน" readonly>
						</div>
					</div>
				</div>
				<div class="field">
					<label>เวลาที่ชำระเงิน</label>
					<input type="text" class="form-control js-time-picker" readonly  name="time_payment" id="time_payment">
                    <div class="docs-picker-container" style="display: block;"></div>
					<!-- <div class="ui calendar" id="time_payment">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input type="text" placeholder="เวลาที่ชำระเงิน" readonly>
						</div>
					</div> -->
				</div>
				<div class="field">
					<label>ธนาคาร</label>
					<select class="ui fluid search selection dropdown" name="bank_payment" id="bank_payment" >
						<option value='' selected>--- กรุณาเลือกธนาคาร ---</option>
	                    <option value='ธนาคารกรุงเทพ'>ธนาคารกรุงเทพ</option>
	                    <option value='ธนาคารกสิกรไทย'>ธนาคารกสิกรไทย</option>
	                    <option value='ธนาคารกรุงไทย'>ธนาคารกรุงไทย</option>
	                    <option value='ธนาคารทหารไทย'>ธนาคารทหารไทย</option>
	                    <option value='ธนาคารไทยพาณิชย์'>ธนาคารไทยพาณิชย์</option>
	                    <option value='ธนาคารกรุงศรีอยุธยา'>ธนาคารกรุงศรีอยุธยา</option>
	                    <option value='ธนาคารเกียรตินาคิน'>ธนาคารเกียรตินาคิน</option>
	                    <option value='ธนาคารซีไอเอ็มบีไทย'>ธนาคารซีไอเอ็มบีไทย</option>
	                    <option value='ธนาคารทิสโก้'>ธนาคารทิสโก้</option>
	                    <option value='ธนาคารธนชาต'>ธนาคารธนชาต</option>
	                    <option value='ธนาคารยูโอบี'>ธนาคารยูโอบี</option>
	                    <option value='ธนาคารออมสิน'>ธนาคารออมสิน</option>
	                    <option value='ธนาคารอาคารสงเคราะห์'>ธนาคารอาคารสงเคราะห์</option>
	                    <option value='ธนาคารอิสลามแห่งประเทศไทย'>ธนาคารอิสลามแห่งประเทศไทย</option>
	                </select>
				</div>
			</div>
			<div class="field">
				<label>หลักฐานการชำระเงิน</label>
				<input type="file" placeholder="" name="pic_payment" id="pic_payment">
			</div>
			<div class="ui column grid">
	            <div class="column" style="text-align: end;">
					<button class="ui blue button btn-save-payment" type="submit">ยืนยันและมัดจำ</button>
	            </div>
	        </div>
		</div>
	<?php else: ?>
		<div class="ui form segment">
	    	<h4>แก้ไขหลักฐานการชำระเงิน</h4>
			<div class="three fields">
				<div class="field">
					<label>วันที่ชำระเงิน</label>
					<div class="ui calendar" id="ed_date_payment">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input type="text" placeholder="วันที่ชำระเงิน" readonly value="<?php echo $getService->date_payment; ?>">
						</div>
					</div>
				</div>
				<div class="field">
					<label>เวลาที่ชำระเงิน</label>
					<input type="text" class="form-control js-time-picker" readonly  name="ed_time_payment" id="ed_time_payment" value="<?php echo $getService->time_payment; ?>">
                    <div class="docs-picker-container" style="display: block;"></div>
					<!-- <div class="ui calendar" id="ed_time_payment">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input type="text" placeholder="เวลาที่ชำระเงิน" readonly value="<?php //echo $getService->time_payment; ?>">
						</div>
					</div> -->
				</div>
				<div class="field">
					<label>ธนาคาร</label>
					<select class="ui fluid search selection dropdown" name="ed_bank_payment" id="ed_bank_payment" >
						<option value='' selected>--- กรุณาเลือกธนาคาร ---</option>
	                    <option value='ธนาคารกรุงเทพ' <?php echo ($getService->bank_payment == 'ธนาคารกรุงเทพ') ? 'selected' : ''; ?> >ธนาคารกรุงเทพ</option>
	                    <option value='ธนาคารกสิกรไทย' <?php echo ($getService->bank_payment == 'ธนาคารกสิกรไทย') ? 'selected' : ''; ?> >ธนาคารกสิกรไทย</option>
	                    <option value='ธนาคารกรุงไทย' <?php echo ($getService->bank_payment == 'ธนาคารกรุงไทย') ? 'selected' : ''; ?> >ธนาคารกรุงไทย</option>
	                    <option value='ธนาคารทหารไทย' <?php echo ($getService->bank_payment == 'ธนาคารทหารไทย') ? 'selected' : ''; ?> >ธนาคารทหารไทย</option>
	                    <option value='ธนาคารไทยพาณิชย์' <?php echo ($getService->bank_payment == 'ธนาคารไทยพาณิชย์') ? 'selected' : ''; ?> >ธนาคารไทยพาณิชย์</option>
	                    <option value='ธนาคารกรุงศรีอยุธยา' <?php echo ($getService->bank_payment == 'ธนาคารกรุงศรีอยุธยา') ? 'selected' : ''; ?> >ธนาคารกรุงศรีอยุธยา</option>
	                    <option value='ธนาคารเกียรตินาคิน' <?php echo ($getService->bank_payment == 'ธนาคารเกียรตินาคิน') ? 'selected' : ''; ?> >ธนาคารเกียรตินาคิน</option>
	                    <option value='ธนาคารซีไอเอ็มบีไทย' <?php echo ($getService->bank_payment == 'ธนาคารซีไอเอ็มบีไทย') ? 'selected' : ''; ?> >ธนาคารซีไอเอ็มบีไทย</option>
	                    <option value='ธนาคารทิสโก้' <?php echo ($getService->bank_payment == 'ธนาคารทิสโก้') ? 'selected' : ''; ?> >ธนาคารทิสโก้</option>
	                    <option value='ธนาคารธนชาต' <?php echo ($getService->bank_payment == 'ธนาคารธนชาต') ? 'selected' : ''; ?> >ธนาคารธนชาต</option>
	                    <option value='ธนาคารยูโอบี' <?php echo ($getService->bank_payment == 'ธนาคารยูโอบี') ? 'selected' : ''; ?> >ธนาคารยูโอบี</option>
	                    <option value='ธนาคารออมสิน' <?php echo ($getService->bank_payment == 'ธนาคารออมสิน') ? 'selected' : ''; ?> >ธนาคารออมสิน</option>
	                    <option value='ธนาคารอาคารสงเคราะห์' <?php echo ($getService->bank_payment == 'ธนาคารอาคารสงเคราะห์') ? 'selected' : ''; ?> >ธนาคารอาคารสงเคราะห์</option>
	                    <option value='ธนาคารอิสลามแห่งประเทศไทย' <?php echo ($getService->bank_payment == 'ธนาคารอิสลามแห่งประเทศไทย') ? 'selected' : ''; ?> >ธนาคารอิสลามแห่งประเทศไทย</option>
	                </select>
				</div>
			</div>
			<div class="field">
				<label>หลักฐานการชำระเงิน</label>
				<input type="file" placeholder="" name="ed_pic_payment" id="ed_pic_payment">
			</div>
			<div class="ui column grid">
	            <div class="column" style="text-align: end;">
					<button class="ui blue button btn-edit-payment" type="submit">บันทึก</button>
	            </div>
	        </div>
		</div>
	<?php endif ?>

</div>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='addslip-url' data-url="<?php echo \URL::route('booking.addslip.post');?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('dashboard.ajax_center.post');?>"></div>
<div id='editslip-url' data-url="<?php echo \URL::route('dashboard.addslip.post');?>"></div>

<div class="ui basic modal">
    <div class="image content">
        <img class="image" src="<?php echo empty($getService->pic_payment) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public","/public", $getService->pic_payment); ?>">
    </div>
    <div class="actions">
        <div class="ui red basic cancel inverted button">
            <i class="remove icon"></i>
            ปิด
        </div>
  </div>
</div>