<!-- <h2 class="ui horizontal divider header">
  ตารางนัดหมาย
</h2>
<br> -->
<div class="ui stackable container" style="margin-top: -5rem;padding-bottom: 15rem;
padding-left: 0%; padding-right: 0%;">
	<div class="ui grid">
		<div class="sixteen wide column">
			<div style="margin-top: 5rem;"><span style="font-weight: 900;font-size: 28px;color: #565656;">ตารางรายการจอง</span></div>
		</div>
		<div class="sixteen wide column">
			<table class="ui teal table">
				<thead>
					<tr>
						<th>วันที่นัด</th>
						<th>ช่วงเวลา</th>
						<th>สัตว์เลี้ยง</th>
						<th>ร้านค้า</th>
						<th>บริการ</th>
						<th>สถานะ</th>
						<th>สถานะการเลื่อนนัด</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php if($getServices->count() != 0): ?>
						<?php foreach ($getServices as $key => $getService): ?>
							<tr>
								<td>
									<?php
										// $datetoday = date('Y-m-d H:i:s');
        								$datetoday = DateThai($getService->booking_date, true, false);
									?>
									<?php echo $datetoday ?>
										
								</td>
								<td>
									<?php 
										$booking_time_txt = "";
										if($getService->booking_time == "บ่าย"){
		                                    // $booking_time_txt = "บ่าย 13.30น.-19.00น.";
		                                    $booking_time_txt = "บ่าย ";
		                                }else if($getService->booking_time == "เช้า"){
		                                    // $booking_time_txt = "เช้า 09.00น.-12.30น.";
		                                    $booking_time_txt = "เช้า";
		                                }

									?>
									<?php echo $booking_time_txt; ?>
								</td>
								<td><?php echo empty($getService->pet) ? '' : $getService->pet->name ?></td>
								<td><?php echo empty($getService->shop) ? '' : $getService->shop->name_shop ?></td>
								<td>
									<?php 
										$service_txt = "";
										if($getService->id_service == "PG0001"){
											$service_txt = "อาบน้ำ-ตัดขน";
										}else if($getService->id_service == "PG0002"){
											$service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
										}else if($getService->id_service == "PV0001"){
											$service_txt = "วัคซีน";
										}else if($getService->id_service == "PV0002"){
											$service_txt = "โปรโมชันวัคซีน";
										}
									?>
									<?php echo $service_txt; ?>
								</td>
								<td>
									<?php 
										$service_statustxt = "";
                                        if($getService->status == "รอชำระเงิน"){
                                            $service_statustxt = "red";
                                        }else if($getService->status == "รอการตรวจสอบยอดเงิน"){
                                            $service_statustxt = "orange";
                                        }else if($getService->status == "การใช้บริการเสร็จสิ้น"){
                                            $service_statustxt = "green";
                                        }else if($getService->status == "ชำระเงินแล้ว"){ 
                                            $service_statustxt = "blue";
                                        }
									?>
									
									<span class="ui <?php echo $service_statustxt ?> label"><?php echo $getService->status ?></span>
									
								</td>
								<td>
									<?php 
										$postponeAdate_statustxt = "";
                                        if($getService->postponeAdate_status == "รอตรวจสอบ"){
                                            $postponeAdate_statustxt = "blue";
                                        }else if($getService->postponeAdate_status == "ไม่สามารถเลื่อนนัดได้"){
                                            $postponeAdate_statustxt = "red";
                                        }else if($getService->postponeAdate_status == "เลื่อนนัดสำเร็จ"){
                                            $postponeAdate_statustxt = "yellow";
                                        }
									?>
									
									<?php if($getService->postponeAdate == 1): ?>
										<span class="ui <?php echo $postponeAdate_statustxt ?> label"><?php echo $getService->postponeAdate_status ?></span>
									<?php else: ?>
										-
									<?php endif ?>
								</td>
								<td>
									<button class="ui teal button btn-view" onclick="window.location.href = '<?php echo $getService->iscourse == true ? '/bookingcoursedetail/'.$getService->id_petservice : '/bookingdetail/'.$getService->id_petservice; ?>'" type="submit">ดูรายละเอียด</button>
									<?php if(empty($getService->booking_time)): ?>
										<input type="hidden" name="id_petservice" id="id_petservice" value="<?php echo $getService->id_petservice; ?>">
										<input type="hidden" name="id_shop" id="id_shop" value="<?php echo $getService->id_shop; ?>">
										<input type="hidden" name="id_service" id="id_service" value="<?php echo $getService->id_service; ?>">
										<input type="hidden" name="booking_date" id="booking_date" value="<?php echo $getService->booking_date; ?>">
										<button class="ui orange button btn-book-time" onclick="addBookingTime(<?php echo "'".$getService->booking_date."', '".$getService->id_service."', ".$getService->id_shop.", ".$getService->id_petservice; ?>)" type="submit">เลือกเวลานัดหมาย</button>
									<?php endif ?>
									<?php if(!empty($getService->booking_time) && $getService->status !== "รอชำระเงิน"): ?>
										<button class="ui red button btn-book-time" onclick="window.location.href = '<?php echo '/postponeAdate/'.$getService->id_service.'/'.$getService->pet_id.'/'.$getService->id_shop.'/'.$getService->id_petservice; ?>'" type="submit">เลื่อนนัด</button>
									<?php endif ?>
								</td>
							</tr>
						<?php endforeach ?>
					<?php endif ?>
				</tbody>
			</table>
		</div>
	</div>
</div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('booking.ajax_center.post');?>"></div>


<div class="ui tiny modal">
	<div class="header">เลือกเวลานัดหมาย</div>
	<div class="content">
		<div class="ui form sixteen wide column">
			<div class="field">
                <label>ช่วงเวลา</label>
                <select class="ui fluid search selection dropdown" name="booking_time" id="booking_time" >
                    <!-- <option>เลือกช่วงเวลา</option> -->
                    <!-- <option value="เช้า">เช้า 09.00น.-12.30น.</option>
                    <option value="บ่าย">บ่าย 13.30น.-19.00น.</option> -->
                </select>
            </div>
		</div>

		<br>
		<br>

		<div class="field" style="text-align: end;">
            <a class="ui violet basic tag label" id="que_txt"></a>
        </div>
	</div>
	<div class="actions">
		<div class="ui green approve button">บันทึก</div>
		<div class="ui red cancel button">ยกเลิก</div>
	</div>
</div>