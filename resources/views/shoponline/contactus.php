<div class="ui stackable" style="margin-top: -5rem;padding-bottom: 5rem;
padding-left: 0%; padding-right: 0%; text-align: center;">
	<h1>ติดต่อเรา</h1>
</div>


<div class="ui stackable container" style="padding-bottom: 5rem;
padding-left: 0%; padding-right: 0%;">
	<!-- <img class="ui fluid image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/map.jpg'; ?>" height="350px"> -->

	<div class="ui stackable three column grid">
		<div class="column" style="text-align: center;">
			<i class="big map marker icon"></i>
			<h3>ที่อยู่</h3>
			<p>เลขที่ 80 ถนนป๊อปปูล่า ต.บ้านใหม่</p>
			<p>อำเภอปากเกร็ด จังหวัดนนทบุรี 11120</p>
		</div>
		<div class="column" style="text-align: center;">
			<i class="big phone alternate icon"></i>
			<h3>เบอร์โทรศัพท์</h3>
			<p>097-261-1125</p>
			<p>096-393-9688</p>
		</div>
		<div class="column" style="text-align: center;">
			<i class="big envelope icon"></i>
			<h3>อีเมล์</h3>
			<p>Petscare406@gmail.com</p>
		</div>
	</div>

	<br>
	<br>

	<div class="ui horizontal divider"> <h2>ส่งข้อความถึงเรา</h2>  </div>

	<div class="ui form">
		<div class="two fields">
			<div class="field">
				<label>ชื่อ-สกุล ผู้ส่ง</label>
				<input type="text" name="name" id="name" placeholder="ชื่อ-สกุล ผู้ส่ง">
			</div>
			<div class="field">
				<label>อีเมล์</label>
				<input type="text" name="email" id="email" placeholder="อีเมล์">
			</div>
		</div>
		<div class="field">
			<label>หัวข้อ</label>
			<input type="text" name="title" id="title" placeholder="หัวข้อ">
		</div>
		<div class="field">
			<label>ข้อความ</label>
			<textarea rows="6" name="textmsg" id="textmsg"></textarea>
		</div>
		
		<button class="ui button" type="submit" onclick="getFormToSendMail()" style="background-color:  #FEE265;">ส่งข้อความ</button>
	</div>
</div>


<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="sendmail" data-url="<?php echo \URL::route('shoponline.postContactForm.post'); ?>"></div>