<h1 class="ui horizontal divider header">
    <i class="syringe icon"></i>
    คอร์สสำหรับร้าน <?php echo $shop->name_shop ?>
</h1>
<br>

<div class="ui stackable container "  style="padding-left: 0%; padding-right: 0%;background-color: #ffffff61;">
	<?php if($courses->count() !== 0): ?>
		<div class="ui fluid styled accordion" id="coursedetail">
			<?php foreach ($courses as $key => $course): ?>
				<div class="title">
					<div class="ui two column grid" style="padding: 15px;">
						<div class="eight wide column">
							<i class="dropdown icon"></i>
							<?php echo $course->name."  ราคา : ".$course->price." บาท"; ?>
						</div>
						<div class="eight wide column">
							<button class="ui right floated blue button btn-clickcourse" data-price="<?php echo $course->price; ?>" data-id="<?php echo $course->id; ?>" data-numoftimes="<?php echo $course->numoftimes; ?>" data-name="<?php echo $course->name; ?>" data-name_shop="<?php echo $shop->name_shop; ?>" data-shopid="<?php echo $course->shop_id; ?>">ซื้อ</button>
						</div>
					</div>
				</div>
				<div class="content">
					<p class="transition hidden">จำนวนการใช้งาน : <?php echo $course->numoftimes; ?></p>
					<p class="transition hidden">รายละเอียด : </p>
					<div><?php echo $course->description; ?></div>
				</div>
			<?php endforeach ?>
		</div>
	<?php endif ?>
	<?php if($courses->count() == 0): ?>
		<div class="ui red large message">
  			<p>ไม่พบข้อมูลที่คุณค้นหา</p>
		</div>
	<?php endif ?>
</div>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id="add_url" data-url="<?php echo \URL::route('booking.add.post'); ?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('booking.ajax_center.post');?>"></div>


<div class="ui small modal">
		<i class="close icon"></i>
		<div class="header" id="courseName">
			ชื่อ Course
		</div>
		<div class="image content">
			<input type="hidden" name="courseID" id="courseID" value="">
			<input type="hidden" name="shopID" id="shopID" value="">
			<div class="description">
				<div class="ui header" id="shopName">ชื่อร้านค้า</div>
				<p>จำนวนการใช้งาน : <span id="md_numoftimes"></span> ครั้ง</p>
				<p>ราคา : <span id="md_price"></span> บาท</p>
				<div class="ui form">
					<div class="field">
						<label>เลือกสัตว์เลี้ยง</label>
						<?php if($getPets->count() != 0): ?>
						<select class="ui fluid search selection dropdown" name="pet_id" id="pet_id" >
								<?php foreach ($getPets as $key => $getPet): ?>
		                        	<option value="<?php echo $getPet->pet_id; ?>"><?php echo $getPet->name." (".$getPet->type.") "; ?></option>
		                    	<?php endforeach ?>
	                    </select>
	                    <?php else: ?>
	                    	<a href="/mypets"><span class="ui red label">กรุณาเพิ่มสัตว์เลี้ยงของท่านก่อนทำรายการ</span>  คลิกเพื่อเพิ่มสัตว์เลี้ยง</a>
	                    <?php endif ?>
					</div>
					<div class="two fields">
						<div class=" fourteen wide field">
							<label>โค้ดส่วนลด</label>
							<input type="text" name="promo_code" id="promo_code" placeholder="">
						</div>
						<div class="two wide field">
							<button class="ui right floated blue button btn-apply-code" style="margin-top: 1.6rem;">apply</button>
						</div>
					</div>
					<div class="field thismsgcheckpro" style="display: none;">
						<div class="ui blue message">
							<div class="header" id="msgcheckpro"></div>
						</div>
					</div>
					
					<hr>

					<div class="sixteen wide field" style="text-align: right;">
						<div class="ui orange button btn-check-time">
							นัดหมายครั้งแรก
						</div>
					</div>
				</div>
				<div class="bookingdate" style="display: none;">
					<div class="ui form">
						<div class="two fields">
							<div class="field">
								<label>วันที่นัดหมาย</label>
								<div class="ui calendar" id="booking_date">
									<div class="ui input left icon">
										<i class="calendar icon"></i>
										<input type="text" placeholder="วันที่นัดหมาย" readonly>
									</div>
								</div>
							</div>
							<div class="field">
								<label>ช่วงเวลา</label>
				                <select class="ui fluid search selection dropdown" name="booking_time" id="booking_time" >
				                   
				                </select>
							</div>
						</div>
					</div>

					<br>
					<br>

					<div class="field" style="text-align: end;">
			            <a class="ui violet basic tag label" id="que_txt"></a>
			        </div>
				</div>
			</div>
		</div>
		<div class="actions">
			<!-- <div class="ui orange button btn-check-time">
				นัดหมายครั้งแรก
			</div> -->
			<div class="ui deny button">
				ปิด
			</div>
			<div class="ui green approve button">
				ซื้อ
			</div>
		</div>
	</div>