<style type="text/css">
	.slick-track{
	    padding-bottom: 2rem;
	}
</style>

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;margin-bottom: 5rem;">
	<div class="ui grid">
		<div class="three column row">
			<div class="column">
				<a href="/search/อาบน้ำ">
					<img class="ui right floated tiny image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/Group 13.jpg"; ?>">
				</a>
			</div>
			<div class="column">
				<a href="/search/อาบน้ำ"><img class="ui centered tiny image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/Group 12.jpg"; ?>"></a>
			</div>
			<div class="column">
				<a href="/search/ฉีดวัคซีน"><img class="ui left floated tiny image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/Group 14.jpg"; ?>"></a>
			</div>
		</div>
	</div>
	<!-- <div class="ui centered tiny images" style="text-align: center;">
		<a href="/search/อาบน้ำ" style="margin: 0rem 4rem 0rem 0rem;">
			<img class="ui image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/Group 1.png"; ?>">
			xxxxxxxxxx
		</a>
		<a href="/search/อาบน้ำ" style="margin: 0rem 4rem 0rem 0rem;"><img class="ui image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/Group 2.png"; ?>"></a>
		<a href="/search/ฉีดวัคซีน"><img class="ui image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '')."/themes/image/Group 3.png"; ?>"></a>
	</div> -->
</div>


<div class="ui container basic" style="padding-left: 0%; padding-right: 0%;">
	<div class="ui grid" style="margin-bottom: 1rem;">
		<div class="four column row">
			<div class="eight wide left floated column">
				<h2 class="ui horizontal header">
				    <span style="background-color: #FEE265;padding: 4px;">ร้านแนะนำ</span>
				</h2>
			</div>
			<div class="eight wide right floated column" style="text-align: end;">
				<a href="/shopnearby">
				    ทั้งหมด >>>
				</a>
			</div>
		</div>
	</div>
	<div class="slider-nav">
		<?php if($shops->count() != 0): ?>
			<?php foreach ($shops as $key => $shop): ?>
				<div>
					<div class="ui centered card">
						<div class="image">
							<img class="ui huge image" style="max-height: 165px;max-width: 290px;" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>">
						</div>
						<div class="content">
							<a class="header" href="/booking/<?php echo $shop->id_shop; ?>"><?php echo $shop->name_shop; ?></a>
						</div>
					</div>
				</div>
			<?php endforeach ?>
		<?php endif ?>
    </div>
</div>


<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<div class="ui grid" style="margin-bottom: 1rem;">
		<div class="four column row">
			<div class="sixteen wide left floated column">
				<h2 class="ui horizontal header">
				    <span style="background-color: #FEE265;padding: 4px;">ร้านใกล้เคียง (ยังไม่เข้าร่วมแพลตฟอร์ม)</span>
				</h2>
			</div>
			<div class="eight wide right floated column" style="text-align: end;">
				<a href="/shopnearbygoogle"> 
				    ทั้งหมด >>>
				</a>
			</div>
		</div>
	</div>
	<div class="">
		<?php //if($shops->count() != 0): ?>
			<?php //foreach ($shops as $key => $shop): ?>
				<div class="ui four stackable centered cards" id="addcard-nearby">
					<!-- <div class="ui centered card">
						<div class="image">
							<img class="ui huge image" src="<?php //echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>" style="max-height: 165px;max-width: 290px;">
						</div>
						<div class="content">
							<a class="header" href="/booking/<?php //echo $shop->id_shop; ?>"><?php //echo $shop->name_shop; ?></a>
						</div>
					</div> -->
				</div>
			<?php //endforeach ?>
		<?php //endif ?>
    </div>
</div>

<div id="map" style="display: none;"></div>


<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGetgbZ37IeA0eTGFVGnq5T2u3wcQsfBM&libraries=places&callback=initMap&language=th&region=TH"
        async defer
    ></script>