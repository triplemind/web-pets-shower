<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PETs CARE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.css">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/css/dataTables.semanticui.min.css">
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/slick-1.8.1/slick/slick-theme.css"/>
    <link  href="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/pickerjs/dist/picker.css" rel="stylesheet">
    

    <?php if (file_exists(base_path().'/public/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo (env('APP_ENV') == 'production' ? '/public' : '').'/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>
</head>

<style type="text/css">
    .body-background-image{
        position: absolute;
        z-index: -1;
        width: 100%;
        height: 100%;
        /*background-image: url('http://127.0.0.1:8001/public/themes/image/bg.png');*/
        /*background-size: cover;*/
        /*opacity: 0.7;*/
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
    }

    /*footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        background-color: red;
        color: white;
        text-align: center;
    }*/
</style>

<!-- background-color: #b8c6db;
background-image: linear-gradient(315deg, #b8c6db 0%, #f5f7fa 74%); -->
<!-- background-color: #5de6de;background-image: linear-gradient(315deg, #5de6de 0%, #b58ecc 74%); -->

<!-- background-color: #2a2a72;
background-image: linear-gradient(315deg, #2a2a72 0%, #009ffd 74%); -->

<body style="background-color: #fff;background-image: linear-gradient(315deg, #fff 0%, #fff 74%);background-attachment: fixed;">

    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/current-device.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/semantic/semantic.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/calendarTH.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/moment-with-locales.js"></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/list/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/interaction/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-common/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/sweetalert2.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/lodash.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/jquery.dataTables.min.js'></script>
    <script src='<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/dataTables.semanticui.min.js'></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-route.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/angular-animate.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/cleave.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/js/cleave/addons/cleave-phone.th.js"></script>
    <script type="text/javascript" src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/slick-1.8.1/slick/slick.min.js"></script>
    <script src="<?php echo env('APP_ENV') == 'production' ? '/public' : '';  ?>/themes/pickerjs/dist/picker.js"></script>


    <?php if (file_exists(base_path().'/public/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo (env('APP_ENV') == 'production' ? '/public' : '').'/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>
       
        <?php //sd(helperGetModule().'/'.helperGetAction()); ?>
    <?php endif ?>


    <div class="ui" id="example1">
        <!-- <?php //if(helperGetAction() == 'index'): ?>
        <div class="body-background-image"></div>
        <?php //endif ?> -->

        <div class="ui grid" style="">
            <!-- mypet -->
            <?php if(helperGetAction() !== ''): ?>
                <div class="computer only row">
                    <div class="column">
                        <div class="ui sticky">
                            <div class="ui large menu" style="<?php echo (helperGetAction() == 'index') ? 'background-color: #ffffff2e;' : 'background-color: #FEE265;' ?>">
                                <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                    <img class="ui mini image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png'; ?>">
                                </a>
                                <div class="ui dropdown <?php echo (helperGetAction() == 'search') ? 'active' : '' ?> item" id="drop_search">
                                    บริการของเรา <i class="dropdown icon"></i>
                                    <div class="menu">
                                        <a class="item" href="/search/อาบน้ำ">อาบน้ำ - ตัดขน</a>
                                        <a class="item" href="/search/ฉีดวัคซีน">ฉีดวัคซีน</a>
                                    </div>
                                </div>
                                <a class="<?php echo (helperGetAction() == 'shopnearby') ? 'active' : '' ?> item" href="/shopnearby">
                                    ร้านค้า
                                </a>
                                <a class="<?php echo (helperGetAction() == 'vaccinedetail') ? 'active' : '' ?> item" href="/vaccinedetail">
                                    ข้อมูลวัคซีน
                                </a>
                                <a class="<?php echo (helperGetAction() == 'contactus') ? 'active' : '' ?> item" href="/contactus">
                                    ติดต่อเรา
                                </a>
                                <a class="<?php echo (helperGetAction() == 'aboutus') ? 'active' : '' ?> item" href="/aboutus">
                                    เกี่ยวกับเรา
                                </a>
                                <div class="right menu">
                                    <?php if(empty($user_type)): ?>
                                        <div class="ui dropdown item" id="drop_login">
                                            ลงชื่อเข้าใช้ <i class="dropdown icon"></i>
                                            <div class="menu">
                                                <a class="item" href="/auth/login">สมาชิกทั่วไป</a>
                                                <a class="item" href="/auth/login">เจ้าของกิจการ</a>
                                            </div>
                                        </div>
                                    <?php else: ?>
                                        <?php //$notiServices = \Session::has('noti') ? \Session::get('noti') : []; ?>
                                        <div class="ui pointing dropdown link item" id="drop_history">
                                            <i class="bell outline icon"></i>
                                            <div class="ui red label"  id="total_cart"><?php echo $count_all ?></div>
                                            <!-- <i class="dropdown icon"></i> -->
                                            <div class="menu">
                                                <?php if(isset($notiServices)): ?>
                                                    <?php foreach ($notiServices as $key => $notiService): ?>
                                                        <div class="item" onclick="window.location.href='/bookingdetail/<?php echo $notiService['id']; ?>'">
                                                            <img class="ui avatar image" src="<?php echo empty($notiService['img']) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $notiService['img']); ?>">
                                                            <?php echo $notiService['name'] ?>
                                                            <span><p style="margin-top: 1rem;font-size: 10px;"><?php echo $notiService['service']. " คิวที่ ".$notiService['que'] ?></p>
                                                            <p style="margin-top: 1rem;font-size: 10px;"><?php echo "วันที่ ".DateThai($notiService['booking_date'], true, false)." เวลา ".$notiService['time_serv'] ?></p></span>
                                                        </div>
                                                        <div class="divider"></div>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="ui dropdown item" id="drop_Profile">
                                            <img class="ui mini circular image" src="<?php echo empty($user_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $user_img); ?>">
                                                <span class="ui small white text" style="margin-left: 1em"><?php echo (empty($name_owner))  ? '' : $name_owner; ?></span> <i class="dropdown icon"></i>
                                            <div class="menu">
                                                <a class="item" href="/profile">โปรไฟล์</a>
                                                <a class="item" href="/mypets">My Pets</a>
                                                <?php if($user_type == "ผู้ดูแลระบบ" || $user_type == "Root" || $user_type == "เจ้าของกิจการ"): ?>
                                                    <a class="item" href="/admin/shop">จัดการร้านค้า</a>
                                                <?php endif ?>
                                                <a class="item" href="/bookinghistory">ตารางนัดหมาย</a>
                                                <a class="item btn-logout">ออกจากระบบ</a>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tablet mobile only row">
                    <div class="column">
                        <div class="ui top fixed small menu" style="<?php echo (helperGetAction() == 'index') ? 'background-color: #ffffff2e;' : 'background-color: #FEE265;' ?>">
                            <div class="menu">
                                <div class="ui simple dropdown item" id="drop_mobile">
                                    <i class="sidebar icon"></i>
                                    <div class="menu">
                                        <a class="item" href="/">หน้าหลัก</a>
                                        <a class="item" href="/search/อาบน้ำ">อาบน้ำ - ตัดขน</a>
                                        <a class="item" href="/search/ฉีดวัคซีน">ฉีดวัคซีน</a>
                                        <a class="<?php echo (helperGetAction() == 'shopnearby') ? 'active' : '' ?> item" href="/shopnearby">
                                            ร้านค้า
                                        </a>
                                        <a class="<?php echo (helperGetAction() == 'vaccinedetail') ? 'active' : '' ?> item" href="/vaccinedetail">
                                            ข้อมูลวัคซีน
                                        </a>
                                        <a class="<?php echo (helperGetAction() == 'contactus') ? 'active' : '' ?> item" href="/contactus">
                                            ติดต่อเรา
                                        </a>
                                        <a class="<?php echo (helperGetAction() == 'aboutus') ? 'active' : '' ?> item" href="/aboutus">
                                            เกี่ยวกับเรา
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <!-- <div class="center menu">
                                <a class="<?php //echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                    <img class="ui mini image" src="<?php //echo url('').'/public/themes/image/logo.png'; ?>">
                                </a>
                            </div> -->
                            <div class="right menu">
                                <?php if(!empty($user_type)): ?>
                                    <?php //$notiServices = \Session::has('noti') ? \Session::get('noti') : []; ?>
                                    <div class="ui pointing dropdown link item" id="drop_history_mobile">
                                        <i class="bell outline icon"></i>
                                        <div class="ui red label"  id="total_cart"><?php echo $count_all ?></div>
                                        <!-- <i class="dropdown icon"></i> -->
                                        <div class="menu">
                                            <?php if(isset($notiServices)): ?>
                                                <?php foreach ($notiServices as $key => $notiService): ?>
                                                    <div class="item" onclick="window.location.href='/bookingdetail/<?php echo $notiService['id']; ?>'">
                                                        <img class="ui avatar image" src="<?php echo empty($notiService['img']) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $notiService['img']); ?>">
                                                        <?php echo $notiService['name'] ?>
                                                        <span><p style="margin-top: 1rem;font-size: 10px;"><?php echo $notiService['service']. " คิวที่ ".$notiService['que'] ?></p>
                                                        <p style="margin-top: 1rem;font-size: 10px;"><?php echo "วันที่ ".DateThai($notiService['booking_date'], true, false)." เวลา ".$notiService['time_serv'] ?></p></span>
                                                    </div>
                                                    <div class="divider"></div>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                <?php endif ?>
                                <?php if(empty($user_type)): ?>
                                    <div class="ui simple dropdown item" id="drop_user_mobile">
                                        <i class="user icon"></i>ลงชื่อเข้าใช้
                                        <div class="menu">
                                            <a class="item" href="/auth/login">สมาชิกทั่วไป</a>
                                            <a class="item" href="/auth/login">เจ้าของกิจการ</a>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="ui simple dropdown item" id="drop_Profile_mobile">
                                        <img class="ui mini circular image" src="<?php echo empty($user_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $user_img); ?>">
                                            <span class="ui small white text" style="margin-left: 1em"><?php echo (empty($name_owner))  ? '' : $name_owner; ?></span> <i class="dropdown icon"></i>
                                        <div class="menu">
                                            <a class="item" href="/profile">โปรไฟล์</a>
                                            <a class="item" href="/mypets">My Pets</a>
                                            <?php if($user_type == "ผู้ดูแลระบบ" || $user_type == "Root" || $user_type == "เจ้าของกิจการ"): ?>
                                                <a class="item" href="/admin/shop">จัดการร้านค้า</a>
                                            <?php endif ?>
                                            <a class="item" href="/bookinghistory">ตารางนัดหมาย</a>
                                            <a class="item btn-logout">ออกจากระบบ</a>
                                        </div>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
            <?php if(helperGetAction() == 'index' || helperGetAction() == 'contactus'): ?>
                <!-- <div style="margin-top: 26em;"></div> -->
                <div class="ui grid" style="z-index: -9999;">
                    <div class="computer only row">
                        <div class="column">
                            <div style="margin-top: -6rem;">
                                <img class="ui centered image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/sd.jpg'; ?>">
                            </div>
                            <p class="txt" style="margin-top: -38rem;position: absolute;font-size: 50px;margin-left: 5rem;width: 650px;"></p>
                        </div>
                    </div>

                    <div class="tablet mobile only row">
                        <div class="column" style="padding-left: 0rem;padding-right: 0rem;">
                            <div style="margin-top: -1rem;">
                                <img class="ui centered image" src="<?php echo url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/sd.jpg'; ?>">
                                <p class="txt" style="margin-top: -10rem;position: absolute;font-size: 12px;margin-left: 2rem;width: 150px;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>

        </div>


        <div class="ui bottom attached" style="height: auto;/*background:#fff;*/border: unset;margin-bottom:unset;">
            <!-- container -->
            <br>
            <br>
            <br>
            <br>

            <?php echo view($page, $data);?> 

            <br>
            <br>

        </div>

    </div>

    <!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id="logout-url"  data-url="<?php echo \URL::route('auth.logout.get');?>">
    <div id='ajax-center-url' data-url=""></div>

<script type="text/javascript">
    var _countOfcart = 0
    $(function(){


        var content = 'ไว้ใจให้เราได้ดูแลการนัดหมายของสัตว์เลี้ยงที่คุณรัก';
        // var content = 'If life is so blue, then select another colour from the rainbow';

        var ele = '<span>' + content.split('').join('</span><span>') + '</span>';


        $(ele).hide().appendTo('.txt').each(function (i) {
            $(this).delay(50 * i).css({
                display: 'inline',
                opacity: 0
            }).animate({
                opacity: 1
            }, 50);
        });


        $('.ui.search').search();
        $('#drop_search').dropdown();
        $('#drop_Profile').dropdown();
        $('#drop_login').dropdown();
        $('#drop_history').dropdown();

        $('#drop_Profile_mobile').dropdown();
        $('#drop_mobile').dropdown();
        $('#drop_history_mobile').dropdown();
        $('#drop_user_mobile').dropdown();

        $('.special.cards .image').dimmer({
            on: 'hover'
        });

        // $('.ui.sticky').sticky({
        //     context: '#example1'
        // });

        $('#search_all').on('click', function(){
            console.log($('#search_txtall').val());
            var txt_search = $('#search_txtall').val();
            window.location.href = '/search/'+txt_search;
        });

        $('#search_txtall').keydown(function(event){ 
            if(event.which == 13){
                console.log($(this).val());
                var txt_search = $(this).val();
                window.location.href = '/search/'+txt_search;
            }
        });

        $('.btn-logout').on('click', function(){
            Swal.fire({
                title: "คุณต้องการออกจากระบบหรือไม่ ?",
                showCancelButton: true,
                confirmButtonColor: "#59B855",
                cancelButtonText: "ยกเลิก",
                cancelButtonColor: "",
                confirmButtonText: "ตกลง"
            }).then(result => {
                if (result.value) {
                    logout_url = $('#logout-url').data('url');
                    console.log(logout_url);
                    window.location = logout_url;
                }
            });


        });

    });

</script>
</body>
<footer style="background-color: #D4D4D4;">
    <div class="ui basic segment" style="text-align: center;color: #000;">
        <p>Copyright © 2020 PETCARE.COM</p>
    </div>
</footer>

</html>