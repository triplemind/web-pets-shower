
<div class="ui grid" style="">
	<div class="computer only row">
        <div class="column">
        	<div class="ui basic" style="margin-left: 6rem;margin-right: 6rem;text-align: center;">
				<span style="font-weight: 900;font-size: 60px;color: #5DACBD;">My pets</span>
			</div>

			<div class="ui horizontal stackable segment" style="background-color: #ffe597;margin-left: 6rem;margin-right: 6rem;">
				<div class="sixteen wide" style="padding: 2rem;text-align: end;">
					<button class="ui violet inverted button btn-add-sp-pet"><i class="plus square icon"></i> เพิ่มสัตว์จรจัด</button>
					<button class="ui blue inverted button btn-add-pet"><i class="plus square icon"></i> เพิ่มสัตว์เลี้ยง</button>
				</div>
				
				<?php if($getPets->count() != 0): ?>
					<div class="ui three column grid">
						<?php foreach ($getPets as $key => $getPet): ?>
							<div class="column">
								<div class="ui card">
									<div class="image">
										<div class="ui <?php echo ($getPet->pet_txt_type == 'สัตว์เลี้ยง') ? 'orange' : 'yellow'; ?> ribbon label">
									        <?php echo $getPet->pet_txt_type;?>
								      	</div>
										<img src="<?php echo empty($getPet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->pic); ?>" style="max-height: 350px;min-height: 350px;">
									</div>
									<div class="content" style="text-align: center;">
										<a class="header" href="/mypets/detail/<?php echo $getPet->pet_id;  ?>"><?php echo $getPet->name;  ?></a>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				<?php else: ?>
					<div style="text-align: center;">
						<h2>คุณยังไม่มีข้อมูลสัตว์เลี้ยงในระบบ กรุณาเพิ่มข้อมูลสัตว์เลี้ยง</h2>
					</div>
				<?php endif ?>
			</div>

        </div>
    </div>


    <div class="tablet mobile only row">
        <div class="column">
        	<div class="ui basic" style="margin-left: 6rem;margin-right: 6rem;text-align: center;">
				<span style="font-weight: 900;font-size: 40px;color: #5DACBD;">My pets</span>
			</div>

			<div class="ui horizontal stackable segment" style="background-color: #ffe597;margin-left: 2rem;margin-right: 2rem;">
				<div class="sixteen wide" style="padding: 2rem;text-align: center;">
					<button class="ui violet inverted button btn-add-sp-pet"><i class="plus square icon"></i> เพิ่มสัตว์จรจัด</button>
					<button class="ui blue basic button btn-add-pet"><i class="plus square icon"></i> เพิ่มสัตว์เลี้ยง</button>
				</div>
				
				<?php if($getPets->count() != 0): ?>
					<div class="ui two column grid">
						<?php foreach ($getPets as $key => $getPet): ?>
							<div class="column">
								<div class="ui card">
									<div class="image">
										<div class="ui <?php echo ($getPet->pet_txt_type == 'สัตว์เลี้ยง') ? 'orange' : 'yellow'; ?> ribbon label">
									        <?php echo $getPet->pet_txt_type;?>
								      	</div>
										<img src="<?php echo empty($getPet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->pic); ?>" style="max-height: 350px;">
									</div>
									<div class="content" style="text-align: center;">
										<a class="header" href="/mypets/detail/<?php echo $getPet->pet_id;  ?>"><?php echo $getPet->name;  ?></a>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				<?php else: ?>
					<div style="text-align: center;">
						<h2>คุณยังไม่มีข้อมูลสัตว์เลี้ยงในระบบ กรุณาเพิ่มข้อมูลสัตว์เลี้ยง</h2>
					</div>
				<?php endif ?>
			</div>


        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('mypets.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('mypets.add.post');?>"></div>
    <div id='addsp-url' data-url="<?php echo \URL::route('mypets.addSP.post');?>"></div>



<div class="ui medium modal" id="addMyPetModal">
    <div class="header">เพิ่มข้อมูลสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form" id="addpetform">
            <div class="field">
                <label>ชื่อสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="pet_name" id="pet_name">
            </div>
            <div class="field">
                <label>ประเภทสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="pet_type" id="pet_type" >
                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                    <?php if(!empty($pettypes)): ?>
                        <?php foreach ($pettypes as $key => $pettype): ?>
                            <option value="<?php echo $pettype->name; ?>"><?php echo $pettype->name; ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สายพันธุ์สัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="breed" id="breed" >
                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                </select>
            </div>
            <div class="two fields">
	            <div class="field">
	                <label>เพศสัตว์เลี้ยง</label>
	                <select class="ui fluid dropdown" name="gender" id="gender" >
	                    <option value="">เลือกเพศสัตว์เลี้ยง</option>
	                    <option value="เพศผู้">เพศผู้</option>
	                    <option value="เพศเมีย">เพศเมีย</option>
	                </select>
	            </div>
	            <div class="field">
	                <label>สีของสัตว์เลี้ยง</label>
	                <input type="text" placeholder="" name="color" id="color">
	            </div>
            </div>
            <div class="two fields">
	            <div class="field">
	                <label>น้ำหนัก (กิโลกรัม)</label>
	                <input type="number" placeholder="" name="weight" id="weight" min="0" oninput="javascript: if (this.value.length > 11) this.value = this.value.slice(0, 11);">
	            </div>
	            <div class="field">
	                <label>วันเกิดสัตว์เลี้ยง</label>
	                <div class="ui calendar" id="birthday">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input type="text" placeholder="Date/Time" readonly name="birthday">
						</div>
					</div>
	            </div>
            </div>
            <div class="field">
                <label>รายละเอียดเพิ่มเติม</label>
                <textarea name="notedescription" id="notedescription" rows="3"></textarea>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="pic" id="pic">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="addMySPPetModal">
    <div class="header">เพิ่มข้อมูลสัตว์จรจัด</div>
    <div class="content">
        <div class="ui medium form" id="addSPpetform">
        	<div class="three fields">
	            <div class="seven wide field">
	                <label>ชื่อสัตว์เลี้ยง</label>
	                <input type="text" placeholder="" name="SP_pet_name" id="SP_pet_name">
	            </div>
	            <div class="seven wide field">
	                <label>สีของสัตว์เลี้ยง</label>
	                <input type="text" placeholder="" name="SP_color" id="SP_color">
	            </div>
	            <div class="two wide field">
	                <label style="margin-bottom: 1.6rem;"></label>
	                <button class="ui green button btn-addnew"><i class="plus square icon"></i> เพิ่ม</button>
	            </div>
            </div>
            <div id="sppets_show">
	            <!-- <div class="three fields">
		            <div class="seven wide field">
		                <label>ชื่อสัตว์เลี้ยง</label>
		            </div>
		            <div class="seven wide field">
		                <label>สีของสัตว์เลี้ยง</label>
		            </div>
		            <div class="two wide field">
		                <i class="large window close link icon"></i>
		            </div>
	            </div> -->
	            
            </div>
            <hr>
            <br>
            <div class="field">
                <label>ประเภทสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="SP_pet_type" id="SP_pet_type" >
                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                    <?php if(!empty($pettypes)): ?>
                        <?php foreach ($pettypes as $key => $pettype): ?>
                            <option value="<?php echo $pettype->name; ?>"><?php echo $pettype->name; ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สายพันธุ์สัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="SP_breed" id="SP_breed" >
                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                </select>
            </div>
            <div class="field">
                <label>เพศสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="SP_gender" id="SP_gender" >
                    <option value="">เลือกเพศสัตว์เลี้ยง</option>
                    <option value="เพศผู้">เพศผู้</option>
                    <option value="เพศเมีย">เพศเมีย</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>