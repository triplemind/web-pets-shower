
<div class="ui grid" style="">
	<div class="computer only row">
        <div class="column">
        	<div class="ui basic" style="margin-left: 6rem;margin-right: 6rem;text-align: center;">
				<span style="font-weight: 900;font-size: 60px;color: #5DACBD;">Profile</span>
			</div>

			<div class="ui horizontal stackable segment" style="background-color: #ffe597;margin-left: 6rem;margin-right: 6rem;">

				<div class="ui grid">
					<div class="six wide column">
						<img class="ui medium circular image" src="<?php echo empty($getPet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->pic); ?>" style="max-width: 300px;max-height: 300px;">
					</div>
					<div class="six wide column">
						<div style="margin-top: 18rem;"><span style="font-weight: 900;font-size: 45px;color: #565656;"><?php echo !empty($getPet) ? $getPet->name : '-'; ?></span></div>
					</div>
					<div class="four wide column">
						<?php if(!empty($getPet->qrcode)): ?>
							<a href="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>" download>
								<img class="ui centered small image" src="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>">
							</a>
							<label style="color: red">*คลิกที่ Qr Code เพื่อทำการดาวน์โหลด</label>
						<?php endif ?>
					</div>
				</div>

				<div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>


				<div class="fields" style="margin-top: 1em;">
			        <div class="sixteen wide field">
			            <table class="ui single line table">
			                <tbody>
			                    <tr>
			                        <td class="two wide active">ประเภทสัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->type : '-'; ?></td>
			                        <td class="two wide active">สายพันธุ์สัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->breed : '-'; ?></td>
			                    </tr>
			                    <tr>
			                        <td class="two wide active">เพศสัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->gender : '-'; ?></td>
			                        <td class="two wide active">สีของสัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->color : '-'; ?></td>
			                    </tr>
			                    <?php if($getPet->pet_txt_type == 'สัตว์เลี้ยง'): ?>
				                    <tr>
				                        <td class="two wide active">วันเกิดสัตว์เลี้ยง</td>
				                        <td class="six wide">
				                        	<?php 

				                        		$difference_in_weeks = 0;
				                        		if(!empty($getPet)){
					                        		if(!empty($getPet->birthday)){
						                        		$datenow 	= new DateTime();
						                        		$birthday   = new DateTime($getPet->birthday);
						                        		$difference_in_weeks = $datenow->diff($birthday)->days / 7;
					                        		}
				                        		}

				                        	?>

				                        	<?php echo !empty($getPet) ? DateThai($getPet->birthday, true, false)."   (อายุ ".number_format($difference_in_weeks)." สัปดาห์)" : '-'; ?>
				                    	</td>
				                        <td class="two wide active">น้ำหนัก (กิโลกรัม)</td>
				                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->weight : '-'; ?></td>
				                    </tr>
				                    <tr>
				                        <td class="two wide active">รายละเอียดเพิ่มเติม</td>
				                        <td class="six wide"  colspan="3"><?php echo !empty($getPet) ? $getPet->notedescription : '-'; ?></td>
				                    </tr>
				                <?php endif ?>
			                </tbody>
			            </table>
			        </div>
			    </div>

			    <div class="ui medium form" style="margin-top: 2rem;text-align: center;">
					<div class="two fields">
				        <div class="field">
				            <button class="ui green basic button btn-edit-pet" data-pet_id="<?php echo !empty($getPet) ? $getPet->pet_id : ''; ?>"><i class="edit icon"></i> แก้ไขข้อมูลสัตว์เลี้ยง</button>
				        </div>
				        <div class="field">
				            <button class="ui red basic button btn-emove-pet" data-pet_id="<?php echo !empty($getPet) ? $getPet->pet_id : ''; ?>"><i class="trash alternate icon"></i> ลบสัตว์เลี้ยง</button>
				        </div>
				    </div>
				</div>

			</div>


			<?php if(!empty($getcoursedata)): ?> 
			<div class="ui stackable" style="background-color: #fff;margin-left: 6rem;margin-right: 6rem;">
	            <div class="ui section divider"></div>

	            <div style="margin-top: 5rem;text-align: center;">
					<span style="font-weight: 900;font-size: 28px;color: #5DACBD;">ข้อมูลการใช้คอร์ส</span>
				</div>

				<?php if(!empty($getcourse)):?>
					<?php if(!empty($getcourse->service)): ?>
						<?php if($getcourse->service->status !== 'ยกเลิกรายการ'): ?>
				            <input type="hidden" placeholder="" value="<?php echo $getcourse->id ?>" name="course_id" id="course_id">
				            <input type="hidden" placeholder="" value="<?php echo $getcourse->shop_id ?>" name="id_shop" id="id_shop">
				            <input type="hidden" placeholder="" value="<?php echo $getcourse->id_petservice ?>" name="id_petservice" id="id_petservice">
				            <div class="ui form">
				            	<h3 class="ui header">การใช้คอร์สครั้งถัดไป</h3>
				                <div class="field">
				                    <label>ชื่อคอร์ส</label>
				                    <input type="text" placeholder="" name="companytxt" id="companytxt" readonly  value="<?php echo $getcourse->course->name; ?>">
				                </div>
				                <div class="four fields">
				                    <div class="two wide field">
				                        <label>จำนวนครั้งคงเหลือที่ใช้ได้</label>
				                        <input type="text" placeholder="" name="companytxt" id="companytxt" readonly  value="<?php echo $getcourse->course->numoftimes-$getcourse->thetime; ?>">
				                    </div>
				                    <div class="two wide field">
				                        <label>ครั้งที่ใช้</label>
				                        <input type="text" placeholder="" name="thetime" id="thetime" readonly value="<?php echo $getcourse->thetime; ?>">
				                    </div>
				                    <div class="four wide field">
				                        <label>วันที่ใช้บริการ</label>
				                        <div class="ui calendar" id="startdate">
				                            <div class="ui input left icon">
				                                <i class="calendar icon"></i>
				                                <input type="text" placeholder="วันที่นัดหมาย" readonly>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="four wide field">
				                        <label>ช่วงเวลา</label>
				                        <select class="ui fluid search selection dropdown" name="booking_time" id="booking_time" >
					                        <option>เลือกช่วงเวลา</option>
					                    </select>
				                    </div>
				                    <div class="four wide field" style="margin-top: 1.7rem;">
				                        <button class="ui fluid blue button btn-saveCourseRC"  type="submit">บันทึก</button>
				                    </div>
				                </div>
				            </div>
			            <?php endif ?>
		            <?php endif ?>
	            <?php endif ?>

	            <br>
	            <br>

	            <div class="sixteen wide field">
	                <table class="ui teal table" id="TBL_onlinelist">
	                    <thead>
	                        <tr>
	                        	<th>ชื่อร้าน</th>
	                            <th>ชื่อคอร์ส</th>
	                            <!-- <th>สัตว์เลี้ยง</th> -->
	                            <th>ครั้งที่ใช้</th>
	                            <th>วันที่ใช้บริการ</th>
	                            <th>ลำดับการใช้บริการ</th>
	                            <th>เวลาใช้บริการ</th>
	                            <th>จำนวนครั้งคงเหลือที่ใช้ได้</th>
	                            <th>สถานะการใช้งาน</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <?php if(!empty($getcoursedata)): ?>
	                            <?php if($getcoursedata->count() != 0): ?>
	                                <?php foreach ($getcoursedata as $key => $getcourses): ?>
	                                    <tr>
	                                        <td><?php echo empty($getcourses->shop) ? "-" : $getcourses->shop->name_shop; ?></td>
	                                        <td><?php echo empty($getcourses->course) ? "-" : $getcourses->course->name; ?></td>
	                                        <!-- <td><?php //echo empty($getcourses->pet) ? "-" : $getcourses->pet->name; ?></td> -->
	                                        <td><?php echo empty($getcourses->thetime) ? "-" : $getcourses->thetime; ?></td>
	                                        <td>
	                                            <?php
	                                                // $datetoday = date('Y-m-d H:i:s');
	                                                $datetoday = empty($getcourses->startdate) ? '-' : DateThai($getcourses->startdate, true, false);
	                                            ?>
	                                            <?php echo $datetoday ?>
	                                                
	                                        </td>
	                                        <td><?php echo empty($getcourses->courseque) ? "-" : $getcourses->courseque; ?></td>
	                                        <td><?php echo empty($getcourses->usetime) ? "-" : $getcourses->usetime; ?></td>
	                                        <td>
	                                            <!-- <?php //if($getcourses->isuse == true): ?> -->
	                                            <?php if(!empty($getcourses->startdate)): ?>
	                                                <?php echo empty($getcourses->thetime) ? "-" : (empty($getcourses->course) ? "-" : $getcourses->course->numoftimes-$getcourses->thetime); ?>
	                                            <?php else: ?>
	                                                -
	                                            <?php endif ?>
	                                        </td>
	                                        <td><span class="ui <?php echo $getcourses->isuse == false ? "red" : "green"; ?> label"><?php echo $getcourses->isuse == false ? "รอใช้บริการ" : "ใช้บริการเสร็จสิ้น"; ?></span></td>
	                                    </tr>
	                                <?php endforeach ?>
	                            <?php endif ?>
	                        <?php endif ?>
	                    </tbody>
	                </table>
	            </div>

	            <div class="ui section divider"></div>
	            <br>
	        </div>
        	<?php endif ?> 



			<div class="ui stackable" style="background-color: #fff;margin-left: 6rem;margin-right: 6rem;">
				<div class="ui grid">
					<div class="sixteen wide column">
						<div style="margin-top: 5rem;text-align: center;">
							<span style="font-weight: 900;font-size: 28px;color: #5DACBD;">ประวัติ การฉีดวัคซีน</span>
						</div>
					</div>
					<div class="sixteen wide column">
						<table class="ui striped definition table">
							<thead  class="full-width" >
								<tr>
									<th style="background-color: #ffe597;text-align: center;">วัคซีนป้องกันโรค</th>
									<th style="background-color: #ffe597;text-align: center;">วันที่ฉีด</th>
									<th style="background-color: #ffe597;text-align: center;">รูปภาพวัคซีน</th>
									<th style="background-color: #ffe597;text-align: center;">สัตวแพทย์/เลขที่ใบอนุญาต</th>
									<th style="background-color: #ffe597;text-align: center;">รายละเอียด</th>
									<th style="background-color: #ffe597;text-align: center;">สถานะการฉีด</th>
								</tr>
							</thead>
							<tbody>
								<?php if(count($vacccineRC_arr) != 0): ?>
									<?php foreach ($vacccineRC_arr as $key => $vaccineall): ?>
										<tr>
											<td class="four wide" style="background-color: #5dacbd;color: #fff">
												<?php echo empty($vaccineall['vaccine_name']) ? '' : $vaccineall['vaccine_name']; ?>
											</td>
											<td>
												<?php echo empty($vaccineall['vRC_created_at']) ? '' : DateThai($vaccineall['vRC_created_at'], true, false); ?>
											</td>
											<td> 
												<?php if(!empty($vaccineall['vRC_created_at'])): ?>
													<img class="ui tiny image" src="<?php echo empty($vaccineall['img_vaccine']) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $vaccineall['img_vaccine']); ?>">
												<?php endif ?>
											</td>
											<td>
												<?php echo empty($vaccineall['veterinarian']) ? '' : $vaccineall['veterinarian']; ?>
											</td>
											<td>
												<?php if(!empty($vaccineall['vRC_id'])):?>
													<button class="ui fluid button btn-view" style="background-color: #808080;color: #fff;border-radius: 10px;" onclick="getModalVaccineDetail(<?php echo $vaccineall['vRC_id'].", '".(env('APP_ENV') == 'production' ? '/public' : '')."'"; ?>)" type="submit">เพิ่มเติม</button>
												<?php else: ?>
													<button class="ui fluid button btn-view" style="background-color: #3f51b5;color: #fff;border-radius: 10px;" onclick="window.location.href = '<?php echo '/vaccineshop/'.$vaccineall['vaccine_id'].'/'.$getPet->pet_id; ?>'" type="submit">นัดหมายวัคซีน</button>
												<?php endif ?>
											</td>
											<td>
												<?php if(!empty($vaccineall['vRC_anewdate'])):?>
													<?php //sd($vaccineall['vRC_anewdate']); ?>
													<?php 
														$expire = strtotime($vaccineall['vRC_anewdate']);
        												$today  = strtotime("today midnight");
													?>
													<?php if($today >= $expire):?>
														<span class="ui red label">ขาดช่วง</span>
													<?php else: ?>
														<span class="ui green label">ปกติ</span>
													<?php endif ?>
												<?php endif ?>
											</td>
										</tr>
									<?php endforeach ?>
								<?php endif ?>
							</tbody>
						</table>
					</div>

					<br>
					<br>

					<div class="sixteen wide column">
						<table class="ui definition table">
							<thead>
								<tr>
									<th></th>
									<th>ชื่อร้าน</th>
									<th>วันที่</th>
									<th>ช่วงเวลา</th>
									<th>บริการ</th>
									<th>หมายเหตุ</th>
									<th>เลื่อนนัด</th>
								</tr>
							</thead>
							<tbody>
								<?php if($getHistories->count() != 0): ?>
									<?php foreach ($getHistories as $key => $getHistory): ?>
										<tr>
											<td style="background-color: #5dacbd;color: #fff;">นัดหมายครั้งถัดไป</td>
											<td><?php echo empty($getHistory->shop) ? '' : $getHistory->shop->name_shop ?></td>
											<td>
												<?php
				                                    $datetoday = DateThai($getHistory->booking_date, true, false);
				                                ?>
				                                <?php echo $datetoday ?>
											</td>
											<td>
												<?php 
				                                    $booking_time_txt = "";
				                                    if($getHistory->booking_time == "บ่าย"){
					                                    $booking_time_txt = "บ่าย : ".(empty($getHistory->endTime) ? "13.30น.-19.00น." : $getHistory->endTime);
					                                }else if($getHistory->booking_time == "เช้า"){
					                                    $booking_time_txt = "เช้า : ".(empty($getHistory->startTime) ? "09.00น.-12.30น." : $getHistory->startTime);
					                                }
				                                ?>
				                                <?php echo $booking_time_txt; ?>
											</td>
											<td>
												<?php 
				                                    $service_txt = "";
				                                    if($getHistory->id_service == "PG0001"){
				                                        $service_txt = "อาบน้ำ-ตัดขน";
				                                    }else if($getHistory->id_service == "PG0002"){
				                                        $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
				                                    }else if($getHistory->id_service == "PV0001"){
				                                        $service_txt = "วัคซีน";
				                                    }else if($getHistory->id_service == "PV0002"){
				                                        $service_txt = "โปรโมชันวัคซีน";
				                                    }
				                                ?>
				                                <?php echo $service_txt; ?>
											</td>
											<td><?php echo $getHistory->note  ?></td>
											<td>
												<button class="ui fluid button btn-view" style="background-color: #ec4c2d;color: #fff;border-radius: 10px;" onclick="addBookingTime(<?php echo "'".$getHistory->id_service."', ".$getHistory->id_shop.", ".$getHistory->id_petservice; ?>)" type="submit"><i class="dog icon"></i> เลื่อนนัด</button>
											</td>
										</tr>
									<?php endforeach ?>
								<?php endif ?>
							</tbody>
						</table>
					</div>

				</div>

				<div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>
			
			</div>

        </div>
    </div>


    <div class="tablet mobile only row">
        <div class="column">
    		<div class="ui basic" style="margin-left: 6rem;margin-right: 6rem;text-align: center;">
				<span style="font-weight: 900;font-size: 40px;color: #5DACBD;">Profile</span>
			</div>

			<div class="ui horizontal stackable segment" style="background-color: #ffe597;margin-left: 2rem;margin-right: 2rem;">

				<div class="ui grid">
					<div class="sixteen wide column">
						<img class="ui centered medium circular image" src="<?php echo empty($getPet->pic) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->pic); ?>" style="max-width: 300px;max-height: 300px;">
					</div>
					<div class="sixteen wide column">
						<?php if(!empty($getPet->qrcode)): ?>
							<a href="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>" download>
								<img class="ui centered small image" src="<?php echo url('').str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getPet->qrcode); ?>">
							</a>
							<label style="color: red">*คลิกที่ Qr Code เพื่อทำการดาวน์โหลด</label>
						<?php endif ?>
					</div>
					<div class="sixteen wide column">
						<div style="text-align: center;"><span style="font-weight: 900;font-size: 30px;color: #565656;"><?php echo !empty($getPet) ? $getPet->name : '-'; ?></span></div>
					</div>
				</div>

				<div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>


				<div class="fields" style="margin-top: 1em;">
			        <div class="sixteen wide field">
			            <table class="ui single line table">
			                <tbody>
			                    <tr>
			                        <td class="two wide active">ประเภทสัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->type : '-'; ?></td>
			                    </tr>
			                    <tr>
			                        <td class="two wide active">สายพันธุ์สัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->breed : '-'; ?></td>
			                    </tr>
			                    <tr>
			                        <td class="two wide active">เพศสัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->gender : '-'; ?></td>
			                    </tr>
			                    <tr>
			                        <td class="two wide active">สีของสัตว์เลี้ยง</td>
			                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->color : '-'; ?></td>
			                    </tr>
			                    <?php if($getPet->pet_txt_type == 'สัตว์เลี้ยง'): ?>
				                    <tr>
				                        <td class="two wide active">วันเกิดสัตว์เลี้ยง</td>
				                        <td class="six wide">
				                        	<?php 

				                        		$difference_in_weeks = 0;
				                        		if(!empty($getPet)){
					                        		if(!empty($getPet->birthday)){
						                        		$datenow 	= new DateTime();
						                        		$birthday   = new DateTime($getPet->birthday);
						                        		$difference_in_weeks = $datenow->diff($birthday)->days / 7;
					                        		}
				                        		}

				                        	?>

				                        	<?php echo !empty($getPet) ? DateThai($getPet->birthday, true, false)."   (อายุ ".number_format($difference_in_weeks)." สัปดาห์)" : '-'; ?>
				                    	</td>
				                    </tr>
				                    <tr>
				                        <td class="two wide active">น้ำหนัก (กิโลกรัม)</td>
				                        <td class="six wide"><?php echo !empty($getPet) ? $getPet->weight : '-'; ?></td>
				                    </tr>
				                    <tr>
				                        <td class="two wide active">รายละเอียดเพิ่มเติม</td>
				                        <td class="six wide"  colspan="3"><?php echo !empty($getPet) ? $getPet->notedescription : '-'; ?></td>
				                    </tr>
				                <?php endif ?>
			                </tbody>
			            </table>
			        </div>
			    </div>

			    <div class="ui medium form" style="margin-top: 2rem;text-align: center;">
					<div class="two fields">
				        <div class="field">
				            <button class="ui green basic button btn-edit-pet" data-pet_id="<?php echo !empty($getPet) ? $getPet->pet_id : ''; ?>"><i class="edit icon"></i> แก้ไขข้อมูลสัตว์เลี้ยง</button>
				        </div>
				        <div class="field">
				            <button class="ui red basic button btn-emove-pet" data-pet_id="<?php echo !empty($getPet) ? $getPet->pet_id : ''; ?>"><i class="trash alternate icon"></i> ลบสัตว์เลี้ยง</button>
				        </div>
				    </div>
				</div>

			</div>


			<div class="ui stackable" style="background-color: #fff;margin-left: 2rem;margin-right: 2rem;">
				<div class="ui grid">
					<div class="sixteen wide column">
						<div style="margin-top: 5rem;text-align: center;">
							<span style="font-weight: 900;font-size: 20px;color: #5DACBD;">ประวัติ การฉีดวัคซีน</span>
						</div>
					</div>
					<!-- <div class="ui stackable column grid" style="padding: 15px;"> -->
	                    <div class="sixteen wide column">
	                        <div class="ui two doubling link cards">
	                            <?php if(count($vacccineRC_arr) != 0): ?>
									<?php foreach ($vacccineRC_arr as $key => $vaccineall): ?>
	                                    <div class="card">
	                                    	<div class="image">
				                                <?php if(!empty($vaccineall['vRC_created_at'])): ?>
													<img src="<?php echo empty($vaccineall['img_vaccine']) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $vaccineall['img_vaccine']); ?>">
												<?php endif ?>
				                            </div>
	                                        <div class="content">
	                                            <div class="description">
	                                            	<div class="header"><?php echo empty($vaccineall['vaccine_name']) ? '-' : $vaccineall['vaccine_name']; ?></div>
	                                                วันที่ฉีด : <span> <?php echo empty($vaccineall['vRC_created_at']) ? '-' : DateThai($vaccineall['vRC_created_at'], true, false); ?> </span><br>
	                                                สัตวแพทย์/เลขที่ใบอนุญาต : <span> <?php echo empty($vaccineall['veterinarian']) ? '-' : $vaccineall['veterinarian']; ?> </span>
	                                            </div>
	                                        </div>
	                                        <div class="extra content">
				                                <div class="ui one buttons" style="padding-top: 1rem;">
				                                    <?php if(!empty($vaccineall['vRC_id'])):?>
														<!-- <div class="ui green button btn-view" style="background-color: #ec4c2d;color: #fff;" onclick="getModalVaccineDetail(<?php //echo $vaccineall['vRC_id'].", '".(env('APP_ENV') == 'production' ? '/public' : '')."' "; ?>)" type="submit">เพิ่มเติม</div> -->
													<?php else: ?>
														<div class="ui green button btn-view" style="background-color: #3f51b5;color: #fff;" onclick="window.location.href = '<?php echo '/vaccineshop/'.$vaccineall['vaccine_id'].'/'.$getPet->pet_id; ?>'" type="submit">นัดหมายวัคซีน</div>
													<?php endif ?>
												</div>
												<?php if(!empty($vaccineall['vRC_id'])):?>
													<div class="ui accordion">
			                                            <div class="title">
			                                                <i class="dropdown icon"></i>
			                                                เพิ่มเติม
			                                            </div>
			                                            <div class="content">
			                                                <div class="ui form sixteen wide column">
			                                                    <div class="field">
			                                                        <h4  style="color: #5DACBD;">ชื่อวัคซีน</h4>
			                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['vaccine_name']) ? '-' : $vaccineall['vaccine_name']; ?></div>
			                                                    </div>
			                                                    <div class="field">
			                                                        <h4  style="color: #5DACBD;">บริษัทผู้ผลิตวัคซีน</h4>
			                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['company']) ? '-' : $vaccineall['company']; ?></div>
			                                                    </div>
			                                                    <div class="field">
			                                                        <h4  style="color: #5DACBD;">สรรพคุณของวัคซีน</h4>
			                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['properties']) ? '-' : $vaccineall['properties']; ?> </div>
			                                                    </div>
			                                                    <div class="field">
			                                                        <h4  style="color: #5DACBD;">สัตวแพทย์</h4>
			                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['veterinarian']) ? '-' : $vaccineall['veterinarian']; ?> </div>
			                                                    </div>
			                                                    <div class="field">
			                                                        <h4  style="color: #5DACBD;">รายละเอียด</h4>
			                                                        <div ><span><i class="dog icon"></i></span> <?php echo empty($vaccineall['description']) ? '-' : $vaccineall['description']; ?> </div>
			                                                    </div>
			                                                </div>
			                                            </div>
			                                        </div>
												<?php endif ?>
				                            </div>
	                                    </div>
	                                <?php endforeach ?>
	                            <?php endif ?>
	                        </div>
	                    <!-- </div> -->
	                </div>

					<br>
					<br>

					<!-- <div class="ui stackable column grid" style="padding: 15px;"> -->
	                    <div class="sixteen wide column">
	                        <div class="ui two doubling link cards">
	                            <?php if($getHistories->count() != 0): ?>
									<?php foreach ($getHistories as $key => $getHistory): ?>
	                                    <div class="card">
	                                        <div class="content">
	                                            <div class="description">
	                                            	<div class="header">นัดหมายครั้งถัดไป</div>
	                                                ชื่อร้าน : <span> <?php echo empty($getHistory->shop) ? '' : $getHistory->shop->name_shop; ?> </span>
	                                                วันที่ : <span> <?php echo DateThai($getHistory->booking_date, true, false); ?> </span>
	                                                <?php 
	                                                    $booking_time_txt = "";
	                                                    if($getHistory->booking_time == "บ่าย"){
	                                                        $booking_time_txt = "บ่าย 13.30น.-19.00น.";
	                                                    }else if($getHistory->booking_time == "เช้า"){
	                                                        $booking_time_txt = "เช้า 09.00น.-12.30น.";
	                                                    }
	                                                ?>
	                                                ช่วงเวลา : <span> <?php echo $booking_time_txt; ?> </span>
	                                                 <?php 
	                                                    $service_txt = "";
	                                                    if($getHistory->id_service == "PG0001"){
	                                                        $service_txt = "อาบน้ำ-ตัดขน";
	                                                    }else if($getHistory->id_service == "PG0002"){
	                                                        $service_txt = "โปรโมชันอาบน้ำ-ตัดขน";
	                                                    }else if($getHistory->id_service == "PV0001"){
	                                                        $service_txt = "วัคซีน";
	                                                    }else if($getHistory->id_service == "PV0002"){
	                                                        $service_txt = "โปรโมชันวัคซีน";
	                                                    }
	                                                ?>
	                                                บริการ : <span> <?php echo $service_txt; ?> </span>
	                                                หมายเหตุ : <span> <?php echo $getHistory->note; ?> </span>
	                                            </div>
	                                        </div>
	                                        <div class="extra content">
				                                <div class="ui one buttons" style="padding-top: 1rem;">
				                                    <div class="ui green button btn-view" style="background-color: #ec4c2d;color: #fff;" onclick="addBookingTime(<?php echo "'".$getHistory->id_service."', ".$getHistory->id_shop.", ".$getHistory->id_petservice; ?>)" type="submit"><i class="dog icon"></i> เลื่อนนัด</div>
				                                </div>
				                            </div>
	                                    </div>
	                                <?php endforeach ?>
	                            <?php endif ?>
	                        </div>
	                    </div>
	                <!-- </div> -->
				</div>

				<div class="ui divider" style="border-top: 1px solid rgb(255 255 255 / 55%);border-bottom: 2px solid rgb(255 255 255);"></div>

			</div>

        </div>
    </div>
</div>

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<div class="ui grid" style="margin-bottom: 1rem;">
		<div class="four column row">
			<div class="sixteen wide left floated column">
				<h2 class="ui horizontal header">
				    <span style="background-color: #FEE265;padding: 4px;">ร้านใกล้เคียง</span>
				</h2>
			</div>
			<div class="eight wide right floated column" style="text-align: end;">
				<a href="/shopnearbygoogle"> 
				    ทั้งหมด >>>
				</a>
			</div>
		</div>
	</div>
	<div class="">
		<?php //if($shops->count() != 0): ?>
			<?php //foreach ($shops as $key => $shop): ?>
				<div class="ui four stackable centered cards" id="addcard-nearby">
					<!-- <div class="ui centered card">
						<div class="image">
							<img class="ui huge image" src="<?php //echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>" style="max-height: 165px;max-width: 290px;">
						</div>
						<div class="content">
							<a class="header" href="/booking/<?php //echo $shop->id_shop; ?>"><?php //echo $shop->name_shop; ?></a>
						</div>
					</div> -->
				</div>
			<?php //endforeach ?>
		<?php //endif ?>
    </div>
</div>

<div id="map" style="display: none;"></div>

<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGetgbZ37IeA0eTGFVGnq5T2u3wcQsfBM&libraries=places&callback=initMap&language=th&region=TH"
        async defer
    ></script>


<!-- Data -->
    <!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('mypets.ajax_center.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('mypets.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('mypets.delete.post');?>"></div>

    <div id='Booking-ajax-center-url' data-url="<?php echo \URL::route('booking.ajax_center.post');?>"></div>
    <div id='dashboard-ajax-center-url' data-url="<?php echo \URL::route('dashboard.ajax_center.post');?>"></div>


<div class="ui medium modal" id="editMyPetModal">
    <div class="header">เพิ่มข้อมูลสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
        	<input type="hidden" placeholder="" name="pet_id" id="pet_id">
            <div class="field">
                <label>ชื่อสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="pet_name" id="pet_name">
            </div>
            <div class="field">
                <label>ประเภทสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="pet_type" id="pet_type" >
                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                    <?php if(!empty($pettypes)): ?>
                        <?php foreach ($pettypes as $key => $pettype): ?>
                            <option value="<?php echo $pettype->name; ?>"><?php echo $pettype->name; ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สายพันธุ์สัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="breed" id="breed" >
                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                </select>
            </div>
            <div class="two fields">
	            <div class="field">
	                <label>เพศสัตว์เลี้ยง</label>
	                <select class="ui fluid dropdown" name="gender" id="gender" >
	                    <option value="">เลือกเพศสัตว์เลี้ยง</option>
	                    <option value="เพศผู้">เพศผู้</option>
	                    <option value="เพศเมีย">เพศเมีย</option>
	                </select>
	            </div>
	            <div class="field">
	                <label>สีของสัตว์เลี้ยง</label>
	                <input type="text" placeholder="" name="color" id="color">
	            </div>
            </div>
            <div class="two fields">
	            <div class="field">
	                <label>น้ำหนัก (กิโลกรัม)</label>
	                <input type="number" placeholder="" name="weight" id="weight" min="0">
	            </div>
	            <div class="field">
	                <label>วันเกิดสัตว์เลี้ยง</label>
	                <div class="ui calendar" id="birthday">
						<div class="ui input left icon">
							<i class="calendar icon"></i>
							<input type="text" placeholder="Date/Time" readonly name="birthday">
						</div>
					</div>
	            </div>
            </div>
            <div class="field">
                <label>รายละเอียดเพิ่มเติม</label>
                <textarea name="notedescription" id="notedescription" rows="3"></textarea>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="pic" id="pic">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>




<div class="ui tiny modal">
	<div class="header">เลือกเวลานัดหมาย</div>
	<div class="content">
		<div class="ui form sixteen wide column">
			<div class="field">
				<label>วันที่นัดหมาย</label>
				<div class="ui calendar" id="booking_date">
					<div class="ui input left icon">
						<i class="calendar icon"></i>
						<input type="text" placeholder="วันที่นัดหมาย" readonly>
					</div>
				</div>
			</div>
			<div class="field">
                <label>ช่วงเวลา</label>
                <select class="ui fluid search selection dropdown" name="booking_time" id="booking_time" >
                    <option>เลือกช่วงเวลา</option>
                    <option value="เช้า">เช้า 09.00น.-12.30น.</option>
                    <option value="บ่าย">บ่าย 13.30น.-19.00น.</option>
                </select>
            </div>
		</div>

		<br>
		<br>

		<div class="field" style="text-align: end;">
            <a class="ui violet basic tag label" id="que_txt"></a>
        </div>
	</div>
	<div class="actions">
		<div class="ui green approve button">บันทึก</div>
		<div class="ui red cancel button">ยกเลิก</div>
	</div>
</div>


<div class="ui small modal">
	<div class="header" style="color: #5DACBD;">ข้อมูลวัคซีน</div>
	<div class="scrolling content">
		<div class="ui form sixteen wide column">
			<div class="field">
				<h4  style="color: #5DACBD;">ชื่อวัคซีน</h4>
				<div class="vaccine_name"></div>
			</div>
			<div class="field">
                <h4  style="color: #5DACBD;">บริษัทผู้ผลิตวัคซีน</h4>
                <div class="company"></div>
            </div>
            <div class="field">
				<h4  style="color: #5DACBD;">สรรพคุณของวัคซีน</h4>
				<div class="properties"></div>
			</div>
			<div class="field">
				<h4  style="color: #5DACBD;">สัตวแพทย์</h4>
				<div class="veterinarian"></div>
			</div>
			<div class="field">
				<h4  style="color: #5DACBD;">รายละเอียด</h4>
				<div class="description"></div>
			</div>
			<div class="field">
				<h4  style="color: #5DACBD;">ฉลากวัคซีน</h4>
				<div class="img_vaccine"></div>
			</div>
		</div>

		<br>
		<br>

		
	</div>
	<div class="actions">
		<div class="ui blue cancel button">ปิด</div>
	</div>
</div>