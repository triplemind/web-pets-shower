
<div class="ui stackable container" style="margin-top: -5rem;padding-bottom: 15rem;
padding-left: 0%; padding-right: 0%;">
	<div class="ui grid">
		<div class="sixteen wide column">
			<!-- <i class="address card outline icon"></i> -->
			<div style="margin-top: 5rem;"><span style="font-weight: 900;font-size: 28px;color: #565656;"><i class="address card outline icon"></i> ข้อมูลส่วนตัว</span></div>
		</div>
		<div class="sixteen wide column">
			<div class="ui placeholder segment">
				<img class="ui small circular centered image" src="<?php echo empty($getUser->user_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getUser->user_img); ?>">
				<h2 style="text-align: center;"><?php echo $getUser->name_owner ?> 
					<i class="edit outline icon editIcon" style="cursor: pointer;" onclick="postAction('edit')"></i>
					<i class="window close outline icon closeIcon" style="cursor: pointer;display: none;" onclick="postAction('close')"></i></i>
				</h2>

				<div class="ui section divider"></div>

				<div class="ui stackable grid">
					<div class="eight wide column">
						<div class="ui large list">
							<div class="item" style="margin-bottom: 1rem;">
								<div class="header">ชื่อผู้ใช้งาน : </div>
								<?php echo $getUser->username ?>
							</div>
							<div class="item" style="margin-bottom: 1rem;">
								<div class="header">เบอร์โทรศัพท์ : </div>
								<?php echo $getUser->telephone ?>
							</div>
							<div class="item" style="margin-bottom: 1rem;">
								<div class="header">อีเมล : </div>
								<?php echo $getUser->email ?>
							</div>
						</div>
					</div>
					<div class="eight wide column">
						<div class="ui large list">
							<div class="item">
								<div class="header">ที่อยู่</div>
								<div class="ui icon yellow message">
	                                <i class="map marker icon"></i>
	                                <div class="content">
	                                    <p class="address"><?php echo $getUser->address ?></p>
	                                </div>
	                            </div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="" id="edit-form" style="display: none;">
				<div class="ui teal segment">
					<h3>แก้ไขข้อมูลส่วนตัว</h3>
					<div class="ui medium form">
			            <input type="hidden" placeholder="" name="id_owner" id="id_owner" value="<?php echo $getUser->id_owner; ?>">
			            <input type="hidden" placeholder="" name="ed_user_type" id="ed_user_type" value="<?php echo $getUser->user_type; ?>">
			            <div class="two fields">
			                <div class="field">
			                    <label>ชื่อ-นามสกุล</label>
			                    <input type="text" placeholder="" name="ed_name_owner" id="ed_name_owner" value="<?php echo $getUser->name_owner; ?>">
			                </div>
				            <div class="field">
				                <label>ชื่อผู้ใช้งาน</label>
				                <input type="text" placeholder="" name="ed_username" id="ed_username" value="<?php echo $getUser->username; ?>">
				            </div>
			            </div>
			            <div class="field">
			                <label>อีเมล</label>
			                <input type="text" placeholder="" name="ed_email" id="ed_email" value="<?php echo $getUser->email; ?>">
			            </div>
			            <div class="field">
			               <label>ที่อยู่อาศัย</label>
			                <textarea rows="3" name="ed_address" id="ed_address"><?php echo $getUser->address; ?></textarea>
			            </div>
		                <div class="field">
		                   <label>เบอร์โทรศัพท์</label>
		                    <input type="text" placeholder="" name="ed_telephone" id="ed_telephone" maxlength="10" value="<?php echo $getUser->telephone; ?>">
		                </div>
			            <div class="two fields">
                            <div class="required field passwordtxt">
                                <label>รหัสผ่าน</label>
                                <div class="ui icon input">
                                    <input type="password" placeholder="รหัสผ่าน" class="password" name="password" id="password" maxlength="8">
                                    <i class="eye link icon toggle-password" toggle="#password"></i>
                                </div>
                            </div>
                            <div class="required field confirm_passtxt">
                                <label>ยืนยันรหัสผ่าน</label>
                                <div class="ui icon input">
                                    <input type="password" class="confirm_pass" placeholder="ยืนยันรหัสผ่าน" name="cfnewpassword" id="cfnewpassword" maxlength="8">
                                    <i class="eye link icon toggle-password" toggle="#cfnewpassword"></i>
                                </div>
                            </div>
                        </div>
			            <div class="field">
			                <label>รูปภาพ</label>
			                <input type="file" placeholder="" name="ed_user_img" id="ed_user_img">
			            </div>


			            <div class="field">
                            <div class="alert alert-dismissible text-center err-pass" style="text-align: center;">
                                <span id='message'></span>
                            </div>
                        </div>


				        <div class="sixteen wide field">
				            <div class="ui unstackable four column grid ">
				                <div class="column">
				                </div>
				                <div class="right floated column">
				                    <div class="fields" style="float: right;">
			                            <div class="field">
			                                <button class="ui fluid blue button btn-save " type="submit" >บันทึก</button>
			                            </div>
				                    </div>
				                </div>
				            </div>
				        </div>

			        </div>

				</div>
			</div>
		</div>

	</div>
</div>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('user.ajax_center.post');?>"></div>
<div id='edit-url' data-url="<?php echo \URL::route('user.edit.post');?>"></div>