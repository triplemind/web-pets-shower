<h2 class="ui horizontal divider header">
    <!-- <i class="center align icon"></i> -->
    คำค้นหา : <?php echo $txt_search; ?>
</h2>
<br>
<h2 class="ui horizontal divider header">
    <i class="search icon"></i>
    ค้นหาร้านค้า
</h2>
<div class="ui container teal segment" style="padding-left: 1rem; padding-right: 1rem;">
    <!-- <div class="ui fluid huge icon input">
        <input class="" type="text" placeholder="ค้นหาร้านค้า..." id="search_txtall">
        <i class="search icon"></i>
    </div> -->
	<div class="ui fluid huge search" id="shop_search">
	    <div class="ui icon fluid input">
	        <input class="prompt" type="text" name="search_txtall" id="search_txtall" placeholder="ค้นหาร้านค้า...">
	        <i class="search icon"></i>
	    </div>
	    <div class="results"></div>
	</div>
</div>


<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<?php if($shops->count() != 0): ?>
		<div class="ui three stackable cards">
			<?php foreach ($shops as $key => $shop): ?>
				<div class="ui centered card">
					<div class="image">
						<img class="ui huge image" style="max-height: 201px;max-width: 357px;" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>">
					</div>
					<div class="content">
						<a class="header" href="/booking/<?php echo $shop->id_shop; ?>"><?php echo $shop->name_shop; ?></a>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	<?php endif ?>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('shoponline.ajax_center.post');?>"></div>

