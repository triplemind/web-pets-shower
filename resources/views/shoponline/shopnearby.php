<h2 class="ui horizontal divider header">
    <i class="search icon"></i>
    ค้นหาร้านค้า
</h2>
<div class="ui container teal segment" style="padding-left: 1rem; padding-right: 1rem;">
    <!-- <div class="ui fluid huge icon input">
        <input class="" type="text" placeholder="ค้นหาร้านค้า..." id="search_txtall">
        <i class="search icon"></i>
    </div> -->
    <div class="ui fluid huge search" id="shop_search">
	    <div class="ui icon fluid input">
	        <input class="prompt" type="text" name="search_txtall" id="search_txtall" placeholder="ค้นหาร้านค้า...">
	        <i class="search icon"></i>
	    </div>
	    <div class="results"></div>
	</div>
</div>

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<?php if($shops->count() != 0): ?>
		<div class="ui three stackable cards">
			<?php foreach ($shops as $key => $shop): ?>
				<div class="ui centered card">
					<div class="image">
						<img class="ui huge image" src="<?php echo empty($shop->shop_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $shop->shop_img); ?>" style="max-height: 165px;">
					</div>
					<div class="content">
						<a class="header" href="/booking/<?php echo $shop->id_shop; ?>"><?php echo $shop->name_shop; ?></a>
					</div>
				</div>
			<?php endforeach ?>
		</div>
	<?php endif ?>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('shoponline.ajax_center.post');?>"></div>



<script type="text/javascript">
	var _shopAllData = [];
$(function(){
	$('#drop_search').dropdown();


	var method      = 'getShopData';
    var ajax_url    = $('#ajax-center-url').data('url');
    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
        },
        success: function(result) {
            if(result.status == 'success'){
                console.log(result.data);
                _shopAllData = result.data;
                setTimeout(function(){
                    _shopAllData.forEach(element => {
                        element['results'] = element.name_shop;
                        element['category'] = element.type_shop;
                        element['title'] = element.name_shop;
                    });

                    console.log(_shopAllData);

                    $('#shop_search').search({
                        ignoreDiacritics: true,
                        fullTextSearch:'exact',
                        type: 'category',
                        source: _shopAllData,
                        onSelect: (result, response) => {
                        	window.location.href = '/search/'+result.name_shop;
                        },
                        error: 
                            {
                                source:"ไม่สามารถค้นหา ไม่ได้ใช้แหล่งที่มาและโมดูล Semantic API ไม่รวมอยู่",
                                noResultsHeader: "ไม่มีผลลัพธ์",
                                noResults: "การค้นหาของคุณไม่เจอผลลัพธ์",
                                logging: "ข้อผิดพลาดในการบันทึกการดีบักกำลังออก",
                                noTemplate: "ไม่ได้ระบุชื่อเทมเพลตที่ถูกต้อง",
                                serverError: "มีปัญหาในการสอบถามเซิร์ฟเวอร์",
                                maxResults: "ผลลัพธ์จะต้องเป็นอาร์เรย์เพื่อใช้การตั้งค่า maxResults",
                                method: "วิธีการที่คุณเรียก ไม่ได้กำหนดไว้"
                            },
                    });

                });
            } 

            if(result.status == 'error'){
                $("body").toast({
                    class: "error",
                    position: 'bottom right',
                    message: result.msg
                });
            }
        }
    });


});

</script>