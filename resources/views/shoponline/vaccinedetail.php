<h1 class="ui horizontal divider header">
    <i class="syringe icon"></i>
    ข้อมูลวัคซีน
</h1>
<br>
<div class="ui stackable container"  style="padding-left: 0%; padding-right: 0%;background-color: #ffffff61;">
	<div class="ui form  teal segment">
	    <h4 class="ui header">ค้นหาวัคซีน</h4>
	    <div class="fields">
	        <div class="twelve wide field">
	            <label>คำค้นหา</label>
	            <input type="text" placeholder="ค้นหาวัคซีน..." name="search_txt" id="search_txt" value="<?php echo isset($filters['search_txt']) ? $filters['search_txt'] : '' ?>">
	        </div>
	        <div class="four wide field">
	            <div class="field">
	                <?php $filters['vaccine_type'] = isset($filters['vaccine_type']) ? $filters['vaccine_type'] : '' ?>
	                <label>ประเภทวัคซีน</label>
	                <select class="ui fluid dropdown" name="vaccine_type" id="vaccine_type" >
	                    <option value="all" <?php echo $filters['vaccine_type'] == 'all' ? 'selected' : '' ?>>ทั้งหมด</option>
	                    <option value="วัคซีนสำหรับแมว" <?php echo $filters['vaccine_type'] == 'วัคซีนสำหรับแมว' ? 'selected' : '' ?>>วัคซีนสำหรับแมว</option>
	                    <option value="วัคซีนสำหรับสุนัข" <?php echo $filters['vaccine_type'] == 'วัคซีนสำหรับสุนัข' ? 'selected' : '' ?>>วัคซีนสำหรับสุนัข</option>
	                </select>
	            </div>
	        </div>
	    </div>
	    <div class="ui unstackable four column grid ">
	        <div class="column">
	        </div>
	        <div class="right floated column">
	            <div class="fields">
	                <div class="eight wide field">
	                    <button class="ui fluid small blue button btn-search" type="submit" style="border-radius: 30px;">ค้นหา</button>
	                </div>
	                <div class="eight wide field">
	                    <button class="ui fluid small red button btn-clear" type="submit" style="border-radius: 30px;">ล้าง</button>
	                </div>
	            </div>
	        </div>
	    </div>
	    <br>
	</div>
</div>
<div class="ui stackable container "  style="padding-left: 0%; padding-right: 0%;background-color: #ffffff61;">
	<?php if($getVaccines->count() !== 0): ?>
		<div class="ui fluid styled accordion">
			<?php foreach ($getVaccines as $key => $getVaccine): ?>
				<div class="title">
					<i class="dropdown icon"></i>
					<?php echo $getVaccine->vaccine_name."  ประเภท : ".$getVaccine->vaccine_type; ?>
				</div>
				<div class="content">
					<p class="transition hidden">การใช้งาน : </p>
					<div><?php echo $getVaccine->vaccine_usage; ?></div>
					<p class="transition hidden">รายละเอียด : </p>
					<div><?php echo $getVaccine->description; ?></div>
				</div>
			<?php endforeach ?>
		</div>
	<?php endif ?>
	<?php if($getVaccines->count() == 0): ?>
		<div class="ui red large message">
  			<p>ไม่พบข้อมูลที่คุณค้นหา</p>
		</div>
	<?php endif ?>
</div>

<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('shoponline.ajax_center.post');?>"></div>