<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="user alternate icon"></i>
                <div class="content">
                    จัดการผู้ใช้งานระบบ
                </div>
            </h3>
        </div>

        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big green button btn-adduser" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลผู้ใช้งาน</button>
                </div>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <br>
        <!-- <div>
            <p><span style="color: red;">**</span>Root : สามารถใช้งานได้ทุกเมนู</p>
            <p><span style="color: red;">**</span>ผู้ดูแลระบบ : สามารถใช้งานเมนู หน้าหลัก, หน้าการขาย, หน้ารายงานได้, เมนูย่อยของหน้าหลัก และ เมนูย่อยของหน้าการขายได้</p>
            <p><span style="color: red;">**</span>ผู้ใช้ทั่วไป : สามารถใช้งานเมนู หน้าหลัก, หน้ารายงานได้ และ เมนูย่อยของหน้าหลักได้</p>
        </div> -->
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_user">
                    <thead>
                        <tr>
                            <th>รูปภาพ</th>
                            <th>ชื่อ - นามสกุล</th>
                            <th>ชื่อผู้ใช้งาน</th>
                            <th>อีเมล</th>
                            <th>ประเภทผู้ใช้งาน</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($users)): ?>
                            <?php foreach ($users as $key => $user):?>
                                <tr>
                                    <td class="wide two">
                                        <img class="ui tiny circular image" src="<?php echo empty($user->user_img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $user->user_img); ?>">
                                    </td>
                                    <td class="wide two"><?php echo $user->name_owner ?></td>
                                    <td class="wide two"><?php echo $user->username ?></td>
                                    <td class="wide two"><?php echo $user->email ?></td>
                                    <td class="wide two"><?php echo $user->user_type ?></td>
                                    <td class="wide two">
                                        <i class="large edit outline icon edituser" data-id="<?php echo $user->id_owner ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deleteuser" data-id="<?php echo $user->id_owner ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('user.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('user.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('user.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('user.delete.post');?>"></div>


<div class="ui medium modal" id="addUserModal">
    <div class="header">เพิ่มข้อมูลผู้ใช้งาน</div>
    <div class="content">
        <div class="ui big form">
            <div class="field">
                <label>ชื่อผู้ใช้งาน</label>
                <input type="text" placeholder="" name="username" id="username">
            </div>
            <div class="field">
                <label>รหัสผ่าน</label>
                <input type="password" placeholder="" name="password" id="password">
            </div>
            <!-- <div class="field">
                <label>ยืนยันรหัสผ่าน</label>
                <input type="password" placeholder="" name="cf_password" id="cf_password">
            </div> -->
            <div class="field">
                <label>ชื่อ-นามสกุล</label>
                <input type="text" placeholder="" name="name_owner" id="name_owner">
            </div>
            <div class="field">
                <label>อีเมล</label>
                <input type="text" placeholder="" name="email" id="email">
            </div>
            <div class="field">
                <label>ที่อยู่</label>
                <textarea name="address" id="address" rows="3"></textarea>
            </div>
            <div class="field">
                <label>เบอร์โทรศัพท์</label>
                <input type="text" placeholder="" name="telephone" id="telephone" maxlength="10">
            </div>
            <div class="field">
                <label>ประเภทผู้ใช้งาน</label>
                <select class="ui fluid dropdown" name="user_type" id="user_type" >
                    <option value="">เลือกผู้ใช้งาน</option>
                    <option value="Root">Root</option>
                    <option value="ผู้ดูแลระบบ">ผู้ดูแลระบบ</option>
                    <option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>
                </select>
            </div>
            <!-- <div class="field">
                <label>เมนูที่สามารถเข้าถึงได้</label>
                <select class="ui fluid dropdown" multiple=""  name="menu_accress" id="menu_accress" >
                    <option value="">เลือกเมนูที่สามารถเข้าถึงได้</option>
                    <option value="หน้าหลัก">หน้าหลัก</option>
                    <option value="หน้าการขาย">หน้าการขาย</option>
                    <option value="คลังสินค้า">คลังสินค้า</option>
                    <option value="จัดการผู้ใช้งานระบบ">จัดการผู้ใช้งานระบบ</option>
                    <option value="จัดการข้อมูลคู่ค้า">จัดการข้อมูลคู่ค้า</option>
                    <option value="รายงาน">รายงาน</option>
                    <option value="บิลรับ">บิลรับ</option>
                </select>
            </div> -->
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="user_img" id="user_img">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="EditUserModal">
    <div class="header">แก้ไขข้อมูลผู้ใช้งาน</div>
    <div class="content">
        <div class="ui big form">
            <input type="hidden" placeholder="" name="user_id" id="user_id">
            <div class="field">
                <label>ชื่อผู้ใช้งาน</label>
                <input type="text" placeholder="" name="ed_username" id="ed_username">
            </div>
            <div class="field">
                <label>รหัสผ่าน</label>
                <input type="password" placeholder="" name="ed_password" id="ed_password">
            </div>
            <!-- <div class="field">
                <label>ยืนยันรหัสผ่าน</label>
                <input type="password" placeholder="" name="ed_cf_password" id="ed_cf_password">
            </div> -->
            <div class="field">
                <label>ชื่อ-นามสกุล</label>
                <input type="text" placeholder="" name="ed_name_owner" id="ed_name_owner">
            </div>
            <div class="field">
                <label>อีเมล</label>
                <input type="text" placeholder="" name="ed_email" id="ed_email">
            </div>
            <div class="field">
                <label>ที่อยู่</label>
                <textarea name="ed_address" id="ed_address" rows="3"></textarea>
            </div>
            <div class="field">
                <label>เบอร์โทรศัพท์</label>
                <input type="text" placeholder="" name="ed_telephone" id="ed_telephone"  maxlength="10">
            </div>
            <div class="field">
                <label>ประเภทผู้ใช้งาน</label>
                <select class="ui fluid dropdown" name="ed_user_type" id="ed_user_type" >
                    <option value="">เลือกผู้ใช้งาน</option>
                    <option value="Root">Root</option>
                    <option value="ผู้ดูแลระบบ">ผู้ดูแลระบบ</option>
                    <option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>
                </select>
            </div>
            <!-- <div class="field">
                <label>เมนูที่สามารถเข้าถึงได้</label>
                <select class="ui fluid dropdown" multiple=""  name="ed_menu_accress" id="ed_menu_accress" >
                    <option value="">เลือกเมนูที่สามารถเข้าถึงได้</option>
                    <option value="หน้าหลัก">หน้าหลัก</option>
                    <option value="หน้าการขาย">หน้าการขาย</option>
                    <option value="คลังสินค้า">คลังสินค้า</option>
                    <option value="จัดการผู้ใช้งานระบบ">จัดการผู้ใช้งานระบบ</option>
                    <option value="จัดการข้อมูลคู่ค้า">จัดการข้อมูลคู่ค้า</option>
                    <option value="รายงาน">รายงาน</option>
                    <option value="บิลรับ">บิลรับ</option>
                </select>
            </div> -->
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="ed_user_img" id="ed_user_img">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>