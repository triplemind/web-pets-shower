<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="syringe icon"></i>
                <div class="content">
                    เพิ่มข้อมูลวัคซีน
                </div>
            </h3>
        </div>
        <div class="right floated column">
           
        </div>
    </div>

	<div class="ui form segment">
		<form class="ui form">
			<div class="field">
				<label>ชื่อวัคซีน</label>
				<input type="text" name="vaccine_name" id="vaccine_name" placeholder="วัคซีนพิสุนัขบ้า">
			</div>
            <div class="field">
                <label>ชื่อตัวยา</label>
                <input type="text" name="drug_name" id="drug_name" placeholder="Canline Distemper-Adenovirus Type 2 ">
            </div>
            <div class="three fields">
                <div class="field">
                    <label>จำนวนครั้งที่ต้องฉีด (ใส่เฉพาะตัวเลข)</label>
                    <input type="number" name="numoftimes" id="numoftimes" placeholder="1">
                </div>
                <div class="field">
                    <label>ระยะเวลาในการฉีด เช่นฉีดทุกๆ 6 เดือน (ใส่เฉพาะตัวเลข)</label>
                    <input type="number" name="timeperiod" id="timeperiod" placeholder="1">
                </div>
                <div class="field">
                    <label>หน่วย</label>
                    <select class="ui fluid dropdown" name="timeperiod_unit" id="timeperiod_unit" >
                        <option value="">เลือกหน่วยสำหรับระยะเวลาในการฉีด</option>
                        <option value="วัน">วัน</option>
                        <option value="เดือน">เดือน</option>
                        <option value="ปี">ปี</option>
                    </select>
                </div>
            </div>
            <div class="field">
                <label>สรรพคุณ</label>
                <input type="text" name="properties" id="properties" placeholder="ป้องกันพิสุนัขบ้า">
            </div>
            <div class="field">
                <label>บริษัทผู้ผลิต</label>
                <input type="text" name="company" id="company" placeholder="บริษัทผู้ผลิต">
            </div>
            <div class="field">
                <label>รูปวัคซีน</label>
                <input type="file" name="img_vaccine" id="img_vaccine">
            </div>
			<div class="field">
				<label>ประเภทวัคซีน</label>
				<select class="ui fluid dropdown" name="vaccine_type" id="vaccine_type" >
                    <option value="">เลือกประเภทวัคซีน</option>
                    <option value="วัคซีนสำหรับแมว">วัคซีนสำหรับแมว</option>
                    <option value="วัคซีนสำหรับสุนัข">วัคซีนสำหรับสุนัข</option>
                </select>
			</div>
			<div class="field">
				<label>สถานะ</label>
				<select class="ui fluid search selection dropdown" name="vaccine_status" id="vaccine_status" >
                    <option value="">เลือกสถานะ</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
			</div>
            <br>
            <br>
            <div class="field">
                <label>การใช้และคำแนะนำ</label>
                <textarea name="vaccine_usage" id="vaccine_usage" rows="10" cols="80">
                </textarea>
            </div>
            <br>
            <br>
			<div class="field">
				<label>รายละเอียดเพิ่มเติม</label>
				<textarea name="description" id="description" rows="10" cols="80">
	            </textarea>
			</div>
		</form>

        <br>
        <br>

        <div class="ui two column grid">
            <div class="column">
                <button class="ui fluid big red button btn-back" type="submit" >ยกเลิก</button>
            </div>
            <div class="column">
                <button class="ui fluid big blue button btn-save" type="submit">บันทึก</button>
            </div>
        </div>
	</div>

</div>



<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<!-- ส่งค่ามาที่ url -->
<div id="add_url" data-url="<?php echo \URL::route('vaccine.add.post'); ?>"></div>
<div id='ajax-center-url' data-url="<?php echo \URL::route('vaccine.ajax_center.post');?>"></div>
