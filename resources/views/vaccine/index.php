<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="syringe icon"></i>
                <div class="content">
                    หน้าจัดการวัคซีน
                </div>
            </h3>
        </div>

        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big green button btn-addvaccine" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลวัคซีน</button>
                </div>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <h4 class="ui header">ค้นหาวัคซีน</h4>
        <div class="fields">
            <div class="eight wide field">
                <label>คำค้นหา</label>
                <input type="text" placeholder="" name="search_txt" id="search_txt" value="<?php echo isset($filters['search_txt']) ? $filters['search_txt'] : '' ?>">
            </div>
            <div class="four wide field">
                <div class="field">
                    <?php $filters['vaccine_type'] = isset($filters['vaccine_type']) ? $filters['vaccine_type'] : '' ?>
                    <label>ประเภทวัคซีน</label>
                    <select class="ui fluid dropdown" name="vaccine_type" id="vaccine_type" >
                        <option value="all" <?php echo $filters['vaccine_type'] == 'all' ? 'selected' : '' ?>>ทั้งหมด</option>
                        <option value="วัคซีนสำหรับแมว" <?php echo $filters['vaccine_type'] == 'วัคซีนสำหรับแมว' ? 'selected' : '' ?>>วัคซีนสำหรับแมว</option>
                        <option value="วัคซีนสำหรับสุนัข" <?php echo $filters['vaccine_type'] == 'วัคซีนสำหรับสุนัข' ? 'selected' : '' ?>>วัคซีนสำหรับสุนัข</option>
                    </select>
                </div>
            </div>
            <div class="four wide field">
                <?php $filters['vaccine_status'] = isset($filters['vaccine_status']) ? $filters['vaccine_status'] : '' ?>
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="vaccine_status" id="vaccine_status" >
                    <option value="all" <?php echo $filters['vaccine_status'] == 'all' ? 'selected' : '' ?>>ทั้งหมด</option>
                    <option value="Active" <?php echo $filters['vaccine_status'] == 'Active' ? 'selected' : '' ?>>Active</option>
                    <option value="Inactive" <?php echo $filters['vaccine_status'] == 'Inactive' ? 'selected' : '' ?>>Inactive</option>
                </select>
            </div>
        </div>
        <div class="ui unstackable four column grid ">
            <div class="column">
            </div>
            <div class="right floated column">
                <div class="fields">
                    <div class="eight wide field">
                        <button class="ui fluid small blue button btn-search" type="submit" style="border-radius: 30px;">ค้นหา</button>
                    </div>
                    <div class="eight wide field">
                        <button class="ui fluid small red button btn-clear" type="submit" style="border-radius: 30px;">ล้าง</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_report">
                    <thead>
                        <tr>
                            <th>รูปวัคซีน</th>
                            <th>ชื่อวัคซีน</th>
                            <th>ประเภทวัคซีน</th>
                            <th>การใช้</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php if(!empty($getVaccines)): ?>
                            <?php foreach ($getVaccines as $key => $getVaccine):?>
                                <tr>
                                    <td class="two wide">
                                        <img class="ui tiny image" src="<?php echo empty($getVaccine->img_vaccine) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $getVaccine->img_vaccine); ?>">
                                    </td>
                                    <td class="two wide">
                                        <?php echo empty($getVaccine->vaccine_name) ? '-' : $getVaccine->vaccine_name ?>
                                    </td>
                                    <td class="two wide">
                                        <?php echo empty($getVaccine->vaccine_type) ? '-' : $getVaccine->vaccine_type ?>
                                    </td>
                                    <td class="six wide">
                                        <?php echo empty($getVaccine->vaccine_usage) ? '-' : $getVaccine->vaccine_usage; ?>
                                    </td>
                                    <td class="two wide">
                                        <?php echo empty($getVaccine->vaccine_status) ? '-' : $getVaccine->vaccine_status; ?>
                                    </td>
                                    <td class="two wide">
                                        <i class="eye outline icon viewvaccine" data-id="<?php echo $getVaccine->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="edit outline icon editvaccine" data-id="<?php echo $getVaccine->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="trash alternate outline icon deletevaccine" data-id="<?php echo $getVaccine->id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr> 
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <?php echo "รายการทั้งหมด  ".number_format($getVaccines->total())."  รายการ"; ?>
                <div class="add-page" style="float: right;">
                    <?php echo $getVaccines->render(); ?>
                </div>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('vaccine.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('vaccine.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('vaccine.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('vaccine.delete.post');?>"></div>

