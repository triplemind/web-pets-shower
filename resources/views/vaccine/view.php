<div class="ui basic segment">
	<br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="syringe icon"></i>
                <div class="content">
                    รายละเอียดวัคซีน
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>

    <?php 
        $lb_color = "";
        $status_txt = "";
        if($vaccine->vaccine_status == "Active"){
            $lb_color = 'green';
            $status_txt = "Active";
        }else if($vaccine->vaccine_status == "Inactive"){
            $lb_color = 'red';
            $status_txt = "Inactive";
        }
    ?>



    <div class="ui form segment">
        <div class="top right attached large ui label">
            <div class="ui <?php echo $lb_color; ?> empty circular label"></div> <?php echo $status_txt ?>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui table">
                    <tbody>
                        <tr>
                            <td class="two wide active">รูปวัคซีน</td>
                            <td class="six wide" colspan="3">
                                <img class="ui medium image" src="<?php echo empty($vaccine->img_vaccine) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $vaccine->img_vaccine); ?>" >
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">จำนวนครั้งที่ต้องฉีด</td>
                            <td class="six wide"><?php echo $vaccine->numoftimes ?> ครั้ง</td>
                            <td class="two wide active">ระยะเวลาในการฉีด</td>
                            <td class="six wide"><?php echo "ทุกๆ  ".$vaccine->timeperiod."  ".$vaccine->timeperiod_unit ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ชื่อวัคซีน</td>
                            <td class="six wide" colspan="3"><?php echo $vaccine->vaccine_name ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ชื่อตัวยา</td>
                            <td class="six wide" colspan="3"><?php echo $vaccine->drug_name ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">สรรพคุณ</td>
                            <td class="six wide" colspan="3"><?php echo $vaccine->properties ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">บริษัทผู้ผลิต</td>
                            <td class="six wide" colspan="3"><?php echo $vaccine->company ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ประเภทวัคซีน</td>
                            <td class="six wide" colspan="3"><?php echo $vaccine->vaccine_type ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">การใช้และคำแนะนำ</td>
                            <td class="six wide" colspan="3"><?php echo $vaccine->vaccine_usage ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">รายละเอียดเพิ่มเติม</td>
                            <td class="six wide" colspan="3" ><?php echo $vaccine->description ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
    
</div>
