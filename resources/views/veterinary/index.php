<div class="ui basic segment">
    <br>
    <div class="ui unstackable two column grid segment">
        <div class="twelve wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="stethoscope icon"></i>
                <div class="content">
                    จัดการสัตวแพทย์ <?php echo empty($Shopname) ? '' : $Shopname->name_shop ?>
                </div>
            </h3>
        </div>

        <div class="right floated four wide  column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big green button btn-addveterinary" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลสัตวแพทย์</button>
                </div>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_veterinary">
                    <thead>
                        <tr>
                            <th>รูปภาพ</th>
                            <th>ชื่อ - นามสกุล</th>
                            <th>เลขที่ใบอนุญาต</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($veterinarys)): ?>
                            <?php foreach ($veterinarys as $key => $veterinary):?>
                                <tr>
                                    <td class="wide two">
                                        <img class="ui tiny circular image" src="<?php echo empty($veterinary->img) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $veterinary->img); ?>">
                                    </td>
                                    <td class="wide two"><?php echo $veterinary->Fname." ".$veterinary->Lname ?></td>
                                    <td class="wide two"><?php echo $veterinary->vaterinary_code ?></td>
                                    <td class="wide two">
                                        <?php 
                                            $labeltxt = '';
                                            if($veterinary->status == 'Active'){
                                                $labeltxt = 'green';
                                            }else{
                                                $labeltxt = 'red';
                                            }
                                        ?>
                                        <span class="ui <?php echo $labeltxt; ?> label"><?php echo $veterinary->status ?></span>
                                    </td>
                                    <td class="wide two">
                                        <i class="large edit outline icon editveterinary" data-id="<?php echo $veterinary->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deletveterinary" data-id="<?php echo $veterinary->id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('veterinary.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('veterinary.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('veterinary.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('veterinary.delete.post');?>"></div>


<div class="ui medium modal" id="addVeterinaryodal">
    <div class="header">เพิ่มข้อมูลสัตวแพทย์</div>
    <div class="content">
        <div class="ui big form">
            <div class="two fields">
                <div class="field">
                    <label>ชื่อ</label>
                    <input type="text" placeholder="" name="Fname" id="Fname">
                </div>
                <div class="field">
                    <label>นามสกุล</label>
                    <input type="text" placeholder="" name="Lname" id="Lname">
                </div>
            </div>
            <div class="field">
                <label>เลขที่ใบอนุญาต</label>
                <input type="text" placeholder="" name="vaterinary_code" id="vaterinary_code">
            </div>
            <div class="field">
                <label>เบอร์โทรศัพท์</label>
                <input type="text" placeholder="" name="tel" id="tel" maxlength="10">
            </div>
            <div class="field">
                <label>รายละเอียยด</label>
                <textarea name="remark" id="remark" rows="3"></textarea>
            </div>
            <div class="field">
                <label>ร้านค้า</label>
                <select class="ui fluid dropdown" name="shop_id" id="shop_id" >
                    <?php if(!empty($shops)): ?>
                        <?php foreach ($shops as $key => $shop): ?>
                            <option value="<?php echo $shop->id_shop ?>"><?php echo $shop->name_shop ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="status" id="status" >
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="img" id="img">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="EditVeterinaryModal">
    <div class="header">แก้ไขข้อมูลสัตวแพทย์</div>
    <div class="content">
        <div class="ui big form">
            <div class="two fields">
                <div class="field">
                    <label>ชื่อ</label>
                    <input type="text" placeholder="" name="ed_Fname" id="ed_Fname">
                </div>
                <div class="field">
                    <label>นามสกุล</label>
                    <input type="text" placeholder="" name="ed_Lname" id="ed_Lname">
                </div>
            </div>
            <div class="field">
                <label>เลขที่ใบอนุญาต</label>
                <input type="text" placeholder="" name="ed_vaterinary_code" id="ed_vaterinary_code">
            </div>
            <div class="field">
                <label>เบอร์โทรศัพท์</label>
                <input type="text" placeholder="" name="ed_tel" id="ed_tel" maxlength="10">
            </div>
            <div class="field">
                <label>รายละเอียยด</label>
                <textarea name="ed_remark" id="ed_remark" rows="3"></textarea>
            </div>
            <div class="field">
                <label>ร้านค้า</label>
                <select class="ui fluid dropdown" name="ed_shop_id" id="ed_shop_id" >
                    <?php if(!empty($shops)): ?>
                        <?php foreach ($shops as $key => $shop): ?>
                            <option value="<?php echo $shop->id_shop ?>"><?php echo $shop->name_shop ?></option>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="ed_status" id="ed_status" >
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="ed_img" id="ed_img">
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>