<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/testview', function () {
//     return view('testview');
// });
Route::get('/mail', function () {
    return view('email.mail_contact');
});

Route::get('/admin', function () {
    return \Redirect::route('auth.login.get');
});
// ->middleware('lang');

// 
Route::get('/mypet/{id}','Shoponline\MyPetsController@getGenerateQRForPet');


Route::get('/auth/login', [
  // 'middleware' => 'lang',
	'uses'	=> 'Auth\LoginController@getLogin',
	'as'	=> 'auth.login.get'
]);
Route::post('/auth/login', [
	'uses'	=> 'Auth\LoginController@postLogin',
	'as'	=> 'auth.login.post'
]);
Route::get('/auth/logout', [
	'uses'	=> 'Auth\LoginController@logout',
	'as'	=> 'auth.logout.get'
]);

Route::get('/auth/forgotpassword', [
	'uses'	=> 'Auth\ForgotPasswordController@getForgotPassword',
	'as'	=> 'auth.forgotpassword.get'
]);
Route::post('/auth/forgotpassword', [
	'uses'	=> 'Auth\ForgotPasswordController@postForgotPassword',
	'as'	=> 'auth.forgotpassword.post'
]);

// REGISTER
Route::get('/auth/register', [
  'uses'  => 'Auth\RegisterController@getRegister',
  'as'  => 'auth.register.get'
]);

Route::get('/auth/register_owner', [
  'uses'  => 'Auth\RegisterController@getRegisterOwner',
  'as'  => 'auth.registerowner.get'
]);

Route::post('/auth/register', [
  'uses'  => 'Auth\RegisterController@postRegister',
  'as'  => 'auth.register.post'
]);
// REGISTER

// Route::post('/setlocale', [
//   'uses'  => 'Controller@setLanguage',
//   'as'  => 'lang.set_locate.post'
// ]);

Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Dashboard'], function(){

  Route::get('/dashboard',[
    'uses' => 'DashboardController@getIndex',
    'as' => 'dashboard.index.get'
  ]);

  Route::get('/dashboard/orderdetail/{id}', [
    'uses'  => 'DashboardController@getOrderDetail',
    'as'  => 'dashboard.orderdetail.get'
  ]);

  Route::get('/dashboard/histories', [
    'uses'  => 'DashboardController@getHitoriesOrder',
    'as'  => 'dashboard.histories.get'
  ]);

  Route::post('/dashboard/ajax_center', [
    'uses'  => 'DashboardController@ajaxCenter',
    'as'  => 'dashboard.ajax_center.post'
  ]);

  Route::post('/dashboard/addslip',[
    'uses' => 'DashboardController@postSavePaySlip',
    'as' => 'dashboard.addslip.post'
  ]);

  Route::post('/dashboard/addVaccineRC',[
    'uses' => 'DashboardController@postSaveVaccineRC',
    'as' => 'dashboard.addVaccineRC.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Stock'], function(){

  Route::get('/stock',[
    'uses' => 'StockController@getIndex',
    'as' => 'stock.index.get'
  ]);

  Route::post('/stock/ajax_center', [
    'uses'  => 'StockController@ajaxCenter',
    'as'  => 'stock.ajax_center.post'
  ]);

  Route::get('/stock/manage/{id}', [
    'uses'  => 'StockController@getManageImage',
    'as'  => 'stock.manageimg.get'
  ]);

  Route::post('/stock/add', [
    'uses'  => 'StockController@postAdd',
    'as'  => 'stock.add.post'
  ]);

  Route::post('/stock/edit', [
    'uses'  => 'StockController@postEdit',
    'as'  => 'stock.edit.post'
  ]);

  Route::post('/stock/delete', [
    'uses'  => 'StockController@postRemove',
    'as'  => 'stock.delete.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Veterinary'], function(){

  Route::get('/veterinary',[
    'uses' => 'VeterinaryController@getIndex',
    'as' => 'veterinary.index.get'
  ]);

  Route::post('/veterinary/ajax_center', [
    'uses'  => 'VeterinaryController@ajaxCenter',
    'as'  => 'veterinary.ajax_center.post'
  ]);

  Route::post('/veterinary/add', [
    'uses'  => 'VeterinaryController@postAdd',
    'as'  => 'veterinary.add.post'
  ]);

  Route::post('/veterinary/edit', [
    'uses'  => 'VeterinaryController@postEdit',
    'as'  => 'veterinary.edit.post'
  ]);

  Route::post('/veterinary/delete', [
    'uses'  => 'VeterinaryController@postRemove',
    'as'  => 'veterinary.delete.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'PetsShop'], function(){

  Route::get('/petsShop',[
    'uses' => 'PetsShopController@getIndex',
    'as' => 'petsShop.index.get'
  ]);

  Route::get('/petsShop/view/{id}',[
    'uses' => 'PetsShopController@getView',
    'as' => 'petsShop.view.get'
  ]);

  Route::get('/petsShop/managespecies',[
    'uses' => 'PetsShopController@getManageSpecies',
    'as' => 'petsShop.managespecies.get'
  ]);

  Route::get('/petsShop/managepettype',[
    'uses' => 'PetsShopController@getManagePetType',
    'as' => 'petsShop.managepettype.get'
  ]);

  Route::get('/petsShop/viewImg/{id}',[
    'uses' => 'PetsShopController@getViewImg',
    'as' => 'petsShop.viewImg.get'
  ]);

  Route::post('/petsShop/ajax_center', [
    'uses'  => 'PetsShopController@ajaxCenter',
    'as'  => 'petsShop.ajax_center.post'
  ]);

  Route::post('/petsShop/add', [
    'uses'  => 'PetsShopController@postAdd',
    'as'  => 'petsShop.add.post'
  ]);

  Route::post('/petsShop/edit', [
    'uses'  => 'PetsShopController@postEdit',
    'as'  => 'petsShop.edit.post'
  ]);

  Route::post('/petsShop/delete', [
    'uses'  => 'PetsShopController@postRemove',
    'as'  => 'petsShop.delete.post'
  ]);

  Route::post('/petsShop/managespecies_add', [
    'uses'  => 'PetsShopController@postManageSpeciesAdd',
    'as'  => 'petsShop.managespeciesadd.post'
  ]);

  Route::post('/petsShop/managespecies_edit', [
    'uses'  => 'PetsShopController@postManageSpeciesEdit',
    'as'  => 'petsShop.managespeciesedit.post'
  ]);

  Route::post('/petsShop/managespecies_delete', [
    'uses'  => 'PetsShopController@postManageSpeciesRemove',
    'as'  => 'petsShop.managespeciesdelete.post'
  ]);

  Route::post('/petsShop/managepettype_add', [
    'uses'  => 'PetsShopController@postManagePetTypeAdd',
    'as'  => 'petsShop.managepettypeadd.post'
  ]);

  Route::post('/petsShop/managepettype_edit', [
    'uses'  => 'PetsShopController@postManagePetTypeEdit',
    'as'  => 'petsShop.managepettypeedit.post'
  ]);

  Route::post('/petsShop/managepettype_delete', [
    'uses'  => 'PetsShopController@postManagePetTypeRemove',
    'as'  => 'petsShop.managepettypedelete.post'
  ]);

  Route::post('/petsShop/addBooking',[
    'uses' => 'PetsShopController@postAddBooking',
    'as' => 'petsShop.addBooking.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Shop'], function(){

  Route::get('/shop',[
    'uses' => 'ShopController@getIndex',
    'as' => 'shop.index.get'
  ]);

  Route::get('/shop/add',[
    'uses' => 'ShopController@getAdd',
    'as' => 'shop.add.get'
  ]);

  Route::get('/shop/edit/{id}',[
    'uses' => 'ShopController@getEdit',
    'as' => 'shop.edit.get'
  ]);

  Route::get('/shop/view/{id}',[
    'uses' => 'ShopController@getView',
    'as' => 'shop.view.get'
  ]);

  Route::get('/shop/manageservice/{id}',[
    'uses' => 'ShopController@getManageService',
    'as' => 'shop.manageservice.get'
  ]);

  Route::post('/shop/ajax_center', [
    'uses'  => 'ShopController@ajaxCenter',
    'as'  => 'shop.ajax_center.post'
  ]);

  Route::post('/shop/add', [
    'uses'  => 'ShopController@postAdd',
    'as'  => 'shop.add.post'
  ]);

  Route::post('/shop/edit', [
    'uses'  => 'ShopController@postEdit',
    'as'  => 'shop.edit.post'
  ]);

  Route::post('/shop/manageservice', [
    'uses'  => 'ShopController@postAddSevice',
    'as'  => 'shop.manageservice.post'
  ]);

  Route::post('/shop/deleteservice', [
    'uses'  => 'ShopController@postRemoveManageService',
    'as'  => 'shop.deleteservice.post'
  ]);

  Route::post('/shop/vaccine', [
    'uses'  => 'ShopController@postAddVaccine',
    'as'  => 'shop.vaccine.post'
  ]);

  Route::post('/shop/delete', [
    'uses'  => 'ShopController@postRemove',
    'as'  => 'shop.delete.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Premium'], function(){

  Route::get('/premium',[
    'uses' => 'PremiumController@getIndex',
    'as' => 'premium.index.get'
  ]);

  Route::post('/premium/ajax_center', [
    'uses'  => 'PremiumController@ajaxCenter',
    'as'  => 'premium.ajax_center.post'
  ]);

  Route::post('/premium/add', [
    'uses'  => 'PremiumController@postAdd',
    'as'  => 'premium.add.post'
  ]);

  Route::post('/premium/edit', [
    'uses'  => 'PremiumController@postEdit',
    'as'  => 'premium.edit.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'User'], function(){

  Route::get('/user',[
    'uses' => 'UserController@getIndex',
    'as' => 'user.index.get'
  ]);

  Route::post('/user/ajax_center', [
    'uses'  => 'UserController@ajaxCenter',
    'as'  => 'user.ajax_center.post'
  ]);

  Route::post('/user/add', [
    'uses'  => 'UserController@postAdd',
    'as'  => 'user.add.post'
  ]);

  Route::post('/user/edit', [
    'uses'  => 'UserController@postEdit',
    'as'  => 'user.edit.post'
  ]);

  Route::post('/user/delete', [
    'uses'  => 'UserController@postRemove',
    'as'  => 'user.delete.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Vaccine'], function(){

  Route::get('/vaccine',[
    'uses' => 'VaccineController@getIndex',
    'as' => 'vaccine.index.get'
  ]);

  Route::post('/vaccine/ajax_center', [
    'uses'  => 'VaccineController@ajaxCenter',
    'as'  => 'vaccine.ajax_center.post'
  ]);

  Route::get('/vaccine/add',[
    'uses' => 'VaccineController@getAdd',
    'as' => 'vaccine.add.get'
  ]);

  Route::get('/vaccine/edit/{id}',[
    'uses' => 'VaccineController@getEdit',
    'as' => 'vaccine.edit.get'
  ]);

  Route::get('/vaccine/view/{id}',[
    'uses' => 'VaccineController@getView',
    'as' => 'vaccine.view.get'
  ]);

  Route::post('/vaccine/add', [
    'uses'  => 'VaccineController@postAdd',
    'as'  => 'vaccine.add.post'
  ]);

  Route::post('/vaccine/edit', [
    'uses'  => 'VaccineController@postEdit',
    'as'  => 'vaccine.edit.post'
  ]);

  Route::post('/vaccine/delete', [
    'uses'  => 'VaccineController@postRemove',
    'as'  => 'vaccine.delete.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Promotion'], function(){

  Route::get('/promotion',[
    'uses' => 'PromotionController@getIndex',
    'as' => 'promotion.index.get'
  ]);

  Route::post('/ajax_center', [
    'uses'  => 'PromotionController@ajaxCenter',
    'as'  => 'promotion.ajax_center.post'
  ]);

  Route::get('/promotion/add',[
    'uses' => 'PromotionController@getAdd',
    'as' => 'promotion.add.get'
  ]);

  Route::get('/promotion/edit/{id}',[
    'uses' => 'PromotionController@getEdit',
    'as' => 'promotion.edit.get'
  ]);

  Route::get('/promotion/view/{id}',[
    'uses' => 'PromotionController@getView',
    'as' => 'promotion.view.get'
  ]);

  Route::post('/promotion/add', [
    'uses'  => 'PromotionController@postAdd',
    'as'  => 'promotion.add.post'
  ]);

  Route::post('/promotion/edit', [
    'uses'  => 'PromotionController@postEdit',
    'as'  => 'promotion.edit.post'
  ]);

  Route::post('/promotion/delete', [
    'uses'  => 'PromotionController@postRemove',
    'as'  => 'promotion.delete.post'
  ]);

});


Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Course'], function(){

  Route::get('/course',[
    'uses' => 'CourseController@getIndex',
    'as' => 'course.index.get'
  ]);

  Route::post('/course/ajax_center', [
    'uses'  => 'CourseController@ajaxCenter',
    'as'  => 'course.ajax_center.post'
  ]);

  Route::get('/course/add',[
    'uses' => 'CourseController@getAdd',
    'as' => 'course.add.get'
  ]);

  Route::get('/course/edit/{id}',[
    'uses' => 'CourseController@getEdit',
    'as' => 'course.edit.get'
  ]);

  Route::get('/course/view/{id}',[
    'uses' => 'CourseController@getView',
    'as' => 'course.view.get'
  ]);

  Route::post('/course/add', [
    'uses'  => 'CourseController@postAdd',
    'as'  => 'course.add.post'
  ]);

  Route::post('/course/edit', [
    'uses'  => 'CourseController@postEdit',
    'as'  => 'course.edit.post'
  ]);

  Route::post('/course/delete', [
    'uses'  => 'CourseController@postRemove',
    'as'  => 'course.delete.post'
  ]);

});


Route::group(['namespace' => 'Shoponline'], function(){

  Route::get('/',[
    'uses' => 'ShoponlineController@getIndex',
    'as' => 'shoponline.index.get'
  ]);
  
  Route::get('/search/{data}', [
    'uses'  => 'ShoponlineController@getPageSearch',
    'as'  => 'shoponline.search.get'
  ]);


  Route::get('/profile', [
    'uses'  => 'ShoponlineController@getProfile',
    'as'  => 'shoponline.profile.get'
  ]);

  Route::get('/aboutus', [
    'uses'  => 'ShoponlineController@getAboutus',
    'as'  => 'shoponline.aboutus.get'
  ]);

  Route::get('/contactus', [
    'uses'  => 'ShoponlineController@getContactus',
    'as'  => 'shoponline.contactus.get'
  ]);

  Route::post('/postContactForm', [
    'uses'  => 'ShoponlineController@postContactForm',
    'as'  => 'shoponline.postContactForm.post'
  ]);

  Route::get('/vaccinedetail', [
    'uses'  => 'ShoponlineController@getPageVaccineDetail',
    'as'  => 'shoponline.vaccinedetail.get'
  ]);

  Route::post('/ajax_center', [
    'uses'  => 'ShoponlineController@ajaxCenter',
    'as'  => 'shoponline.ajax_center.post'
  ]);


  Route::post('/add', [
    'uses'  => 'ShoponlineController@postAdd',
    'as'  => 'shoponline.add.post'
  ]);

  // Route::post('/delete', [
  //   'uses'  => 'ShoponlineController@postRemove',
  //   'as'  => 'shoponline.delete.post'
  // ]);

// สัตว์เลี้ยง
  Route::get('/mypets',[
    'uses' => 'MyPetsController@getIndex',
    'as' => 'mypets.mypets.get'
  ]);

  Route::get('/mypets/detail/{id}',[
    'uses' => 'MyPetsController@getPetDetail',
    'as' => 'mypets.mypetsdetail.get'
  ]);

  Route::post('/mypets/ajax_center', [
    'uses'  => 'MyPetsController@ajaxCenter',
    'as'  => 'mypets.ajax_center.post'
  ]);

  Route::post('/mypets/add', [
    'uses'  => 'MyPetsController@postAdd',
    'as'  => 'mypets.add.post'
  ]);

  Route::post('/mypets/addSP', [
    'uses'  => 'MyPetsController@postAddSP',
    'as'  => 'mypets.addSP.post'
  ]);

  Route::post('/mypets/edit', [
    'uses'  => 'MyPetsController@postEdit',
    'as'  => 'mypets.edit.post'
  ]);

  Route::post('/mypets/delete', [
    'uses'  => 'MyPetsController@postRemove',
    'as'  => 'mypets.delete.post'
  ]);


  Route::get('/course/{id}',[
    'uses' => 'MyBookingController@getCourse',
    'as' => 'booking.course.get'
  ]);


// ร้านค้า
  Route::get('/shopnearby',[
    'uses' => 'ShoponlineController@getShopNearBy',
    'as' => 'shopnearby.shopnearby.get'
  ]);

  Route::get('/shopnearbygoogle',[
    'uses' => 'ShoponlineController@getShopNearByGoogle',
    'as' => 'shoponline.shopnearbygoogle.get'
  ]);


  // การจอง
  Route::get('/booking/{id}',[
    'uses' => 'MyBookingController@getIndex',
    'as' => 'booking.booking.get'
  ]);

  Route::get('/vaccinebooking/{id}/{petid}/{data}',[
    'uses' => 'MyBookingController@getVaccineBooking',
    'as' => 'booking.vaccinebooking.get'
  ]);

  Route::get('/postponeAdate/{id}/{petid}/{shopid}/{bookingid}',[
    'uses' => 'MyBookingController@getPostponeADate',
    'as' => 'booking.postponeAdate.get'
  ]);

  Route::get('/vaccineshop/{id}/{petid}',[
    'uses' => 'MyBookingController@getVaccineShop',
    'as' => 'booking.vaccineshop.get'
  ]);

  Route::get('/bookinghistory',[
    'uses' => 'MyBookingController@getBookingHistory',
    'as' => 'booking.bookinghistory.get'
  ]);

  Route::get('/bookingdetail/{id}',[
    'uses' => 'MyBookingController@getBookingDetail',
    'as' => 'booking.bookingdetail.get'
  ]);

  Route::get('/bookingcoursedetail/{id}',[
    'uses' => 'MyBookingController@getBookingCourseDetail',
    'as' => 'booking.bookingcoursedetail.get'
  ]);

  Route::post('/booking/add',[
    'uses' => 'MyBookingController@postAdd',
    'as' => 'booking.add.post'
  ]);

  Route::post('/booking/edit',[
    'uses' => 'MyBookingController@postEdit',
    'as' => 'booking.edit.post'
  ]);

  Route::post('/booking/addslip',[
    'uses' => 'MyBookingController@postSavePaySlip',
    'as' => 'booking.addslip.post'
  ]);

  Route::post('/booking/ajax_center',[
    'uses' => 'MyBookingController@ajaxCenter',
    'as' => 'booking.ajax_center.post'
  ]);

});